import QtQuick 2.0

Item {
    id: root
    width: 40 * r
    height: 40 * r

    property string src: "qrc:/icons/noicon.svg"

    Image {
        anchors.fill: parent
        sourceSize {
            width: parent.width
            height: parent.height
        }
        source: src
    }

    signal clicked()

    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked()
    }
}
