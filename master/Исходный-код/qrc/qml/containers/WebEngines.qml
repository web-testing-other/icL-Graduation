import QtQuick 2.12
import QtWebEngine 1.10
import QtQuick.Window 2.12

Item {
    id: root
    property WebEngineView userCurrent: null
    property WebEngineView driverCurrent: null
    property int driverIndex: -1
    property int userIndex: -1
    property var engines: []

    function open(uuid) {
        var obj = {
            "uuid": uuid
        }

        obj.item = viewComponent.createObject(this, {
                                                  "z": -1,
                                                  "uuid": uuid
                                              })
        engines.push(obj)

        return obj.item
    }

    function switchTo(uuid) {
        if (!!driverCurrent && driverCurrent.uuid === uuid)
            return

        for (let i in engines) {
            let engine = engines[i]

            if (engine.uuid === uuid) {
                if (!bus.silent || !userCurrent) {
                    if (!!userCurrent)
                        userCurrent.z = -1

                    engine.item.z = 1
                    userIndex = i
                    userCurrent = engines[i].item
                }
                driverIndex = i
                driverCurrent = engines[i].item
                break
            }
        }
    }

    function switchUserTo(index) {
        if (!bus.silent)
            return

        if (!!userCurrent)
            userCurrent.z = -1

        userIndex = index
        userCurrent = engines[index].item
        userCurrent.z = 1
    }

    function sync() {
        if (userCurrent != driverCurrent) {
            userCurrent.z = -1
            driverCurrent.z = 1
            userCurrent = driverCurrent
            userIndex = driverIndex
        }
    }

    function close(uuid) {
        if (!!userCurrent && userCurrent.uuid === uuid) {
            if (userCurrent == driverCurrent) {
                userCurrent = null
                userIndex = -1
            } else {
                userCurrent = driverCurrent
                userIndex = driverIndex
            }
        }

        if (!!driverCurrent && driverCurrent.uuid === uuid) {
            engines[driverIndex].item.destroy()
            driverIndex = -1
        } else {
            for (let i in engines) {
                let engine = engines[i]

                if (engine.uuid === uuid) {
                    engine.item.destroy()
                    break
                }
            }
        }
    }

    Component {
        id: viewComponent

        WebEngineView {
            id: view
            property string uuid
            property bool domReady: false

            onLoadingChanged: {if (loading) {/*bus.handleJsResult('{"type":"void","value":null}')*/} console.log("loading", loading)}
            onLoadProgressChanged: {domReady = loadProgress == 100; console.log("progress", loadProgress)}
            onDomReadyChanged: console.log("domReady", domReady)
            onJavaScriptConsoleMessage: {
                if (sourceID == "userscript:icL.js") {
                    if (message == "DOM-ready") {
//                        domReady = true;
                    }
                    else {
                        console.warn(message);
                    }
                }
            }

//            state: WebEngineView.Active

            url: "about:blank"
            anchors.fill: parent
            profile.userScripts: [
                WebEngineScript {
                    injectionPoint: WebEngineScript.DocumentCreation
                    name: "engine.js"
                    sourceUrl: "qrc:/js/extended-engine.js"
                    worldId: WebEngineScript.MainWorld
                },
                WebEngineScript {
                    injectionPoint: WebEngineScript.DocumentCreation
                    name: "icL.js"
                    sourceUrl: "qrc:/js/user-script.js"
                    worldId: WebEngineScript.MainWorld
                }
            ]
            devToolsView: debug

            Component.onCompleted: {
//                wwin.show();
            }

            Window {
                id: wwin
                width: 600
                height: 500
//                visible: true

                WebEngineView {
                    id: debug
                    width: wwin.width
                    height: wwin.height
                    inspectedView: view
                }
            }
        }
    }
}
