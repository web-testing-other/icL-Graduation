import QtQuick 2.12
import QtWebEngine 1.8

Item {
    height: 40
    clip: true

    property int userTab: -1
    property int driverTab: -1
    property var tabs: []

    function open(tab) {
        var obj = {
            "index": tabs.length,
            "tab": tab.view,
            "uuid": tab.uuid
        }

        obj.item = tabComponent.createObject(container, obj)
        tabs.push(obj)

        driverTab = obj.index
        if (!bus.silent)
            userTab = obj.index

        return obj.tab
    }

    function switchTo(uuid) {
        if (!!driverTab && driverTab.uuid === uuid)
            return

        for (let i in tabs) {
            let tab = tabs[i]

            if (tab.uuid === uuid) {

                driverTab = i

                if (!bus.silent || userTab == -1 || userTab >= tabs.length)
                    userTab = i
            }
        }
    }

    function switchUserTo(index) {
        if (bus.silent)
            userTab = index
    }

    function sync() {
        userTab = driverTab
    }

    function close(uuid) {
        let found = false
        let index = -1

        for (let tab of tabs) {
            if (found) {
                tab.index--
            } else if (tab.uuid === uuid) {
                tab.item.destroy()
                index = tab.index
                found = true
            }
        }

        if (index !== -1) {
            tabs.splice(index, 1)
        }
    }

    Component {
        id: tabComponent

        Item {
            id: tabItem
            width: 240
            height: 40
            x: index * width

            property int index: 0
            property WebEngineView tab: null
            property bool active: userTab == index

            function remove() {
                destroy()
            }

            Rectangle {
                anchors.fill: parent
                color: "#ffffff"
                opacity: mouseArea.containsMouse ? 0.3 : 0.0
            }

            Text {
                id: leftText
                text: String(parent.index)
                verticalAlignment: Text.AlignVCenter
                color: tabItem.active ? "#45ca6e" : "#616161"

                font {
                    pixelSize: 21 * r
                    family: font_family
                }

                anchors {
                    top: parent.top
                    topMargin: 4 * r
                    left: parent.left
                    leftMargin: 10 * r
                    bottom: parent.bottom
                    bottomMargin: 2 * r
                }
            }

            Text {
                id: rightText
                text: parent.tab.title
                verticalAlignment: Text.AlignVCenter
                color: tabItem.active ? "#95b196" : "#7c7c7c"
                elide: Text.ElideRight

                font {
                    pixelSize: 15 * r
                    family: font_family
                }

                anchors {
                    top: parent.top
                    topMargin: 6 * r
                    left: leftText.right
                    leftMargin: 10 * r
                    right: parent.right
                    rightMargin: 10 * r
                    bottom: parent.bottom
                }
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                hoverEnabled: true

                onClicked: {
                    if (bus.silent) {
                        proxy.switchUserTo(tabItem.index)
                    }
                }
            }
        }
    }

    Rectangle {
        width: 240 * r
        height: 40 * r
        x: width * userTab
        color: "#ffffff"
    }

    Item {
        id: container
        anchors.fill: parent
    }

    Rectangle {
        width: 230 * r
        height: 2 * r
        y: 5 * r
        x: (5 + 240 * driverTab) * r
        color: bus.running
               ? "#ff7854"
               : bus.eventing
                 ? "#ae10ff"
                 : driverTab < 0
                   ? "#000000"
                   : !tabs[driverTab].tab.domReady
                     ? "#ff0000"
                     : "#17b848"
    }
}
