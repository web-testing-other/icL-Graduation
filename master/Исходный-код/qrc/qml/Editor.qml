import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

Window {
    id: wwin
    width: 600
    height: 500
    visible: true

    ScrollView {
        width: wwin.width
        height: wwin.height

        TextArea {
            id: textarea
            text: vms.text
            font.family: "Noto Mono"
            font.pointSize: 12
            selectionColor: vms.color

            Connections {
                target: vms
                onSelect: textarea.select(begin, end)
            }

            Keys.onPressed: {
                if (event.key == Qt.Key_F8) {
                    vms.makeAStepForever()
                }
                else if (event.key == Qt.Key_F9) {
                    vms.makeAStep()
                }
                else if (event.key == Qt.Key_F10) {
                    vms.makeAStepOver()
                }
                else if (event.key == Qt.Key_F11) {
                    vms.makeAStepInto()
                }
                else if (event.key == Qt.Key_F12) {
                    vms.makeAStepOut()
                }
            }
        }
    }
}
