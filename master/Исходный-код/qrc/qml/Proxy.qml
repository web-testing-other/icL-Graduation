import QtQuick 2.0

Item {
    visible: false

    function open(uuid, p2, p3) {
        let view = webEngines.open(uuid)
        let tab = tabBar.open({
                        "uuid": uuid,
                        "view": view
                    })
        switchTo(uuid)
        bus.tab = view
        return null
    }

    function switchTo(uuid, p2, p3) {
        tabBar.switchTo(uuid)
        webEngines.switchTo(uuid)
    }

    function switchUserTo(index, p2, p3) {
        tabBar.switchUserTo(index)
        webEngines.switchUserTo(index)
    }

    function sync(silent, p2, p3) {
        if (!silent) {
            tabBar.sync()
            webEngines.sync()
        }
    }

    function close(uuid, p2, p3) {
        tabBar.close(uuid)
        webEngines.close(uuid)
    }

    function runJs(code, sync, p3) {
        bus.workingTab().runJavaScript(code, sync ? function (result) {
            bus.handleJsResult(result)
        } : undefined)
        console.log("js to run", bus.workingTab().uuid, code)
    }

    function addUserScript(code, persistent, p3) {
        if (!persistent) {
            // webEngines.driverCurrent.userScripts.
        }
    }

    Connections {
        target: bus

        function onSilentChanged(silent) {
            sync(silent)
        }
    }

    Component.onCompleted: {
        bus.proxy = proxy
        bus.win   = win
    }
}
