import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12

import "controls"
import "containers" as Containers

Window {
    id: win
    visible: true
    width: 1360
    height: 720
    title: qsTr("icL-Magic")
    color: "#f2f2f2"

    property real r: 1.0
    property string font_family: "Noto Sans, Helvetica, Arial, sansserif"

    Component.onCompleted: {
        flags = flags | Qt.FramelessWindowHint
    }

    Proxy {
        id: proxy
    }

    Item {
        id: root
        width: win.width
        height: win.height

        Rectangle {
            id: header
            anchors {
                left: parent.left
                right: parent.right
            }
            height: 84 * r
            color: "#efefef"

            Item {
                anchors {
                    fill: parent
                    leftMargin: 10 * r
                    rightMargin: 10 * r
                    topMargin: 2 * r
                    bottomMargin: 2 * r
                }

                RowLayout {
                    id: tabBarContainer

                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    height: 40

                    ClickableIcon {
                        width: 74 * r
                        src: "qrc:/icons/icL.svg"
                    }

                    Containers.TabBar {
                        id: tabBar
                        Layout.fillWidth: true
                    }

                    ClickableIcon {
                        src: "qrc:/icons/minimize.svg"
                    }

                    ClickableIcon {
                        src: "qrc:/icons/maximize.svg"
                    }

                    ClickableIcon {
                        src: "qrc:/icons/close.svg"
//                        onClicked: Qt.quit()
                    }
                }

                Rectangle {
                    anchors {
                        top: tabBarContainer.bottom
                        left: parent.left
                        leftMargin: -10 * r
                        right: parent.right
                        rightMargin: -10 * r
                        bottom: parent.bottom
                    }

                    color: "#ffffff"
                }

                RowLayout {
                    id: urlContainer

                    anchors {
                        top: tabBarContainer.bottom
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }

                    ClickableIcon {
                        src: "qrc:/icons/site.svg"
                    }

                    Item {
                        height: 40
                        Layout.fillWidth: true

                        Text {
                            anchors.fill: parent
                            anchors.margins: 5 * r
                            verticalAlignment: Text.AlignVCenter
                            font {
                                family: font_family
                                pixelSize: 18 * r
                            }
                            color: "#646464"
                            text: !!webEngines.userCurrent ? webEngines.userCurrent.url : ""
                            elide: Text.ElideMiddle
                        }
                    }

                    ClickableIcon {
                        src: "qrc:/icons/icL-lang.svg"
                    }
                }
            }
        }

        Item {
            id: body
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Containers.WebEngines {
                id: webEngines
                anchors.fill: parent
            }
        }
    }

//    Editor {}
}
