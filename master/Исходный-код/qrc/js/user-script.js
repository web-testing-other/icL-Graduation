
/**
  intra-cloud Lightning js framework
  Copyring (c) 2020 Lelițac Vasile (lixcode)
  Distributed under GPL v3 license
  */
var icL = new function () {
    //-
    //   fields
    //-
    this.buffer = [];
    this.doc = window.document;
    this.docs = [];
    this.error = null;

    var els = {}

    //-
    //   frame methods
    //-
    this.switchToFrameById = function (id) {
        if (!!window[id] && window[id].constructor.name === "Window"
                && !!window[id].document
                && !!window[id].document.constructor.name === "HTMLDocument") {
            this.docs.push(this.doc)
            this.doc = window[id].document
        } else {
            return this.z_error("NoSuchFrame", "frame " + id + " not found")
        }
    }

    this.switchtoFrame = function (element) {
        let iframe = element.nm

        if (iframe.constructor.name !== "HTMLIFrameElement") {
            return this.z_error("NoSuchFrame", "web element is not a frame")
        }

        this.docs.push(this.doc)
        this.doc = iframe.contentDocument
    }

    this.switchToParent = function () {
        if (!!this.docs.length) {
            this.doc = this.docs.pop()
        } else {
            return this.z_error("NoSuchFrame", "no parent available")
        }
    }

    //-
    //    quering elements
    //-
    this.zx_xpath = function (node, selector, _s) {
        var it = this.doc.evaluate(selector, node, null,
                                   XPathResult.UNORDERED_NODE_ITERATOR_TYPE,
                                   null)
        var thisNode = it.iterateNext()
        var nodes = []

        while (thisNode) {
            nodes.push(thisNode)
            thisNode = it.iterateNext()
        }

        return icLnmXs(nodes, null, _s)
    }

    this.zx_filter = function (nodes, text, min, max, _s) {
        var nm = []
        var out = []

        for (let node of nodes) {
            let mark = xt.proximity(node.innerText, text)

            if (mark >= min && mark <= max) {
                out.push({mark, node})
            }
        }

        let n = out.length

        for (let i = 0; i < n - 1; i++) {
            for (let j = 0; j < n - i - 1; j++) {
                if (out[j].mark < out[j+1].mark)  {
                    let tmp = out[j+1]
                    out[j+1] = out[j]
                    out[j] = tmp
                }
            }
        }

        for (let item of out) {
            nm.push(item.node)
        }

        return icLnmXs(nm, null, _s)
    }

    this.zx_plinks = function (doc, text, _s) {
        let as = doc.querySelectorAll("a")
        let out = []

        for (let a of as) {
            if (a.innerText.indexOf(text) >= 0) {
                out.push(a)
            }
        }

        return icLnmXs(out, null, _s)
    }

    this.zx_tags = function (doc, tag, text, min, max, _s) {
        if (max < 0.0) {
            let as = doc.querySelectorAll(tag)
            let out = []

            for (let a of as) {
                if (a.innerText === text) {
                    out.push(a)
                }
            }

            return icLnmXs(out, null, _s)
        } else {
            return this.zx_filter(doc.querySelectorAll(tag), text,
                                  min, max, _s)
        }
    }

    this.zx_tags_rx = function (doc, tag, rx, _s) {
        let as = doc.querySelectorAll(tag)
        let out = []

        for (let a of as) {
            if (rx.test(a.innerText)) {
                out.push(a)
            }
        }

        return icLnmXs(out, null, _s)
    }

    this.zx_fields = function (labels, _s) {
        let out = []

        for (let label of labels) {
            if (!!label.control) {
                out.push(label.control)
            }
        }

        return icLnmXs(out, null, _s)
    }

    //-
    //    export functions
    //-
    this.nm_css = function (selector) {
        return icLnmXs(this.doc.querySelectorAll(selector), null,
                       "css[" + selector + "]")
    }

    this.nm_xpath = function (selector) {
        return this.zx_xpath(this.doc, selector, "xpath[" + selector + "]")
    }

    this.nm_links = function (text, min, max) {
        return this.nm_tags("a", text, min, max, "links[" + text + "]")
    }

    this.nm_links_rx = function (rx) {
        return this.nm_tags_rx("a", rx, "links[" + rx + "]")
    }

    this.nm_plinks = function (fragment) {
        return this.zx_plinks(this.doc, fragment,
                              "links:fragment[" + fragment + "]")
    }

    this.nm_tags = function (tag, text, min, max, _s) {
        return this.zx_tags(this.doc, tag, text, min, max,
                            _s || tag + "[" + text + "]")
    }

    this.nm_tags_rx = function (tag, rx) {
        return this.zx_tags_rx(this.doc, tag, rx, tag + "[" + rx + "]")
    }

    this.nm_input = function (name) {
        return this.nm_css("input[name=" + name + "]")
    }

    this.nm_field = function (text, min, max) {
        return this.zx_fields(this.nm_tag("label", text, min, max).nm,
                              "field[" + text + "]")
    }

    this.nm_field_rx = function (rx) {
        return this.zx_fields(this.nm_tag_rx("label", rx).nm,
                              "field[" + rx + "]")
    }

    //-
    //    icL communication functions
    //-
    this.z_data = function (data) {
        switch (data.type) {
            //-
        case "bool":
        case "double":
        case "string":
        case "list":
            return data.value
            //-
        case "int":
            return data.value | 0
            //-
        case "element":
        case "elements":
            if (!els[data.value.variable]) {
                this.error = this.z_error(
                         "StaleElementReference",
                         "stale element reference: " + data.value.selector)
                return
            } else {
                return els[data.value.variable]
            }
            //-
        case "object":
            let obj = {}

            for (let i in data.value) {
                obj[i] = this.z_data(obj[i])
            }

            return obj
            //-
        case "set":
            let arr = []

            for (let item of data.value) {
                arr.push(this.z_data(item))
            }

            return arr
            //-
        case "regex":
            let mods = ""

            if (data.value.i)
                mods += "i"
            if (data.value.m)
                mods += "m"
            if (data.value.s)
                mods += "s"
            if (data.value.u)
                mods += "u"

            return new RegExp(data.value.pattern, mods)
        }
    }

    this.z_object = function (data) {
        if (!!this.error) {
            let temp = this.error
            this.error = null
            return temp
        }

        if (typeof data == "undefined") {
            return {
                "type": "void",
                "value": null
            }
        }

        switch (data.constructor.name) {
            //-
        case "Boolean":
            return {
                "type": "bool",
                "value": data
            }
            //-
        case "Number":
            return {
                "type": "double",
                "value": data
            }
            //-
        case "String":
            return {
                "type": "string",
                "value": data
            }
            //-
        case "Array":
            let isStringList = true
            let isElementsList = true

            for (let item of data) {
                isStringList = isStringList
                        && item.constructor.name === "String"
                isElementsList = isElementsList && item instanceof Element
            }

            if (isStringList) {
                return {
                    "type": "list",
                    "value": data
                }
            } else if (isElementsList) {
                return this.z_object(icLnmXs(data))
            }
            break
            //-
        case "Object":
            if (data.type === "set-8ba66190-a4bf-44d8-8722-7e17927498ed"
                    || data.type === "err-8ba66190-a4bf-44d8-8722-7e17927498ed")
                return data

            let ret = {}

            for (let i in data) {
                ret[i] = this.z_object(data[i])
            }

            return {
                "type": "object",
                "value": ret
            }
            //-
        case "icLnmXs":
        case "icLnmX":
            let uuid = "icL-nm-" + this.z_uuid()

            els[uuid] = data

            return {
                "type": data instanceof icLnmX ? "element" : "elements",
                "value": {
                    "selector": data.selector,
                    "variable": uuid,
                    "size":     data.nm.length
                }
            }
        }

        return {
            "type": "void",
            "value": null
        }
    }

    this.z_set = function (arr) {
        let ret = []

        for (let item of arr) {
            ret.push(this.z_object(item))
        }

        return {
            "type": "set-8ba66190-a4bf-44d8-8722-7e17927498ed",
            "value": ret
        }
    }

    this.z_error = function (code, error) {
        console.log(error)
        return {
            "type": "err-8ba66190-a4bf-44d8-8722-7e17927498ed",
            "value": {
                "signal": code,
                "error": String(error)
            }
        }
    }

    this.z_uuid = function () {
        function ______y(c) {
            // @disable-check M126
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8)
            return v.toString(16)
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, ______y)
    }
}

function icLnmXs(elements, elements1, _s) {
    if (!(this instanceof icLnmXs))
        return new icLnmXs(elements, elements1, _s) // @disable-check M307

    if (elements instanceof icLnmX) {
        this.nm = [elements, elements1]
    }
    else {
        this.nm = []
        for (let element of elements) {
            if (element.tagName) {
                this.nm.push(element)
            }
        }
    }

    this.selector = _s

    //-
    //    work with elements
    //-
    this.first = function (nr) {
        // @disable-check M307
        let _new = []

        for (var i = 0; i < nr; i++) {
            _new.push(this.nm[i])
        }

        return icLnmXs(_new, undefined, this.selector + ":first(" + nr + ")")
    }

    this.add = function (el_s) {
        if (el_s instanceof icLnmX) {
            this.nm.push(el_s.nm)
        } else if (el_s instanceof icLnmXs) {
            for (let el of el_s.nm) {
                this.nm.push(el)
            }
        }

        this.selector = "add(" + this.selector + ", " + el_s.selector + ")"

        return this
    }

    //-
    //    properties
    //-
    this.attrs = function (name) {
        let list = []

        for (let el of this.nm) {
            list.push(el.getAttribute(name) || "")
        }

        return list
    }

    this.props = function (name) {
        let list = []

        for (let el of this.nm) {
            list.push(el[name] || "")
        }

        return list
    }

    this.texts = function () {
        let list = []

        for (let el of this.nm) {
            window.getSelection().removeAllRanges()

            var range = icL.doc.createRange()
            range.selectNode(el)
            window.getSelection().addRange(range)

            return window.getSelection().toString().trim()
        }

        return list
    }

    this.names = function () {
        let list = []

        for (let el of this.nm) {
            list.push(el.tagName)
        }

        return list
    }

    this.rects = function (name) {
        let list = []

        for (let el of this.nm) {
            list.push(el.getBoundingClientRect())
        }

        return list
    }

    //-
    //    manipulation
    //-
    this.copy = function () {
        return icLnmXs(this.nm, null, this.selector)
    }

    this.get = function (i) {
        return icLnmX(this.nm[i], this.selector + "[" + i + "]")
    }

    this.filter = function (selector) {
        let els = []

        for (let el of this.nm) {
            if (el.matches(selector))
                list.push(el.tagName)
        }

        return icLnmXs(els, null, this.selector + ":filter(" + selector + ")")
    }

    this.contains = function (substr) {
        let els = []

        for (let el of this.nm) {
            if (el.innerText.includes(substr))
                list.push(el.tagName)
        }

        return icLnmXs(els, null, this.selector + ":contains(" + selector + ")")
    }
}

function icLnmX(element, selector) {

    if (!element) {
        return icL.z_error("NoSuchElement", "no such element: " + selector)
    }

    if (!(this instanceof icLnmX)) {
        return new icLnmX(element, selector) // @disable-check M307
    }

    //-
    //    this is a element
    //-
    this.nm = element
    this.selector = selector

    //-
    //     quering elements
    //-
    this.nm_css = function (selector) {
        return icLnmXs(this.nm.querySelectorAll(selector), null,
                       this.selector + " css[" + selector + "]")
    }

    this.nm_xpath = function (selector) {
        return icL.zx_xpath(this.nm, selector,
                            this.selector + " xpath[" + selector + "]")
    }

    this.nm_links = function (text, min, max) {
        return icL.zx_tags(this.nm, "a", text, min, max,
                           this.selector + " links[" + text + "]")
    }

    this.nm_links_rx = function (rx) {
        return icL.zx_tags_rx(this.nm, "a", rx,
                              this.selector + " links[" + rx + "]")
    }

    this.nm_plinks = function (fragment) {
        return icL.zx_plinks(
                    this.nm, fragment,
                    this.selector + " links:fragment[" + fragment + "]")
    }

    this.nm_tags = function (tag, text, min, max) {
        return icL.zx_tags(this.nm, tag, text, min, max,
                           this.selector + " " + tag + "[" + text + "]")
    }

    this.nm_tags_rx = function (tag, rx) {
        return icL.zx_tags_rx(this.nm, tag, rx,
                              this.selector + " " + tag + "[" + rx + "]")
    }

    this.nm_input = function (name) {
        return this.nm_css("input[name=" + name + "]")
    }

    this.nm_field = function (text, min, max) {
        return icL.zx_fields(this.nm_tags("label", text, min, max),
                             this.selector + " field[" + text + "]")
    }

    this.nm_field_rx = function (rx) {
        return icL.zx_fields(this.nm_tags_rx("label", rx),
                             this.selector + " field[" + rx + "]")
    }

    //-
    //    properties
    //-
    this.clickable = function () {
        return this.flash(0.5, 0.5, -1, -1).visible && this.enabled()
    }

    this.visible = function () {
        this.flash(0.5, 0.5, -1, -1)
        let rect   = this.rect()
        let bottom = window.innerHeight
        let right  = window.innerWidth

        return !(rect.bottom < 0 || rect.top >= bottom || rect.right < 0
                 || rect.left >= right)
    }

    this.enabled = function () {
        let ret = true

        if (this.nm.disabled.constructor.name === "Boolean") {
            ret = !this.nm.disabled
        }

        return ret
    }

    this.selected = function () {
        let ret = false

        if (this.nm.checked.constructor.name === "Boolean") {
            ret = this.nm.checked
        } else if (this.nm.selected.constructor.name === "Boolean") {
            ret = this.nm.selected
        }

        return ret
    }

    this.attr = function (name) {
        return this.nm.getAttribute(name)
    }

    this.prop = function (name) {
        return this.nm[name]
    }

    this.css = function (name) {
        return window.getComputedStyle(this.nm)[name]
    }

    this.text = function () {
        window.getSelection().removeAllRanges()

        var range = icL.doc.createRange()
        range.selectNode(this.nm)
        window.getSelection().addRange(range)

        return window.getSelection().toString().trim()
    }

    this.name = function () {
        return this.nm.tagName
    }

    this.rect = function () {
        return this.nm.getBoundingClientRect()
    }

    //-
    //    manipulations
    //-
    this.flash = function (rx, ry, ax, ay) {
        var rect = this.rect()
        var right = window.innerWidth
        var bottom = window.innerHeight

        var point = {
            "x": (rect.left + rect.right - right) / 2,
            "y": (rect.top + rect.bottom - bottom) / 2
        }

        window.scrollBy(point.x, point.y)

        rect = this.rect()

        var x = ax >= 0 ? rect.x + ax : rect.x + rect.width * rx;
        var y = ay >= 0 ? rect.y + ay : rect.y + rect.height * ry;

        return {
            x, y,
            "visible": icL.doc.elementsFromPoint(x, y).includes(this.nm)
        }
    }

    this.copy = function () {
        return icLnmX(this.nm)
    }

    this.prepareForType = function () {
        this.nm.focus()
        if (typeof this.nm.value == "string")
            this.nm.selectionStart = this.nm.selectionEnd = this.nm.value.length
    }

    this.put = function (text) {
        this.nm.value += text
    }

    this.clear = function () {
        if (!this.enabled()) {
            if (this.nm.tagName.toLowerCase() === "input"
                    || this.nm.tagName.toLowerCase() === "textarea") {
                return icL.z_error("InvalidElementState", "input is disabled")
            } else {
                return icL.z_error("ElementNotInteractable",
                                   "element is not interactable")
            }
        }

        if (this.nm.tagName.toLowerCase() === "input") {

            if (this.nm.readOnly) {
                return icL.z_error("InvalidElementState", "input is readonly")
            }

            switch (this.nm.type) {
            case "checkbox":
            case "radio":
                this.nm.checked = false
                break
            case "color":
            case "date":
            case "datetime-local":
            case "email":
            case "file":
            case "hidden":
            case "month":
            case "number":
            case "password":
            case "range":
            case "search":
            case "tel":
            case "text":
            case "time":
            case "url":
            case "week":
                this.nm.value = ""
                break
            default:
                return icL.z_error("InvalidElementState",
                                   "incompatible input type")
            }
        } else if (this.nm.tagName.toLowerCase() === "textarea") {
            if (this.nm.readOnly) {
                return icL.z_error("InvalidElementState", "input is readonly")
            }
            this.nm.value = ""
        } else {
            return icL.z_error("InvalidElementState",
                               "element content is not editable")
        }
    }

    this.superClick = function () {
        var data = {
            "view": window,
            "bubbles": true,
            "cancelable": true
        }
        var down = new MouseEvent("mousedown", data)
        var up = new MouseEvent("mouseup", data)
        var clk = new MouseEvent("click", data)

        this.nm.dispatchEvent(down)
        this.nm.dispatchEvent(up)
        this.nm.dispatchEvent(clk)
    }

    //-
    //    neightboards
    //-
    this.next = function () {
        return icLnmX(this.nm.nextElementSibling, this.selector + ":next")
    }

    this.prev = function () {
        return icLnmX(this.nm.previousElementSibling,
                      this.selector + ":previous")
    }
    this.parent = function () {
        return icLnmX(this.nm.parentElement, this.selector + ":parent")
    }

    this.child = function (i) {
        return icLnmX(this.nm.children[i], this.selector + ":child(" + i + ")")
    }

    this.children = function () {
        return icLnmXs(this.nm.children, null, this.selector + ":children")
    }

    this.closest = function (selector) {
        return icLnmX(this.nm.closest(selector),
                      this.selector + ":closest(" + selector + ")")
        }
}

window.icL     = icL;
window.icLnmX  = icLnmX;
window.icLnmXs = icLnmXs;

window.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM-ready');
});