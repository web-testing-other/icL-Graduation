
/**
  Entended Engine template comparing
  Copyring (c) 2019 Lelițac Vasile (lixcode)
  Distributed under GPL v3 license
  */
 var xt = new function () {

    var Key = function (word) {
        return {
            top : {}, // multimap int -> Key*
            word: word,
            paired: false
        }
    }

    var FindResult = function (index, length) {
        return {
            index: index,
            length: length
        }
    }

    var insert = function (top, id, key) {
        if (typeof top[id] != "object") {
            top[id] = [];
        }
        top[id].push(key);
    }

    var remove = function (top, id, key) {
        if (typeof top[id] != "object") return;

        if (top[id].length == 1 && top[id][0] == key) {
            delete top[id];
        }
        else {
            var index = top[id].indexOf(key);

            if (index != -1) {
                top[id] = top[id].slice(index);
            }
        }
    }

    var pair = function (key1, key2) {
        if (!key1.paired && !key2.paired && key1.word == key2.word) {
            key1.paired = key2.paired = true;
            key1.top = {pair: key2, commom: key1.word.length};
            key2.top = {pair: key1, commom: key2.word.length};
        }
    }

    var find = function (str1, index1, str2, index2) {
        let index = index1, length = 0, i = index;

        while (i < str1.length) {
            if (str1.charAt(i) == str2.charAt(index2)) {
                let j = 0;

                while (j + i < str1.length && index2 + j < str2.length
                    && str1.charAt(i + j) == str2.charAt(index2 + j)) {
                    j++;
                }

                if (j > length) {
                    length = j;
                    index = i;
                }
            }

            i++;
        }

        return FindResult(index, length);
    }

    var calcMark = function (str1, str2) {
        let i1 = 0, i2 = 0, mark = 0;

        while (i1 < str1.length && i2 < str2.length) {
            let result1 = find(str1, i1, str2, i2);
            let result2 = find(str2, i2, str1, i1);

            if (result1.length == 0 && result2.length == 0) {
                break;
            }

            if ((result1.index < result2.index && result1.length > 0) || result2.length == 0) {
                mark += result1.length;
                i1 = result1.index + result1.length;
                i2+= result1.length;
            }
            else {
                mark += result2.length;
                i2 = result2.index + result2.length;
                i1+=result2.length;
            }
        }

        return mark;
    }

    var join = function (key1, key2) {
        let mark = calcMark(key1.word, key2.word);

        if (mark > 0) {
            insert(key1.top, mark, key2);
            insert(key2.top, mark, key1);
        }
    }

    var maxOfTop = function (top) {
        let max = 0;

        for (let nmax in top) {
            if (nmax > max) {
                max = nmax;
            }
        }

        return max;
    }

    var normalize = function (keys) {
        let max;

        do {
            max = 0;

            for (let key of keys) {
                if (key.paired) continue;

                let nmax = maxOfTop(key.top);

                if (nmax > max) {
                    max = nmax;
                }
            }

            if (max == 0) {
                break;
            }

            for (let key of keys) {
                if (key.paired) continue;

                let commom = maxOfTop(key.top);

                if (commom == max && !key.paired) {
                    let pair = key.top[commom].pop();

                    key.top = {pair, commom};

                    key.paired = pair.paired = true;
                    pair.top = {pair: key, commom};

                    for (let k1 of keys) {
                        if (k1.paired) continue;
                        for (let k2 in k1.top) {
                            remove(k1.top, k2, key);
                            remove(k1.top, k2, pair);
                        }
                    }
                }
            }
        } while (max > 0);
    }

    var mesh = function (keys1, keys2) {
        for (let key1 of keys1) {
            for (let key2 of keys2) {
                pair(key1, key2);
            }

            if (key1.paired) continue;

            for (let key2 of keys2) {
                if (!key2.paired) {
                    join(key1, key2);
                }
            }
        }

        normalize(keys2);
    }

    var calculate = function (l1, l2) {
        let k1 = [], k2 = [];

        for (let str of l1) {
            k1.push(Key(str));
        }

        for (let str of l2) {
            k2.push(Key(str));
        }

        mesh(k1, k2);

        let catched1 = 0, sum1 = 0, catched2 = 0, sum2 = 0;

        for (let key of k1) {
            if (!!key.top.commom) {
                catched1 += Number(key.top.commom);
            }
            sum1 += key.word.length;
        }

        for (let key of k2) {
            if (!!key.top.commom) {
                catched2 += Number(key.top.commom);
            }
            sum2 += key.word.length;
        }

        return (catched1 / sum1 + catched2 / sum2) * 0.5;
    }

    this.proximity = function (str1, str2) {
        var regex = /[^\w\p{L}]+/iu;
        let s1 = str1.toLowerCase();
        let s2 = str2.toLowerCase();
        let l1 = s1.split(regex);
        let l2 = s2.split(regex);

        return calculate(l1, l2);
    }
}

window.xt = xt;
