#include "keyword-packer.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>

#include <icL-ce/keyword/advanced/assert.h++>
#include <icL-ce/keyword/advanced/emit.h++>
#include <icL-ce/keyword/advanced/emitter.h++>
#include <icL-ce/keyword/advanced/jammer.h++>
#include <icL-ce/keyword/advanced/now.h++>
#include <icL-ce/keyword/advanced/slot.h++>
#include <icL-ce/keyword/advanced/wait.h++>
#include <icL-ce/keyword/control/any.h++>
#include <icL-ce/keyword/control/case.h++>
#include <icL-ce/keyword/control/else.h++>
#include <icL-ce/keyword/control/exists.h++>
#include <icL-ce/keyword/control/if-proxy.h++>
#include <icL-ce/keyword/control/switch.h++>
#include <icL-ce/keyword/loop/do.h++>
#include <icL-ce/keyword/loop/filter.h++>
#include <icL-ce/keyword/loop/for-proxy.h++>
#include <icL-ce/keyword/loop/range.h++>
#include <icL-ce/keyword/loop/while.h++>
#include <icL-ce/literal/const/bool.h++>
#include <icL-ce/literal/static/identifier.h++>
#include <icL-ce/literal/static/type.h++>
#include <icL-ce/value-system/datetime.h++>
#include <icL-ce/value-system/import.h++>
#include <icL-ce/value-system/log.h++>
#include <icL-ce/value-system/math.h++>
#include <icL-ce/value-system/signal.h++>
#include <icL-ce/value-system/stack.h++>
#include <icL-ce/value-system/stacks.h++>
#include <icL-ce/value-system/state.h++>
#include <icL-ce/value-system/types.h++>

#include <icL-memory/state/memory.h++>



namespace icL::core::cp {

KeywordPacker::KeywordPacker() {
    keys = {{"any", &KeywordPacker::pack_any},
            {"assert", &KeywordPacker::pack_assert},
            {"case", &KeywordPacker::pack_case},
            {"do", &KeywordPacker::pack_do},
            {"else", &KeywordPacker::pack_else},
            {"emit", &KeywordPacker::pack_emit},
            {"emitter", &KeywordPacker::pack_emitter},
            {"exists", &KeywordPacker::pack_exists},
            {"filter", &KeywordPacker::pack_filter},
            {"for", &KeywordPacker::pack_for},
            {"if", &KeywordPacker::pack_if},
            {"now", &KeywordPacker::pack_now},
            {"range", &KeywordPacker::pack_range},
            {"slot", &KeywordPacker::pack_slot},
            {"switch", &KeywordPacker::pack_switch},
            {"wait", &KeywordPacker::pack_wait},
            {"while", &KeywordPacker::pack_while},
            {"Datetime", &KeywordPacker::pack_Datetime},
            {"Import", &KeywordPacker::pack_Import},
            {"Log", &KeywordPacker::pack_Log},
            {"Math", &KeywordPacker::pack_Math},
            {"Signal", &KeywordPacker::pack_Signal},
            {"Stack", &KeywordPacker::pack_Stack},
            {"Stacks", &KeywordPacker::pack_Stacks},
            {"State", &KeywordPacker::pack_State},
            {"Types", &KeywordPacker::pack_Types},
            {"bool", &KeywordPacker::pack_bool},
            {"datetime", &KeywordPacker::pack_datetime},
            {"double", &KeywordPacker::pack_double},
            {"int", &KeywordPacker::pack_int},
            {"jammer", &KeywordPacker::pack_jammer},
            {"lambda-icl", &KeywordPacker::pack_lambda_icl},
            {"list", &KeywordPacker::pack_list},
            {"object", &KeywordPacker::pack_object},
            {"set", &KeywordPacker::pack_set},
            {"string", &KeywordPacker::pack_string},
            {"void", &KeywordPacker::pack_void},
            {"alert", &KeywordPacker::pack_system_type},
            {"tabs", &KeywordPacker::pack_system_type},
            {"windows", &KeywordPacker::pack_system_type},
            {"sessions", &KeywordPacker::pack_system_type},
            {"true", &KeywordPacker::pack_bool_literal},
            {"false", &KeywordPacker::pack_bool_literal}};
}

il::CE * KeywordPacker::withModifiers(
  ce::Keyword * keyword, IFlyer * fly, icStringList (*getter)(IFlyer *)) {
    keyword->giveModifiers((*getter)(fly));
    return keyword;
}

il::CE * KeywordPacker::withModifiersWithNotSupport(
  ce::Keyword * keyword, IFlyer * fly) {
    return withModifiers(keyword, fly, &icL::getModifiersWithNotSupport);
}

il::CE * KeywordPacker::pack_key(const PackData & pd) {
    auto it = keys.find(pd.result.key);

    if (it != keys.end()) {
        return (*it.value())(pd);
    }

    return pack_identifier(pd);
}

il::CE * KeywordPacker::pack_identifier(const PackData & pd) {
    return new ce::Identifier(pd.il, pd.result.key);
}

il::CE * KeywordPacker::pack_assert(const PackData & pd) {
    return withModifiersWithNotSupport(new ce::Assert{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_any(const PackData & pd) {
    return new ce::Any{pd.il};
}

il::CE * KeywordPacker::pack_case(const PackData & pd) {
    return new ce::Case(pd.il);
}

il::CE * KeywordPacker::pack_do(const PackData & pd) {
    return new ce::Do{pd.il};
}

il::CE * KeywordPacker::pack_else(const PackData & pd) {
    return new ce::Else(pd.il);
}

il::CE * KeywordPacker::pack_emit(const PackData & pd) {
    return withModifiers(new ce::Emit{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_emitter(const PackData & pd) {
    return withModifiers(new ce::Emitter{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_exists(const PackData & pd) {
    return new ce::Exists{pd.il};
}

il::CE * KeywordPacker::pack_filter(const PackData & pd) {
    return withModifiers(new ce::Filter{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_for(const PackData & pd) {
    return withModifiers(new ce::ForProxy{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_if(const PackData & pd) {
    return withModifiersWithNotSupport(new ce::IfProxy(pd.il), pd.fly);
}

il::CE * KeywordPacker::pack_jammer(const PackData & pd) {
    return withModifiers(new ce::Jammer{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_now(const PackData & pd) {
    return new ce::Now{pd.il};
}

il::CE * KeywordPacker::pack_range(const PackData & pd) {
    return withModifiers(new ce::Range{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_slot(const PackData & pd) {
    return withModifiers(new ce::Slot{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_switch(const PackData & pd) {
    return new ce::Switch(pd.il);
}

il::CE * KeywordPacker::pack_wait(const PackData & pd) {
    return withModifiers(new ce::Wait{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_while(const PackData & pd) {
    return withModifiersWithNotSupport(new ce::While{pd.il}, pd.fly);
}

il::CE * KeywordPacker::pack_Datetime(const PackData & pd) {
    return new ce::DateTime{pd.il};
}


il::CE * KeywordPacker::pack_Import(const PackData & pd) {
    return new ce::Import{pd.il};
}

il::CE * KeywordPacker::pack_Log(const PackData & pd) {
    return new ce::Log{pd.il};
}

il::CE * KeywordPacker::pack_Math(const PackData & pd) {
    return new ce::Math{pd.il};
}

il::CE * KeywordPacker::pack_Signal(const PackData & pd) {
    return new ce::Signal{pd.il};
}

il::CE * KeywordPacker::pack_Stack(const PackData & pd) {
    return new ce::Stack{pd.il, pd.il->mem->stackIt().stack()};
}

il::CE * KeywordPacker::pack_Stacks(const PackData & pd) {
    return new ce::Stacks{pd.il};
}

il::CE * KeywordPacker::pack_State(const PackData & pd) {
    return new ce::State{pd.il};
}

il::CE * KeywordPacker::pack_Types(const PackData & pd) {
    return new ce::Types{pd.il};
}

using ce::Type;

il::CE * KeywordPacker::pack_bool(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::BoolValue};
}

il::CE * KeywordPacker::pack_datetime(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::DatetimeValue};
}

il::CE * KeywordPacker::pack_double(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::DoubleValue};
}

il::CE * KeywordPacker::pack_int(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::IntValue};
}

il::CE * KeywordPacker::pack_lambda_icl(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::LambdaValue};
}

il::CE * KeywordPacker::pack_list(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::ListValue};
}

il::CE * KeywordPacker::pack_object(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::ObjectValue};
}

il::CE * KeywordPacker::pack_set(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::SetValue};
}

il::CE * KeywordPacker::pack_string(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::StringValue};
}

il::CE * KeywordPacker::pack_void(const PackData & pd) {
    return new ce::TypeToken{pd.il, Type::VoidValue};
}

il::CE * KeywordPacker::pack_system_type(const PackData & pd) {
    pd.il->vm->cp_sig(
      pd.fly->issueWithPath(pd.result.begin) %
      "Using of system types is restricted. Detected: " % pd.result.key);
    return nullptr;
}

il::CE * KeywordPacker::pack_bool_literal(const PackData & pd) {
    return new ce::Bool{pd.il, pd.result.key};
}

}  // namespace icL::core::cp
