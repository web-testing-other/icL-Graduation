#ifndef core_cp_FlyResult
#define core_cp_FlyResult

#include "type.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/structures/position.h++>



namespace icL::core::cp {


/**
 * @brief The BaseFlyResult struct represents a result of a any fly
 */
struct icL_core_cp_EXPORT BaseFlyResult
{
    /// \brief key is the catched string
    icString key;

    /// \brief begin is the begin of token
    il::Position begin;

    /// \brief end is the end of token
    il::Position end;

    /// \brief data is a container for addition data of each case
    icObject<icString, icVariant> data;
};

/**
 * @brief The FlyResult struct represents a result of a icL fly
 */
struct icL_core_cp_EXPORT FlyResult : public BaseFlyResult
{
    /// \brief type is the type of result
    int type = ResultType::CommandEnd;
};


}  // namespace icL::core::cp

#endif  // core_cp_FlyResult
