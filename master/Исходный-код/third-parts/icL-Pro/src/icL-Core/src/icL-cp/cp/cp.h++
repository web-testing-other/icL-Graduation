#ifndef core_cp_CP
#define core_cp_CP

#include "../flyer/i-flyer.h++"
#include "../result/structs.h++"

#include <icL-il/auxiliary/source-of-code.h++>
#include <icL-il/main/cp.h++>
#include <icL-il/structures/code-fragment.h++>

#include <icL-ce/base/main/ce.h++>



namespace icL::core {

namespace ce {
class Keyword;
}

namespace cp {

struct PackData
{
    il::InterLevel *  il;
    IFlyer *          fly;
    const FlyResult & result;
};

/**
 * @brief The CP class represents a command processor
 */
class icL_core_cp_EXPORT CP
    : public il::CP
    , public il::Node
{
    /// \brief flyer is the tokenizer engine
    IFlyer * flyer;

    /// \brief m_lastResult is the last key as string
    FlyResult m_lastToken;

    /// \brief code is a fragment of shared source of code
    il::CodeFragment code;

    /// \brief repeatToken forces to repeat the current token
    bool repeatToken = false;

protected:
    icObject<int, il::CE * (*)(const PackData &)> proxies;

    icObject<int, icString> keysTypes;

public:
    CP(il::InterLevel * il, IFlyer * flyer, const il::CodeFragment & code);

    il::CE * parseNext() override;
    icString lastKey() override;
    icString getFilePath() override;
    icString getFilePathLineChar() override;

    /**
     * @brief toCodeF cast to code fragment
     * @param result is the parsing result
     * @return a code fragment based on parsing result
     */
    static il::CodeFragment toCodeF(const PackData & pd);

    void reset(const il::CodeFragment & code) override;
    void repeat() override;

private:
    // common

    static il::CE * pack_local(const PackData & pd);
    static il::CE * pack_global(const PackData & pd);
    static il::CE * pack_function(const PackData & pd);
    static il::CE * pack_property(const PackData & pd);
    static il::CE * pack_method(const PackData & pd);
    static il::CE * pack_key(const PackData & pd);

    // literals

    static il::CE * pack_int_literal(const PackData & pd);
    static il::CE * pack_double_literal(const PackData & pd);
    static il::CE * pack_string_literal(const PackData & pd);
    static il::CE * pack_regex_literal(const PackData & pd);

    // operators

    static il::CE * pack_And(const PackData & pd);
    static il::CE * pack_Bg(const PackData & pd);
    static il::CE * pack_BgEq(const PackData & pd);
    static il::CE * pack_BgEqSm(const PackData & pd);
    static il::CE * pack_BgSm(const PackData & pd);
    static il::CE * pack_Bs(const PackData & pd);
    static il::CE * pack_Cl(const PackData & pd);
    static il::CE * pack_ClAs(const PackData & pd);
    static il::CE * pack_ClCl(const PackData & pd);
    static il::CE * pack_ClSt(const PackData & pd);
    static il::CE * pack_ClEq(const PackData & pd);
    static il::CE * pack_Eq(const PackData & pd);
    static il::CE * pack_EqEq(const PackData & pd);
    static il::CE * pack_Mn(const PackData & pd);
    static il::CE * pack_Nt(const PackData & pd);
    static il::CE * pack_NtEq(const PackData & pd);
    static il::CE * pack_NtSm(const PackData & pd);
    static il::CE * pack_NtSt(const PackData & pd);
    static il::CE * pack_Or(const PackData & pd);
    static il::CE * pack_Or2(const PackData & pd);
    static il::CE * pack_Pc(const PackData & pd);
    static il::CE * pack_Pl(const PackData & pd);
    static il::CE * pack_Sm(const PackData & pd);
    static il::CE * pack_SmBg(const PackData & pd);
    static il::CE * pack_SmEq(const PackData & pd);
    static il::CE * pack_SmEqBg(const PackData & pd);
    static il::CE * pack_SmSm(const PackData & pd);
    static il::CE * pack_SmSt(const PackData & pd);
    static il::CE * pack_Sl(const PackData & pd);
    static il::CE * pack_SlAp(const PackData & pd);
    static il::CE * pack_St(const PackData & pd);
    static il::CE * pack_StSt(const PackData & pd);
    static il::CE * pack_Tl(const PackData & pd);
    static il::CE * pack_RunCx(const PackData & pd);
    static il::CE * pack_LimitedCx(const PackData & pd);
    static il::CE * pack_ValueCx(const PackData & pd);
    static il::CE * pack_comma(const PackData & pd);
    static il::CE * pack_CommandEnd(const PackData & pd);
    static il::CE * pack_unexpected(const PackData & pd);

    // CP interface
public:
    icList<il::CodeFragment> splitCommands(
      const il::CodeFragment & code) override;

    icString lastTokenPosition() override;
};

}  // namespace cp
}  // namespace icL::core

#endif  // core_cp_CP
