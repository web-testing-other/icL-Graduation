#include "source-of-code.h++"

#include <icL-types/replaces/ic-string.h++>



namespace icL::core::cp {

SourceOfCode::SourceOfCode(
  il::InterLevel * il, std::shared_ptr<icFile> file, const il::Position & begin,
  const il::Position & end)
    : il::Node(il)
    , file(file)
    , stream()
    , prevPosition(begin)
    , position(begin)
    , end(end) {
    assert(file->isReadable());
    stream.open(file);
    stream.seekTo(begin.byte);
}

// block delete file od standart descructor
SourceOfCode::~SourceOfCode() {}

icString SourceOfCode::getFilePath() {
    return file->getFileName();
}

il::SourceOfCode * SourceOfCode::fragment(
  const il::Position & begin, const il::Position & end) {
    return new SourceOfCode{il, file, begin, end};
}

icChar SourceOfCode::current() {
    return currentChar;
}

icChar SourceOfCode::next() {
    prevPosition = position;
    prevChar     = currentChar;

    stream >> currentChar;

    if (currentChar == '\n') {
        lastLineLength    = position.relative;
        position.relative = 1;
        position.line++;
    }
    else {
        position.relative++;
    }

    position.absolute++;
    position.byte = stream.pos();

    //    std::cout << "next step " << prevChar << " -> " << currentChar <<
    //    std::endl;

    return currentChar;
}

icChar SourceOfCode::prev() {
    //    std::cout << "prev step " << currentChar << " <- " << prevChar <<
    //    std::endl;
    stream.seekTo(prevPosition.byte);
    position = prevPosition;

    return currentChar;
}

bool SourceOfCode::atEnd() {
    return (position.absolute >= end.absolute && end.line != -1) ||
           stream.atEnd();
}

bool SourceOfCode::seek(il::Position position) {
    this->position = position;
    return stream.seekTo(position.byte);
}

il::Position SourceOfCode::getPosition() {
    return position;
}

}  // namespace icL::core::cp
