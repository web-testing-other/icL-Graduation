#include "factory.h++"

#include "cp.h++"



namespace icL::core::cp {

il::CP * Factory::create(il::InterLevel * il, const il::CodeFragment & code) {
    return new CP(il, nullptr, code);
}

}  // namespace icL::core::cp
