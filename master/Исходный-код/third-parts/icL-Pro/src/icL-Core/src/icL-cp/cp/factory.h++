#ifndef ICL_CORE_CP_FACTORY_H
#define ICL_CORE_CP_FACTORY_H

#include <icL-il/main/cp-factory.h++>



namespace icL::core::cp {

class Factory : public il::CpFactory
{
public:
    // CpFactory interface
public:
    il::CP * create(
      il::InterLevel * il, const il::CodeFragment & code) override;
};

}  // namespace icL::core::cp

#endif // ICL_CORE_CP_FACTORY_H
