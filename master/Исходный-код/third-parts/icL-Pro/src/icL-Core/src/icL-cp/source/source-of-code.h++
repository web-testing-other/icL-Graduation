#ifndef core_cp_SourceOfCode
#define core_cp_SourceOfCode

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-file.h++>
#include <icL-types/replaces/ic-text-stream.h++>

#include <icL-il/auxiliary/source-of-code.h++>
#include <icL-il/main/node.h++>
#include <icL-il/structures/position.h++>

#include <memory>



namespace icL::core::cp {

class icL_core_cp_EXPORT SourceOfCode
    : public il::SourceOfCode
    , public il::Node
{
    std::shared_ptr<icFile> file;
    icTextStream            stream;
    il::Position            prevPosition;
    il::Position            position;
    il::Position            end;

    short  lastLineLength = 0;
    icChar currentChar    = '\0';
    icChar prevChar       = '\0';

public:
    SourceOfCode(
      il::InterLevel * il, std::shared_ptr<icFile> file,
      const il::Position & begin, const il::Position & end);
    ~SourceOfCode();


    // SourceOfCode interface
public:
    icString getFilePath() override;

    il::SourceOfCode * fragment(
      const il::Position & begin, const il::Position & end) override;

    icChar current() override;
    icChar next() override;
    icChar prev() override;

    bool atEnd() override;
    bool seek(il::Position position) override;

    il::Position getPosition() override;
};

}  // namespace icL::core::cp

#endif  // core_cp_SourceOfCode
