#ifndef core_cp_KeywordPacker
#define core_cp_KeywordPacker

#include "cp.h++"

#include <icL-il/main/ce.h++>

#include <icL-cp/flyer/icL.h++>


namespace icL::core::cp {

class KeywordPacker
{
protected:
    icObject<icString, il::CE * (*)(const PackData &)> keys;

protected:
    static il::CE * withModifiers(
      ce::Keyword * keyword, IFlyer * fly,
      icStringList (*getter)(IFlyer *) = &icL::getModifiers);

    static il::CE * withModifiersWithNotSupport(
      ce::Keyword * keyword, IFlyer * fly);

public:
    KeywordPacker();

    il::CE * pack_key(const PackData & pd);

    // identifiers

    static il::CE * pack_identifier(const PackData & pd);

    // keywords

    static il::CE * pack_assert(const PackData & pd);
    static il::CE * pack_any(const PackData & pd);
    static il::CE * pack_case(const PackData & pd);
    static il::CE * pack_do(const PackData & pd);
    static il::CE * pack_else(const PackData & pd);
    static il::CE * pack_emit(const PackData & pd);
    static il::CE * pack_emitter(const PackData & pd);
    static il::CE * pack_exists(const PackData & pd);
    static il::CE * pack_filter(const PackData & pd);
    static il::CE * pack_for(const PackData & pd);
    static il::CE * pack_if(const PackData & pd);
    static il::CE * pack_jammer(const PackData & pd);
    static il::CE * pack_now(const PackData & pd);
    static il::CE * pack_range(const PackData & pd);
    static il::CE * pack_slot(const PackData & pd);
    static il::CE * pack_switch(const PackData & pd);
    static il::CE * pack_wait(const PackData & pd);
    static il::CE * pack_while(const PackData & pd);

    // singletons and commands

    static il::CE * pack_Datetime(const PackData & pd);
    static il::CE * pack_Import(const PackData & pd);
    static il::CE * pack_Log(const PackData & pd);
    static il::CE * pack_Math(const PackData & pd);
    static il::CE * pack_Signal(const PackData & pd);
    static il::CE * pack_Stack(const PackData & pd);
    static il::CE * pack_Stacks(const PackData & pd);
    static il::CE * pack_State(const PackData & pd);
    static il::CE * pack_Types(const PackData & pd);

    // types

    static il::CE * pack_bool(const PackData & pd);
    static il::CE * pack_datetime(const PackData & pd);
    static il::CE * pack_double(const PackData & pd);
    static il::CE * pack_int(const PackData & pd);
    static il::CE * pack_lambda_icl(const PackData & pd);
    static il::CE * pack_list(const PackData & pd);
    static il::CE * pack_object(const PackData & pd);
    static il::CE * pack_set(const PackData & pd);
    static il::CE * pack_string(const PackData & pd);
    static il::CE * pack_void(const PackData & pd);
    static il::CE * pack_system_type(const PackData & pd);

    // other literal

    static il::CE * pack_bool_literal(const PackData & pd);
};

}  // namespace icL::core::cp

#endif  // core_cp_KeywordPacker
