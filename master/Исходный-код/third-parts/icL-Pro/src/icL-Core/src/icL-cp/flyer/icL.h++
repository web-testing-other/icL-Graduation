#ifndef core_cp_Flyer_icL
#define core_cp_Flyer_icL

#include "../result/type.h++"
#include "i-flyer.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/structures/position.h++>



namespace icL::core::cp {

/**
 * @brief The Flyer class represents an flying cursor
 */
class icL_core_cp_EXPORT icL : virtual public IFlyer
{
protected:
    icObject<icChar, void (*)(IFlyer *, FlyResult &)> proxies;

public:
    icL();

    /**
     * @brief flyNext flies the next token
     * @return the result of fly
     */
    FlyResult flyNext(
      bool packContexts = true, bool skipErrors = false) override;

    /**
     * @brief finishContext finish a context parsing
     * @param result is the fly result to evaluate
     */
    void finishContext(FlyResult & result);

    /**
     * @brief getModifiers gets modifiers as list
     * @return the modifiers of keyword
     */
    static icStringList getModifiers(IFlyer * fly);

    /**
     * @brief getModifiersWithNotSupport gets modifiers with `!` support
     * @return the list of modifiers (`! == not`)
     */
    static icStringList getModifiersWithNotSupport(IFlyer * fly);

protected:
    /**
     * @brief flyToNext flies to next not-white character
     * @return the next non-white character or 0x0 at end of source
     */
    static icChar flyToNext(IFlyer * fly);

    /**
     * @brief flyName extracts a name of a method/property/variable/function
     * @return the extracted name
     */
    static icString flyName(IFlyer * fly);

    /**
     * @brief flyLiteral flyies a bracket literal
     * @param result will add to data `lit-begin`, `lit-end` & `lit-str`
     */
    static void flyLiteral(IFlyer * fly, FlyResult & result);

    // Common tokens

    /// fly comments
    static void flyComment(IFlyer * fly, FlyResult & result);

    /// `@local`
    static void flyLocal(IFlyer * fly, FlyResult & result);

    /// `#global`
    static void flyGlobal(IFlyer * fly, FlyResult & result);

    /// `$function`
    static void flyFunction(IFlyer * fly, FlyResult & result);

    /// `'property`
    static void flyProperty(IFlyer * fly, FlyResult & result);

    /// `.method`
    static void flyMethod(IFlyer * fly, FlyResult & result);

    /// `key`
    static void flyKey(IFlyer * fly, FlyResult & result);

    // Literals

    /// `d+.d+`
    static void flyDouble(IFlyer * fly, FlyResult & result);

    /// `d+`
    static void flyInt(IFlyer * fly, FlyResult & result);

    /// `d+` & `d+.d+`
    static void flyNumber(IFlyer * fly, FlyResult & result);

    /// `/xREGEXx/`
    static void flyRegEx(IFlyer * fly, FlyResult & result);

    /// `"string"`
    static void flyString(IFlyer * fly, FlyResult & result);

    // Operators

    /// `&`
    static void flyAnd(IFlyer * fly, FlyResult & result);

    /// `>` & `>=` & `>=<` & `><`
    static void flyBg(IFlyer * fly, FlyResult & result);

    /// `>=`
    static void flyBgEq(IFlyer * fly, FlyResult & result);

    /// `>=<`
    static void flyBgEqSm(IFlyer * fly, FlyResult & result);

    /// `><`
    static void flyBgSm(IFlyer * fly, FlyResult & result);

    /// `\`
    static void flyBs(IFlyer * fly, FlyResult & result);

    /// `:` & `:?` & `::` & `:*`
    static void flyCl(IFlyer * fly, FlyResult & result);

    /// `:?`
    static void flyClAs(IFlyer * fly, FlyResult & result);

    /// `::`
    static void flyClCl(IFlyer * fly, FlyResult & result);

    /// `:*`
    static void flyClSt(IFlyer * fly, FlyResult & result);

    /// `:=`
    static void flyClEq(IFlyer * fly, FlyResult & result);

    /// `=` & `==`
    static void flyEq(IFlyer * fly, FlyResult & result);

    /// `==`
    static void flyEqEq(IFlyer * fly, FlyResult & result);

    /// `-`
    static void flyMn(IFlyer * fly, FlyResult & result);

    /// `!` & `!=` & `!<` & `!*`
    static void flyNt(IFlyer * fly, FlyResult & result);

    /// `!=`
    static void flyNtEq(IFlyer * fly, FlyResult & result);

    /// `!<`
    static void flyNtSm(IFlyer * fly, FlyResult & result);

    /// `!*`
    static void flyNtSt(IFlyer * fly, FlyResult & result);

    /// `|`
    static void flyOr(IFlyer * fly, FlyResult & result);

    /// `^`
    static void flyOr2(IFlyer * fly, FlyResult & result);

    /// `%`
    static void flyPc(IFlyer * fly, FlyResult & result);

    /// `+`
    static void flyPl(IFlyer * fly, FlyResult & result);

    /// `<` & `<>` & `<=` & `<=>` & `<<` & `<*`
    static void flySm(IFlyer * fly, FlyResult & result);

    /// `<>`
    static void flySmBg(IFlyer * fly, FlyResult & result);

    /// `<=`
    static void flySmEq(IFlyer * fly, FlyResult & result);

    /// `<=>`
    static void flySmEqBg(IFlyer * fly, FlyResult & result);

    /// `<<`
    static void flySmSm(IFlyer * fly, FlyResult & result);

    /// `<*`
    static void flySmSt(IFlyer * fly, FlyResult & result);

    /// `/` & `/'`
    static void flySl(IFlyer * fly, FlyResult & result);

    /// `/'`
    static void flySlAp(IFlyer * fly, FlyResult & result);

    /// `*` & `**`
    static void flySt(IFlyer * fly, FlyResult & result);

    /// `**`
    static void flyStSt(IFlyer * fly, FlyResult & result);

    /// `~`
    static void flyTl(IFlyer * fly, FlyResult & result);

    // Contexts

    /// `{`
    static void flyRunOp(IFlyer * fly, FlyResult & result);

    /// `}`
    static void flyRunCl(IFlyer * fly, FlyResult & result);

    /// `[`
    static void flyLimitedOp(IFlyer * fly, FlyResult & result);

    /// `]`
    static void flyLimitedCl(IFlyer * fly, FlyResult & result);

    /// `(`
    static void flyValueOp(IFlyer * fly, FlyResult & result);

    /// `)`
    static void flyValueCl(IFlyer * fly, FlyResult & result);

    /// `,`
    static void flyComma(IFlyer * fly, FlyResult & result);

    // End of commands

    /// `;`
    static void flyCommandEnd(IFlyer * fly, FlyResult & result);
};

}  // namespace icL::core::cp

#endif  // core_cp_Flyer_icL
