#include "flyer.h++"

#include "icL-types/replaces/ic-variant.h++"



namespace icL::core::cp {

Flyer::Flyer(il::InterLevel * il, const il::CodeFragment & code)
    : il::Node(il) {
    if (code.source != nullptr)
        m_source.reset(code.source->fragment(code.begin, code.end));
}

void Flyer::seek(const il::Position & pos) {
    m_source->seek(pos);
}

icString Flyer::getFilePath() {
    return m_source->getFilePath();
}

void Flyer::reset(const il::CodeFragment & code) {
    m_source.reset(code.source->fragment(code.begin, code.end));
}

std::shared_ptr<il::SourceOfCode> & Flyer::source() {
    return m_source;
}

icString Flyer::issue(const il::Position & pos) const {
    return icString::number(pos.line) % ':' % icString::number(pos.relative);
}

icString Flyer::issueWithPath(const il::Position & pos) const {
    return m_source->getFilePath() % ':' % issue(pos) % ": ";
}

il::InterLevel * Flyer::core() {
    return il;
}

}  // namespace icL::core::cp
