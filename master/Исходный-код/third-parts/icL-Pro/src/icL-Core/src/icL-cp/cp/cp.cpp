#include "cp.h++"

#include "flyer.h++"
#include "keyword-packer.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-ce/literal/const/double.h++>
#include <icL-ce/literal/const/int.h++>
#include <icL-ce/literal/const/regex.h++>
#include <icL-ce/literal/const/string.h++>
#include <icL-ce/literal/functional/function.h++>
#include <icL-ce/literal/functional/method.h++>
#include <icL-ce/literal/functional/property.h++>
#include <icL-ce/operators-ALU/arithmetical/addition.h++>
#include <icL-ce/operators-ALU/arithmetical/division.h++>
#include <icL-ce/operators-ALU/arithmetical/exponentiation.h++>
#include <icL-ce/operators-ALU/arithmetical/multiplication.h++>
#include <icL-ce/operators-ALU/arithmetical/remainder.h++>
#include <icL-ce/operators-ALU/arithmetical/root.h++>
#include <icL-ce/operators-ALU/arithmetical/subtraction.h++>
#include <icL-ce/operators-ALU/logic/biconditional.h++>
#include <icL-ce/operators-ALU/logic/conjunction.h++>
#include <icL-ce/operators-ALU/logic/disjunction.h++>
#include <icL-ce/operators-ALU/logic/exclusive-disjunction.h++>
#include <icL-ce/operators-ALU/logic/negation.h++>
#include <icL-ce/operators-ALU/logic/selective-select.h++>
#include <icL-ce/operators-ALU/system/assign.h++>
#include <icL-ce/operators-ALU/system/comma.h++>
#include <icL-ce/operators-ALU/system/limited-context.h++>
#include <icL-ce/operators-ALU/system/overload.h++>
#include <icL-ce/operators-ALU/system/run-context.h++>
#include <icL-ce/operators-ALU/system/value-context.h++>
#include <icL-ce/operators-advanced/cast/can-cast.h++>
#include <icL-ce/operators-advanced/cast/cast.h++>
#include <icL-ce/operators-advanced/cast/error-less-cast.h++>
#include <icL-ce/operators-advanced/cast/type-of.h++>
#include <icL-ce/operators-advanced/compare/bigger-equal-smaller.h++>
#include <icL-ce/operators-advanced/compare/bigger-equal.h++>
#include <icL-ce/operators-advanced/compare/bigger-smaller.h++>
#include <icL-ce/operators-advanced/compare/bigger.h++>
#include <icL-ce/operators-advanced/compare/equality.h++>
#include <icL-ce/operators-advanced/compare/inequality.h++>
#include <icL-ce/operators-advanced/compare/smaller-bigger.h++>
#include <icL-ce/operators-advanced/compare/smaller-equal-bigger.h++>
#include <icL-ce/operators-advanced/compare/smaller-equal.h++>
#include <icL-ce/operators-advanced/compare/smaller.h++>
#include <icL-ce/operators-advanced/including/not-contains-template.h++>
#include <icL-ce/operators-advanced/including/not-contains.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/lambda-target.h++>

//#include <iostream>



namespace icL::core::cp {

using memory::Type;

CP::CP(il::InterLevel * il, IFlyer * flyer, const il::CodeFragment & code)
    : il::Node(il)
    , flyer(flyer ? flyer : new Flyer(il, code))
    , code(code) {
    proxies = {{ResultType::Local, &CP::pack_local},
               {ResultType::Global, &CP::pack_global},
               {ResultType::Function, &CP::pack_function},
               {ResultType::Property, &CP::pack_property},
               {ResultType::Method, &CP::pack_method},
               {ResultType::Key, &CP::pack_key},
               {ResultType::Double, &CP::pack_double_literal},
               {ResultType::Int, &CP::pack_int_literal},
               {ResultType::RegEx, &CP::pack_regex_literal},
               {ResultType::String, &CP::pack_string_literal},
               {ResultType::And, &CP::pack_And},
               {ResultType::Bg, &CP::pack_Bg},
               {ResultType::BgEq, &CP::pack_BgEq},
               {ResultType::BgEqSm, &CP::pack_BgEqSm},
               {ResultType::BgSm, &CP::pack_BgSm},
               {ResultType::Bs, &CP::pack_Bs},
               {ResultType::Cl, &CP::pack_Cl},
               {ResultType::ClAs, &CP::pack_ClAs},
               {ResultType::ClCl, &CP::pack_ClCl},
               {ResultType::ClSt, &CP::pack_ClSt},
               {ResultType::ClEq, &CP::pack_ClEq},
               {ResultType::Eq, &CP::pack_Eq},
               {ResultType::EqEq, &CP::pack_EqEq},
               {ResultType::Mn, &CP::pack_Mn},
               {ResultType::Nt, &CP::pack_Nt},
               {ResultType::NtEq, &CP::pack_NtEq},
               {ResultType::NtSm, &CP::pack_NtSm},
               {ResultType::NtSt, &CP::pack_NtSt},
               {ResultType::Or, &CP::pack_Or},
               {ResultType::Or2, &CP::pack_Or2},
               {ResultType::Pc, &CP::pack_Pc},
               {ResultType::Pl, &CP::pack_Pl},
               {ResultType::Sm, &CP::pack_Sm},
               {ResultType::SmBg, &CP::pack_SmBg},
               {ResultType::SmEq, &CP::pack_SmEq},
               {ResultType::SmEqBg, &CP::pack_SmEqBg},
               {ResultType::SmSm, &CP::pack_SmSm},
               {ResultType::SmSt, &CP::pack_SmSt},
               {ResultType::Sl, &CP::pack_Sl},
               {ResultType::SlAp, &CP::pack_SlAp},
               {ResultType::St, &CP::pack_St},
               {ResultType::StSt, &CP::pack_StSt},
               {ResultType::Tl, &CP::pack_Tl},
               {ResultType::RunOp, &CP::pack_RunCx},
               {ResultType::LimitedOp, &CP::pack_LimitedCx},
               {ResultType::ValueOp, &CP::pack_ValueCx},
               {ResultType::Comma, &CP::pack_comma},
               {ResultType::CommandEnd, &CP::pack_CommandEnd},
               {ResultType::RunCl, &CP::pack_unexpected},
               {ResultType::LimitedCl, &CP::pack_unexpected},
               {ResultType::ValueCl, &CP::pack_unexpected}};

    keysTypes = {{ResultType::Comment, "comment"},
                 {ResultType::Local, "local variable"},
                 {ResultType::Global, "global variable"},
                 {ResultType::Property, "property"},
                 {ResultType::Method, "method"},
                 {ResultType::Key, "keyword"},
                 {ResultType::Double, "double literal"},
                 {ResultType::Int, "integer literal"},
                 {ResultType::RegEx, "regular expression literal"},
                 {ResultType::String, "string literal"},
                 {ResultType::RunOp, "run context"},
                 {ResultType::LimitedOp, "limited context"},
                 {ResultType::ValueOp, "value context"},
                 {ResultType::CommandEnd, "command delimiter"}};
}

il::CE * CP::parseNext() {

    FlyResult token;

    if (repeatToken) {
        flyer->seek(m_lastToken.end);
        token       = m_lastToken;
        repeatToken = false;
    }
    else {
        do {
            token = flyer->flyNext();

            if (
              il->submode == il::Submode::Colorize &&
              token.type == ResultType::Comment) {
                il::CodeFragment cf = code;

                cf.begin = token.begin;
                cf.end   = token.end;

                il->vms->markToken(cf, il::Token::Comment);
            }
        } while (token.type == ResultType::Comment);
    }

    m_lastToken = token;
    auto proxy  = proxies.find(token.type);

    auto result = (**proxy)({il, flyer, token});
    if (result != nullptr) {
        auto & frag = result->fragmentData();
        frag.source = flyer->source();
        frag.begin  = token.begin;
        frag.end    = token.end;
    }
    return result;
}

icString CP::lastKey() {
    icString keyType = keysTypes.value(m_lastToken.type, "operator");
    icString token   = m_lastToken.key;

    int first = token.indexOf('\n');
    int last  = token.lastIndexOf('\n');

    if (first >= 0) {
        token.replace(first, last - first + 1, "..");
    }

    return keyType % ": " % token;
}

icString CP::getFilePath() {
    return flyer->source()->getFilePath().remove(il->vms->getRootDir());
}

icString CP::getFilePathLineChar() {
    return flyer->source()->getFilePath().remove(il->vms->getRootDir()) % ':' %
           icString::number(flyer->source()->getPosition().line) % ':' %
           icString::number(flyer->source()->getPosition().relative) % ':';
}

il::CodeFragment CP::toCodeF(const PackData & pd) {
    il::CodeFragment code;

    code.source = pd.fly->source();
    code.begin  = pd.result.data["begin"];
    code.end    = pd.result.data["end"];

    if (pd.result.data.contains("name")) {
        code.name = pd.result.data["name"];
    }

    return code;
}

void CP::reset(const il::CodeFragment & code) {
    this->code.source = code.source;
    flyer->source().reset(code.source->fragment(code.begin, code.end));
}

void CP::repeat() {
    repeatToken = true;
}

il::CE * CP::pack_local(const PackData & pd) {
    return pd.il->factory->fromValue(
      pd.il,
      pd.il->mem->stackIt().getContainerForVariable(pd.result.data["name"]),
      pd.result.data["name"]);
}

il::CE * CP::pack_global(const PackData & pd) {
    return pd.il->factory->fromValue(
      pd.il, pd.il->mem->stateIt().state(), pd.result.data["name"]);
}

il::CE * CP::pack_function(const PackData & pd) {
    return new ce::Function{pd.il, pd.result.data["name"]};
}

il::CE * CP::pack_property(const PackData & pd) {
    return new ce::Property{pd.il, ce::Prefix::None, pd.result.data["name"]};
}

il::CE * CP::pack_method(const PackData & pd) {
    return new ce::Method{pd.il, pd.result.data["name"]};
}

il::CE * CP::pack_key(const PackData & pd) {
    static KeywordPacker kp;
    return kp.pack_key(pd);
}

il::CE * CP::pack_int_literal(const PackData & pd) {
    return new ce::Int{pd.il, pd.result.key};
}

il::CE * CP::pack_double_literal(const PackData & pd) {
    return new ce::Double{pd.il, pd.result.key};
}

il::CE * CP::pack_string_literal(const PackData & pd) {
    return new ce::String{pd.il, pd.result.data["content"]};
}

il::CE * CP::pack_regex_literal(const PackData & pd) {
    return new ce::RegEx{pd.il, pd.result.data["regex"],
                         pd.result.data["mods"]};
}

il::CE * CP::pack_And(const PackData & pd) {
    return new ce::Conjunction(pd.il);
}

il::CE * CP::pack_Bg(const PackData & pd) {
    return new ce::Bigger(pd.il);
}

il::CE * CP::pack_BgEq(const PackData & pd) {
    return new ce::BiggerEqual(pd.il);
}

il::CE * CP::pack_BgEqSm(const PackData & pd) {
    return new ce::BiggerEqualSmaller(pd.il);
}

il::CE * CP::pack_BgSm(const PackData & pd) {
    return new ce::BiggerSmaller(pd.il);
}

il::CE * CP::pack_Bs(const PackData & pd) {
    return new ce::Remainder(pd.il);
}

il::CE * CP::pack_Cl(const PackData & pd) {
    return new ce::Cast{pd.il};
}

il::CE * CP::pack_ClAs(const PackData & pd) {
    return new ce::ErrorLessCast{pd.il};
}

il::CE * CP::pack_ClCl(const PackData & pd) {
    return new ce::TypeOf{pd.il};
}

il::CE * CP::pack_ClSt(const PackData & pd) {
    return new ce::CanCast{pd.il};
}

il::CE * CP::pack_ClEq(const PackData & pd) {
    return new ce::Overload{pd.il};
}

il::CE * CP::pack_Eq(const PackData & pd) {
    return new ce::Assign{pd.il};
}

il::CE * CP::pack_EqEq(const PackData & pd) {
    return new ce::Equality(pd.il);
}

il::CE * CP::pack_Mn(const PackData & pd) {
    return new ce::Subtraction(pd.il);
}

il::CE * CP::pack_Nt(const PackData & pd) {
    return new ce::Negation(pd.il);
}

il::CE * CP::pack_NtEq(const PackData & pd) {
    return new ce::Inequality(pd.il);
}

il::CE * CP::pack_NtSm(const PackData & pd) {
    return new ce::NotContains(pd.il);
}

il::CE * CP::pack_NtSt(const PackData & pd) {
    return new ce::NotContainsTemplate(pd.il);
}

il::CE * CP::pack_Or(const PackData & pd) {
    return new ce::Disjunction(pd.il);
}

il::CE * CP::pack_Or2(const PackData & pd) {
    return new ce::ExclusiveDisjunction(pd.il);
}

il::CE * CP::pack_Pc(const PackData & pd) {
    return new ce::SelectiveSelect(pd.il);
}

il::CE * CP::pack_Pl(const PackData & pd) {
    return new ce::Addition(pd.il);
}

il::CE * CP::pack_Sm(const PackData & pd) {
    return new ce::Smaller(pd.il);
}

il::CE * CP::pack_SmBg(const PackData & pd) {
    return new ce::SmallerBigger(pd.il);
}

il::CE * CP::pack_SmEq(const PackData & pd) {
    return new ce::SmallerEqual(pd.il);
}

il::CE * CP::pack_SmEqBg(const PackData & pd) {
    return new ce::SmallerEqualBigger(pd.il);
}

il::CE * CP::pack_SmSm(const PackData & pd) {
    return new ce::Contains(pd.il);
}

il::CE * CP::pack_SmSt(const PackData & pd) {
    return new ce::ContainsTemplate(pd.il);
}

il::CE * CP::pack_Sl(const PackData & pd) {
    return new ce::Division(pd.il);
}

il::CE * CP::pack_SlAp(const PackData & pd) {
    return new ce::Root(pd.il);
}

il::CE * CP::pack_St(const PackData & pd) {
    return new ce::Multiplication(pd.il);
}

il::CE * CP::pack_StSt(const PackData & pd) {
    return new ce::Exponentiation(pd.il);
}

il::CE * CP::pack_Tl(const PackData & pd) {
    return new ce::Biconditional(pd.il);
}

il::CE * CP::pack_RunCx(const PackData & pd) {
    return new ce::RunContext(pd.il, toCodeF(pd), pd.result.data["name"]);
}

il::CE * CP::pack_LimitedCx(const PackData & pd) {
    return new ce::LimitedContext(pd.il, toCodeF(pd));
}

il::CE * CP::pack_ValueCx(const PackData & pd) {
    return new ce::ValueContext(pd.il, toCodeF(pd));
}

il::CE * CP::pack_comma(const PackData & pd) {
    return new ce::Comma(pd.il);
}

il::CE * CP::pack_CommandEnd(const PackData & /*pd*/) {
    return nullptr;
}

il::CE * CP::pack_unexpected(const PackData & pd) {
    pd.il->vm->cp_sig("Unexpected token: " + pd.result.key);
    return nullptr;
}

icList<il::CodeFragment> CP::splitCommands(const il::CodeFragment & code) {
    Flyer fly{il, code};

    icList<il::CodeFragment> commands;
    il::CodeFragment         current;

    current.source     = code.source;
    current.begin.line = -1;

    while (true) {
        FlyResult next = fly.flyNext(true);

        if (next.type == ResultType::CommandEnd) {
            if (current.begin.line == -1) {
                break;
            }

            current.end = next.end;
            commands.append(current);
            current.begin.line = -1;
        }
        else if (current.begin.line == -1) {
            current.begin = next.begin;
        }
    }

    return commands;
}

icString CP::lastTokenPosition() {
    return flyer->issueWithPath(m_lastToken.begin)
      .remove(il->vms->getRootDir());
}

}  // namespace icL::core::cp
