#include "icL.h++"

#include "structs.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/auxiliary/source-of-code.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-memory/state/memory.h++>

//#include <iostream>



namespace icL::core::cp {

icL::icL() {
    proxies = {{'`', &icL::flyComment},   {'@', &icL::flyLocal},
               {'#', &icL::flyGlobal},    {'\'', &icL::flyProperty},
               {'.', &icL::flyMethod},    {'$', &icL::flyFunction},
               {'0', &icL::flyNumber},    {'1', &icL::flyNumber},
               {'2', &icL::flyNumber},    {'3', &icL::flyNumber},
               {'4', &icL::flyNumber},    {'5', &icL::flyNumber},
               {'6', &icL::flyNumber},    {'7', &icL::flyNumber},
               {'8', &icL::flyNumber},    {'9', &icL::flyNumber},
               {'"', &icL::flyString},    {'&', &icL::flyAnd},
               {'>', &icL::flyBg},        {'\\', &icL::flyBs},
               {':', &icL::flyCl},        {'=', &icL::flyEq},
               {'-', &icL::flyMn},        {'!', &icL::flyNt},
               {'|', &icL::flyOr},        {'^', &icL::flyOr2},
               {'%', &icL::flyPc},        {'+', &icL::flyPl},
               {'<', &icL::flySm},        {'/', &icL::flySl},
               {'*', &icL::flySt},        {'~', &icL::flyTl},
               {'{', &icL::flyRunOp},     {'[', &icL::flyLimitedOp},
               {'(', &icL::flyValueOp},   {'}', &icL::flyRunCl},
               {']', &icL::flyLimitedCl}, {')', &icL::flyValueCl},
               {',', &icL::flyComma},     {';', &icL::flyCommandEnd}};
}

FlyResult icL::flyNext(bool packContexts, bool skipErrors) {

    source()->seek(source()->getPosition());

    FlyResult result;
    icChar    firstChar = flyToNext(this);
    auto      proxy     = proxies.find(firstChar);

    result.begin = source()->getPosition();
    result.begin.relative -= 1;
    result.begin.absolute -= 1;
    result.begin.byte -= 1;

    if (proxy != proxies.end()) {
        (**proxy)(this, result);
    }
    else if (firstChar.isLetter()) {
        flyKey(this, result);
    }
    else if (firstChar == '\0') {
        flyCommandEnd(this, result);
    }
    else {
        result.type = ResultType::CommandEnd;
        if (!skipErrors) {
            core()->vm->cp_sig(
              issueWithPath(result.begin) % ": Unexpected token: " % firstChar);
        }
    }

    if (packContexts) {
        if (
          result.type == ResultType::RunOp ||
          result.type == ResultType::LimitedOp ||
          result.type == ResultType::ValueOp) {
            finishContext(result);
        }
    }

    result.end = source()->getPosition();

    return result;
}

void icL::finishContext(FlyResult & result) {
    struct Match
    {
        ResultType::ResultType expected;
        icChar                 ch;
        il::Position position;
    };

    icList<Match> matches;
    FlyResult     current = result;

    result.data.insert("begin", source()->getPosition());

    while (true) {
        switch (current.type) {
        case ResultType::RunOp:
            matches.append({ResultType::RunCl, '{', current.begin});
            break;

        case ResultType::LimitedOp:
            matches.append({ResultType::LimitedCl, '[', current.begin});
            break;

        case ResultType::ValueOp:
            matches.append({ResultType::ValueCl, '(', current.begin});
            break;

        case ResultType::RunCl:
        case ResultType::LimitedCl:
        case ResultType::ValueCl:
            if (matches.last().expected == current.type) {
                matches.removeLast();
            }
            else {
                auto & last = matches.last();
                core()->vm->cp_sig(
                  source()->getFilePath() % ": Missmatched brackets: `" %
                  last.ch % "`(" % issue(last.position) % ") & `" %
                  current.key % "`(" % issue(source()->getPosition()) % ')');
            }
            break;

        default: /* do nothing */;
        }

        if (matches.isEmpty() || source()->atEnd()) {
            break;
        }

        //        std::cout << "Key ready: " << current.key.toString() <<
        //        std::endl;
        current = flyNext(false, true);
    }

    if (!matches.isEmpty()) {
        core()->vm->cp_sig(
          issueWithPath(matches.first().position) % ": Bracket pair not found");
    }

    result.data.insert("end", current.begin);
}

icStringList icL::getModifiers(IFlyer * fly) {
    fly->source()->next();

    icStringList modifiers;
    while (fly->source()->current() == '-') {
        icString modifier;

        while (fly->source()->next().isLetterOrDigit()) {
            modifier += fly->source()->current();
        }

        if (modifier.isEmpty()) {
            fly->core()->vm->cp_sig(
              fly->issueWithPath(fly->source()->getPosition()) %
              "Modifier name can not be empty");
            break;
        }

        modifiers.append(modifier);
    }

    fly->source()->prev();

    return modifiers;
}

icStringList icL::getModifiersWithNotSupport(IFlyer * fly) {
    if (fly->source()->next() == '!') {
        return icStringList{"not"};
    }

    fly->source()->prev();
    return getModifiers(fly);
}

icChar icL::flyToNext(IFlyer * fly) {
    icChar next = '\0';

    if (fly->source()->atEnd()) {
        return next;
    }

    do {
        next = fly->source()->next();
    } while (next.isWhite() && !fly->source()->atEnd());

    return next.isWhite() ? icChar{'\0'} : next;
}

icString icL::flyName(IFlyer * fly) {
    icString ret;

    while (fly->source()->next().isLetterOrDigit() ||
           fly->source()->current() == '_') {
        ret += fly->source()->current();
    }

    fly->source()->prev();

    return ret;
}

void icL::flyLiteral(IFlyer * fly, FlyResult & result) {
    int foldLevel = 0;

    fly->source()->next();
    result.data.insert("lit-begin", fly->source()->getPosition());
    fly->source()->prev();

    icString litStr;

    while (!fly->source()->atEnd()) {
        icChar next = fly->source()->next();

        if (next == '[') {
            foldLevel++;
        }
        else if (next == ']') {
            if (foldLevel == 0) {
                break;
            }

            foldLevel--;
        }
        else if (next == '\\') {
            litStr += fly->source()->next();
        }
        else {
            litStr += next;
        }
    }

    if (fly->source()->current() != ']') {
        fly->core()->vm->cp_sig(
          fly->issueWithPath(result.begin) % "Failed to find end of literal");
    }

    result.data.insert("lit-end", fly->source()->getPosition());
    result.data.insert("lit-str", litStr);
}

void icL::flyComment(IFlyer * fly, FlyResult & result) {
    icChar next = fly->source()->next();
    // The position where the comment end
    il::Position end = fly->source()->getPosition();

    result.type = ResultType::Comment;

    if (next == '`') {
        next = fly->source()->next();

        if (next == '`') {
            next = fly->source()->next();

            result.data["begin"] = fly->source()->getPosition();
            result.data["type"]  = 3;

            auto currc = next, prevc = currc;  // previous chars
            auto currp = end, prevp = currp;   // previous positions

            while (next != '\0' &&
                   !(next == '`' && currc == '`' && prevc == '`')) {
                // clang-format off
                prevc = currc; currc = next;  next  = fly->source()->next();
                end   = prevp; prevp = currp; currp = fly->source()->getPosition();
                // clang-format on
            }

            if (next != '`') {
                fly->core()->vm->cp_sig(
                  fly->issueWithPath(result.begin) %
                  "Multiline comment not finished");
            }
        }
        else {
            result.data["begin"] = fly->source()->getPosition();
            result.data["type"]  = 2;

            while (next != '\n' && next != '\0') {
                next = fly->source()->next();
                end  = fly->source()->getPosition();
            }
        }
    }
    else {
        result.data["begin"] = fly->source()->getPosition();
        result.data["type"]  = 1;

        while (next != '\n' && next != '`' && next != '\0') {
            next = fly->source()->next();
            end  = fly->source()->getPosition();
        }

        if (next != '`') {
            fly->core()->vm->cp_sig(
              fly->issueWithPath(result.begin) % "Inline comment not finished");
        }
    }
    result.data["end"] = end;
}

void icL::flyLocal(IFlyer * fly, FlyResult & result) {
    icString name = flyName(fly);
    result.key    = "@" % name;

    if (name.isEmpty()) {
        name = "@";
    }

    result.data.insert("name", name);
    result.type = ResultType::Local;
}

void icL::flyGlobal(IFlyer * fly, FlyResult & result) {
    icString name = flyName(fly);
    result.key    = "#" % name;

    if (name.isEmpty()) {
        name        = "#";
        result.type = ResultType::Local;
    }
    else {
        result.type = ResultType::Global;
    }

    result.data.insert("name", name);
}

void icL::flyFunction(IFlyer * fly, FlyResult & result) {
    icString name = flyName(fly);
    result.key    = "$" % name;
    result.type   = ResultType::Function;

    result.data.insert("name", name);
}

void icL::flyProperty(IFlyer * fly, FlyResult & result) {
    icString name = flyName(fly);

    result.type = ResultType::Property;
    result.key  = '\'' + name;
    // send the name of property as meta data
    result.data.insert("name", name);

    if (name.isEmpty()) {
        fly->core()->vm->cp_sig(
          fly->issueWithPath(result.begin) % "Property has empty name");
    }
}

void icL::flyMethod(IFlyer * fly, FlyResult & result) {
    icString name = flyName(fly);

    result.type = ResultType::Method;
    result.key  = '.' + name;
    // send the name of method as meta data
    result.data["name"] = name;

    if (name.isEmpty()) {
        fly->core()->vm->cp_sig(
          fly->issueWithPath(result.begin) % "Method name is empty");
    }
}

void icL::flyKey(IFlyer * fly, FlyResult & result) {
    fly->source()->prev();

    result.key  = flyName(fly);
    result.type = ResultType::Key;
}

void icL::flyDouble(IFlyer * /*fly*/, FlyResult & result) {
    result.type = ResultType::Double;
}

void icL::flyInt(IFlyer * /*fly*/, FlyResult & result) {
    result.type = ResultType::Int;
}

void icL::flyNumber(IFlyer * fly, FlyResult & result) {
    result.key = fly->source()->current();

    while (fly->source()->next().isDigit()) {
        result.key += fly->source()->current();
    }

    if (fly->source()->current() == '.') {
        if (fly->source()->next().isDigit()) {
            result.key += '.';
            result.key += fly->source()->current();

            while (fly->source()->next().isDigit()) {
                result.key += fly->source()->current();
            }

            fly->source()->prev();
            flyDouble(fly, result);
            return;
        }

        fly->source()->prev();
        fly->source()->prev();
    }

    fly->source()->prev();
    flyInt(fly, result);
}

void icL::flyRegEx(IFlyer * fly, FlyResult & result) {
    icChar   delimiter = fly->source()->current();
    auto     backup    = fly->source()->getPosition();
    icChar   last      = '\0';
    icString regex;

    while (!fly->source()->next().isWhite()) {
        if (last == delimiter && fly->source()->current() == '/') {
            break;
        }

        last = fly->source()->current();
        regex += last;
    }

    if (last == delimiter && fly->source()->current() == '/') {
        result.end  = fly->source()->getPosition();
        result.type = ResultType::RegEx;

        regex.remove(regex.length() - 1);
        result.data.insert("regex", regex);

        icString modifiers;

        while (fly->source()->next().isLetter()) {
            modifiers += fly->source()->current();
        }

        result.data.insert("mods", modifiers);
        fly->source()->prev();
    }
    else {
        fly->source()->seek(backup);
        fly->source()->prev();
    }
}

void icL::flyString(IFlyer * fly, FlyResult & result) {
    icString content;

    while (fly->source()->next() != '"' && !fly->source()->atEnd()) {
        content += fly->source()->current();

        if (fly->source()->current() == '\\') {
            content += fly->source()->next();
        }
        else if (fly->source()->current() == '\n') {
            fly->core()->vm->cp_sig(
              fly->issueWithPath(fly->source()->getPosition()) %
              "Expected \" instead of \\n");
            break;
        }
    }

    if (fly->source()->atEnd() && fly->source()->current() != '"') {
        fly->core()->vm->cp_sig(
          fly->issueWithPath(result.begin) % "End of string not found");
    }

    result.type = ResultType::String;
    result.key  = '"' % content % '"';
    result.data.insert("content", content);
}

void icL::flyAnd(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "&";
    result.type = ResultType::And;
}

void icL::flyBg(IFlyer * fly, FlyResult & result) {
    if (fly->source()->next() == '=') {
        if (fly->source()->next() == '<') {
            flyBgEqSm(fly, result);
        }
        else {
            fly->source()->prev();
            flyBgEq(fly, result);
        }
    }
    else if (fly->source()->current() == '<') {
        flyBgSm(fly, result);
    }
    else {
        result.key  = ">";
        result.type = ResultType::Bg;
        fly->source()->prev();
    }
}

void icL::flyBgEq(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = ">=";
    result.type = ResultType::BgEq;
}

void icL::flyBgEqSm(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = ">=<";
    result.type = ResultType::BgEqSm;
}

void icL::flyBgSm(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "><";
    result.type = ResultType::BgSm;
}

void icL::flyBs(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "\\";
    result.type = ResultType::Bs;
}

void icL::flyCl(IFlyer * fly, FlyResult & result) {
    icChar next = fly->source()->next();

    if (next == '?') {
        flyClAs(fly, result);
    }
    else if (next == ':') {
        flyClCl(fly, result);
    }
    else if (next == '*') {
        flyClSt(fly, result);
    }
    else if (next == '=') {
        flyClEq(fly, result);
    }
    else {
        fly->source()->prev();
        result.key  = ":";
        result.type = ResultType::Cl;
    }
}

void icL::flyClAs(IFlyer * /*fly*/, FlyResult & /*result*/) {}

void icL::flyClCl(IFlyer * /*fly*/, FlyResult & /*result*/) {}

void icL::flyClSt(IFlyer * /*fly*/, FlyResult & /*result*/) {}

void icL::flyClEq(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = ":=";
    result.type = ResultType::ClEq;
}

void icL::flyEq(IFlyer * fly, FlyResult & result) {
    if (fly->source()->next() == '=') {
        flyEqEq(fly, result);
    }
    else {
        result.key  = "=";
        result.type = ResultType::Eq;
        fly->source()->prev();
    }
}

void icL::flyEqEq(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "==";
    result.type = ResultType::EqEq;
}

void icL::flyMn(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "-";
    result.type = ResultType::Mn;
}

void icL::flyNt(IFlyer * fly, FlyResult & result) {
    if (fly->source()->next() == '=') {
        flyNtEq(fly, result);
    }
    else if (fly->source()->current() == '<') {
        flyNtSm(fly, result);
    }
    else if (fly->source()->current() == '*') {
        flyNtSt(fly, result);
    }
    else {
        result.key  = "!";
        result.type = ResultType::Nt;
        fly->source()->prev();
    }
}

void icL::flyNtEq(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "!=";
    result.type = ResultType::NtEq;
}

void icL::flyNtSm(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "!<";
    result.type = ResultType::NtSm;
}

void icL::flyNtSt(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "!*";
    result.type = ResultType::NtSt;
}

void icL::flyOr(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "|";
    result.type = ResultType::Or;
}

void icL::flyOr2(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "^";
    result.type = ResultType::Or2;
}

void icL::flyPc(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "%";
    result.type = ResultType::Pc;
}

void icL::flyPl(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "+";
    result.type = ResultType::Pl;
}

void icL::flySm(IFlyer * fly, FlyResult & result) {
    if (fly->source()->next() == '>') {
        flySmBg(fly, result);
    }
    else if (fly->source()->current() == '=') {
        if (fly->source()->next() == '>') {
            flySmEqBg(fly, result);
        }
        else {
            flySmEq(fly, result);
            fly->source()->prev();
        }
    }
    else if (fly->source()->current() == '<') {
        flySmSm(fly, result);
    }
    else if (fly->source()->current() == '*') {
        flySmSt(fly, result);
    }
    else {
        result.key  = "<";
        result.type = ResultType::Sm;
        fly->source()->prev();
    }
}

void icL::flySmBg(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "<>";
    result.type = ResultType::SmBg;
}

void icL::flySmEq(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "<=";
    result.type = ResultType::SmEq;
}

void icL::flySmEqBg(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "<=>";
    result.type = ResultType::SmEqBg;
}

void icL::flySmSm(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "<<";
    result.type = ResultType::SmSm;
}

void icL::flySmSt(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "<*";
    result.type = ResultType::SmSt;
}

void icL::flySl(IFlyer * fly, FlyResult & result) {
    icChar next = fly->source()->next();

    if (next == '\'') {
        flySlAp(fly, result);
    }
    else if (!next.isWhite() && !"@#$"_str.contains(next)) {
        flyRegEx(fly, result);
    }
    else {
        result.key  = "/";
        result.type = ResultType::Sl;
        fly->source()->prev();
    }
}

void icL::flySlAp(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "/'";
    result.type = ResultType::SlAp;
}

void icL::flySt(IFlyer * fly, FlyResult & result) {
    if (fly->source()->next() == '*') {
        flyStSt(fly, result);
    }
    else {
        result.key  = "*";
        result.type = ResultType::St;
        fly->source()->prev();
    }
}

void icL::flyStSt(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "**";
    result.type = ResultType::StSt;
}

void icL::flyTl(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "~";
    result.type = ResultType::Tl;
}

void icL::flyRunOp(IFlyer * fly, FlyResult & result) {
    result.key  = "{";
    result.type = ResultType::RunOp;

    if (fly->source()->next() == ':') {
        icString name;

        while (fly->source()->next().isLetterOrDigit()) {
            name += fly->source()->current();
        }

        result.key += ':';
        result.key += name;
        result.data.insert("name", name);
    }
    else {
        result.data.insert("name", icString{});
    }

    fly->source()->prev();
}

void icL::flyRunCl(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "}";
    result.type = ResultType::RunCl;
}

void icL::flyLimitedOp(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "[";
    result.type = ResultType::LimitedOp;
}

void icL::flyLimitedCl(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "]";
    result.type = ResultType::LimitedCl;
}

void icL::flyValueOp(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = "(";
    result.type = ResultType::ValueOp;
}

void icL::flyValueCl(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = ")";
    result.type = ResultType::ValueCl;
}

void icL::flyComma(IFlyer * /*fly*/, FlyResult & result) {
    result.key  = ",";
    result.type = ResultType::Comma;
}

void icL::flyCommandEnd(IFlyer * /*fly*/, FlyResult & result) {
    result.type = ResultType::CommandEnd;
}

}  // namespace icL::core::cp
