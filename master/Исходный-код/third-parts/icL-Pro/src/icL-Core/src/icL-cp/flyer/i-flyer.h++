#ifndef core_cp_IFlyer
#define core_cp_IFlyer

#include "../result/structs.h++"

#include <icL-il/auxiliary/source-of-code.h++>

#include <icL-service/main/values/inode.h++>

#include <memory>



namespace icL::core::cp {

struct icL_core_cp_EXPORT IFlyer : virtual public service::INode
{
    /// virtual desctructor of interface
    virtual ~IFlyer() = default;

    /**
     * @brief source get the source of code
     * @return the souce of code
     */
    virtual std::shared_ptr<il::SourceOfCode> & source() = 0;

    /**
     * @brief seek moves the text cursor
     * @param pos is the needed postition
     */
    virtual void seek(const il::Position & pos) = 0;

    /**
     * @brief flyNext flies the next token
     * @return the result of fly
     */
    virtual FlyResult flyNext(
      bool packContexts = true, bool skipErrors = false) = 0;

    /**
     * @brief issue generate a string of format line:relative
     * @param pos is the postion to evaluate
     * @return a string of format line:relative:
     */
    virtual icString issue(const il::Position & pos) const = 0;

    /**
     * @brief issueWithPath a string of format file:line:relative
     * @param pos is the postion to evaluate
     * @return a string of format file:line:relative:
     */
    virtual icString issueWithPath(const il::Position & pos) const = 0;
};

}  // namespace icL::core::cp

#endif  // core_cp_IFlyer
