#ifndef core_cp_Flyer
#define core_cp_Flyer

#include "icL.h++"

#include <icL-il/main/node.h++>
#include <icL-il/structures/code-fragment.h++>



namespace icL::core::cp {

class icL_core_cp_EXPORT Flyer
    : public icL
    , public il::Node
{
    std::shared_ptr<il::SourceOfCode> m_source;

public:
    Flyer(il::InterLevel * il, const il::CodeFragment & code);

    void seek(const il::Position & pos) override;

    /**
     * @brief getSource gets the source of code
     * @return the source of code
     */
    icString getFilePath();

    /**
     * @brief reset resets flayers position and range
     * @param code is the code to execute
     * @note used in keep alive layers
     */
    void reset(const il::CodeFragment & code);

    // IFlyer interface
public:
    std::shared_ptr<il::SourceOfCode> & source() override;
    icString issue(const il::Position & pos) const override;
    icString issueWithPath(const il::Position & pos) const override;

    // INode interface
public:
    il::InterLevel * core() override;
};

}  // namespace icL::core::cp

#endif  // core_cp_Flyer
