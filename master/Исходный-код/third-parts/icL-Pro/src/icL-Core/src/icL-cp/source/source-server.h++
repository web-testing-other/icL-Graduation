#ifndef core_cp_SourceServer
#define core_cp_SourceServer

// must be before
#include <icL-types/replaces/ic-string.h++>

// includes
#include <icL-types/replaces/ic-object.h++>

#include <icL-il/main/node.h++>
#include <icL-il/main/source-server.h++>



namespace icL::core::cp {

class icL_core_cp_EXPORT SourceServer
    : public il::SourceServer
    , public il::Node
{
    icObject<icString, std::shared_ptr<il::SourceOfCode>> sources;

public:
    SourceServer(il::InterLevel * il);

    // SourceServer interface
public:
    std::shared_ptr<il::SourceOfCode> getSource(const icString & path) override;
    void closeSource(const icString & path) override;
};

}  // namespace icL::core::cp

#endif  // core_cp_SourceServer
