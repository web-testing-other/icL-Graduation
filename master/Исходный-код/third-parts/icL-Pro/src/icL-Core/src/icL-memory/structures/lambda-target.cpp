#include "lambda-target.h++"



namespace icL::core::il {

LambdaTarget::LambdaTarget(const icVariant & value) {
    assert(value.is(icType::Lambda));
    *this = to<LambdaTarget>(value);
}

icL::core::il::LambdaTarget::operator icVariant() const {
    return icVariant::fromValueTemplate(*this, icType::Lambda);
}

bool LambdaTarget::operator==(const LambdaTarget & other) const {
    return target == other.target;
}

}  // namespace icL::core::il
