#include "stackcontainer.h++"

#include "memory.h++"

namespace icL::core::memory {

StackContainer::StackContainer(
  il::InterLevel * il, StackContainer * prev, il::VMLayer * vm,
  bool isContainer)
    : DataContainer(il) {
    m_prev = prev;
    m_vm   = vm;

    m_isContainer = isContainer;
}

StackContainer * StackContainer::getPrev() {
    return m_prev;
}

bool StackContainer::isLast() {
    return m_prev == nullptr;
}

icVariant StackContainer::getStackValue() {
    if (contains("@"))
        return getValue("@");

    return m_prev->getStackValue();
}

bool StackContainer::isContainer() const {
    return m_isContainer;
}

void StackContainer::makeFinally() {
    m_isFinally = true;
}

bool StackContainer::isFinally() const {
    return m_isFinally;
}

bool StackContainer::isNamed(const icString & name) {
    return m_name == name;
}

void StackContainer::setName(const icString & name) {
    m_name = name;
}

icString StackContainer::getName() const {
    return m_name;
}

void StackContainer::addDescription(const icString & description) {
    m_vm->addDescription(description);
}

void StackContainer::break_() {
    m_vm->break_();
}

void StackContainer::clear() {
    dataMap.clear();
}

void StackContainer::continue_() {
    m_vm->continue_();
}

void StackContainer::destroy() {
    // TODO: write this function
}

void StackContainer::ignore() {
    // TODO: write this function
}

void StackContainer::listen() {
    // TODO: write this function
}

void StackContainer::markStep(const icString & name) {
    m_vm->markStep(name);
}

void StackContainer::markTest(const icString & name) {
    m_vm->markTest(name);
}

void StackContainer::return_(const icVariant & value) {
    m_vm->return_(value);
}

ContainerType StackContainer::getContainerType() const {
    return ContainerType::Stack;
}

StackIt::StackIt(il::InterLevel * il)
    : il::Node(il) {}

StackIt::~StackIt() {
    clear();
}

StackContainer * StackIt::stack() {
    return m_stack;
}

void StackIt::openNewStack(il::VMLayer * vm, bool isContainer) {
    m_stack = new StackContainer(il, m_stack, vm, isContainer);
}

void StackIt::closeStack() {
    StackContainer * prev = m_stack->getPrev();

    delete m_stack;
    m_stack = prev;
}

StackContainer * StackIt::getContainerForVariable(const icString & name) {
    StackContainer * ret = nullptr;
    StackContainer * it  = m_stack;

    while (ret == nullptr && it != nullptr) {
        if (it->isContainer() && it->contains(name)) {
            ret = it;
        }
        else if (it->isFinally()) {
            break;
        }
        else {
            it = it->getPrev();
        }
    }

    return ret == nullptr ? m_stack : ret;
}

void StackIt::clear() {
    StackContainer * tmp;

    while (m_stack != nullptr) {
        tmp = m_stack->getPrev();
        delete m_stack;
        m_stack = tmp;
    }
}

}  // namespace icL::core::memory
