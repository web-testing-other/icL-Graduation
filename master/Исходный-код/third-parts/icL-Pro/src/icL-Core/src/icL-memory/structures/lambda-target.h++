#ifndef core_il_LambdaTarget
#define core_il_LambdaTarget

#include <icL-memory/structures/function.h++>

#include <memory>



namespace icL::core::il {

/**
 * @brief The LambdaTarget struct contains a pointer to a lambda function
 */
struct icL_core_il_EXPORT LambdaTarget
{
    std::shared_ptr<memory::Function> target;  ///< Function target

    LambdaTarget() = default;
    LambdaTarget(const icVariant & value);

    /**
     * @brief operator icVariant casts the lambda value to icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == check if 2 lambdas has the same target
     * @param other is the lambda to compare with
     * @return true if they contain the same target, otherwise false
     */
    bool operator==(const LambdaTarget & other) const;
};

struct icL_core_il_EXPORT CodeFragmentTarget
{
    std::shared_ptr<il::CodeFragment> target;  ///< code fragment target

    CodeFragmentTarget() = default;
};

}  // namespace icL::core::il

#endif  // core_il_LambdaTarget
