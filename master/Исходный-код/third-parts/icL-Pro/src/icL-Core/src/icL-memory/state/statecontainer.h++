#ifndef core_memory_StateContainer
#define core_memory_StateContainer

#include "datacontainer.h++"

#include <icL-il/export/global.h++>



namespace icL::core::memory {

/**
 * @brief The StateContainer class is a gloabal memory data unit
 */
class icL_core_memory_EXPORT StateContainer : public DataContainer
{
public:
    StateContainer(il::InterLevel * il);

    /**
     * @brief hasPrev checks if there is a previous container
     * @return true if this container has a previous, otherwise false
     */
    bool hasPrev() const;

    /**
     * @brief hasNext checks if there is a next container
     * @return true if this container has a next, otherwise false
     */
    bool hasNext() const;

    /**
     * @brief getPrev gets a pointer to previous container
     * @return a pointer to previous container
     */
    StateContainer * getPrev() const;

    /**
     * @brief getNext gets a pointer to next container
     * @return a pointer to next container
     */
    StateContainer * getNext() const;

    /**
     * @brief setPrev sets the pointer to prev cotainer
     * @param state is the new prev container
     */
    void setPrev(StateContainer * state);

    /**
     * @brief setNext sets the pointer to next container
     * @param state is the new next container
     */
    void setNext(StateContainer * state);

    /**
     * @brief getOrderNumber get the order nuber of state
     * @return the order number of state in linked list
     */
    int getOrderNumber() const;

    // DataContainer interface
public:
    ContainerType getContainerType() const override;

private:
    /// \brief m_prev is a pointer to previous container
    StateContainer * m_prev = nullptr;

    /// \brief m_next is a pointer to next container
    StateContainer * m_next = nullptr;
};

/**
 * @brief The StateContainerIterator class - iterator releasing the data
 structore
 */
class StateIt : il::Node
{
public:
    /**
     * @brief StateIt cosntructs a new state iterator
     */
    StateIt(il::InterLevel * il);
    ~StateIt();

    /**
     * @brief state gets the current state container
     * @return the current state container
     */
    StateContainer * state() const;

    /// \brief `States.delete () : void`
    void delete_();

    /// \brief `States.toFirst () : void`
    void iterateToFirst();

    /// \brief `States.toLast () : void`
    void iterateToLast();

    /// \brief `States.toPrev () : bool`
    bool iterateToPrev();

    /// \brief `States.toNext () : bool`
    bool iterateToNext();

    /// \brief `States.new (data = [=]) : void`
    void appendNewAfter(DataContainer * dc);

    /// \brief `States.newAtEnd (data = [=] : void)`
    void appendNewAtEnd(DataContainer * dc);

    /// \brief `States.clear () : void`
    void clear();

private:
    /// \brief m_state is a pointer to the current state
    StateContainer * m_state = nullptr;
};

}  // namespace icL::core::memory

#endif  // core_memory_StateContainer
