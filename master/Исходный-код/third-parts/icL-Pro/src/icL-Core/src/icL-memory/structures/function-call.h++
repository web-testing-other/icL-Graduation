#ifndef core_memory_FunctionCall
#define core_memory_FunctionCall

#include "arg-value.h++"

#include <icL-il/structures/code-fragment.h++>



namespace icL::core::memory {

namespace ContextType {
enum ContextType {  ///< types of context
    Limited,        ///< limited use context `[]`
    Run,            ///< full runnable context `{}`
    Value           ///< value generating context `()`
};
}

/**
 * @brief The FunctionCall struct is a call to a function
 */
struct icL_core_memory_EXPORT FunctionCall
{
    /// \brief args is the list of local variables to create in stack container
    ArgValueList args;

    /// \brief code is the code to run on
    il::CodeFragment code;

    /// \brief isolateLocalVariables is true on fuction call, false on code
    /// fragment call (round or curly brackets)
    bool isolateLocalVariables = false;

    /// \brief isolateGlobalVariables is true on external file execute,
    /// otherwise is false
    bool isolateGlobalVariables = false;

    /// \brief createLayer is true if a new layer must be created
    bool createLayer = true;

    /// \brief contextType is the type of context which will be generated
    int contextType = ContextType::Run;

    /// \brief returnType is the return type of function
    int returnType = icType::Initial;

    /// \brief contextName is the name of a run context `{:name code }`
    icString contextName;
};

}  // namespace icL::core::memory

#endif  // core_memory_FunctionCall
