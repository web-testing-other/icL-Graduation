#include "statecontainer.h++"

namespace icL::core::memory {

StateContainer::StateContainer(il::InterLevel * il)
    : DataContainer(il) {}

bool StateContainer::hasPrev() const {
    return m_prev != nullptr;
}

bool StateContainer::hasNext() const {
    return m_next != nullptr;
}

StateContainer * StateContainer::getPrev() const {
    return m_prev;
}

StateContainer * StateContainer::getNext() const {
    return m_next;
}

void StateContainer::setPrev(StateContainer * state) {
    m_prev = state;
}

void StateContainer::setNext(StateContainer * state) {
    m_next = state;
}

int StateContainer::getOrderNumber() const {
    int  order = 0;
    auto it    = this;

    while (it->m_prev != nullptr) {
        it = it->m_prev;
        order++;
    }

    return order;
}

ContainerType StateContainer::getContainerType() const {
    return ContainerType::State;
}

StateIt::StateIt(il::InterLevel * il)
    : il::Node(il) {
    m_state = new StateContainer(il);
}

StateIt::~StateIt() {
    clear();
}

StateContainer * StateIt::state() const {
    return m_state;
}

void StateIt::delete_() {
    if (m_state->getNext() == nullptr && m_state->getPrev() == nullptr) {
        delete m_state;
        m_state = nullptr;
    }
    else {
        StateContainer * prev = m_state->getPrev();
        StateContainer * next = m_state->getNext();

        if (prev != nullptr) {
            prev->setNext(next);
        }
        if (next != nullptr) {
            next->setPrev(prev);
        }

        delete m_state;
        m_state = next != nullptr ? next : prev;
    }
}


void StateIt::iterateToFirst() {
    if (m_state != nullptr) {
        while (m_state->getPrev() != nullptr) {
            m_state = m_state->getPrev();
        }
    }
}

void StateIt::iterateToLast() {
    if (m_state != nullptr) {
        while (m_state->getNext() != nullptr) {
            m_state = m_state->getNext();
        }
    }
}

bool StateIt::iterateToPrev() {
    if (m_state->hasPrev()) {
        m_state = m_state->getPrev();
        return true;
    }

    return false;
}

bool StateIt::iterateToNext() {
    if (m_state->hasNext()) {
        m_state = m_state->getNext();
        return true;
    }

    return false;
}

void StateIt::appendNewAfter(DataContainer * dc) {
    if (m_state == nullptr) {
        m_state = new StateContainer(il);
    }
    else {
        auto *           new_state  = new StateContainer(il);
        StateContainer * next_state = m_state->getNext();

        if (next_state != nullptr) {
            next_state->setPrev(new_state);
        }
        new_state->setNext(next_state);
        new_state->setPrev(m_state);
        m_state->setNext(new_state);

        m_state = new_state;
    }

    if (dc != nullptr) {
        dc->cloneDataTo(m_state);
    }
}

void StateIt::appendNewAtEnd(DataContainer * dc) {
    iterateToLast();
    appendNewAfter(dc);
}

void StateIt::clear() {
    iterateToFirst();

    StateContainer * tmp;

    while (m_state != nullptr) {
        tmp = m_state->getNext();
        delete m_state;
        m_state = tmp;
    }
}

}  // namespace icL::core::memory
