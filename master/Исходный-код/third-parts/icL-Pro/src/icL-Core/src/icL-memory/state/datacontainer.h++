#ifndef core_memory_DataContainer
#define core_memory_DataContainer

#include "../structures/type.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/global.h++>
#include <icL-il/main/node.h++>



class icString;

using icVariantMap = icObject<icString, icVariant>;

namespace icL::core::memory {

namespace ContainerTypeNM {
enum ContainerType { Default, Stack, State, Last };
}

using ContainerTypeNM::ContainerType;

/**
 * @brief The DataContainer class is an interface for stack state and memory
 * state
 */
class icL_core_memory_EXPORT DataContainer : public il::Node
{
public:
    DataContainer(il::InterLevel * il);
    virtual ~DataContainer() = default;

    // Functions to add data to containers

    /**
     * @brief setValue set the variable value
     * @param name is the name of varaible
     * @param value is the new value for variable
     */
    void setValue(const icString & name, const icVariant & value);

    // Functions to access data and metadata from containers

    /**
     * @brief contains check if so variable exists in this container
     * @param name is the name of needed variable
     * @return true if so variable exists, otherwise false
     */
    bool contains(const icString & name);

    /**
     * @brief getType return the type of variable
     * @param name is the name of variable
     * @return the type of variable
     */
    Type getType(const icString & name) const;

    /**
     * @brief checkType checks the type of variable
     * @param name is the name of variable
     * @param type is the type to match
     * @return true if type is matched, otherwise false
     */
    bool checkType(const icString & name, const int & type);

    /**
     * @brief getValue gets the value of variable
     * @param name is the name of variable
     * @return the value of variable, or icVariant() if invalid
     */
    icVariant getValue(const icString & name) const;

    /**
     * @brief countVariables counts variables in this container
     * @return the number of variables in container
     */
    int countVariables();

    /**
     * @brief cloneDataTo copy all information of this container to dc
     * @param dc is the container which will get all fields
     */
    void cloneDataTo(DataContainer * dc);

    /**
     * @brief getMap gets the map of container data
     * @return the map of container data
     */
    const icVariantMap & getMap();

    /**
     * @brief getType gets the type of container
     * @return Stack or State
     */
    virtual ContainerType getContainerType() const;

    /**
     * @brief operator == check if containers contain the same values
     * @param other is the container to compare with
     * @return true if they contains the same values, otherwise false
     */
    bool operator==(const DataContainer & other) const;

protected:
    /// Data container
    icVariantMap dataMap;
};

}  // namespace icL::core::memory

#endif  // core_memory_DataContainer
