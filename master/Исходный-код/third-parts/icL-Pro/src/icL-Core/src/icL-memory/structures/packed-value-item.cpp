#include "packed-value-item.h++"

namespace icL::core::memory {

PackedValue::PackedValue(const icVariant & var) {
    assert(var.is(icType::Packed));
    *this = to<PackedValue>(var);
}

icL::core::memory::PackedValue::operator icVariant() {
    return icVariant::fromValueTemplate(*this, icType::Packed);
}

bool PackedValue::operator==(const PackedValue & other) const {
    return data == other.data;
}

}  // namespace icL::core::memory
