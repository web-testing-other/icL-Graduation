#ifndef core_memory_FunctionContainer
#define core_memory_FunctionContainer

#include "function.h++"

#include <icL-memory/structures/argument.h++>



namespace icL::core::memory {

/**
 * @brief The FunctionContainer class is container of all functions in current
 * context
 */
class icL_core_memory_EXPORT FunctionContainer
{
public:
    FunctionContainer() = default;

    /**
     * @brief getFunction get function by name
     * @param name is the name of funtion
     * @return a reference to needed function
     */
    Function getFunction(const icString & name, ArgList args);

    /**
     * @brief contains checks if a function exists
     * @param name is the name of needed function
     * @return true if so function exists, otherwise false
     */
    bool contains(const icString & name);

    /**
     * @brief registerFunction add a new function to context
     * @param name is the name of function
     * @param func is the function header and body
     */
    bool registerFunction(const icString & name, const Function & func);

    /**
     * @brief getMap get the map of functions
     * @return a const reference of function map
     */
    const FunctionMap & getMap();

private:
    /// \brief fmap is a dictionary of `name` - `function` pairs
    FunctionMap fmap;
};

}  // namespace icL::core::memory

#endif  // core_memory_FunctionContainer
