#include "memory.h++"

namespace icL::core::memory {

Memory::Memory(il::InterLevel * il)
    : il::Node(il)
    , m_stateIt(il)
    , m_stackIt(il) {}

StateIt & Memory::stateIt() {
    return m_stateIt;
}

StackIt & Memory::stackIt() {
    return m_stackIt;
}

FunctionContainer & Memory::functions() {
    return m_functions;
}

}  // namespace icL::core::memory
