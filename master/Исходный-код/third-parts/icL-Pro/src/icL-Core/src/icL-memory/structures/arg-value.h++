#ifndef core_memory_ArgValue
#define core_memory_ArgValue

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/global.h++>


namespace icL::core::memory {

/**
 * @brief The ArgValue struct is a value of a argument, with a name of a field,
 * which will create a local variable in function stack container
 */
struct icL_core_memory_EXPORT ArgValue
{
    /// \brief name is the name of argument
    icString name;

    /// \brief value is the value of argument
    icVariant value;
};

/**
 * ArgValueList is a list of arguments values
 */
using ArgValueList = icList<ArgValue>;

}  // namespace icL::core::memory

#endif  // core_memory_ArgValue
