#ifndef core_memory_Parameter
#define core_memory_Parameter

#include "type.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/global.h++>



namespace icL::core::memory {


/**
 * @brief The Parameter struct decribes a function parameter
 * `func = (@param1 : string, @param2 = 23) { .. }`
 */
struct icL_core_memory_EXPORT Parameter
{
    /// \brief name is the name of parameter
    icString name;

    /// \brief type is the type of parameter
    short type;

    /// \brief value is the value of paramter
    /// is invalid if parameter has no a default value
    icVariant value;

    Parameter() = default;

    /**
     * @brief Parameter constructs a parameter with type, for ex. (@param1 :
     * string)
     * @param name is the name without @, for ex. param1
     * @param type is the type, for ex. string -> memory.Type.String
     */
    Parameter(icString name, short type);

    /**
     * @brief Parameter constructs a parameter with default value, fo ex.
     * (@param2 = 23)
     * @param name is the name without @, for. ex. param2
     * @param value is the value, for ex. 23
     */
    Parameter(icString name, icVariant value);

    /**
     * @brief operator == check if two parameters has the same signature
     * @param other is the parameter to compare with
     * @return true if the signature is the same
     */
    bool operator==(const Parameter & other) const;
};

/**
 * ParamList is a list of parameters
 */
using ParamList = icList<Parameter>;


}  // namespace icL::core::memory

#endif  // core_memory_Parameter
