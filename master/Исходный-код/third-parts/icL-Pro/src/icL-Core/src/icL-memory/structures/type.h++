#ifndef core_memory_Type
#define core_memory_Type

#include <icL-types/replaces/ic-string.h++>

class icVariant;

namespace icL::core::memory {

namespace TypeNM {
enum Type : short {
    // base types
    AnyValue = 0,
    BoolValue,
    DatetimeValue,
    DoubleValue,
    IntValue,
    ListValue,
    ObjectValue,
    RegexValue,
    SetValue,
    StringValue,
    VoidValue,

    // additional values
    PackedValue,
    LambdaValue,

    // system types
    Datetime,
    Import,
    Log,
    Math,
    Signal,
    Stack,
    Stacks,
    State,
    Types,

    // ArgumentList additional
    Identifier,
    TypeId,

    // for extensions
    Last,
    Array = 0x7000
};
}

using TypeNM::Type;

}  // namespace icL::core::memory

#endif  // core_memory_Type
