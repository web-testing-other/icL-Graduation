#ifndef core_memory_Memory
#define core_memory_Memory

#include "../structures/functioncontainer.h++"
#include "stackcontainer.h++"
#include "statecontainer.h++"



namespace icL::core::memory {

class icL_core_memory_EXPORT Memory : public il::Node
{
public:
    /**
     * @brief Memory initialize memory with default signals
     */
    Memory(il::InterLevel * il);

    /**
     * @brief stateIt gets the state iterator
     * @return the state iterator
     */
    StateIt & stateIt();

    /**
     * @brief stackIt gets the stack iterator
     * @return the stack iterator
     */
    StackIt & stackIt();

    /**
     * @brief functions gets the function continer
     * @return the function container
     */
    FunctionContainer & functions();

private:
    /// \brief m_stateIt the state iterator
    StateIt m_stateIt;

    /// \brief m_stackIt the stack iterator
    StackIt m_stackIt;

    /// \brief m_functions the function container
    FunctionContainer m_functions;
};


}  // namespace icL::core::memory

#endif  // core_memory_Memory
