#include "set.h++"

namespace icL::core::memory {

Column::Column(Type type, const icString & name)
    : name(name)
    , type(type) {}

bool Column::operator==(const Column & other) const {
    return name == other.name;
}

Set::Set(const icVariant & var) {
    assert(var.is(icType::Set));
    *this = to<Set>(var);
}

bool Set::operator==(const Set & other) const {
    return setData == other.setData;
}

icL::core::memory::Set::operator icVariant() const {
    return icVariant::fromValueTemplate(*this, icType::Set);
}

Object::Object(il::InterLevel * il)
    : il::Node(il) {}

Object::Object(const icVariant & var)
    : il::Node(to<Object>(var).il) {
    assert(var.is(icType::Object));
    *this = to<Object>(var);
}

Object::Object(il::InterLevel * il, const icVariantObject & hash)
    : il::Node(il) {
    data = std::make_shared<DataContainer>(il);

    for (auto it = hash.begin(); it != hash.end(); it++) {
        data->setValue(it.key(), it.value());
    }
}

bool Object::operator==(const Object & other) const {
    return data == other.data;
}

icL::core::memory::Object::operator icVariant() const {
    return icVariant::fromValueTemplate(*this, icType::Object);
}

}  // namespace icL::core::memory
