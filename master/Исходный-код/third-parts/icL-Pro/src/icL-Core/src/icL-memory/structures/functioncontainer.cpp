#include "functioncontainer.h++"

#include "set.h++"
#include "type.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-memory/structures/argument.h++>

#include <utility>

namespace icL::core::memory {


Function FunctionContainer::getFunction(
  const icString & name, memory::ArgList args) {
    auto it = fmap.find(name);

    Function ret;

    while (it != fmap.end() && it.key() == name) {
        // normal call check
        bool ok = it.value().paramList.length() == args.length();

        if (ok) {
            auto it1 = it.value().paramList.begin();
            auto it2 = args.begin();

            for (; it2 != args.end(); it1++, it2++) {
                ok = ok &&
                     (it1->type == it2->type ||
                      (it2->type == Type::VoidValue && it1->value.isValid()));
            }

            if (ok) {
                ret = it.value();
                break;
            }
        }

        // acronim call check
        if (args.length() == 1) {
            // number of parameters which have not a default value
            int notDefaultCount = 0;
            // the last parameter without a default value
            memory::Parameter notDefaultParam;

            for (auto & param : it.value().paramList) {
                if (param.value.isNull()) {
                    notDefaultCount++;
                    notDefaultParam = param;
                }
            }

            // We have one non default param on any position of needed type
            if (notDefaultCount == 1 && notDefaultParam.type == args[0].type) {
                if (ret.body.source == nullptr) {
                    ret = it.value();
                }
            }
        }

        it++;
    }


    return ret;
}

bool FunctionContainer::contains(const icString & name) {
    return fmap.contains(name);
}

bool FunctionContainer::registerFunction(
  const icString & name, const Function & func) {
    auto it     = fmap.find(name);
    bool exists = false;

    while (it != fmap.end() && it.key() == name) {
        bool ok = it.value().paramList.length() == func.paramList.length();

        if (ok) {
            auto it1 = it.value().paramList.begin();
            auto it2 = func.paramList.begin();

            while (it2 != func.paramList.end()) {
                ok = ok && it1->type == it2->type;
                it1++;
                it2++;
            }

            if (ok) {
                exists = true;
            }
        }
        it++;
    }

    if (!exists) {
        fmap.insert(name, func);
    }

    return !exists;
}

const FunctionMap & FunctionContainer::getMap() {
    return fmap;
}

}  // namespace icL::core::memory
