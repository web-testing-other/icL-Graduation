#ifndef core_memory_Function
#define core_memory_Function

#include "parameter.h++"

#include <icL-types/replaces/ic-multi.h++>

#include <icL-il/structures/code-fragment.h++>



namespace icL::core::memory {

/**
 * @brief The Function struct represent a function definition which can be
 * assigned to an identifier
 * for ex. `(@param1 : string, @param2 = 23) : int { .. }`
 */
struct icL_core_memory_EXPORT Function
{
    /// \brief paramList is the list of parameters `(@param1 : string, @param2 =
    /// 23)`
    ParamList paramList;

    /// \brief returnType is the return type, for ex. `: int`
    Type returnType;

    /// \brief body is the body of funtion, for ex. `{ .. }`
    il::CodeFragment body;
};

/**
 * FunctionMap is a simple map of function, pairs `name` - `function`
 */
using FunctionMap = icMulti<icString, Function>;

}  // namespace icL::core::memory

#endif  // core_memory_Function
