#ifndef core_memory_Argument
#define core_memory_Argument

#include "type.h++"

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/global.h++>

#include <memory>



class icDateTime;
class icRegEx;
class icStringList;
template <typename>
class icList;

namespace icL::core {

namespace il {
struct Element;
struct File;
struct Cookie;
struct Tab;
struct Session;
struct Window;
struct DB;
struct Query;
struct LambdaTarget;
}  // namespace il

namespace memory {
class DataContainer;
}

namespace memory {

struct Set;
struct Object;

/**
 * @brief The Argument struct represents a argument of function call
 */
struct icL_core_memory_EXPORT Argument
{
    /// \brief type is the type of argument value
    enum TypeNM::Type type = Type::VoidValue;

    /// \brief value is the value itself, will be invalid if argument is a type,
    /// not a value
    icVariant value;

    /// \brief varName is the name of value in container
    icString varName;

    /// \brief container is the container of value
    memory::DataContainer * container = nullptr;

    /// \brief operator Type casts the argument value to Type
    /// \note It must be a type value
    operator Type() const;

    template <typename T>
    operator T() const {
        return value;
    }
};

/**
 * ArgList is a list of arguments used for calls of functions and methods
 */
using ArgList = icList<Argument>;


}  // namespace memory
}  // namespace icL::core

#endif  // core_memory_Argument
