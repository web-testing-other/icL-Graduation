#include "datacontainer.h++"

#include "../structures/set.h++"
#include "type.h++"

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>



namespace icL::core::memory {

DataContainer::DataContainer(il::InterLevel * il)
    : Node(il) {}

void DataContainer::setValue(const icString & name, const icVariant & value) {
    dataMap.insert(name, value);
}

bool DataContainer::contains(const icString & name) {
    return dataMap.contains(name);
}

Type DataContainer::getType(const icString & name) const {
    return il->factory->variantToType(dataMap[name]);
}

bool DataContainer::checkType(const icString & name, const int & type) {
    return dataMap.value(name, icVariant::makeVoid())
      .is(il->factory->typeToVarType(static_cast<Type>(type)));
}

icVariant DataContainer::getValue(const icString & name) const {
    return dataMap.value(name, icVariant::makeVoid());
}

int DataContainer::countVariables() {
    return dataMap.count();
}

void DataContainer::cloneDataTo(DataContainer * dc) {
    for (auto it = dataMap.begin(); it != dataMap.end(); it++) {
        dc->dataMap.insert(it.key(), it.value());
    }
}

const icVariantMap & DataContainer::getMap() {
    return dataMap;
}

ContainerType DataContainer::getContainerType() const {
    return ContainerType::Default;
}

bool DataContainer::operator==(const DataContainer & other) const {
    return dataMap == other.dataMap;
}

}  // namespace icL::core::memory
