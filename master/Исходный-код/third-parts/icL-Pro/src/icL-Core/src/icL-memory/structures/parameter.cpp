#include "parameter.h++"

#include <utility>

icL::core::memory::Parameter::Parameter(icString name, short type)
    : name(std::move(name))
    , type(type) {}

icL::core::memory::Parameter::Parameter(icString name, icVariant value)
    : name(std::move(name))
    , value(std::move(value)) {}

bool icL::core::memory::Parameter::operator==(
  const icL::core::memory::Parameter & other) const {
    return name == other.name;
}
