#ifndef core_ce_Filter
#define core_ce_Filter

#include <icL-service/keyword/loop/filter.h++>

#include <icL-ce/base/keyword/loop-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Filter
    : public LoopKeyword
    , public service::Filter
{
public:
    Filter(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

    // INode interface
protected:
    il::InterLevel * core() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Filter
