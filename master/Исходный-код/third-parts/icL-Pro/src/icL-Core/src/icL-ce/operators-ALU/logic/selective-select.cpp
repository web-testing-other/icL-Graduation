#include "selective-select.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/value-base/base/double-value.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/complex/list-value.h++>
#include <icL-ce/value-base/complex/set-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

SelectiveSelect::SelectiveSelect(il::InterLevel * il)
    : LogicOperator(il) {}

icString SelectiveSelect::toString() {
    return "%";
}

void SelectiveSelect::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (left[0].type == Type::VoidValue) {
        m_newContext = il->factory->fromValue(il, right[0].value);
    }
    if (right[0].type == Type::VoidValue) {
        m_newContext = il->factory->fromValue(il, left[0].value);
    }
    else {
        switch (left[0].type) {
        case Type::IntValue:
            if (right[0].type == Type::IntValue) {
                m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
            }
            break;

        case Type::DoubleValue:
            if (right[0].type == Type::DoubleValue) {
                m_newContext =
                  il->factory->fromValue(il, doubleDouble(left[0], right[0]));
            }
            break;

        case Type::StringValue:
            if (right[0].type == Type::StringValue) {
                m_newContext =
                  il->factory->fromValue(il, stringString(left[0], right[0]));
            }
            else if (right[0].type == Type::ListValue) {
                m_newContext = il->factory->fromValue(il, stringList(left[0], right[0]));
            }
            break;

        case Type::ListValue:
            if (right[0].type == Type::StringValue) {
                m_newContext = il->factory->fromValue(il, listString(left[0], right[0]));
            }
            else if (right[0].type == Type::ListValue) {
                m_newContext = il->factory->fromValue(il, listList(left[0], right[0]));
            }
            break;

        case Type::SetValue:
            if (right[0].type == Type::SetValue) {
                m_newContext = il->factory->fromValue(il, setSet(left[0], right[0]));
            }
            else if (right[0].type == Type::ObjectValue) {
                m_newContext = il->factory->fromValue(il, setObject(left[0], right[0]));
            }
            break;

        case Type::ObjectValue:
            if (right[0].type == Type::SetValue) {
                m_newContext = il->factory->fromValue(il, objectSet(left[0], right[0]));
            }
            else if (right[0].type == Type::ObjectValue) {
                m_newContext =
                  il->factory->fromValue(il, objectObject(left[0], right[0]));
            }
            break;

        default:
            void();  // Elude warning
        }
    }

    if (m_newContext == nullptr) {
        il->vm->signal({il::Signals::System, "No such operator"});
    }
}

}  // namespace icL::core::ce
