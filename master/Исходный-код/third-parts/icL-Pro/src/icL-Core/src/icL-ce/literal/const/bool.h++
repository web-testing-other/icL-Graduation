#ifndef core_ce_Bool
#define core_ce_Bool

#include <icL-ce/base/literal/const-literal.h++>



namespace icL::core::ce {

/**
 * @brief The Bool class represent a bool literal `true` or `false`
 */
class icL_core_ce_literal_EXPORT Bool : public ConstLiteral
{
public:
    Bool(il::InterLevel * il, const icString & pattern);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;

    // il.CE interface
public:
    void colorize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Bool
