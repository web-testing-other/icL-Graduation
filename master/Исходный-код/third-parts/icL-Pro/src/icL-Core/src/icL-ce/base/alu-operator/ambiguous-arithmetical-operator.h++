#ifndef core_ce_AmbiguousArithmeticalOperator
#define core_ce_AmbiguousArithmeticalOperator

#include "arithmetical-operator.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT AmbiguousArithmeticalOperator
    : public ArithmeticalOperator
{
public:
    AmbiguousArithmeticalOperator(il::InterLevel * il);

protected:
    /**
     * @brief runAbmiguousRank gets the ambiguous run rank
     * @return the ambiguous run rank
     * @note normal run rank is for call of type `operand operator operand`
     * @note ambiguous run rank is for call of type `operator operand`
     */
    virtual int runAbmiguousRank() = 0;

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    il::CE * firstToReplace() override;

protected:
    const icSet<int> & acceptedPrevs() override;

    // fields
protected:
    bool asAmbiguous = false;

    // padding
    long : 56;
};

}  // namespace icL::core::ce

#endif  // core_ce_AmbiguousArithmeticalOperator
