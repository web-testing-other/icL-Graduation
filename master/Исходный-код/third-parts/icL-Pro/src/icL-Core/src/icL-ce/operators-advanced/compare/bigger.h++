#ifndef core_ce_Bigger
#define core_ce_Bigger

#include <icL-service/operators-advanced/compare/bigger.h++>

#include <icL-ce/base/advanced-operator/compare-operator.h++>



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT Bigger
    : public CompareOperator
    , public service::Bigger
{
public:
    Bigger(il::InterLevel * il);

    /// `int > int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double > double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Bigger
