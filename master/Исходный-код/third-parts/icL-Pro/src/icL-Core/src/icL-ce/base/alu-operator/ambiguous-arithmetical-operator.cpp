#include "ambiguous-arithmetical-operator.h++"

#include <icL-types/replaces/ic-set.h++>

namespace icL::core::ce {

AmbiguousArithmeticalOperator::AmbiguousArithmeticalOperator(
  il::InterLevel * il)
    : ArithmeticalOperator(il) {}

int AmbiguousArithmeticalOperator::currentRunRank(bool rtl) {
    bool leftIsValue  = m_prev != nullptr && m_prev->role() == Role::Value;
    bool rightIsValue = m_next->role() == Role::Value;

    asAmbiguous = rightIsValue && !leftIsValue;

    return asAmbiguous && !rtl
             ? runAbmiguousRank()
             : rightIsValue && leftIsValue && rtl ? runRank() : -1;
}

il::CE * AmbiguousArithmeticalOperator::firstToReplace() {
    return asAmbiguous ? this : m_prev;
}

const icSet<int> & AmbiguousArithmeticalOperator::acceptedPrevs() {
    static const icSet<int> roles{ArithmeticalOperator::acceptedPrevs(),
                                   {Role::NoRole, Role::Function, Role::Method,
                                    Role::Assign, Role::Comma, Role::Operator}};
    return roles;
}

}  // namespace icL::core::ce
