#include "functional-js-literal.h++"

#include <icL-types/replaces/ic-set.h++>



namespace icL::core::ce {

FunctionalJsLiteral::FunctionalJsLiteral(il::InterLevel * il)
    : Literal(il) {}

const icSet<int> & FunctionalJsLiteral::acceptedPrevs() {
    static icSet<int> roles = {Role::NoRole, Role::Method, Role::Function,
                                Role::Assign, Role::Comma,  Role::Operator,
                                Role::JsRun};
    return roles;
}

const icSet<int> & FunctionalJsLiteral::acceptedNexts() {
    static icSet<int> roles = {Role::Method, Role::Property};
    return roles;
}

}  // namespace icL::core::ce
