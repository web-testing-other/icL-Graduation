#ifndef core_ce_FunctionalJsLiteral
#define core_ce_FunctionalJsLiteral

#include "../main/literal.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT FunctionalJsLiteral : public Literal
{
public:
    FunctionalJsLiteral(il::InterLevel * il);

    // CE interface
protected:
    const icSet<int> & acceptedPrevs();
    const icSet<int> & acceptedNexts();
};

}  // namespace icL::core::ce

#endif  // core_ce_FunctionalJsLiteral
