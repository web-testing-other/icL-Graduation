#include "string.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

namespace icL::core::ce {

String::String(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icString String::toString() {
    return '"' % pattern % '"';
}

void String::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::String);
}

icVariant String::getValueOf() {
    icString ret = pattern;

    ret.replace(R"(\t)", "\t");
    ret.replace(R"(\n)", "\n");
    ret.replace(R"(\b)", "\b");
    ret.replace(R"(\")", R"(")");
    ret.replace(R"(\\)", R"(\)");

    return ret;
}

}  // namespace icL::core::ce
