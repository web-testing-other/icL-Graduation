#ifndef core_ce_TypeOf
#define core_ce_TypeOf

#include "cast-operator.h++"



namespace icL::core {

namespace memory {
struct Argument;
}

namespace ce {

class icL_core_ce_operators_advanced_EXPORT TypeOf : public CastOperator
{
public:
    TypeOf(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // CastOperator interface
public:
    void runCast(const memory::Argument & left, Type right) override;
    void runCast(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_TypeOf
