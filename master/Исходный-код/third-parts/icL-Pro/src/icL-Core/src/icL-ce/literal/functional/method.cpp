#include "method.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-service/main/args/listify.h++>

#include <icL-ce/base/main/value.h++>
#include <icL-ce/base/value/base-value.h++>

#include <icL-memory/structures/argument.h++>

#include <utility>



namespace icL::core::ce {

Method::Method(il::InterLevel * il, const icString & name)
    : FunctionalLiteral(il)
    , name(name) {}

icString Method::toString() {
    return '.' % name;
}

int Method::currentRunRank(bool rtl) {
    bool runnable =
      (m_prev->role() == Role::Value || m_prev->role() == Role::SystemValue) &&
      m_next->role() == Role::Value;

    return runnable && !rtl ? 7 : -1;
}

StepType Method::runNow() {
    auto value = dynamic_cast<Value *>(m_prev);
    auto args  = dynamic_cast<il::PackableValue *>(m_next);

    if (service::Listify::isArgList(args)) {
        value->runMethod(name, il->factory->listify(il, args));
    }
    else if (service::Listify::isParamList(args)) {
        value->runMeta(name, service::Listify::toParamList(il, args->value()));
    }
    else {
        il->vm->syssig(
          "Invalid arguments list: it must contains values & types for a "
          "method call or parameters for a meta-method call");
    }

    m_newContext = m_prev->newCE();

    return StepType::CommandEnd;
}

il::CE * Method::firstToReplace() {
    return m_prev;
}

il::CE * Method::lastToReplace() {
    return m_next;
}

int Method::role() {
    return Role::Method;
}

void Method::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Method);
}

const icSet<int> & Method::acceptedPrevs() {
    static const icSet<int> roles{Role::Value, Role::SystemValue,
                                   Role::LimitedContext, Role::ValueContext,
                                   Role::Property, Role::JsFile};
    return roles;
}

const icSet<int> & Method::acceptedNexts() {
    static const icSet<int> roles{Role::Value, Role::SystemValue,
                                   Role::ValueContext, Role::LimitedContext};
    return roles;
}

}  // namespace icL::core::ce
