#include "disjunction.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

Disjunction::Disjunction(il::InterLevel * il)
    : LogicOperator(il) {}

bool Disjunction::run(bool left, bool right) {
    return left || right;
}

icString Disjunction::toString() {
    return "|";
}

void Disjunction::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (left[0].type == Type::BoolValue && right[0].type == Type::BoolValue) {
        m_newContext = il->factory->fromValue(il, run(bool(left[0]), right[0]));
    }
    else if (
      left[0].type == Type::VoidValue && right[0].type == Type::VoidValue) {
        m_newContext = il->factory->fromValue(il, {});
    }
    else if (left[0].type == Type::VoidValue) {
        m_newContext = il->factory->fromValue(il, right[0].value);
    }
    else if (right[0].type == left[0].type) {
        m_newContext = il->factory->fromValue(il, left[0].value);
    }
    else {
        il->vm->signal({il::Signals::System, "No such operator"});
    }
}

}  // namespace icL::core::ce
