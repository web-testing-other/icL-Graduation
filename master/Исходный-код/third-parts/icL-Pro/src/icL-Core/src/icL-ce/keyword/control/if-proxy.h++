#ifndef core_ce_IfProxy
#define core_ce_IfProxy

#include "if-base.h++"



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT IfProxy : public IfBase
{
    bool notModifier = false;

public:
    IfProxy(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

protected:
    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

    // IfBase interface
protected:
    bool hasNotModifier() override;
    void setNotModifier() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_IfProxy
