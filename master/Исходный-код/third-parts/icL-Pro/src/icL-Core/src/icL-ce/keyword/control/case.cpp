#include "case.h++"

#include <icL-types/replaces/ic-set.h++>

#include <cassert>



namespace icL::core::ce {

Case::Case(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Case::toString() {
    return "case";
}

int Case::currentRunRank(bool /*rtl*/) {
    return -1;
}

StepType Case::runNow() {
    assert(false);  // to be never called
    return StepType::None;
}

int Case::role() {
    return Role::Case;
}

const icSet<int> & Case::acceptedPrevs() {
    static const icSet<int> roles{Role::ValueContext, Role::Value,
                                   Role::RunContext};
    return roles;
}

const icSet<int> & Case::acceptedNexts() {
    static const icSet<int> roles{Role::ValueContext, Role::Value};
    return roles;
}

}  // namespace icL::core::ce
