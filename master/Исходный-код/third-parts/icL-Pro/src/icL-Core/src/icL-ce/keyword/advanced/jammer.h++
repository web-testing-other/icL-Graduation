#ifndef core_ce_Jammer
#define core_ce_Jammer

#include <icL-service/keyword/advanced/jammer.h++>

#include <icL-ce/base/keyword/advanced-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Jammer
    : public AdvancedKeyword
    , public service::Jammer
{
public:
    Jammer(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int      role() override;
    il::CE * lastToReplace() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Jammer
