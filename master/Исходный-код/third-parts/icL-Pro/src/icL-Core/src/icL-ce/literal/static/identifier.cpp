#include "identifier.h++"

namespace icL::core::ce {

Identifier::Identifier(il::InterLevel * il, const icString & name)
    : StaticLiteral(il)
    , name(name) {}

const icString & Identifier::getName() const {
    return name;
}

icString Identifier::toString() {
    return name;
}

int Identifier::role() {
    return Role::Identifier;
}

bool Identifier::hasValue() {
    return false;
}

const icSet<int> & Identifier::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole, Role::Comma};
    return roles;
}

const icSet<int> & Identifier::acceptedNexts() {
    static const icSet<int> roles{Role::Assign, Role::Cast};
    return roles;
}

il::PackableType Identifier::packableType() const {
    assert(false);
    return il::PackableType::Type;
}

}  // namespace icL::core::ce
