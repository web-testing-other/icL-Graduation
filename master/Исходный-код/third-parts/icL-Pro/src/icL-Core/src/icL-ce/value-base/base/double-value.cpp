#include "double-value.h++"

namespace icL::core::ce {

DoubleValue::DoubleValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DoubleValue::DoubleValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DoubleValue::DoubleValue(BaseValue * value)
    : BaseValue(value) {}

Type DoubleValue::type() const {
    return Type::DoubleValue;
}

icString DoubleValue::typeName() {
    return "double";
}

}  // namespace icL::core::ce
