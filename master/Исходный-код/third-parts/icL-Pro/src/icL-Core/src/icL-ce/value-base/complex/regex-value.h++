#ifndef core_ce_RegexValue
#define core_ce_RegexValue

#include <icL-ce/base/value/base-value.h++>



namespace icL::core::ce {

/**
 * @brief The RegexValue class represents a `regex` value
 */
class icL_core_ce_value_base_EXPORT RegexValue : public BaseValue
{
public:
    /// @brief RegexValue calls BaseValue(il, container, varName, readonly)
    RegexValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief RegexValue calls BaseValue(il, rvalue)
    RegexValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief RegexValue calls BaseValue(value)
    RegexValue(BaseValue * value);


    // Value interface
public:
    Type type() const override;
    icString typeName() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_RegexValue
