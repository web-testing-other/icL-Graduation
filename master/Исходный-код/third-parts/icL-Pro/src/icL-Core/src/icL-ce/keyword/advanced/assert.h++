#ifndef core_ce_Assert
#define core_ce_Assert

#include <icL-service/keyword/advanced/assert.h++>

#include <icL-ce/base/keyword/advanced-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Assert
    : public AdvancedKeyword
    , public service::Assert
{
public:
    Assert(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    il::CE * lastToReplace() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Assert
