#ifndef core_ce_Double
#define core_ce_Double

#include <icL-ce/base/literal/const-literal.h++>



namespace icL::core::ce {

/**
 * @brief The Double class represent a double literal `\-?\d+\.\d+`
 */
class icL_core_ce_literal_EXPORT Double : public ConstLiteral
{
public:
    Double(il::InterLevel * il, const icString & pattern);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;

    // il.CE interface
public:
    void colorize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Double
