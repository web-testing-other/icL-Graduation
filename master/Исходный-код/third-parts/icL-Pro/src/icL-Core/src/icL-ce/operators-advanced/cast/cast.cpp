#include "cast.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-service/cast/base/bool-cast.h++>
#include <icL-service/cast/base/datetime-cast.h++>
#include <icL-service/cast/base/double-cast.h++>
#include <icL-service/cast/base/int-cast.h++>
#include <icL-service/cast/base/list-cast.h++>
#include <icL-service/cast/base/object-cast.h++>
#include <icL-service/cast/base/set-cast.h++>
#include <icL-service/cast/base/string-cast.h++>
#include <icL-service/cast/base/void-cast.h++>

#include <icL-ce/literal/static/column.h++>
#include <icL-ce/literal/static/parameter.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/packed-value-item.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

Cast::Cast(il::InterLevel * il)
    : CastOperator(il) {}

void Cast::runUnhandled(const memory::Argument & /*left*/) {}

using namespace icL::core::service;

void Cast::runVoidBool(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, VoidCast::toBool());
}

void Cast::runVoidInt(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, VoidCast::toInt());
}

void Cast::runVoidDouble(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, VoidCast::toDouble());
}

void Cast::runVoidString(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, VoidCast::toString());
}

void Cast::runVoidList(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, VoidCast::toList());
}

void Cast::runVoidObject(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, VoidCast::toObject(il));
}

void Cast::runVoidSet(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, VoidCast::toSet());
}

void Cast::runBoolInt(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, BoolCast::toInt(left));
}

void Cast::runBoolDouble(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, BoolCast::toDouble(left));
}

void Cast::runBoolString(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, BoolCast::toString(left));
}

void Cast::runIntBool(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, IntCast::toBool(left));
}

void Cast::runIntDouble(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, IntCast::toDouble(left));
}

void Cast::runIntString(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, IntCast::toString(left));
}

void Cast::runDoubleBool(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, DoubleCast::toBool(left));
}

void Cast::runDoubleInt(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, DoubleCast::toInt(left));
}

void Cast::runDoubleString(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, DoubleCast::toString(left));
}

void Cast::runStringBool(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, StringCast::toBool(left));
}

void Cast::runStringInt(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, StringCast::toInt(il, left));
}

void Cast::runStringDouble(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, StringCast::toDouble(il, left));
}

void Cast::runStringList(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, StringCast::toList(left));
}

void Cast::runStringObject(const memory::Argument & left) {
    m_newContext =
      il->factory->fromValue(il, StringCast::toObject(il, icString(left)));
}

void Cast::runStringSet(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, StringCast::toSet(il, left));
}

void Cast::runListBool(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, ListCast::toBool(left));
}

void Cast::runListString(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, ListCast::toString(il, left));
}

void Cast::runListSet(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, ListCast::toSet(il, left));
}

void Cast::runObjectBool(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, ObjectCast::toBool(left));
}

void Cast::runObjectString(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, ObjectCast::toString(left));
}

void Cast::runSetBool(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, SetCast::toBool(left));
}

void Cast::runSetString(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, SetCast::toString(il, left));
}

void Cast::runSetList(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, SetCast::toList(il, left));
}

void Cast::runSetObject(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, SetCast::toObject(il, left));
}

void Cast::runStringDatetime(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, DatetimeCast::toDatetime(left));
}

void Cast::runDatetimeString(const memory::Argument & left) {
    m_newContext = il->factory->fromValue(il, DatetimeCast::toString(left));
}

void Cast::runStringPack(
  const memory::Argument & left, const memory::Argument & right) {
    m_newContext =
      il->factory->fromValue(il, DatetimeCast::toDatetime(left, right));
}

void Cast::runDatetimePack(
  const memory::Argument & left, const memory::Argument & right) {
    m_newContext =
      il->factory->fromValue(il, DatetimeCast::toString(left, right));
}

void Cast::runIdentifierType(const memory::Argument & left, Type right) {
    m_newContext = new Column(il, left.varName, right);
}

il::CE * Cast::declareCast(const memory::Argument & left, Type right) {
    icVariant value = VoidCast::fromType(il, right);

    left.container->setValue(left.varName, value);
    return il->factory->fromValue(il, left.container, left.varName);
}

int Cast::role() {
    return Role::Cast;
}

icString Cast::toString() {
    return ":";
}

const icSet<int> & Cast::acceptedPrevs() {
    static const icList<icSet<int>> roles{{Role::Identifier, Role::NoRole},
                                           {Role::Value}};
    return byContext(CastOperator::acceptedPrevs(), roles[0], roles[1]);
}

const icSet<int> & Cast::acceptedNexts() {
    static const icList<icSet<int>> roles{{Role::Type, Role::NoRole},
                                           {Role::Type}};
    return byContext(CastOperator::acceptedNexts(), roles[0], roles[1]);
}

il::CE * Cast::firstToReplace() {
    return m_prev != nullptr ? m_prev : this;
}

il::CE * Cast::lastToReplace() {
    return m_next != nullptr ? m_next : this;
}

void Cast::runCast(const memory::Argument & left, Type right) {
    static icObject<
      icPair<short, short>, void (Cast::*)(const memory::Argument &)>
      operators{
        {{Type::VoidValue, Type::BoolValue}, &Cast::runVoidBool},
        {{Type::VoidValue, Type::IntValue}, &Cast::runVoidInt},
        {{Type::VoidValue, Type::DoubleValue}, &Cast::runVoidDouble},
        {{Type::VoidValue, Type::StringValue}, &Cast::runVoidString},
        {{Type::VoidValue, Type::ListValue}, &Cast::runVoidList},
        {{Type::VoidValue, Type::ObjectValue}, &Cast::runVoidObject},
        {{Type::VoidValue, Type::SetValue}, &Cast::runVoidSet},
        {{Type::BoolValue, Type::IntValue}, &Cast::runBoolInt},
        {{Type::BoolValue, Type::DoubleValue}, &Cast::runBoolDouble},
        {{Type::BoolValue, Type::StringValue}, &Cast::runBoolString},
        {{Type::IntValue, Type::BoolValue}, &Cast::runIntBool},
        {{Type::IntValue, Type::DoubleValue}, &Cast::runIntDouble},
        {{Type::IntValue, Type::StringValue}, &Cast::runIntString},
        {{Type::DoubleValue, Type::BoolValue}, &Cast::runDoubleBool},
        {{Type::DoubleValue, Type::IntValue}, &Cast::runDoubleInt},
        {{Type::DoubleValue, Type::StringValue}, &Cast::runDoubleString},
        {{Type::StringValue, Type::BoolValue}, &Cast::runStringBool},
        {{Type::StringValue, Type::IntValue}, &Cast::runStringInt},
        {{Type::StringValue, Type::DoubleValue}, &Cast::runStringDouble},
        {{Type::StringValue, Type::ListValue}, &Cast::runStringList},
        {{Type::StringValue, Type::ObjectValue}, &Cast::runStringObject},
        {{Type::StringValue, Type::SetValue}, &Cast::runStringSet},
        {{Type::ListValue, Type::BoolValue}, &Cast::runListBool},
        {{Type::ListValue, Type::StringValue}, &Cast::runListString},
        {{Type::ListValue, Type::SetValue}, &Cast::runListSet},
        {{Type::ObjectValue, Type::BoolValue}, &Cast::runObjectBool},
        {{Type::ObjectValue, Type::StringValue}, &Cast::runObjectString},
        {{Type::SetValue, Type::BoolValue}, &Cast::runSetBool},
        {{Type::SetValue, Type::StringValue}, &Cast::runSetString},
        {{Type::SetValue, Type::ListValue}, &Cast::runSetList},
        {{Type::SetValue, Type::ObjectValue}, &Cast::runSetObject},
        {{Type::StringValue, Type::DatetimeValue}, &Cast::runStringDatetime},
        {{Type::DatetimeValue, Type::StringValue}, &Cast::runDatetimeString}};

    if (il->vm->get(0).toInt() == memory::ContextType::Value) {
        if (left.container != nullptr) {
            m_newContext = new Parameter{il, left.varName, right};
            return;
        }
    }

    if (left.type == right) {
        m_newContext = il->factory->fromValue(il, left.value);
        return;
    }

    auto it = operators.find({left.type, right});

    if (left.type == Type::VoidValue && left.container != nullptr) {
        m_newContext = declareCast(left, right);
    }
    else if (it == operators.end()) {
        if (left.type == Type::Identifier) {
            runIdentifierType(left, right);
        }
        else {
            runUnhandled(left);
        }
    }
    else {
        (this->*it.value())(left);
    }
}

void Cast::runCast(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (
      checkArgs(left, {Type::StringValue}) &&
      checkArgs(right, {Type::TypeId, Type::StringValue})) {
        if (Type::DatetimeValue == right[0].type) {
            runStringPack(left[0], right[1]);
        }
    }
    else if (
      checkArgs(left, {Type::DatetimeValue}) &&
      checkArgs(right, {Type::TypeId, Type::StringValue})) {
        if (Type::StringValue == right[0].type) {
            runDatetimePack(left[0], right[1]);
        }
    }
}

}  // namespace icL::core::ce
