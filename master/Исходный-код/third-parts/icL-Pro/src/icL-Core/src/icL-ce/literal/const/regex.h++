#ifndef core_ce_RegEx
#define core_ce_RegEx

#include <icL-ce/base/literal/const-literal.h++>



namespace icL::core::ce {

/**
 * @brief The regex class represents a regex token `//pattern//mods`
 */
class icL_core_ce_literal_EXPORT RegEx : public ConstLiteral
{
public:
    RegEx(il::InterLevel * il, const icString & pattern, const icString & mods);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;

    // il.CE interface
public:
    void colorize() override;

    // fields
private:
    icString mods;
};

}  // namespace icL::core::ce



#endif  // core_ce_RegEx
