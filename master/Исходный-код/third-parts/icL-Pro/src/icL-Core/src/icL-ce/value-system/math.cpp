#include "math.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-service/main/args/listify-args-to-list.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/double-value.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

Math::Math(il::InterLevel * il)
    : SystemValue(il) {}

void Math::run1divPi() {
    m_newContext = il->factory->fromValue(il, _1divPi());
}

void Math::run1divSqrt2() {
    m_newContext = il->factory->fromValue(il, _1divSqrt2());
}

void Math::run2divPi() {
    m_newContext = il->factory->fromValue(il, _2divPi());
}

void Math::run2divSqrtPi() {
    m_newContext = il->factory->fromValue(il, _2divSqrtPi());
}

void Math::runE() {
    m_newContext = il->factory->fromValue(il, e());
}

void Math::runLn2() {
    m_newContext = il->factory->fromValue(il, ln2());
}

void Math::runLn10() {
    m_newContext = il->factory->fromValue(il, ln10());
}

void Math::runLog2e() {
    m_newContext = il->factory->fromValue(il, log2e());
}

void Math::runLog10e() {
    m_newContext = il->factory->fromValue(il, log10e());
}

void Math::runPi() {
    m_newContext = il->factory->fromValue(il, pi());
}

void Math::runPiDiv2() {
    m_newContext = il->factory->fromValue(il, piDiv2());
}

void Math::runPiDiv4() {
    m_newContext = il->factory->fromValue(il, piDiv4());
}

void Math::runSqrt2() {
    m_newContext = il->factory->fromValue(il, sqrt2());
}

void Math::runAcos(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, acos(args[0]));
    }
}

void Math::runAsin(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, asin(args[0]));
    }
}

void Math::runAtan(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, atan(args[0]));
    }
}

void Math::runCeil(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, ceil(args[0]));
    }
}

void Math::runCos(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, cos(args[0]));
    }
}

void Math::runDegreesToRadians(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, radiansToDegrees(args[0]));
    }
}

void Math::runExp(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, exp(args[0]));
    }
}

void Math::runFloor(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, floor(args[0]));
    }
}

void Math::runLn(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, ln(args[0]));
    }
}

void Math::runMax(const memory::ArgList & args) {
    if (checkArgs(Type::IntValue, args)) {
        m_newContext =
          il->factory->fromValue(il, max(service::Listify::fromArgs<int>(args)));
    }
    if (checkArgs(Type::DoubleValue, args)) {
        m_newContext =
          il->factory->fromValue(il, max(service::Listify::fromArgs<double>(args)));
    }
}

void Math::runMin(const memory::ArgList & args) {
    if (checkArgs(Type::IntValue, args)) {
        m_newContext =
          il->factory->fromValue(il, min(service::Listify::fromArgs<int>(args)));
    }
    if (checkArgs(Type::DoubleValue, args)) {
        m_newContext =
          il->factory->fromValue(il, min(service::Listify::fromArgs<double>(args)));
    }
}

void Math::runRadiansToDegrees(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, radiansToDegrees(args[0]));
    }
}

void Math::runRound(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, round(args[0]));
    }
}

void Math::runSin(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, sin(args[0]));
    }
}

void Math::runTan(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DoubleValue})) {
        m_newContext = il->factory->fromValue(il, tan(args[0]));
    }
}

Type Math::type() const {
    return Type::Math;
}

icString Math::typeName() {
    return "Math";
}

void Math::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Math::*)()> properties{
      {"1divPi", &Math::run1divPi}, {"1divSqrt2", &Math::run1divSqrt2},
      {"2divPi", &Math::run1divPi}, {"2divSqrtPi", &Math::run2divSqrtPi},
      {"e", &Math::runE},           {"ln2", &Math::runLn2},
      {"ln10", &Math::runLn10},     {"log2e", &Math::runLog2e},
      {"log10e", &Math::runLog10e}, {"pi", &Math::runPi},
      {"piDiv2", &Math::runPiDiv2}, {"piDiv4", &Math::runPiDiv4},
      {"sqrt2", &Math::runSqrt2}};

    runPropertyWithPrefixCheck<Math, SystemValue>(properties, prefix, name);
}

void Math::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Math::*)(const memory::ArgList &)> methods{
      {"acos", &Math::runAcos},
      {"asin", &Math::runAsin},
      {"atan", &Math::runAtan},
      {"ceil", &Math::runCeil},
      {"cos", &Math::runCos},
      {"degreesToRadians", &Math::runDegreesToRadians},
      {"exp", &Math::runExp},
      {"floor", &Math::runFloor},
      {"ln", &Math::runLn},
      {"max", &Math::runMax},
      {"min", &Math::runMin},
      {"radiansToDegrees", &Math::runRadiansToDegrees},
      {"round", &Math::runRound},
      {"sin", &Math::runSin},
      {"tan", &Math::runTan}};

    runMethodNow<Math, SystemValue>(methods, name, args);
}

}  // namespace icL::core::ce
