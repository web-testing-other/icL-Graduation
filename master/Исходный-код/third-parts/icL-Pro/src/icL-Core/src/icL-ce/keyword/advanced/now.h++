#ifndef core_ce_Now
#define core_ce_Now

#include <icL-ce/base/keyword/advanced-keyword.h++>



namespace icL::core::ce {

/**
 * @brief The Now class respresent the `now` keyword
 */
class icL_core_ce_keyword_EXPORT Now : public AdvancedKeyword
{
public:
    Now(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    icString toString() override;

    // CE interface
public:
    int      role() override;
    il::CE * lastToReplace() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Now
