#include "else.h++"

#include <icL-types/replaces/ic-set.h++>

#include <cassert>



namespace icL::core::ce {

Else::Else(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Else::toString() {
    return "else";
}

int Else::currentRunRank(bool /*rtl*/) {
    return -1;
}

StepType Else::runNow() {
    assert(false);  // to be never called
    return StepType::None;
}

int Else::role() {
    return Role::Else;
}

const icSet<int> & Else::acceptedPrevs() {
    static const icSet<int> roles = {Role::RunContext};
    return roles;
}

const icSet<int> & Else::acceptedNexts() {
    static const icSet<int> roles = {Role::If, Role::RunContext};
    return roles;
}

}  // namespace icL::core::ce
