#include "bool.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/signals.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>

namespace icL::core::ce {

Bool::Bool(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icVariant Bool::getValueOf() {
    bool ret;

    if (pattern == "true") {
        ret = true;
    }
    else if (pattern == "false") {
        ret = false;
    }
    else {
        ret = false;  // elude warning
        il->vm->signal({il::Signals::System, "Wrong bool literal"});
    }

    return ret;
}

void Bool::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Constant);
}

}  // namespace icL::core::ce
