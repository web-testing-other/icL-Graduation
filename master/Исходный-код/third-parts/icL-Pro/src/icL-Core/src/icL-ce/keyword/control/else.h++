#ifndef core_ce_Else
#define core_ce_Else

#include <icL-ce/base/keyword/control-keyword.h++>



namespace icL::core::ce {

/**
 * @brief The Else class represents an `else` token
 */
class icL_core_ce_keyword_EXPORT Else : public ControlKeyword
{
public:
    Else(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Else
