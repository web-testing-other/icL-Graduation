#include "bigger-equal-smaller.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

BiggerEqualSmaller::BiggerEqualSmaller(il::InterLevel * il)
    : CompareOperator(il) {}

void BiggerEqualSmaller::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext =
      il->factory->fromValue(il, intIntInt(left[0], right[0], right[1]));
}

void BiggerEqualSmaller::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(
      il, doubleDoubleDouble(left[0], right[0], right[1]));
}

icString BiggerEqualSmaller::toString() {
    return ">=<";
}

void BiggerEqualSmaller::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (BiggerEqualSmaller::*)(
        const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue, Type::IntValue}},
                 &BiggerEqualSmaller::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue, Type::DoubleValue}},
                 &BiggerEqualSmaller::runDoubleDouble}};

    runNow<BiggerEqualSmaller>(operators, left, right);
}

}  // namespace icL::core::ce
