#include "state.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

State::State(il::InterLevel * il)
    : SystemValue(il) {}

void State::runClear(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void State::runDelete(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        delete_();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void State::runNew(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        new_(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
    else if (checkArgs(args, {})) {
        new_({il});
        m_newContext = il->factory->fromValue(il, {});
    }
}

void State::runNewAtEnd(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        newAtEnd(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
    else if (checkArgs(args, {})) {
        newAtEnd({il});
        m_newContext = il->factory->fromValue(il, {});
    }
}

void State::runToFirst(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toFirst();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void State::runToLast(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toLast();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void State::runToNext(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toNext();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void State::runToPrev(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toPrev();
        m_newContext = il->factory->fromValue(il, {});
    }
}

const icSet<int> & State::acceptedNexts() {
    static icSet<int> roles{SystemValue::acceptedNexts(), {Role::NoRole}};
    return roles;
}

Type State::type() const {
    return Type::State;
}

icString State::typeName() {
    return "State";
}

void State::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (State::*)(const memory::ArgList &)> methods{
      {"clear", &State::runClear},     {"delete", &State::runDelete},
      {"new", &State::runNew},         {"newAtEnd", &State::runNewAtEnd},
      {"toFirst", &State::runToFirst}, {"toLast", &State::runToLast},
      {"toNext", &State::runToNext},   {"toPrev", &State::runToPrev}};

    runMethodNow<State, SystemValue>(methods, name, args);
}

}  // namespace icL::core::ce
