#include "datetime.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>



namespace icL::core::ce {

DateTime::DateTime(il::InterLevel * il)
    : SystemValue(il) {}

void DateTime::runCurrent(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, current());
    }
}

void DateTime::runCurrentUTC(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, currentUTC());
    }
}

Type DateTime::type() const {
    return Type::Datetime;
}

icString DateTime::typeName() {
    return "DateTime";
}

void DateTime::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (DateTime::*)(const memory::ArgList &)>
      methods{{"current", &DateTime::runCurrent},
              {"currentUTC", &DateTime::runCurrentUTC}};

    runMethodNow<DateTime, SystemValue>(methods, name, args);
}

}  // namespace icL::core::ce
