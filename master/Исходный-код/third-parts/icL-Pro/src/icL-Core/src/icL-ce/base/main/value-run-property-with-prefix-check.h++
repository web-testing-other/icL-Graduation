#ifndef core_ce_Value_runPropertyWithPrefixCheck
#define core_ce_Value_runPropertyWithPrefixCheck

#include "value.h++"

namespace icL::core::ce {

template <typename This, typename ParentClass>
void Value::runPropertyWithPrefixCheck(
  const icObject<icString, void (This::*)()> & properties, Prefix prefix,
  const icString & name) {
    auto this_ = dynamic_cast<This *>(this);

    if (prefix == Prefix::None) {
        auto it = properties.find(name);

        if (it != properties.end()) {
            (this_->*it.value())();
        }
        else {
            this_->ParentClass::runProperty(prefix, name);
        }
    }
    else {
        this_->ParentClass::runProperty(prefix, name);
    }
}

}  // namespace icL::core::ce

#endif  // core_ce_Value_runPropertyWithPrefixCheck
