#ifndef core_ce_Context
#define core_ce_Context

#include <icL-il/structures/code-fragment.h++>

#include <icL-ce/base/alu-operator/system-operator.h++>



namespace icL::core::ce {

/**
 * @brief The Context class represent any bracket-isolated code `[] () {}`
 */
class icL_core_ce_operators_ALU_EXPORT Context : public SystemOperator
{
public:
    Context(il::InterLevel * il, const il::CodeFragment & code);

    /**
     * @brief getCode gets the code fragment of context
     * @return the code fragment of context
     */
    const il::CodeFragment & getCode();

    // CE interface
public:
    int currentRunRank(bool rtl) override;

    // Operator interface
public:
    void run(const memory::ArgList &, const memory::ArgList &) override;

    // fields
protected:
    /// \brief code is the fragment of code isolated in brackets
    il::CodeFragment code;
    /// \brief executed is true if the code was executed, otherwise false
    bool executed = false;

    // padding
    long : 56;

    // CE interface
public:
    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

    void colorize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Context
