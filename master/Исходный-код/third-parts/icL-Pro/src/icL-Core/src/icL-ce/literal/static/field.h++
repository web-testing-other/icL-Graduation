#ifndef core_ce_Field
#define core_ce_Field

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-ce/base/literal/static-literal.h++>



namespace icL::core::ce {

/**
 * @brief The Field class represent a field token - `name = value`
 */
class icL_core_ce_literal_EXPORT Field : public StaticLiteral
{
public:
    /**
     * @brief Field constructs a inited field
     * @param il is the InterLevel node
     * @param name is the name of field
     * @param type is the type of field
     */
    Field(il::InterLevel * il, const icString & name, const icVariant & value);

    /**
     * @brief getName gets the name of field
     * @return the name of the field
     */
    const icString & getName();

    /**
     * @brief getValue gets the value of field
     * @return the value of the field
     */
    const icVariant & getFieldValue();

    // CE interface
public:
    icString toString() override;
    int      role() override;

    // PackableValue interface
    memory::PackedValueItem packNow() const override;
    il::PackableType        packableType() const override;

private:
    /// \brief name is the name of the field
    icString name;
    /// \brief value is the value of field
    icVariant value;
};

}  // namespace icL::core::ce

#endif  // core_ce_Field
