#include "including-operator.h++"

namespace icL::core::ce {

IncludingOperator::IncludingOperator(il::InterLevel * il)
    : AdvancedOperator(il) {}

int IncludingOperator::currentRunRank(bool rtl) {
    bool runnable =
      m_prev->role() == Role::Value && m_next->role() == Role::Value;
    return runnable && !rtl ? 3 : -1;
}

}  // namespace icL::core::ce
