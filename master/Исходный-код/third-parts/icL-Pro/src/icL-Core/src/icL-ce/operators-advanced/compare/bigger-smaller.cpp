#include "bigger-smaller.h++"

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

BiggerSmaller::BiggerSmaller(il::InterLevel * il)
    : CompareOperator(il) {}

void BiggerSmaller::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext =
      il->factory->fromValue(il, intIntInt(left[0], right[0], right[1]));
}

void BiggerSmaller::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(
      il, doubleDoubleDouble(left[0], right[0], right[1]));
}

icString BiggerSmaller::toString() {
    return "><";
}

void BiggerSmaller::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (BiggerSmaller::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue, Type::IntValue}},
                 &BiggerSmaller::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue, Type::DoubleValue}},
                 &BiggerSmaller::runDoubleDouble}};

    runNow<BiggerSmaller>(operators, left, right);
}

}  // namespace icL::core::ce
