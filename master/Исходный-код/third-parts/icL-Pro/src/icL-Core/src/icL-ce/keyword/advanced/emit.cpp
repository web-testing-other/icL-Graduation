#include "emit.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/stateful-server.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/base/value/base-value.h++>

namespace icL::core::ce {

Emit::Emit(il::InterLevel * il)
    : AdvancedKeyword(il) {}

icString Emit::toString() {
    return "emit-" % icString::number(errorCode);
}

int Emit::currentRunRank(bool rtl) {
    return m_next->role() == Role::Value && rtl ? 1 : -1;
}

StepType Emit::runNow() {
    auto valuePtr = dynamic_cast<il::ReadableValue *>(m_next);
    if (valuePtr->type() == Type::StringValue) {
        if (il->mode != il::Mode::Check) {
            il->vm->signal({errorCode, valuePtr->value().toString()});
        }
    }
    else {
        il->vm->signal(
          {il::Signals::System, "emit: message must be a string"});
    }
    return StepType::CommandEnd;
}

int Emit::role() {
    return Role::Emit;
}

const icSet<int> & Emit::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole};
    return roles;
}

const icSet<int> & Emit::acceptedNexts() {
    static const icSet<int> roles{Role::Value, Role::ValueContext};
    return roles;
}

void Emit::giveModifiers(const icStringList & modifiers) {
    if (modifiers.length() > 1) {
        il->vm->cpe_sig("emit: There is not a support for multiple modifiers");
    }
    else if (modifiers.isEmpty()) {
        errorCode = il::Signals::Exit;
    }
    else {
        errorCode = il->stateful->getSignal(modifiers[0]);
    }
}

}  // namespace icL::core::ce
