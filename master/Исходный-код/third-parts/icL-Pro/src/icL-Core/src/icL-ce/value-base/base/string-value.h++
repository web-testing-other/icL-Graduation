#ifndef core_ce_StringValue
#define core_ce_StringValue

#include <icL-service/value-base/base/string-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL::core::ce {

/**
 * @brief The StringValue class represents a `string` value
 */
class icL_core_ce_value_base_EXPORT StringValue
    : public virtual service::StringValue
    , public BaseValue
{
public:
    /// @brief StringValue calls BaseValue(il, container, varName, readonly)
    StringValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief StringValue calls BaseValue(il, rvalue)
    StringValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief StringValue calls BaseValue(value)
    StringValue(BaseValue * value);

    // properties level 2

    /// `string'empty`
    void runEmpty();

    /// `string'length`
    void runLength();

    /// `string'last`
    void runLast();

    // methods level 2

    /// `string.append`
    void runAppend(const memory::ArgList & args);

    /// `string.at`
    void runAt(const memory::ArgList & args);

    /// `string.beginsWith`
    void runBeginsWith(const memory::ArgList & args);

    /// `string.compare`
    void runCompare(const memory::ArgList & args);

    /// `string.count`
    void runCount(const memory::ArgList & args);

    /// `string.endsWith`
    void runEndsWith(const memory::ArgList & args);

    /// `string.indexOf`
    void runIndexOf(const memory::ArgList & args);

    /// `string.insert`
    void runInsert(const memory::ArgList & args);

    /// `string.lastIndexOf`
    void runLastIndexOf(const memory::ArgList & args);

    /// `string.left`
    void runLeft(const memory::ArgList & args);

    /// `string.leftJustified`
    void runLeftJustified(const memory::ArgList & args);

    /// `string.mid`
    void runMid(const memory::ArgList & args);

    /// `string.prepend`
    void runPrepend(const memory::ArgList & args);

    /// `string.remove`
    void runRemove(const memory::ArgList & args);

    /// `string.replace`
    void runReplace(const memory::ArgList & args);

    /// `string.right`
    void runRight(const memory::ArgList & args);

    /// `string.rightJustified`
    void runRightJustified(const memory::ArgList & args);

    /// `string.split`
    void runSplit(const memory::ArgList & args);

    /// `string.substring`
    void runSubstring(const memory::ArgList & args);

    /// `string.toLowerCase`
    void runToLowerCase(const memory::ArgList & args);

    /// `string.toUpperCase`
    void runToUpperCase(const memory::ArgList & args);

    /// `string.trim`
    void runTrim(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const memory::ArgList & args) override;

    // CE interface
protected:
    const icSet<int> & acceptedPrevs() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_StringValue
