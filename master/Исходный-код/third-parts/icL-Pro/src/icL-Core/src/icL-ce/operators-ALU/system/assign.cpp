#include "assign.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/args/listify.h++>

#include <icL-ce/literal/static/default-parameter.h++>
#include <icL-ce/literal/static/field.h++>
#include <icL-ce/literal/static/identifier.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>

namespace icL::core::ce {

using memory::ContainerType;
using memory::Type;

Assign::Assign(il::InterLevel * il)
    : SystemOperator(il) {}

void Assign::tryToAssign(
  const memory::Argument & arg, short type, const icVariant & value) {
    auto * var  = dynamic_cast<il::PackableValue *>(m_prev);
    auto * pack = dynamic_cast<il::ValuePack *>(m_prev);

    if (pack != nullptr || var != nullptr) {
        if (
          arg.container != nullptr &&
          arg.container->getType(arg.varName) == type) {
            arg.container->setValue(arg.varName, value);
        }

        if (var == nullptr) {
            if (m_newContext == nullptr) {
                m_newContext = il->factory->clone(pack);
            }
        }
        else if (var->type() == Type::VoidValue || var->type() == type) {
            var->reset(value);
            m_newContext = il->factory->clone(var);
        }
    }
}

void Assign::assignBool(const memory::Argument & arg, bool value) {
    tryToAssign(arg, Type::BoolValue, value);
}

void Assign::assignDouble(const memory::Argument & arg, double value) {
    tryToAssign(arg, Type::DoubleValue, value);
}

void Assign::assignInt(const memory::Argument & arg, int value) {
    tryToAssign(arg, Type::IntValue, value);
}

void Assign::assignString(
  const memory::Argument & arg, const icString & value) {
    tryToAssign(arg, Type::StringValue, value);
}

void Assign::assignList(
  const memory::Argument & arg, const icStringList & value) {
    tryToAssign(arg, Type::ListValue, value);
}

void Assign::assignDatetime(
  const memory::Argument & arg, const icDateTime & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign(arg, Type::DatetimeValue, value);
    }
}

void Assign::assignRegex(const memory::Argument & arg, const icRegEx & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign(arg, Type::RegexValue, value);
    }
}

void Assign::assignObject(
  const memory::Argument & arg, const memory::Object & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign(arg, Type::ObjectValue, value);
    }
}

void Assign::assignSet(
  const memory::Argument & arg, const memory::Set & value) {
    if (arg.container->getContainerType() != ContainerType::Default) {
        tryToAssign(arg, Type::SetValue, value);
    }
}

void Assign::assignAny(
  const memory::Argument & arg, const memory::Argument & value) {
    switch (value.type) {
    case Type::BoolValue:
        assignBool(arg, value);
        break;

    case Type::DoubleValue:
        assignDouble(arg, value);
        break;

    case Type::IntValue:
        assignInt(arg, value);
        break;

    case Type::StringValue:
        assignString(arg, value);
        break;

    case Type::ListValue:
        assignList(arg, value);
        break;

    case Type::DatetimeValue:
        assignDatetime(arg, value);
        break;

    case Type::RegexValue:
        assignRegex(arg, value);
        break;

    case Type::ObjectValue:
        assignObject(arg, value);
        break;

    case Type::SetValue:
        assignSet(arg, value);
        break;

    default:
        // Elude unused enums warning
        break;
    }
}

void Assign::runAssign(
  const memory::ArgList & left, const memory::ArgList & right) {
    using memory::ContextType::ContextType;
    auto type = static_cast<ContextType>(int(il->vm->get(0)));

    if (type == ContextType::Run) {
        // case 1:1 and case m:m
        if (left.length() == right.length()) {
            for (int i = 0; i < left.length(); i++) {
                auto & arg   = left[i];
                auto & value = right[i];

                assignAny(arg, value);
            }
        }
        // case m:1
        else if (left.length() >= 1 && right.length() == 1) {
            auto & value = right[0];

            for (const auto & arg : left) {
                assignAny(arg, value);
            }
        }

        if (m_newContext == nullptr) {
            il->vm->signal({il::Signals::System, "No such operator"});
        }
    }
    else if (type == ContextType::Value) {
        if (left.length() == 1 && right.length() == 1) {
            if (left[0].varName.isEmpty()) {
                il->vm->syssig("Parameter name cannot be empty");
            }
            else if (right[0].type == Type::VoidValue) {
                il->vm->syssig("Parameter default value cannot be void");
            }
            else {
                m_newContext =
                  new DefaultParameter(il, left[0].varName, right[0].value);
            }
        }
        else {
            il->vm->syssig("Column syntax is wrong, expected: name : type");
        }
    }
}

icString Assign::toString() {
    return "=";
}

int Assign::currentRunRank(bool rtl) {
    bool runnable =
      (m_prev == nullptr &&
       (m_next == nullptr || m_next->role() == Role::Comma)) ||
      (m_prev != nullptr && m_next != nullptr &&
       (m_prev->role() == Role::Value || m_prev->role() == Role::Identifier) &&
       m_next->role() == Role::Value);
    return rtl && runnable ? 1 : -1;
}

StepType Assign::runNow() {
    auto * asValue = dynamic_cast<il::PackableValue *>(m_prev);
    auto * asId    = dynamic_cast<Identifier *>(m_prev);
    auto * next    = dynamic_cast<il::PackableValue *>(m_next);

    memory::ArgList right;

    if (next != nullptr) {
        right = il->factory->listify(il, next);
    }

    if (asId != nullptr) {
        if (next == nullptr) {
            m_newContext = new Field(il, {}, {});
        }
        else if (right.length() == 1) {
            if (right[0].type != Type::VoidValue) {
                m_newContext = new Field(il, asId->getName(), right[0].value);
            }
            else {
                il->vm->syssig("Column value cannot be void");
            }
        }
        else {
            il->vm->syssig("Column value cannot be packed");
        }
    }
    else if (asValue != nullptr) {
        memory::ArgList left = il->factory->listify(il, asValue);

        runAssign(left, right);
    }
    else {
        m_newContext = new Field{il, {}, {}};
    }

    return StepType::CommandEnd;
}

int Assign::role() {
    return Role::Assign;
}

const icSet<int> & Assign::acceptedPrevs() {
    static const icList<icSet<int>> roles{
      {Role::Value, Role::Function, Role::ValueContext, Role::Property,
       Role::Type},
      {Role::NoRole, Role::Identifier},
      {Role::Value}};

    return byContext(roles[0], roles[1], roles[2]);
}

const icSet<int> & Assign::acceptedNexts() {
    static const icList<icSet<int>> roles{
      {Role::Value, Role::SystemValue, Role::Exists, Role::Function,
       Role::ValueContext, Role::LimitedContext, Role::Operator},
      {Role::Comma, Role::Value, Role::ValueContext, Role::NoRole},
      {Role::Value}};

    return byContext(roles[0], roles[1], roles[2]);
}

il::CE * Assign::firstToReplace() {
    auto * asValue = dynamic_cast<il::PackableValue *>(m_prev);
    auto * asId    = dynamic_cast<Identifier *>(m_prev);
    return asValue != nullptr || asId != nullptr ? m_prev : this;
}

il::CE * Assign::lastToReplace() {
    auto * next = dynamic_cast<il::PackableValue *>(m_next);
    return next != nullptr ? m_next : this;
}

void Assign::run(const memory::ArgList & left, const memory::ArgList & right) {
    runAssign(left, right);
}

}  // namespace icL::core::ce
