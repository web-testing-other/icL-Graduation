#include "equality.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

Equality::Equality(il::InterLevel * il)
    : CompareOperator(il) {}

void Equality::runBoolBool(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, boolBool(left[0], right[0]));
}

void Equality::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
}

void Equality::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleDouble(left[0], right[0]));
}

void Equality::runStringString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, stringString(left[0], right[0]));
}

void Equality::runListList(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, listList(left[0], right[0]));
}

void Equality::runObjectObject(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, objectObject(left[0], right[0]));
}

void Equality::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, setSet(left[0], right[0]));
}

icString Equality::toString() {
    return "==";
}

void Equality::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (Equality::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::BoolValue}, {Type::BoolValue}}, &Equality::runBoolBool},
        {{{Type::IntValue}, {Type::IntValue}}, &Equality::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &Equality::runDoubleDouble},
        {{{Type::StringValue}, {Type::StringValue}},
         &Equality::runStringString},
        {{{Type::ListValue}, {Type::ListValue}}, &Equality::runListList},
        {{{Type::ObjectValue}, {Type::ObjectValue}},
         &Equality::runObjectObject},
        {{{Type::SetValue}, {Type::SetValue}}, &Equality::runSetSet}};

    runNow<Equality>(operators, left, right);
}

}  // namespace icL::core::ce
