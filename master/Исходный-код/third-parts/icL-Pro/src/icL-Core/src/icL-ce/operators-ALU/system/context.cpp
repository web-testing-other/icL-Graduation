#include "context.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <utility>



namespace icL::core::ce {

Context::Context(il::InterLevel * il, const il::CodeFragment & code)
    : SystemOperator(il)
    , code(code) {}

const il::CodeFragment & Context::getCode() {
    return code;
}

int Context::currentRunRank(bool rtl) {
    return !rtl ? 8 : -1;
}

void Context::run(const memory::ArgList &, const memory::ArgList &) {
    assert(false);
}

il::CE * Context::firstToReplace() {
    return this;
}

il::CE * Context::lastToReplace() {
    return this;
}

void Context::colorize() {
    auto openBracket  = m_fragmentData;
    auto closeBracket = m_fragmentData;

    openBracket.end    = openBracket.begin;
    closeBracket.begin = closeBracket.end;

    openBracket.end.relative++;
    closeBracket.begin.relative--;

    il->vms->markToken(openBracket, il::Token::Operator);
    il->vms->markToken(closeBracket, il::Token::Operator);
}

}  // namespace icL::core::ce
