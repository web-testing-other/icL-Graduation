#include "keyword.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/export/signals.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>



namespace icL::core::ce {

Keyword::Keyword(il::InterLevel * il)
    : CE(il) {}

void Keyword::giveModifiers(const icStringList & modifiers) {
    if (!modifiers.isEmpty()) {
        il->vm->signal(
          {il::Signals::System, "No such modifier: " + modifiers[0]});
    }
}

void Keyword::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Keyword);
}

}  // namespace icL::core::ce
