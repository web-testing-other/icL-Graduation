#ifndef core_ce_Property
#define core_ce_Property

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce/base/literal/functional-literal.h++>
#include <icL-ce/base/main/value.h++>



namespace icL::core::ce {

class icL_core_ce_literal_EXPORT Property : public FunctionalLiteral
{
public:
    Property(il::InterLevel * il, Prefix prefix, const icString & name);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    il::CE * firstToReplace() override;
    int      role() override;

    void colorize() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // fields
protected:
    /// \brief prefix is the prefix of the property
    Prefix prefix;

    // padding
    int : 32;

    /// \brief name is the name of the property
    icString name;
};

}  // namespace icL::core::ce

#endif  // core_ce_Property
