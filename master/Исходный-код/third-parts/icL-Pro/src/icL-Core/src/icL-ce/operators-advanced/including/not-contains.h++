#ifndef core_ce_NotContains
#define core_ce_NotContains

#include "contains.h++"



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT NotContains : public Contains
{
public:
    NotContains(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_NotContains
