#include "can-cast.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-service/cast/base/list-cast.h++>
#include <icL-service/cast/base/set-cast.h++>
#include <icL-service/cast/base/string-cast.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>
#include <icL-mock/vmlayer.h++>

namespace icL::core::ce {

CanCast::CanCast(il::InterLevel * il)
    : CastOperator(il) {}

template <typename ReturnType, typename ArgType>
void CanCast::runAny(
  ReturnType (*func)(il::InterLevel *, const ArgType &),
  const memory::Argument & left) {
    il::InterLevel il{};

    il.vm = new mock::VMLayer();
    func(&il, left);

    m_newContext = this->il->factory->fromValue(this->il, il.vm->hasOkState());
    delete il.vm;
}

void CanCast::runAlwaysTrue(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, true);
}

void CanCast::runAlwaysFalse(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, false);
}

void CanCast::runStringInt(const memory::Argument & left) {
    runAny<int, icString>(service::StringCast::toInt, left);
}

void CanCast::runStringDouble(const memory::Argument & left) {
    runAny<double, icString>(service::StringCast::toDouble, left);
}

void CanCast::runStringObject(const memory::Argument & left) {
    runAny<memory::Object, icString>(service::StringCast::toObject, left);
}

void CanCast::runStringSet(const memory::Argument & left) {
    runAny<memory::Set, icString>(service::StringCast::toSet, left);
}

void CanCast::runListString(const memory::Argument & left) {
    runAny<icString, icStringList>(service::ListCast::toString, left);
}

void CanCast::runListSet(const memory::Argument & left) {
    runAny<memory::Set, icStringList>(service::ListCast::toSet, left);
}

void CanCast::runSetObject(const memory::Argument & left) {
    runAny<memory::Object, memory::Set>(service::SetCast::toObject, left);
}

icString CanCast::toString() {
    return ":*";
}

void CanCast::runCast(const memory::Argument & left, Type right) {
    static icObject<
      icPair<short, short>, void (CanCast::*)(const memory::Argument &)>
      operators{
        {{Type::VoidValue, Type::BoolValue}, &CanCast::runAlwaysTrue},
        {{Type::VoidValue, Type::IntValue}, &CanCast::runAlwaysTrue},
        {{Type::VoidValue, Type::DoubleValue}, &CanCast::runAlwaysTrue},
        {{Type::VoidValue, Type::StringValue}, &CanCast::runAlwaysTrue},
        {{Type::VoidValue, Type::ListValue}, &CanCast::runAlwaysTrue},
        {{Type::VoidValue, Type::ObjectValue}, &CanCast::runAlwaysTrue},
        {{Type::VoidValue, Type::SetValue}, &CanCast::runAlwaysTrue},
        {{Type::BoolValue, Type::VoidValue}, &CanCast::runAlwaysFalse},
        {{Type::BoolValue, Type::IntValue}, &CanCast::runAlwaysTrue},
        {{Type::BoolValue, Type::DoubleValue}, &CanCast::runAlwaysTrue},
        {{Type::BoolValue, Type::StringValue}, &CanCast::runAlwaysTrue},
        {{Type::BoolValue, Type::ListValue}, &CanCast::runAlwaysFalse},
        {{Type::BoolValue, Type::ObjectValue}, &CanCast::runAlwaysFalse},
        {{Type::BoolValue, Type::SetValue}, &CanCast::runAlwaysFalse},
        {{Type::IntValue, Type::VoidValue}, &CanCast::runAlwaysFalse},
        {{Type::IntValue, Type::BoolValue}, &CanCast::runAlwaysTrue},
        {{Type::IntValue, Type::DoubleValue}, &CanCast::runAlwaysTrue},
        {{Type::IntValue, Type::StringValue}, &CanCast::runAlwaysTrue},
        {{Type::IntValue, Type::ListValue}, &CanCast::runAlwaysFalse},
        {{Type::IntValue, Type::ObjectValue}, &CanCast::runAlwaysFalse},
        {{Type::IntValue, Type::SetValue}, &CanCast::runAlwaysFalse},
        {{Type::DoubleValue, Type::VoidValue}, &CanCast::runAlwaysFalse},
        {{Type::DoubleValue, Type::BoolValue}, &CanCast::runAlwaysTrue},
        {{Type::DoubleValue, Type::DoubleValue}, &CanCast::runAlwaysTrue},
        {{Type::DoubleValue, Type::StringValue}, &CanCast::runAlwaysTrue},
        {{Type::DoubleValue, Type::ListValue}, &CanCast::runAlwaysFalse},
        {{Type::DoubleValue, Type::ObjectValue}, &CanCast::runAlwaysFalse},
        {{Type::DoubleValue, Type::SetValue}, &CanCast::runAlwaysFalse},
        {{Type::StringValue, Type::VoidValue}, &CanCast::runAlwaysFalse},
        {{Type::StringValue, Type::BoolValue}, &CanCast::runAlwaysTrue},
        {{Type::StringValue, Type::IntValue}, &CanCast::runStringInt},
        {{Type::StringValue, Type::DoubleValue}, &CanCast::runStringDouble},
        {{Type::StringValue, Type::ListValue}, &CanCast::runAlwaysTrue},
        {{Type::StringValue, Type::ObjectValue}, &CanCast::runStringObject},
        {{Type::StringValue, Type::SetValue}, &CanCast::runStringSet},
        {{Type::StringValue, Type::DatetimeValue}, &CanCast::runAlwaysTrue},
        {{Type::ListValue, Type::VoidValue}, &CanCast::runAlwaysFalse},
        {{Type::ListValue, Type::BoolValue}, &CanCast::runAlwaysTrue},
        {{Type::ListValue, Type::IntValue}, &CanCast::runAlwaysFalse},
        {{Type::ListValue, Type::DoubleValue}, &CanCast::runAlwaysFalse},
        {{Type::ListValue, Type::StringValue}, &CanCast::runListString},
        {{Type::ListValue, Type::ObjectValue}, &CanCast::runAlwaysFalse},
        {{Type::ListValue, Type::SetValue}, &CanCast::runListSet},
        {{Type::ObjectValue, Type::VoidValue}, &CanCast::runAlwaysFalse},
        {{Type::ObjectValue, Type::BoolValue}, &CanCast::runAlwaysTrue},
        {{Type::ObjectValue, Type::IntValue}, &CanCast::runAlwaysFalse},
        {{Type::ObjectValue, Type::DoubleValue}, &CanCast::runAlwaysFalse},
        {{Type::ObjectValue, Type::StringValue}, &CanCast::runAlwaysTrue},
        {{Type::ObjectValue, Type::ListValue}, &CanCast::runAlwaysFalse},
        {{Type::ObjectValue, Type::SetValue}, &CanCast::runAlwaysFalse},
        {{Type::SetValue, Type::VoidValue}, &CanCast::runAlwaysFalse},
        {{Type::SetValue, Type::BoolValue}, &CanCast::runAlwaysTrue},
        {{Type::SetValue, Type::IntValue}, &CanCast::runAlwaysFalse},
        {{Type::SetValue, Type::DoubleValue}, &CanCast::runAlwaysFalse},
        {{Type::SetValue, Type::StringValue}, &CanCast::runAlwaysTrue},
        {{Type::SetValue, Type::ObjectValue}, &CanCast::runSetObject}};

    if (left.type == right) {
        runAlwaysTrue(left);
        return;
    }

    auto it = operators.find({left.type, right});

    if (it == operators.end()) {
        runAlwaysFalse(left);
    }
    else {
        (this->*it.value())(left);
    }
}

void CanCast::runCast(
  const memory::ArgList & /*left*/, const memory::ArgList & /*right*/) {}


}  // namespace icL::core::ce
