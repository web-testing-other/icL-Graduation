#include "assert.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce/operators-ALU/system/context.h++>
#include <icL-ce/value-base/base/void-value.h++>



namespace icL::core::ce {

Assert::Assert(il::InterLevel * il)
    : AdvancedKeyword(il) {}

icString Assert::toString() {
    return "assert";
}

int Assert::currentRunRank(bool) {
    return m_next->next() == nullptr ? 9 : -1;
}

StepType Assert::runNow() {
    return transact();
}

int Assert::role() {
    return Role::Assert;
}

const icSet<int> & Assert::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole};
    return roles;
}

const icSet<int> & Assert::acceptedNexts() {
    static const icSet<int> roles{Role::ValueContext};
    return roles;
}

il::CE * Assert::lastToReplace() {
    return m_next;
}

void Assert::giveModifiers(const icStringList & modifiers) {
    for (auto & mod : modifiers) {
        if (mod == "not") {
            if (notMod) {
                il->vm->cpe_sig("assert: `not` modifier setted twince");
            }
            else {
                notMod = true;
            }
        }
        else {
            il->vm->cpe_sig("assert: unknown modifier: " % mod);
        }
    }
}

void Assert::initialize() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    if (commands.length() == 1) {
        condition = commands[0];
    }
    else if (commands.length() == 2) {
        condition = commands[0];
        message   = commands[1];
    }
    else {
        il->vm->syssig("assert: Wrong number of commands");
    }
}

void Assert::finalize() {
    m_newContext = il->factory->fromValue(il, {});
}

}  // namespace icL::core::ce
