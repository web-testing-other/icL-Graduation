#ifndef core_ce_Operator
#define core_ce_Operator

#include "ce.h++"



class icVariant;
template <typename, typename>
class icObject;
template <typename, typename>
class icPair;

namespace icL::core::ce {

/**
 * @brief The Operator class represent an operator token (abstract)
 */
class Operator : public CE
{
public:
    Operator(il::InterLevel * il);

    /**
     * @brief run runs the operator now
     * @param left is the left operand
     * @param right is the right operand
     */
    virtual void run(
      const memory::ArgList & left, const memory::ArgList & right) = 0;

    /**
     * @brief runOperator runs a operator form hash map
     * @param operators are the hash map of operators
     * @param left is the left operand
     * @param right is the right operand
     */
    template <typename This>
    void runNow(
      const icObject<
        icPair<icList<Type>, icList<Type>>,
        void (This::*)(const memory::ArgList &, const memory::ArgList &)> &
                              operators,
      const memory::ArgList & left, const memory::ArgList & right);

protected:
    /**
     * @brief byContext selects a roles set by context
     * @param run returns it if current context is of run type
     * @param limited returns it if current context is of limited type
     * @param value returns it if current context is of value type
     * @return one of given contextes
     */
    const icSet<int> & byContext(
      const icSet<int> & run, const icSet<int> & limited,
      const icSet<int> & value);

    // CE interface
public:
    int      role() override;
    StepType runNow() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

    void colorize() override;
};

}  // namespace icL::core::ce

// uint qhash(const icL::int & type);

#endif  // core_ce_Operator
