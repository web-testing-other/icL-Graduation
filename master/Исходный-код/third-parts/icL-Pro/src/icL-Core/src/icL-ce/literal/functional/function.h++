#ifndef core_ce_Function
#define core_ce_Function

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce/base/literal/functional-literal.h++>



namespace icL::core::ce {

class icL_core_ce_literal_EXPORT Function : public FunctionalLiteral
{
public:
    Function(il::InterLevel * il, const icString & name);

    const icString & getName() const;

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    il::CE * lastToReplace() override;
    int      role() override;

    void colorize() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // field
private:
    /// \brief name is the name of function
    icString name;
    /// \brief executed is true after function executed
    bool executed = false;

    // padding
    long : 56;
};

}  // namespace icL::core::ce

#endif  // core_ce_Function
