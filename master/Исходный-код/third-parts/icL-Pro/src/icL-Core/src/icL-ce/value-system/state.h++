#ifndef core_ce_States
#define core_ce_States

#include <icL-service/value-system/state.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::core::ce {

class icL_core_ce_value_system_EXPORT State
    : public SystemValue
    , public service::State
{
public:
    State(il::InterLevel * il);

    // methods level 2

    /// `State.clear`
    void runClear(const memory::ArgList & args);

    /// `State.delete`
    void runDelete(const memory::ArgList & args);

    /// `State.new`
    void runNew(const memory::ArgList & args);

    /// `State.newAtEnd`
    void runNewAtEnd(const memory::ArgList & args);

    /// `State.toFirst`
    void runToFirst(const memory::ArgList & args);

    /// `State.toLast`
    void runToLast(const memory::ArgList & args);

    /// `State.toNext`
    void runToNext(const memory::ArgList & args);

    /// `State.toPrev`
    void runToPrev(const memory::ArgList & args);

    // CE interface
protected:
    const icSet<int> & acceptedNexts() override;

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_States
