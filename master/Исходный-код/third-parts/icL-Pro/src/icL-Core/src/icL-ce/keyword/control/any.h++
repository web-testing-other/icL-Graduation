#ifndef core_ce_Any
#define core_ce_Any

#include <icL-ce/base/keyword/control-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Any : public ControlKeyword
{
public:
    Any(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int      role() override;
    il::CE * firstToReplace() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

private:
    bool executed = false;  ///< executed is true if the code was executed
};

}  // namespace icL::core::ce

#endif  // core_ce_Any
