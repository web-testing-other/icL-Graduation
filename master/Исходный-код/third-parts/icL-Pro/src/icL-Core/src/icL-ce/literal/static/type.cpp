#include "type.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-memory/structures/packed-value-item.h++>



namespace icL::core::ce {

TypeToken::TypeToken(il::InterLevel * il, int type)
    : StaticLiteral(il)
    , type(static_cast<Type>(type)) {}

Type TypeToken::getType() {
    return type;
}

icString TypeToken::toString() {
    return il->factory->typeToString(type);
}

int TypeToken::role() {
    return Role::Type;
}

memory::PackedValueItem TypeToken::packNow() const {
    memory::PackedValueItem ret;

    ret.itemType = il::PackableType::Type;
    ret.type     = type;

    return ret;
}

void TypeToken::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Type);
}

const icSet<int> & TypeToken::acceptedNexts() {
    const static icSet<int> roles{StaticLiteral::acceptedNexts(),
                                   {Role::Assign}};
    return roles;
}

}  // namespace icL::core::ce
