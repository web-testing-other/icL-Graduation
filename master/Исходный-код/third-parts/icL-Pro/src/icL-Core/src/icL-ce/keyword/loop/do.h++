#ifndef core_ce_Do
#define core_ce_Do

#include <icL-ce/base/keyword/loop-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Do : public LoopKeyword
{
public:
    Do(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Do
