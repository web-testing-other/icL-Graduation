#ifndef core_ce_PackedValue
#define core_ce_PackedValue

#include "base-value.h++"

#include <icL-memory/structures/packed-value-item.h++>


namespace icL::core::ce {

/**
 * @brief The PackedValue class represent a icL packed value (read standard)
 */
class icL_core_ce_base_EXPORT PackedValue
    : public BaseValue
    , public il::ValuePack
{
public:
    /**
     * @brief PackedValue constructs a packed value on values merge
     * @param il is the inter-level node
     * @param first is the left comma operand
     * @param second is the right comma operand
     */
    PackedValue(
      il::InterLevel * il, PackableValue * first = nullptr,
      PackableValue * second = nullptr);

    /**
     * @brief PackedValue copy a packed value
     * @param il is the interl-level node
     * @param other is the pack to copy
     */
    PackedValue(il::InterLevel * il, const memory::PackedValue & other);

    /**
     * @brief PackedValue create a copy of
     * @param value is the value to copy
     */
    PackedValue(ValuePack * value);

    /**
     * @brief getValues gets the packed values
     * @return the packed values
     */
    const memory::PackedValue & getValues();

protected:
    /**
     * @brief extractDataFrom extracts data from a CE node
     * @param ce is the node to extract data of
     */
    void extractDataFrom(PackableValue * ce);

    // CE interface
protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;

    // ValuePack interface
public:
    const memory::PackedItems & getValues() const override;

    // fields
private:
    memory::PackedValue values;
};

}  // namespace icL::core::ce

#endif  // core_ce_PackedValue
