#ifndef core_ce_DateTime
#define core_ce_DateTime

#include <icL-service/value-system/datetime.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::core::ce {

/**
 * @brief The icDateTime class respresent a `icDateTime` token
 */
class icL_core_ce_value_system_EXPORT DateTime
    : public SystemValue
    , public service::DateTime
{
public:
    DateTime(il::InterLevel * il);

    // methods level 2

    /// `icDateTime.current`
    void runCurrent(const memory::ArgList & args);

    /// `icDateTime.currentUTC`
    void runCurrentUTC(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_DateTime
