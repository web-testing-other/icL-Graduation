#ifndef core_ce_LogicOperator
#define core_ce_LogicOperator

#include "alu-operator.h++"



namespace icL::core::ce {

/**
 * @brief The LogicOperator class represents a logic operator `&, |, ~, ^, %, !`
 */
class icL_core_ce_base_EXPORT LogicOperator : public AluOperator
{
public:
    LogicOperator(il::InterLevel * il);

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_LogicOperator
