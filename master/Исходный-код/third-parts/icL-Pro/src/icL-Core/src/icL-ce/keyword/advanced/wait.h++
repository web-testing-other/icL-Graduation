#ifndef core_ce_Wait
#define core_ce_Wait

#include <icL-service/keyword/advanced/wait.h++>

#include <icL-ce/base/keyword/advanced-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Wait
    : public AdvancedKeyword
    , public service::Wait
{
public:
    Wait(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    il::CE * lastToReplace() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Wait
