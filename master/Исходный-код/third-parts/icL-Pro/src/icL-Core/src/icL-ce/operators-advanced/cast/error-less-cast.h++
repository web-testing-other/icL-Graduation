#ifndef core_ce_ErrorLessCast
#define core_ce_ErrorLessCast

#include "cast.h++"



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT ErrorLessCast : public Cast
{
public:
    ErrorLessCast(il::InterLevel * il);

private:
    template <typename ReturnType, typename ArgType>
    void runAny(
      ReturnType (*func)(il::InterLevel *, const ArgType &),
      const memory::Argument & left);

public:
    /// all unreal casts
    void runAlwaysVoid(const memory::Argument & left);

    // CE interface
public:
    icString toString() override;

    // Cast interface
public:
    void runUnhandled(const memory::Argument & left) override;
    void runStringInt(const memory::Argument & left) override;
    void runStringDouble(const memory::Argument & left) override;
    void runStringObject(const memory::Argument & left) override;
    void runStringSet(const memory::Argument & left) override;
    void runListString(const memory::Argument & left) override;
    void runListSet(const memory::Argument & left) override;
    void runSetObject(const memory::Argument & left) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ErrorLessCast
