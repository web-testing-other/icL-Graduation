#include "jammer.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/stateful-server.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce/operators-ALU/system/context.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

Jammer::Jammer(il::InterLevel * il)
    : AdvancedKeyword(il) {
    catchSystem = il->stateful->catchSystem();
}

icString Jammer::toString() {
    icString str = "jammer";

    if (catchSystem)
        str += "-system";

    return str;
}

int icL::core::ce::Jammer::currentRunRank(bool) {
    return m_next->next() == nullptr ? 9 : -1;
}

StepType Jammer::runNow() {
    return transact();
}

int Jammer::role() {
    return Role::Jammer;
}

const icSet<int> & Jammer::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole};
    return roles;
}

const icSet<int> & Jammer::acceptedNexts() {
    static const icSet<int> roles{Role::RunContext, Role::ValueContext};
    return roles;
}

void Jammer::initialize() {
    code = dynamic_cast<Context *>(m_next)->getCode();
}

void Jammer::finalize() {
    m_newContext = il->factory->fromValue(il, errorCode);
}

void Jammer::giveModifiers(const icStringList & modifiers) {
    for (auto & mod : modifiers) {
        if (mod == "system") {
            if (catchSystem) {
                il->vm->cpe_sig("jammer: system modifier setted twince");
            }
            else {
                catchSystem = true;
            }
        }
        else {
            il->vm->cpe_sig("jammer: Unknown modifier: " + mod);
        }
    }
}

il::CE * Jammer::lastToReplace() {
    return m_next;
}

}  // namespace icL::core::ce
