#ifndef core_ce_Import
#define core_ce_Import

#include <icL-service/value-system/import.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::core::ce {

class icL_core_ce_value_system_EXPORT Import
    : public SystemValue
    , public service::Import
{
public:
    Import(il::InterLevel * il);

    // methods level 2

    /// `Import.all`
    void runAll(const memory::ArgList & args);

    /// `Import.functions`
    void runFunctions(const memory::ArgList & args);

    /// `Import.none`
    void runNone(const memory::ArgList & args);

    /// `Import.run`
    void runRun(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Import
