#include "lambda-base-value.h++"

namespace icL::core::ce {

LambdaValueBase::LambdaValueBase(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

LambdaValueBase::LambdaValueBase(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

LambdaValueBase::LambdaValueBase(BaseValue * value)
    : BaseValue(value) {}

}  // namespace icL::core::ce
