#include "addition.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

Addition::Addition(il::InterLevel * il)
    : AmbiguousArithmeticalOperator(il) {}

void Addition::runInt(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, voidInt(right[0]));
}

void Addition::runDouble(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, voidDouble(right[0]));
}

void Addition::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
}

void Addition::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleDouble(left[0], right[0]));
}

void Addition::runStringString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, stringString(left[0], right[0]));
}

void Addition::runStringList(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, stringList(left[0], right[0]));
}

void Addition::runListString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, listString(left[0], right[0]));
}

void Addition::runListList(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, listList(left[0], right[0]));
}

void Addition::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, setSet(left[0], right[0]));
}

icString Addition::toString() {
    return "+";
}

void Addition::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (Addition::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{}, {Type::IntValue}}, &Addition::runInt},
        {{{}, {Type::DoubleValue}}, &Addition::runDouble},
        {{{Type::IntValue}, {Type::IntValue}}, &Addition::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &Addition::runDoubleDouble},
        {{{Type::StringValue}, {Type::StringValue}},
         &Addition::runStringString},
        {{{Type::StringValue}, {Type::ListValue}}, &Addition::runStringList},
        {{{Type::ListValue}, {Type::StringValue}}, &Addition::runListString},
        {{{Type::ListValue}, {Type::ListValue}}, &Addition::runListList},
        {{{Type::SetValue}, {Type::SetValue}}, &Addition::runSetSet}};

    runNow<Addition>(operators, left, right);
}

int Addition::runRank() {
    return 4;
}

int Addition::runAbmiguousRank() {
    return 7;
}

}  // namespace icL::core::ce
