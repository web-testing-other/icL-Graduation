#ifndef core_ce_NotContainsTemplate
#define core_ce_NotContainsTemplate

#include "contains-template.h++"



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT NotContainsTemplate
    : public ContainsTemplate
{
public:
    NotContainsTemplate(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_NotContainsTemplate
