#include "range.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce/operators-ALU/system/context.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

Range::Range(il::InterLevel * il)
    : LoopKeyword(il) {}

icString Range::toString() {
    icString str = "range";

    if (reverse)
        str += "-reverse";
    if (max != -1)
        str += "-max" % icString::number(max);

    return str;
}

int Range::currentRunRank(bool /*rtl*/) {
    bool runnable =
      dynamic_cast<CE *>(m_next->next())->role() == Role::RunContext &&
      m_next->next()->next() == nullptr;
    return runnable ? 9 : -1;
}

StepType Range::runNow() {
    return transact();
}

int Range::role() {
    return Role::Range;
}

const icSet<int> & Range::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole};
    return roles;
}

const icSet<int> & Range::acceptedNexts() {
    static const icSet<int> roles{Role::ValueContext};
    return roles;
}

void Range::initialize() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    if (commands.length() != 2 && commands.length() != 3) {
        il->vm->syssig(
          "range: the round brackets content must contains 2 or 3 commands");
    }
    else {
        collectionCode = commands[0];
        startCode      = commands[1];

        if (commands.length() == 3) {
            finishCode = commands[2];
        }
        else {
            finishCode = commands[3];
        }

        loopBody = dynamic_cast<Context *>(m_next->next())->getCode();
    }
}

void Range::finalize() {
    m_newContext = il->factory->fromValue(il, executed);
}

void Range::giveModifiers(const icStringList & modifiers) {
    static icRegEx maxRx = R"(max(\d+))"_rx;

    icRegExMatch match;

    for (auto & mod : modifiers) {
        if (mod == "reverse") {
            if (reverse) {
                il->vm->cpe_sig("range: Reverse modifier setted twince");
            }
            reverse = true;
        }
        else if ((match = maxRx.match(mod)).hasMatch()) {
            int _max = match.captured(0).toInt();

            if (_max < 2) {
                il->vm->cpe_sig("range: Minimal value of `max` modifier is 2");
            }
            else if (max > 0) {
                il->vm->cpe_sig("range: `max` modifer setted twince");
            }

            max = _max;
        }
        else {
            il->vm->cpe_sig("range: Unknown modifier: " % mod);
        }
    }
}

il::InterLevel * Range::core() {
    return il;
}

}  // namespace icL::core::ce
