#ifndef core_ce_RunContext
#define core_ce_RunContext

#include "context.h++"



namespace icL::core::ce {

/**
 * @brief The RunContext class represent a run context `{}`
 */
class icL_core_ce_operators_ALU_EXPORT RunContext : public Context
{
public:
    RunContext(
      il::InterLevel * il, const il::CodeFragment & code,
      const icString & name);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    int      role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // fields
private:
    icString name;
};

}  // namespace icL::core::ce

#endif  // core_ce_RunContext
