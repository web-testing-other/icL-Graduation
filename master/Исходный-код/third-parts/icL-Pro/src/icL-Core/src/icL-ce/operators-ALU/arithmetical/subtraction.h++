#ifndef core_ce_Substraction
#define core_ce_Substraction

#include <icL-service/operators-ALU/arithmetical/substraction.h++>

#include <icL-ce/base/alu-operator/ambiguous-arithmetical-operator.h++>



namespace icL::core {

namespace memory {
struct Set;
}

namespace ce {

class icL_core_ce_operators_ALU_EXPORT Subtraction
    : public AmbiguousArithmeticalOperator
    , public service::Subtraction
{
public:
    Subtraction(il::InterLevel * il);

    // level 2

    /// `-int`
    void runInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `-double`
    void runDouble(const memory::ArgList & left, const memory::ArgList & right);

    /// `int - int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double - double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set - set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;

    // AmbiguousArithmeticalOperator interface
protected:
    int runAbmiguousRank() override;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_Substraction
