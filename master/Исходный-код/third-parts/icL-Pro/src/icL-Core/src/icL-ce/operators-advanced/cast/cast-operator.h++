#ifndef core_ce_CastOperator
#define core_ce_CastOperator

#include <icL-ce/base/advanced-operator/advanced-operator.h++>



namespace icL::core::ce {

class icL_core_ce_base_EXPORT CastOperator : public AdvancedOperator
{
public:
    CastOperator(il::InterLevel * il);

    /**
     * @brief runRank gets the run rank of cast operator
     * @return the run rank of cast operator
     */
    virtual int runRank();

    /**
     * @brief runCast run the cast operation
     * @param left is the left operand
     * @param right is the right operand
     */
    virtual void runCast(const memory::Argument & left, Type right) = 0;

    /**
     * @brief runCast run an untipically cast
     * @param left is the left operand
     * @param right is the right operand
     */
    virtual void runCast(
      const memory::ArgList & left, const memory::ArgList & right) = 0;

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

protected:
    const icSet<int> & acceptedNexts() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_CastOperator
