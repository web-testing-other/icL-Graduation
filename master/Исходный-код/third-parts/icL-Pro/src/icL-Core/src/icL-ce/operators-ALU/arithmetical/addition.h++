#ifndef core_ce_Addition
#define core_ce_Addition

#include <icL-service/operators-ALU/arithmetical/addition.h++>

#include <icL-ce/base/alu-operator/ambiguous-arithmetical-operator.h++>



class icString;
class icStringList;

namespace icL::core {

namespace memory {
struct Set;
}

namespace ce {

class icL_core_ce_operators_ALU_EXPORT Addition
    : public AmbiguousArithmeticalOperator
    , public service::Addition
{
public:
    Addition(il::InterLevel * il);

    // level 2

    /// `+int`
    void runInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `+double`
    void runDouble(const memory::ArgList & left, const memory::ArgList & right);

    /// `int + int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double + double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string + string`
    void runStringString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string + list`
    void runStringList(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `list + string`
    void runListString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `list + list`
    void runListList(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set + set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;

    // AmbiguousArithmeticalOperator interface
protected:
    int runAbmiguousRank() override;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_Addition
