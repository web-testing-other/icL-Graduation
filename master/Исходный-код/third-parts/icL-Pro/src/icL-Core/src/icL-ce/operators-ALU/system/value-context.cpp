#include "value-context.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-ce/base/value/packed-value.h++>

#include <icL-memory/structures/function-call.h++>

namespace icL::core::ce {

ValueContext::ValueContext(il::InterLevel * il, const il::CodeFragment & code)
    : Context(il, code) {}

icString ValueContext::toString() {
    return "(..)";
}

StepType ValueContext::runNow() {
    if (executed) {
        return StepType::CommandEnd;
    }

    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.contextType = memory::ContextType::Value;

    il->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code != il::Signals::NoError) {
            il->vm->signal(ret.signal);
            return false;
        }

        m_newContext = il->factory->fromValue(il, ret.consoleValue);
        auto typeable = dynamic_cast<il::TypeableValue *>(m_newContext);

        if (typeable->type() == Type::VoidValue) {
            delete m_newContext;
            m_newContext = new PackedValue{il};
        }

        return false;
    });

    executed = true;
    return StepType::CommandIn;
}

int ValueContext::role() {
    return Role::ValueContext;
}

const icSet<int> & ValueContext::acceptedPrevs() {
    static const icSet<int> roles{
      Role::NoRole,  Role::Any,    Role::Case,  Role::Exists,   Role::If,
      Role::Switch,  Role::Assert, Role::Emit,  Role::Listen,   Role::Filter,
      Role::For,     Role::Range,  Role::While, Role::Method,   Role::Function,
      Role::Assign,  Role::Comma,  Role::Cast,  Role::Operator, Role::JsFile,
      Role::Overload};
    return roles;
}

const icSet<int> & ValueContext::acceptedNexts() {
    static const icSet<int> roles{
      Role::NoRole,     Role::Method, Role::Property, Role::Assign, Role::Comma,
      Role::RunContext, Role::Cast,   Role::Operator, Role::Case};
    return roles;
}

}  // namespace icL::core::ce
