#ifndef core_ce_Column
#define core_ce_Column

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce/base/literal/static-literal.h++>

#include <icL-memory/structures/type.h++>



namespace icL::core::ce {

/**
 * @brief The Column class represent a column token `name : type`
 */
class icL_core_ce_literal_EXPORT Column : public StaticLiteral
{
public:
    Column(il::InterLevel * il, const icString & name, Type type);

    /**
     * @brief getName gets the name of column
     * @return the name of column
     */
    const icString & getName();

    /**
     * @brief getType gets the type of column
     * @return the type of column
     */
    int getType();

    // CE interface
public:
    icString toString() override;
    int      role() override;

    // PackableValue interface
    memory::PackedValueItem packNow() const override;
    il::PackableType        packableType() const override;

private:
    /// \brief name is the name of the column
    icString name;
    /// \brief type is the type of the column
    Type type;
};

}  // namespace icL::core::ce

#endif  // core_ce_Column
