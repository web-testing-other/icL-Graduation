#include "now.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/value/base-value.h++>
#include <icL-ce/value-base/base/void-value.h++>



namespace icL::core::ce {

Now::Now(il::InterLevel * il)
    : AdvancedKeyword(il) {}

int Now::currentRunRank(bool rtl) {
    return rtl ? 9 : -1;
}

StepType Now::runNow() {
    auto * value = dynamic_cast<BaseValue *>(m_next);

    if (value->isLValue()) {
        value->rebind();
        m_newContext = il->factory->clone(value);
    }

    return StepType::CommandEnd;
}

icString Now::toString() {
    return "now";
}

int Now::role() {
    return Role::Now;
}

il::CE * Now::lastToReplace() {
    return m_next;
}

const icSet<int> & Now::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole, Role::Assign, Role::Comma};
    return roles;
}

const icSet<int> & Now::acceptedNexts() {
    static const icSet<int> roles{Role::Value};
    return roles;
}

}  // namespace icL::core::ce
