#include "property.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <utility>

namespace icL::core::ce {

Property::Property(il::InterLevel * il, Prefix prefix, const icString & name)
    : FunctionalLiteral(il)
    , prefix(prefix)
    , name(name) {}

icString Property::toString() {
    return '\'' % name;
}

int Property::currentRunRank(bool rtl) {
    bool runnable =
      m_prev->role() == Role::Value || m_prev->role() == Role::SystemValue;
    return !rtl && runnable ? 8 : -1;
}

StepType Property::runNow() {
    auto * value = dynamic_cast<Value *>(m_prev);
    value->runProperty(prefix, name);
    m_newContext = value->newCE();

    return StepType::CommandEnd;
}

il::CE * Property::firstToReplace() {
    return m_prev;
}

int Property::role() {
    return Role::Property;
}

void Property::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Property);
}

const icSet<int> & Property::acceptedPrevs() {
    static const icSet<int> roles{Role::Value, Role::SystemValue,
                                   Role::Property, Role::LimitedContext,
                                   Role::ValueContext};
    return roles;
}

const icSet<int> & Property::acceptedNexts() {
    static const icSet<int> roles{Role::Property, Role::Method, Role::Assign,
                                   Role::Comma,    Role::Cast,   Role::Operator,
                                   Role::NoRole};
    return roles;
}

}  // namespace icL::core::ce
