#ifndef core_ce_Exponentiation
#define core_ce_Exponentiation

#include <icL-service/operators-ALU/arithmetical/exponentiation.h++>

#include <icL-ce/base/alu-operator/ambiguous-arithmetical-operator.h++>



class icStringList;
class icRegEx;

namespace icL::core {

namespace memory {
struct Set;
struct Object;
}  // namespace memory

namespace ce {

class icL_core_ce_operators_ALU_EXPORT Exponentiation
    : public AmbiguousArithmeticalOperator
    , public service::Exponentiation
{
public:
    Exponentiation(il::InterLevel * il);

    // level 2

    /// `int**`
    void runInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double**`
    void runDouble(const memory::ArgList & left, const memory::ArgList & right);

    /// `int ** int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double ** int`
    void runDoubleInt(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `double ** double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string ** string`
    void runStringString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `list ** list`
    void runListList(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set ** set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    /// `string ** regex`
    void runStringRegex(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;

    // AmbiguousArithmeticalOperator interface
protected:
    int runAbmiguousRank() override;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_Exponentiation
