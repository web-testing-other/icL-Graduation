#ifndef core_ce_IncludingOperator
#define core_ce_IncludingOperator

#include "advanced-operator.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT IncludingOperator : public AdvancedOperator
{
public:
    IncludingOperator(il::InterLevel * il);

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_IncludingOperator
