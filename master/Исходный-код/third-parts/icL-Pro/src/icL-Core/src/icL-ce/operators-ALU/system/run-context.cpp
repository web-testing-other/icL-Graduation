#include "run-context.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-memory/structures/function-call.h++>

#include <utility>

namespace icL::core::ce {

RunContext::RunContext(
  il::InterLevel * il, const il::CodeFragment & code, const icString & name)
    : Context(il, code)
    , name(name) {}

icString RunContext::toString() {
    return "{..}";
}

int RunContext::currentRunRank(bool rtl) {
    bool runnable = m_next == nullptr && m_prev == nullptr;
    return runnable && rtl ? 1 : -1;
}

StepType RunContext::runNow() {
    if (executed) {
        return StepType::CommandEnd;
    }

    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.contextName = name;
    fcall.contextType = memory::ContextType::Run;

    il->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code != il::Signals::NoError) {
            il->vm->signal(ret.signal);
            return false;
        }

        m_newContext = il->factory->fromValue(il, ret.returnValue);
        return false;
    });

    executed = true;
    return StepType::CommandIn;
}

int RunContext::role() {
    return Role::RunContext;
}

const icSet<int> & RunContext::acceptedPrevs() {
    static const icSet<int> roles{
      Role::NoRole, Role::Value,   Role::Else,           Role::Slot,
      Role::Wait,   Role::Do,      Role::LimitedContext, Role::ValueContext,
      Role::Type,   Role::Emitter, Role::Jammer};
    return roles;
}

const icSet<int> & RunContext::acceptedNexts() {
    static const icSet<int> roles{Role::NoRole, Role::Case, Role::Else,
                                   Role::Slot, Role::While};
    return roles;
}

}  // namespace icL::core::ce
