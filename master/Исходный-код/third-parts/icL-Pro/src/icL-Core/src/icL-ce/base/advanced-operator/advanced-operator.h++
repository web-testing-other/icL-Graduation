#ifndef core_ce_AdvancedOperator
#define core_ce_AdvancedOperator

#include "../main/operator.h++"


namespace icL::core::ce {

class icL_core_ce_base_EXPORT AdvancedOperator : public Operator
{
public:
    AdvancedOperator(il::InterLevel * il);
};

}  // namespace icL::core::ce

#endif  // core_ce_AdvancedOperator
