#include "for-ever.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/export/signals.h++>
#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/operators-ALU/system/context.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

using il::Signals::Signals;

ForEver::ForEver(il::InterLevel * il, int xTimes)
    : ForBase(il)
    , service::ForEver(xTimes) {}

StepType ForEver::runNow() {
    return transact();
}

icString ForEver::toString() {
    icString str = "for";

    if (xTimes != -1) {
        str += '-' % icString::number(xTimes) % "times";
    }
    else {
        str += "-ever";
    }

    return str;
}

void ForEver::initialize() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    if (xTimes == -1) {
        if (commands.length() == 1) {
            condition = commands[0];
            body      = dynamic_cast<Context *>(m_next->next())->getCode();
        }
        else if (commands.length() != 0) {
            il->vm->signal({Signals::System,
                            "for:ever: round brackets content must contains a "
                            "condition or be empty"});
        }
    }
    else {
        if (commands.length() != 0) {
            il->vm->signal(
              {Signals::System,
               "for:Xtimes: round brackets content must be empty"});
        }
        else {
            body = dynamic_cast<Context *>(m_next->next())->getCode();
        }
    }
}

void ForEver::finalize() {
    m_newContext = il->factory->fromValue(il, retValue);
}

}  // namespace icL::core::ce
