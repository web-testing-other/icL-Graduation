#ifndef core_ce_ValueContext
#define core_ce_ValueContext

#include "context.h++"



namespace icL::core::ce {

class icL_core_ce_operators_ALU_EXPORT ValueContext : public Context
{
public:
    ValueContext(il::InterLevel * il, const il::CodeFragment & code);

    // CE interface
public:
    icString toString() override;
    StepType runNow() override;
    int      role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ValueContext
