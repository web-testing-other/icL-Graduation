#ifndef core_ce_Listen
#define core_ce_Listen

#include <icL-ce/base/keyword/advanced-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Listen : public AdvancedKeyword
{
public:
    Listen(il::InterLevel * il);
};

}  // namespace icL::core::ce

#endif  // core_ce_Listen
