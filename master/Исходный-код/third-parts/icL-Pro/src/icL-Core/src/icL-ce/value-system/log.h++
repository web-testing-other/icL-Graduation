#ifndef core_ce_Log
#define core_ce_Log

#include <icL-service/value-system/log.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::core::ce {

class icL_core_ce_value_system_EXPORT Log
    : public SystemValue
    , public service::Log
{
public:
    Log(il::InterLevel * il);

    // methods level 2

    /// `Log.error`
    void runError(const memory::ArgList & args);

    /// `Log.info`
    void runInfo(const memory::ArgList & args);

    /// `Log.out`
    void runOut(const memory::ArgList & args);

    /// `Log.stack`
    void runStack(const memory::ArgList & args);

    /// `Log.state`
    void runState(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Log
