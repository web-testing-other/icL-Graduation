#ifndef core_ce_AdvancedKeyword
#define core_ce_AdvancedKeyword

#include "../main/keyword.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT AdvancedKeyword : public Keyword
{
public:
    AdvancedKeyword(il::InterLevel * il);
};

}  // namespace icL::core::ce

#endif  // core_ce_AdvancedKeyword
