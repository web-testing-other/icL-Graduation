#include "bool-value.h++"

namespace icL::core::ce {

BoolValue::BoolValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

BoolValue::BoolValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

BoolValue::BoolValue(BaseValue * value)
    : BaseValue(value) {}

Type BoolValue::type() const {
    return Type::BoolValue;
}

icString BoolValue::typeName() {
    return "bool";
}

}  // namespace icL::core::ce
