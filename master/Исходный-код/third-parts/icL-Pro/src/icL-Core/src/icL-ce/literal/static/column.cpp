#include "column.h++"

#include <icL-il/main/interlevel.h++>

#include <icL-memory/structures/packed-value-item.h++>

#include <utility>

namespace icL::core::ce {

Column::Column(il::InterLevel * il, const icString & name, Type type)
    : StaticLiteral(il)
    , name(name)
    , type(type) {}

const icString & Column::getName() {
    return name;
}

int Column::getType() {
    return type;
}

icString Column::toString() {
    return "column[" % name % " : " % il->factory->typeToString(type) % ']';
}

int Column::role() {
    return Role::Column;
}

memory::PackedValueItem Column::packNow() const {
    memory::PackedValueItem ret;

    ret.itemType = il::PackableType::Column;
    ret.name     = name;
    ret.type     = type;

    return ret;
}

il::PackableType Column::packableType() const {
    return il::PackableType::Column;
}

}  // namespace icL::core::ce
