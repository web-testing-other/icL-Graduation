#include "limited-context.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-ce/value-base/complex/list-value.h++>
#include <icL-ce/value-base/complex/object-value.h++>
#include <icL-ce/value-base/complex/set-value.h++>

#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

LimitedContext::LimitedContext(
  il::InterLevel * il, const il::CodeFragment & code)
    : Context(il, code) {}

void LimitedContext::tryToMakeSomething(const memory::PackedItems & items) {
    if (checkItems(items, il::PackableType::Value, il::PackableType::Value)) {
        if (checkItems(items, Type::StringValue, Type::ListValue)) {
            m_newContext = il->factory->fromValue(il, makeList(items));
        }
        else if (checkItems(items, Type::ObjectValue, Type::SetValue)) {
            m_newContext = il->factory->fromValue(il, makeSet(items));
        }
    }
    else if (checkItems(
               items, il::PackableType::Field, il::PackableType::Value)) {
        m_newContext = il->factory->fromValue(il, makeObject(items));
    }
    else if (checkItems(
               items, il::PackableType::Column, il::PackableType::Type)) {
        m_newContext = il->factory->fromValue(il, makeEmptySet(items));
    }
    else if (items.isEmpty()) {
        m_newContext = il->factory->fromValue(il, icStringList{});
    }
}

icString LimitedContext::toString() {
    return "[..]";
}

StepType LimitedContext::runNow() {
    if (executed) {
        return StepType::CommandEnd;
    }

    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.contextType = memory::ContextType::Limited;

    il->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code != il::Signals::NoError) {
            il->vm->signal(ret.signal);
            return false;
        }

        memory::PackedValue packed;

        if (ret.consoleValue.is(icType::Packed)) {
            packed = ret.consoleValue;
        }
        else {
            packed.data = std::make_shared<memory::PackedItems>();

            if (ret.consoleValue.isValid()) {
                memory::PackedValueItem packedItem;

                packedItem.itemType = il::PackableType::Value;
                packedItem.value    = ret.consoleValue;
                packedItem.type = il->factory->variantToType(ret.consoleValue);

                packed.data->append(packedItem);
            }
        }

        tryToMakeSomething(*packed.data);

        if (m_newContext == nullptr) {
            il->vm->signal({il::Signals::System, "No such operator []"});
        }
        return false;
    });

    executed = true;
    return StepType::CommandIn;
}

int LimitedContext::role() {
    return Role::LimitedContext;
}

const icSet<int> & LimitedContext::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole,   Role::Any,    Role::Switch,
                                   Role::Case,     Role::For,    Role::Filter,
                                   Role::Range,    Role::Assign, Role::Comma,
                                   Role::Operator, Role::Method};
    return roles;
}

const icSet<int> & LimitedContext::acceptedNexts() {
    static const icSet<int> roles{
      Role::NoRole,     Role::Method, Role::Property, Role::Comma,
      Role::RunContext, Role::Cast,   Role::Operator};
    return roles;
}

}  // namespace icL::core::ce
