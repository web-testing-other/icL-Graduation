#include "signal.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-own-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

Signal::Signal(il::InterLevel * il)
    : SystemValue(il) {}

void Signal::runPropertyLevel2(const icString & name) {
    m_newContext = il->factory->fromValue(il, property(name));
}

void Signal::runAdd(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        add(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
}

Type Signal::type() const {
    return Type::Signal;
}

icString Signal::typeName() {
    return "Signal";
}

void Signal::runProperty(Prefix prefix, const icString & name) {
    runOwnPropertyWithPrefixCheck<Signal, SystemValue>(
      &Signal::runPropertyLevel2, prefix, name);
}

void Signal::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Signal::*)(const memory::ArgList &)>
      methods{{"add", &Signal::runAdd}};

    runMethodNow<Signal, SystemValue>(methods, name, args);
}

}  // namespace icL::core::ce
