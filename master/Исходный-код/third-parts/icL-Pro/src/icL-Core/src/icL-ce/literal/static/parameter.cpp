#include "parameter.h++"

#include <icL-il/main/interlevel.h++>

#include <icL-memory/structures/packed-value-item.h++>

#include <utility>

namespace icL::core::ce {

Parameter::Parameter(il::InterLevel * il, const icString & name, Type type)
    : StaticLiteral(il)
    , name(name)
    , type(type) {}

const icString & Parameter::getName() {
    return name;
}

Type Parameter::getType() {
    return type;
}

bool Parameter::isDefault() const {
    return false;
}

icString Parameter::toString() {
    return "parameter[" % name % " : " % il->factory->typeToString(type) % ']';
}

int Parameter::role() {
    return Role::Parameter;
}

memory::PackedValueItem Parameter::packNow() const {
    memory::PackedValueItem ret;

    ret.itemType = il::PackableType::Parameter;
    ret.name     = name;
    ret.type     = type;

    return ret;
}

il::PackableType Parameter::packableType() const {
    return il::PackableType::Parameter;
}

}  // namespace icL::core::ce
