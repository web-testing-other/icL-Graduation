#include "not-contains.h++"

#include <icL-ce/value-base/base/bool-value.h++>

namespace icL::core::ce {

NotContains::NotContains(il::InterLevel * il)
    : Contains(il) {}

icString NotContains::toString() {
    return "!<";
}

void NotContains::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    Contains::run(left, right);

    if (m_newContext == nullptr) {
        return;
    }

    auto * value = dynamic_cast<il::PackableValue *>(m_newContext);

    value->reset(!value->value().toBool());
}

}  // namespace icL::core::ce
