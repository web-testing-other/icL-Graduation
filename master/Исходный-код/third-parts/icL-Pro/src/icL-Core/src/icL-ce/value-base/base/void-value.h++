#ifndef core_ce_VoidValue
#define core_ce_VoidValue

#include <icL-ce/base/value/base-value.h++>



namespace icL::core::ce {

/**
 * @brief The VoidValue class represents a `void` value or `~` token
 */
class icL_core_ce_value_base_EXPORT VoidValue : public BaseValue
{
public:
    VoidValue(il::InterLevel * il);

    /// @brief VoidValue calls BaseValue(il, container, varName, readonly)
    VoidValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief VoidValue calls BaseValue(il, rvalue)
    VoidValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief VoidValue calls BaseValue(value)
    VoidValue(BaseValue * value);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;

    void runProperty(Prefix, const icString & name) override;
    void runMethod(const icString &, const memory::ArgList &) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_VoidValue
