#include "double.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>



namespace icL::core::ce {

Double::Double(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icVariant Double::getValueOf() {
    double ret;
    bool   ok;

    ret = pattern.toDouble(ok);

    if (!ok) {
        il->vm->signal({il::Signals::System, "Wrong double literal"});
    }

    return ret;
}

void Double::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Number);
}

}  // namespace icL::core::ce
