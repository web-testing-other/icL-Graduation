#ifndef core_ce_Overload
#define core_ce_Overload

#include <icL-ce/base/alu-operator/system-operator.h++>



namespace icL::core::ce {

/**
 * @brief The Overload class represent `:=` token
 */
class icL_core_ce_operators_ALU_EXPORT Overload : public SystemOperator
{
public:
    Overload(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    icString toString() override;
    StepType runNow() override;

    // CE interface
public:
    int role() override;

protected:
    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Overload
