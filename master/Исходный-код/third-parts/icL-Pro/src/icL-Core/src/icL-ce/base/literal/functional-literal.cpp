#include "functional-literal.h++"

namespace icL::core::ce {

FunctionalLiteral::FunctionalLiteral(il::InterLevel * il)
    : Literal(il) {}

}  // namespace icL::core::ce
