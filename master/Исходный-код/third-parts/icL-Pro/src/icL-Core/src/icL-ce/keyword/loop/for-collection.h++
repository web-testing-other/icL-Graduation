#ifndef core_ce_ForCollection
#define core_ce_ForCollection

#include "for-base.h++"

#include <icL-service/keyword/loop/for-collection.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT ForCollection
    : public ForBase
    , public service::ForCollection
{
public:
    ForCollection(il::InterLevel * il, bool reverse, int maxX);

    // CE interface
public:
    StepType runNow() override;
    icString toString() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ForCollection
