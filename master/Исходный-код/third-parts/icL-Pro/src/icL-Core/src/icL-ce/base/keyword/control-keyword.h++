#ifndef core_ce_ControlKeyword
#define core_ce_ControlKeyword

#include "../main/keyword.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT ControlKeyword : public Keyword
{
public:
    ControlKeyword(il::InterLevel * il);

    // CE interface
protected:
    il::CE * lastToReplace() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ControlKeyword
