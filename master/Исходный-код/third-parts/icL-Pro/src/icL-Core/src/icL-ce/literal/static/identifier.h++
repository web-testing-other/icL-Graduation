#ifndef core_ce_Identifier
#define core_ce_Identifier

#include <icL-ce/base/literal/static-literal.h++>


namespace icL::core::ce {

class icL_core_ce_literal_EXPORT Identifier : public StaticLiteral
{
public:
    Identifier(il::InterLevel * il, const icString & name);

    const icString & getName() const;

    // CE interface
public:
    icString toString() override;

    // CE interface
public:
    int  role() override;
    bool hasValue() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // PackableValue interface
    il::PackableType packableType() const override;

private:
    /// \brief name is the name of identifier
    icString name;
};

}  // namespace icL::core::ce

#endif  // core_ce_Identifier
