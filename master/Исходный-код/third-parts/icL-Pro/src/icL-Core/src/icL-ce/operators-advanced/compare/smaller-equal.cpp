#include "smaller-equal.h++"

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

SmallerEqual::SmallerEqual(il::InterLevel * il)
    : CompareOperator(il) {}

void SmallerEqual::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
}

void SmallerEqual::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleDouble(left[0], right[0]));
}

icString SmallerEqual::toString() {
    return "<=";
}

void SmallerEqual::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (SmallerEqual::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::IntValue}, {Type::IntValue}}, &SmallerEqual::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &SmallerEqual::runDoubleDouble}};

    runNow<SmallerEqual>(operators, left, right);
}

}  // namespace icL::core::ce
