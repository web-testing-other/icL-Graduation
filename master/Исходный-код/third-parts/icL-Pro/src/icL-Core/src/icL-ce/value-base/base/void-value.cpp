#include "void-value.h++"

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>



namespace icL::core::ce {

VoidValue::VoidValue(il::InterLevel * il)
    : BaseValue(il, icVariant::makeVoid()) {}

VoidValue::VoidValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

VoidValue::VoidValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

VoidValue::VoidValue(BaseValue * value)
    : BaseValue(value) {}

Type VoidValue::type() const {
    return Type::VoidValue;
}

icString VoidValue::typeName() {
    return "void";
}

void VoidValue::runProperty(Prefix prefix, const icString & name) {
    if (prefix == Prefix::None) {
        BaseValue::runProperty(prefix, name);
    }

    // if property doesn't exist
    if (m_newContext == nullptr)
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
}

void VoidValue::runMethod(const icString &, const memory::ArgList &) {
    m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
}

}  // namespace icL::core::ce
