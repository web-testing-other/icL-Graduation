#include "function.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/cast/base/void-cast.h++>
#include <icL-service/main/args/listify.h++>

#include <icL-ce/base/main/value.h++>
#include <icL-ce/value-base/base/void-value.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/function-call.h++>

#include <utility>



namespace icL::core::ce {

Function::Function(il::InterLevel * il, const icString & name)
    : FunctionalLiteral(il)
    , name(name) {}

const icString & Function::getName() const {
    return name;
}

icString Function::toString() {
    return '$' % name;
}

int Function::currentRunRank(bool rtl) {
    bool runnable = m_next->role() == Role::Value;
    return !rtl && runnable ? 1 : -1;
}

StepType Function::runNow() {
    using namespace memory;

    if (executed) {
        return StepType::CommandEnd;
    }

    ArgList args = il->factory->listify(il, dynamic_cast<BaseValue *>(m_next));

    memory::Function func = il->mem->functions().getFunction(name, args);

    if (func.body.source == nullptr) {
        il->vm->signal({il::Signals::System, "No such function: $" + name});
        return StepType::None;
    }

    FunctionCall fcall;
    ArgValueList argValues;

    // Normal call
    if (func.paramList.length() == args.length()) {
        for (int i = 0; i < args.length(); i++) {
            Argument  arg   = args[i];
            Parameter param = func.paramList[i];
            ArgValue  argValue;

            argValue.name = param.name;

            if (arg.type == Type::VoidValue) {
                argValue.value = param.value;
            }
            else {
                argValue.value = arg.value;
            }

            argValues.append(argValue);
        }
    }
    // Acronim call
    else {
        for (int i = 0; i < args.length(); i++) {
            Parameter param = func.paramList[i];
            ArgValue  argValue;

            argValue.name = param.name;

            if (param.value.isNull()) {
                argValue.value = args[0].value;
            }
            else {
                argValue.value = param.value;
            }

            argValues.append(argValue);
        }
    }

    fcall.code = func.body;
    fcall.args = argValues;

    if (func.returnType != Type::VoidValue) {
        fcall.args.append(
          {"@", service::VoidCast::fromType(il, func.returnType)});
    }

    fcall.returnType = il->factory->typeToVarType(func.returnType);

    il->vms->interrupt(fcall, [this, func](const il::Return & ret) {
        auto * stack = il->mem->stackIt().stack();

        if (ret.signal.code != il::Signals::NoError) {
            il->vm->signal(ret.signal);
        }
        else if (func.returnType == Type::VoidValue) {
            m_newContext = il->factory->fromValue(il, {});
        }
        else if (ret.returnValue.isValid()) {
            if (
              il->factory->variantToType(ret.returnValue) == func.returnType) {
                m_newContext = il->factory->fromValue(il, ret.returnValue);
            }
            else {
                il->vm->syssig(
                  "Return value doesn't match the return type of function");
            }
        }
        else {
            m_newContext = il->factory->fromValue(il, stack->getStackValue());
        }
        return false;
    });

    executed = true;

    return StepType::CommandIn;
}

il::CE * Function::lastToReplace() {
    return m_next;
}

int Function::role() {
    return Role::Function;
}

void Function::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Function);
}

const icSet<int> & Function::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole, Role::Comma, Role::Operator,
                                   Role::Assign};
    return roles;
}

const icSet<int> & Function::acceptedNexts() {
    static const icSet<int> roles{Role::Value,        Role::SystemValue,
                                   Role::Overload,     Role::LimitedContext,
                                   Role::ValueContext, Role::RunContext,
                                   Role::Cast};
    return roles;
}

}  // namespace icL::core::ce
