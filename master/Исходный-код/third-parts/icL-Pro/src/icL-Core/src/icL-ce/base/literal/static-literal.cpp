#include "static-literal.h++"

#include <icL-memory/structures/type.h++>

#include <cassert>


namespace icL::core::ce {

int StaticLiteral::currentRunRank(bool) {
    return -1;
}

StepType StaticLiteral::runNow() {
    return StepType::None;
}

bool StaticLiteral::hasValue() {
    return true;
}

icVariant StaticLiteral::value() const {
    memory::PackedValue value;

    value.data = std::make_shared<memory::PackedItems>();
    value.data->append(packNow());

    return value;
}

void StaticLiteral::colorize() {
    return;
}

const icSet<int> & StaticLiteral::acceptedPrevs() {
    static const icSet<int> accepted{Role::NoRole, Role::Comma, Role::Cast};
    return accepted;
}

const icSet<int> & StaticLiteral::acceptedNexts() {
    static const icSet<int> accepted{Role::NoRole, Role::RunContext,
                                     Role::Comma};
    return accepted;
}

memory::PackedValueItem StaticLiteral::packNow() const {
    assert(false);
}

il::PackableType StaticLiteral::packableType() const {
    assert(false);
    return il::PackableType::Type;
}

Type StaticLiteral::type() const {
    assert(false);
    return Type::Last;
}

void StaticLiteral::reset(const icVariant & /*value*/) {
    assert(false);
    return;
}

}  // namespace icL::core::ce
