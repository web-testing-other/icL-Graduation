#ifndef core_ce_Value_runPropertyWithIndexCheck
#define core_ce_Value_runPropertyWithIndexCheck

#include "value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <functional>

namespace icL::core::ce {

template <typename This, typename ParentClass>
void Value::runPropertyWithIndexCheck(
  const icObject<icString, void (This::*)()> & properties, Prefix prefix,
  const icString & name, const std::function<icVariant(int)> & at) {
    auto this_ = dynamic_cast<This *>(this);

    if (prefix == Prefix::None) {
        bool isInt;
        int  nameAsInt = name.toInt(isInt);

        if (isInt) {
            m_newContext = il->factory->fromValue(il, at(nameAsInt));
        }
        else {
            auto it = properties.find(name);

            if (it != properties.end()) {
                (this_->*it.value())();
            }
            else {
                this_->ParentClass::runProperty(prefix, name);
            }
        }
    }
    else {
        this_->ParentClass::runProperty(prefix, name);
    }
}

}  // namespace icL::core::ce

#endif  // core_ce_Value_runPropertyWithIndexCheck
