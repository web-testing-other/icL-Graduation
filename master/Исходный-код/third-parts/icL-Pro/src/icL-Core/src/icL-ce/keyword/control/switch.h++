#ifndef core_ce_Switch
#define core_ce_Switch

#include <icL-service/keyword/control/switch.h++>

#include <icL-ce/base/keyword/control-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Switch
    : public ControlKeyword
    , public service::Switch
{
public:
    Switch(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Switch
