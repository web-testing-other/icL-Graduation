#ifndef core_ce_AluOperator
#define core_ce_AluOperator

#include "../main/operator.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT AluOperator : public Operator
{
public:
    AluOperator(il::InterLevel * il);
};

}  // namespace icL::core::ce

#endif  // core_ce_AluOperator
