#include "slot.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/stateful-server.h++>

#include <cassert>



namespace icL::core::ce {

Slot::Slot(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Slot::toString() {
    icString str = "slot";

    for (auto & code : handleCodes) {
        str += '-' + icString::number(code);
    }

    return str;
}

int Slot::currentRunRank(bool) {
    return -1;
}

StepType Slot::runNow() {
    // Never to be executed
    assert(false);
    return StepType::MiniStep;
}

int Slot::role() {
    return Role::Slot;
}

const icSet<int> & Slot::acceptedPrevs() {
    static const icSet<int> roles{Role::RunContext};
    return roles;
}

const icSet<int> & Slot::acceptedNexts() {
    static const icSet<int> roles{Role::RunContext};
    return roles;
}

void Slot::giveModifiers(const icStringList & modifiers) {
    for (auto & mod : modifiers) {
        handleCodes.append(il->stateful->getSignal(mod));
    }
}

bool Slot::check(int code) {
    for (auto & handler : handleCodes) {
        if (handler == code || handler == 0) {
            return true;
        }
    }
    return false;
}

}  // namespace icL::core::ce
