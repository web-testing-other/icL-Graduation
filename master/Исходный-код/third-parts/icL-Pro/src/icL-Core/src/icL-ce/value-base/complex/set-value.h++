#ifndef core_ce_SetValue
#define core_ce_SetValue

#include <icL-service/value-base/complex/set-value.h++>

#include <icL-ce/base/value/base-value.h++>


class icRect;
template <typename>
class icList;

namespace icL::core {

namespace memory {
struct Set;
struct Object;
}  // namespace memory

namespace ce {

/**
 * @brief The SetValue class represents a `set` value
 */
class icL_core_ce_value_base_EXPORT SetValue
    : public BaseValue
    , public service::SetValue
{
public:
    /// @brief SetValue calls BaseValue(il, container, varName, readonly)
    SetValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief SetValue calls BaseValue(il, rvalue)
    SetValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief SetValue calls BaseValue(value)
    SetValue(BaseValue * value);

    /// @brief SetValue calls BaseValue(il, rvalue)
    SetValue(il::InterLevel * il, const icList<icRect> & rects);

    // properties level 2

    /// `set'capacity`
    void runCapacity();

    /// `set'empty`
    void runEmpty();

    // methods level 2

    /// `set.applicate`
    void runApplicate(const memory::ArgList & args);

    /// `set.clear`
    void runClear(const memory::ArgList & args);

    /// `set.clone`
    void runClone(const memory::ArgList & args);

    /// `set.getField`
    void runGetField(const memory::ArgList & args);

    /// `set.hasField`
    void runHasField(const memory::ArgList & args);

    /// `set.insert`
    void runInsert(const memory::ArgList & args);

    /// `set.insertField`
    void runInsertField(const memory::ArgList & args);

    /// `set.remove`
    void runRemove(const memory::ArgList & args);

    /// `set.removeField`
    void runRemoveField(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_SetValue
