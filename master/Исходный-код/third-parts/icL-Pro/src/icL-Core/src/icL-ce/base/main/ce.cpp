#include "ce.h++"

#include "base-value.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>

#include <cassert>



namespace icL::core::ce {

CE::CE(il::InterLevel * il)
    : Node(il) {}

il::CE * CE::newCE() {
    return m_newContext;
}

BaseValue * CE::asValue() {
    return dynamic_cast<BaseValue *>(this);
}

il::CE * CE::getFirst() {
    il::CE * it = this;

    while (it->prev() != nullptr) {
        it = it->prev();
    }

    return it;
}

il::CE * CE::getLast() {
    il::CE * it = this;

    while (it->next() != nullptr) {
        it = it->next();
    }

    return it;
}

il::InterLevel * CE::core() {
    return il;
}

bool CE::checkPrev(il::CE * ilCE) {
    m_prev = dynamic_cast<CE *>(ilCE);

    const auto & accepted = acceptedPrevs();

    int role = m_prev != nullptr ? m_prev->role() : Role::NoRole;

    return accepted.contains(role);
}

bool CE::checkNext(il::CE * ilCE) {
    const auto & accepted = acceptedNexts();

    CE * ce   = dynamic_cast<CE *>(ilCE);
    int  role = ce != nullptr ? ce->role() : Role::NoRole;

    return accepted.contains(role);
}

il::CE * CE::firstToReplace() {
    return this;
}

il::CE * CE::lastToReplace() {
    return this;
}

void CE::release() {
    il::CE * first = firstToReplace();
    il::CE * last  = lastToReplace();

    if (m_newContext == nullptr) {
        m_newContext = il->factory->fromValue(il, {});
        assert(false);
    }

    m_newContext->fragmentData()     = first->fragmentData();
    m_newContext->fragmentData().end = last->fragmentData().end;
}

il::CE *& CE::prev() {
    return m_prev;
}

il::CE *& CE::next() {
    return m_next;
}

void CE::linkNode(il::CE * node) {

    node->next() = nullptr;
    node->prev() = this;
    this->m_next = node;
}

void CE::replaceNode(il::CE * node) {

    this->m_prev = node->prev();
    this->m_next = node->next();

    if (node->prev() != nullptr) {
        node->prev()->next() = this;
    }
    if (node->next() != nullptr) {
        node->next()->prev() = this;
    }

    delete node;
}

void CE::replaceNodes(il::CE * begin, il::CE * end) {

    if (begin->prev() != nullptr) {
        begin->prev()->next() = this;
    }

    if (end->next() != nullptr) {
        end->next()->prev() = this;
    }

    this->m_next = end->next();
    this->m_prev = begin->prev();

    end->next() = nullptr;

    while (begin != nullptr) {
        auto * next = begin->next();

        delete begin;
        begin = next;
    }
}

il::CodeFragment & CE::fragmentData() {
    return m_fragmentData;
}

bool CE::hasValue() {
    return false;
}

}  // namespace icL::core::ce
