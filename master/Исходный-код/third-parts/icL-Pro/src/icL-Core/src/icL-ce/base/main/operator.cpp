#include "operator.h++"

#include "operator-run-now.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/vmstack.h++>

#include <icL-service/main/args/listify.h++>

#include <icL-ce/base/value/base-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/function-call.h++>



namespace icL::core::ce {

Operator::Operator(il::InterLevel * il)
    : CE(il) {}

const icSet<int> & Operator::byContext(
  const icSet<int> & run, const icSet<int> & limited,
  const icSet<int> & value) {
    using memory::ContextType::ContextType;

    switch (il->vm->get(0).toInt()) {
    case ContextType::Run:
        return run;

    case ContextType::Limited:
        return limited;

    case ContextType::Value:
        return value;
    }

    return run;
}

int Operator::role() {
    return Role::Operator;
}

StepType Operator::runNow() {
    auto * leftPtr  = dynamic_cast<BaseValue *>(m_prev);
    auto * rightPtr = dynamic_cast<BaseValue *>(m_next);

    memory::ArgList left;
    memory::ArgList right;

    if (leftPtr != nullptr) {
        left = il->factory->listify(il, leftPtr);
    }

    if (rightPtr != nullptr) {
        right = il->factory->listify(il, rightPtr);
    }

    run(left, right);
    return StepType::CommandEnd;
}

const icSet<int> & Operator::acceptedPrevs() {
    static const icSet<int> roles{
      Role::Value,    Role::Property, Role::LimitedContext, Role::ValueContext,
      Role::Function, Role::JsValue,  Role::Operator};
    return roles;
}

const icSet<int> & Operator::acceptedNexts() {
    static const icSet<int> roles{Role::Value,        Role::SystemValue,
                                   Role::Function,     Role::LimitedContext,
                                   Role::ValueContext, Role::JsValue,
                                   Role::Operator};
    return roles;
}

il::CE * Operator::firstToReplace() {
    return m_prev;
}

il::CE * Operator::lastToReplace() {
    return m_next;
}

void Operator::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Operator);
}

}  // namespace icL::core::ce
