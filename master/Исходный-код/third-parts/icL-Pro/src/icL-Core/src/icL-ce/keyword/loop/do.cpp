#include "do.h++"

#include <icL-types/replaces/ic-set.h++>

namespace icL::core::ce {

Do::Do(il::InterLevel * il)
    : LoopKeyword(il) {}

icString Do::toString() {
    return "do";
}

int Do::currentRunRank(bool) {
    return -1;
}

StepType Do::runNow() {
    return StepType::None;
}

int Do::role() {
    return Role::Do;
}

const icSet<int> & Do::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole};
    return roles;
}

const icSet<int> & Do::acceptedNexts() {
    static const icSet<int> roles{Role::RunContext};
    return roles;
}

}  // namespace icL::core::ce
