#ifndef core_ce_Keyword
#define core_ce_Keyword

#include "ce.h++"

class icStringList;



namespace icL::core::ce {

class icL_core_ce_base_EXPORT Keyword : public CE
{
public:
    Keyword(il::InterLevel * il);

    /**
     * @brief giveModifiers gives modifiers from tokenizer `:mod :[mod1,mod1]`
     * @param modifiers is a icList of modifiers as strings
     */
    virtual void giveModifiers(const icStringList & modifiers);

    // il.CE interface
public:
    void colorize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Keyword
