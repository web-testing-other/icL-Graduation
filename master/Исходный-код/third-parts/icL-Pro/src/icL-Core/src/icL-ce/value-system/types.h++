#ifndef core_ce_Types
#define core_ce_Types

#include <icL-ce/base/value/system-value.h++>



namespace icL::core::ce {

/**
 * @brief The Types class represents `Types` token
 */
class icL_core_ce_value_system_EXPORT Types : public SystemValue
{
public:
    Types(il::InterLevel * il);

    // properties level 2

    /// `Types'bool`
    void runBool();

    /// `Types'double`
    void runDouble();

    /// `Types'int`
    void runInt();

    /// `Types'lambda-icl`
    void runLambda_icL();

    /// `Types'list`
    void runList();

    /// `Types'object`
    void runObject();

    /// `Types'set`
    void runSet();

    /// `Types'string`
    void runString();

    /// `Types'void`
    void runVoid();

    // Value interface
public:
    Type type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Types
