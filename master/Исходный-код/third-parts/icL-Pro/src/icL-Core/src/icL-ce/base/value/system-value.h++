#ifndef core_ce_SystemValue
#define core_ce_SystemValue

#include "../main/value.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT SystemValue : public Value
{
public:
    SystemValue(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    void colorize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_SystemValue
