#ifndef core_ce_Remainder
#define core_ce_Remainder

#include <icL-service/operators-ALU/arithmetical/remainder.h++>

#include <icL-ce/base/alu-operator/arithmetical-operator.h++>



namespace icL::core {

namespace memory {
struct Set;
}

namespace ce {

class icL_core_ce_operators_ALU_EXPORT Remainder
    : public ArithmeticalOperator
    , public service::Remainder
{
public:
    Remainder(il::InterLevel * il);

    // level 2

    /// `int \ int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `set \ set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_Remainder
