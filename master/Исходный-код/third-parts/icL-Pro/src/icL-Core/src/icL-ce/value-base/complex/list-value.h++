#ifndef core_ce_ListValue
#define core_ce_ListValue

#include <icL-service/value-base/complex/list-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL::core::ce {

/**
 * @brief The ListValue class represents a `list` value
 */
class icL_core_ce_value_base_EXPORT ListValue
    : public BaseValue
    , public service::ListValue
{
public:
    /// @brief ListValue calls BaseValue(il, container, varName, readonly)
    ListValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief ListValue calls BaseValue(il, rvalue)
    ListValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief ListValue calls BaseValue(value)
    ListValue(BaseValue * value);

    // properties level 2

    /// `list'empty`
    void runEmpty();

    /// `list'length`
    void runLength();

    /// `list'last`
    void runLast();

    // methods level 2

    /// `list.append`
    void runAppend(const memory::ArgList & args);

    /// `list.at`
    void runAt(const memory::ArgList & args);

    /// `list.contains`
    void runContains(const memory::ArgList & args);

    /// `list.clear`
    void runClear(const memory::ArgList & args);

    /// `list.count`
    void runCount(const memory::ArgList & args);

    /// `list.filter`
    void runFilter(const memory::ArgList & args);

    /// `list.indexOf`
    void runIndexOf(const memory::ArgList & args);

    /// `list.insert`
    void runInsert(const memory::ArgList & args);

    /// `list.join`
    void runJoin(const memory::ArgList & args);

    /// `list.lastIndexOf`
    void runLastIndexOf(const memory::ArgList & args);

    /// `list.mid`
    void runMid(const memory::ArgList & args);

    /// `list.prepend`
    void runPrepend(const memory::ArgList & args);

    /// `list.move`
    void runMove(const memory::ArgList & args);

    /// `list.removeAll`
    void runRemoveAll(const memory::ArgList & args);

    /// `list.removeAt`
    void runRemoveAt(const memory::ArgList & args);

    /// `list.removeDublicates`
    void runRemoveDublicates(const memory::ArgList & args);

    /// `list.removeFirst`
    void runRemoveFirst(const memory::ArgList & args);

    /// `list.removeLast`
    void runRemoveLast(const memory::ArgList & args);

    /// `list.removeOne`
    void runRemoveOne(const memory::ArgList & args);

    /// `list.replaceInStrings`
    void runReplaceInStrings(const memory::ArgList & args);

    /// `list.sort`
    void runSort(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ListValue
