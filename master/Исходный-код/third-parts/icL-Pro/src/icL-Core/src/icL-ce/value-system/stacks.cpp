#include "stacks.h++"

#include "stack.h++"

#include <icL-ce/base/main/value-run-own-property-with-prefix-check.h++>



namespace icL::core::ce {

Stacks::Stacks(il::InterLevel * il)
    : SystemValue(il) {}

void Stacks::runPropertyLevel2(const icString & name) {
    m_newContext = new Stack{il, property(name)};
}

Type Stacks::type() const {
    return Type::Stacks;
}

icString Stacks::typeName() {
    return "Stacks";
}

void Stacks::runProperty(Prefix prefix, const icString & name) {
    runOwnPropertyWithPrefixCheck<Stacks, SystemValue>(
      &Stacks::runPropertyLevel2, prefix, name);
}

}  // namespace icL::core::ce
