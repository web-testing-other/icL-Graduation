#include "if-exists.h++"

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce/operators-ALU/system/context.h++>

#include <exists.h++>



namespace icL::core::ce {

IfExists::IfExists(il::InterLevel * il, bool notMod)
    : IfBase(il)
    , service::IfExists(notMod) {}

StepType IfExists::runNow() {
    return transact();
}

void IfExists::initialize() {
    bool ok   = true;
    CE * it   = this;
    int  prev = Role::NoRole;

    while (it != nullptr && ok) {
        int next = it->next() == nullptr
                     ? Role::NoRole
                     : dynamic_cast<CE *>(it->next())->role();

        switch (it->role()) {
        case Role::If:
            ok = prev == Role::NoRole || prev == Role::Else;
            break;

        case Role::Exists:
            ok = prev == Role::If;
            break;

        case Role::Value:
        case Role::ValueContext:
            ok = prev == Role::Exists;
            break;

        case Role::RunContext:
            ok = prev == Role::Value || prev == Role::ValueContext ||
                 (prev == Role::Else && next == Role::NoRole);
            break;

        case Role::Else:
            ok = prev == Role::RunContext;
            break;

        default:
            ok = false;
        }

        prev = it->role();
        it   = dynamic_cast<CE *>(it->next());
    }

    if (!ok || prev != Role::RunContext) {
        il->vm->syssig("Syntax error in if-exists-else statement");
        return;
    }

    it = this;

    while (it != nullptr) {
        if (it->role() == Role::If) {
            IfData ifData;

            auto * exists = dynamic_cast<Exists *>(it->next());
            auto * value  = dynamic_cast<CE *>(exists->next());
            auto * runCx  = dynamic_cast<Context *>(value->next());

            ifData.notModifer = dynamic_cast<IfExists *>(it)->notModifier;
            ifData.exists     = exists;
            ifData.body       = runCx->getCode();

            if (!ifs.isEmpty() && ifs.last().notModifer) {
                il->vm->syssig(
                  "`if:not exists` can not be succeeded by if-exists");
            }
            else {
                ifs.append(ifData);
            }
            it = dynamic_cast<CE *>(runCx->next());
        }
        else /* role == else */ {
            if (ifs.last().notModifer) {
                il->vm->syssig("`if:not exists` can not be succeeded by else");
            }
            else {
                elseCode = dynamic_cast<Context *>(it->next())->getCode();
                it       = nullptr;
            }
        }
    }
}

void IfExists::finalize() {
    m_newContext = il->factory->fromValue(il, value);
}

bool IfExists::hasNotModifier() {
    return notModifier;
}

void IfExists::setNotModifier() {
    notModifier = true;
}



}  // namespace icL::core::ce
