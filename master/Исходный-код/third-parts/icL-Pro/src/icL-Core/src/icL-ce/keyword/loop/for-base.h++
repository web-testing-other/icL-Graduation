#ifndef core_ce_ForBase
#define core_ce_ForBase

#include <icL-service/keyword/loop/loop.h++>

#include <icL-ce/base/keyword/loop-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT ForBase : public LoopKeyword
{
public:
    ForBase(il::InterLevel * il);

    // il.CE interface
public:
    int currentRunRank(bool rtl) override;

private:
    // ce.CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ForBase
