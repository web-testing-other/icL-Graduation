#ifndef core_ce_Slot
#define core_ce_Slot

#include <icL-types/replaces/ic-list.h++>

#include <icL-service/keyword/advanced/slot.h++>

#include <icL-ce/base/keyword/control-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Slot
    : public ControlKeyword
    , public service::Slot
{
public:
    Slot(il::InterLevel * il);
    ~Slot() override = default;

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

private:
    icList<int> handleCodes;

    // Slot interface
public:
    bool check(int code) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Slot
