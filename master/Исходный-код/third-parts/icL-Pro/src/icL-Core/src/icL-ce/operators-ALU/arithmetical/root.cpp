#include "root.h++"

#include <icL-types/replaces/ic-math.h++>

#include <icL-ce/base/main/operator-run-now.h++>
#include <icL-ce/value-base/base/double-value.h++>
#include <icL-ce/value-base/base/int-value.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::core::ce {

Root::Root(il::InterLevel * il)
    : AmbiguousArithmeticalOperator(il) {}

void Root::runInt(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, voidInt(right[0]));
}

void Root::runDouble(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, voidDouble(right[0]));
}

void Root::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
}

void Root::runIntDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intDouble(left[0], right[0]));
}

void Root::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleDouble(left[0], right[0]));
}

icString Root::toString() {
    return "/'";
}

void Root::run(const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (Root::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{}, {Type::IntValue}}, &Root::runInt},
        {{{}, {Type::DoubleValue}}, &Root::runDouble},
        {{{Type::IntValue}, {Type::IntValue}}, &Root::runIntInt},
        {{{Type::IntValue}, {Type::DoubleValue}}, &Root::runIntDouble},
        {{{Type::DoubleValue}, {Type::DoubleValue}}, &Root::runDoubleDouble}};

    runNow<Root>(operators, left, right);
}

int Root::runRank() {
    return 5;
}

int Root::runAbmiguousRank() {
    return 5;
}

}  // namespace icL::core::ce
