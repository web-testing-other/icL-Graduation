#include "field.h++"

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>

#include <utility>



namespace icL::core::ce {

Field::Field(
  il::InterLevel * il, const icString & name, const icVariant & value)
    : StaticLiteral(il)
    , name(name)
    , value(value) {}

const icString & Field::getName() {
    return name;
}

const icVariant & Field::getFieldValue() {
    return value;
}

icString Field::toString() {
    memory::Argument arg;
    arg.value = value;
    return "field[" % name % " = " %
           service::Stringify::argConstructor(il, arg, nullptr);
}

int Field::role() {
    return Role::Field;
}

memory::PackedValueItem Field::packNow() const {
    memory::PackedValueItem ret;

    ret.itemType = il::PackableType::Field;
    ret.name     = name;
    ret.value    = value;

    return ret;
}

il::PackableType Field::packableType() const {
    return il::PackableType::Field;
}

}  // namespace icL::core::ce
