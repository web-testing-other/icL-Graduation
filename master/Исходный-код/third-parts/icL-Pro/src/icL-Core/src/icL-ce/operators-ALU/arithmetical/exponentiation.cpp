#include "exponentiation.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>

namespace icL::core::ce {

Exponentiation::Exponentiation(il::InterLevel * il)
    : AmbiguousArithmeticalOperator(il) {}

void Exponentiation::runInt(
  const memory::ArgList & left, const memory::ArgList & /*right*/) {
    m_newContext = il->factory->fromValue(il, intVoid(left[0]));
}

void Exponentiation::runDouble(
  const memory::ArgList & left, const memory::ArgList & /*right*/) {
    m_newContext = il->factory->fromValue(il, doubleVoid(left[0]));
}

void Exponentiation::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
}

void Exponentiation::runDoubleInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleInt(left[0], right[0]));
}

void Exponentiation::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleDouble(left[0], right[0]));
}

void Exponentiation::runStringString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, stringString(left[0], right[0]));
}

void Exponentiation::runListList(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, listList(left[0], right[0]));
}

void Exponentiation::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, setSet(left[0], right[0]));
}

void Exponentiation::runStringRegex(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, stringRegex(left[0], right[0]));
}

int Exponentiation::currentRunRank(bool rtl) {
    bool leftIsValue  = m_prev->role() == Role::Value;
    bool rightIsValue = m_next != nullptr && m_next->role() == Role::Value;

    asAmbiguous = !rightIsValue && leftIsValue;

    return asAmbiguous && rtl
             ? runAbmiguousRank()
             : rightIsValue && leftIsValue && !rtl ? runRank() : -1;
}

il::CE * Exponentiation::firstToReplace() {
    return m_prev;
}

il::CE * Exponentiation::lastToReplace() {
    return asAmbiguous ? this : m_next;
}

const icSet<int> & Exponentiation::acceptedPrevs() {
    return icL::core::ce::ArithmeticalOperator::acceptedPrevs();
}

const icSet<int> & Exponentiation::acceptedNexts() {
    static const icSet<int> roles{ArithmeticalOperator::acceptedNexts(),
                                   {Role::NoRole, Role::Comma, Role::Operator}};
    return roles;
}

icString Exponentiation::toString() {
    return "**";
}

void Exponentiation::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (Exponentiation::*)(
        const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::IntValue}, {}}, &Exponentiation::runInt},
        {{{Type::DoubleValue}, {}}, &Exponentiation::runDouble},
        {{{Type::IntValue}, {Type::IntValue}}, &Exponentiation::runIntInt},
        {{{Type::DoubleValue}, {Type::IntValue}},
         &Exponentiation::runDoubleInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &Exponentiation::runDoubleDouble},
        {{{Type::StringValue}, {Type::StringValue}},
         &Exponentiation::runStringString},
        {{{Type::ListValue}, {Type::ListValue}}, &Exponentiation::runListList},
        {{{Type::SetValue}, {Type::SetValue}}, &Exponentiation::runSetSet},
        {{{Type::StringValue}, {Type::RegexValue}},
         &Exponentiation::runStringRegex}};

    runNow<Exponentiation>(operators, left, right);
}

int Exponentiation::runRank() {
    return 6;
}

int Exponentiation::runAbmiguousRank() {
    return 6;
}

}  // namespace icL::core::ce
