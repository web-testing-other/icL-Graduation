#ifndef core_ce_Biconditional
#define core_ce_Biconditional

#include <icL-ce/base/alu-operator/logic-operator.h++>



class icVariant;

namespace icL::core::ce {

/**
 * @brief The Biconditional class represents the beconditional token `~`
 */
class icL_core_ce_operators_ALU_EXPORT Biconditional : public LogicOperator
{
public:
    Biconditional(il::InterLevel * il);

    /**
     * @brief run applicates the logic operation biconditional
     * @param left is the left operand
     * @param right is the right operand
     * @return true if both are false or true, otherwise false
     */
    bool run(bool left, bool right);

    // CE interface
public:
    int currentRunRank(bool rtl) override;

    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // CE interface
public:
    icString toString() override;

    // Operator interface

    /**
     * @brief run applicates the secondary select operation
     */
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // fields
private:
    bool asVoidValue = false;

    // padding
    long : 56;
};

}  // namespace icL::core::ce

#endif  // core_ce_Biconditional
