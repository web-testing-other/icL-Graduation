#ifndef core_ce_Parameter
#define core_ce_Parameter

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce/base/literal/static-literal.h++>

#include <icL-memory/structures/type.h++>



namespace icL::core::ce {

/**
 * @brief The Parameter class represents a parameter token `@name = value`
 */
class icL_core_ce_literal_EXPORT Parameter : public StaticLiteral
{
public:
    Parameter(il::InterLevel * il, const icString & name, Type type);

    /**
     * @brief getName gets the name of the parameter
     * @return the name of the parameter
     */
    const icString & getName();

    /**
     * @brief getType gets the type of the parameter
     * @return the type of the parameter
     */
    Type getType();

    /**
     * @brief isDefault checks if is a default parameter
     * @return true if is a default parameter, otherwise false
     */
    virtual bool isDefault() const;

    // CE interface
public:
    icString toString() override;
    int      role() override;

    // PackableValue interface
    memory::PackedValueItem packNow() const override;
    il::PackableType        packableType() const override;

private:
    /// \brief name is the name of parameter
    icString name;
    /// \brief type is the type of parameter
    Type type;

    // padding
    int : 32;
};

}  // namespace icL::core::ce

#endif  // core_ce_Parameter
