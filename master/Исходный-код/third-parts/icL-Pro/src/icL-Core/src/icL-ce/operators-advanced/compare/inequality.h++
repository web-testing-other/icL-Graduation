#ifndef core_ce_Inequality
#define core_ce_Inequality

#include "equality.h++"



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT Inequality : public Equality
{
public:
    Inequality(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Inequality
