#include "contains.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

Contains::Contains(il::InterLevel * il)
    : IncludingOperator(il) {}

void Contains::runListString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, listString(left[0], right[0]));
}

void Contains::runStringString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, stringString(left[0], right[0]));
}

void Contains::runSetObject(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, setObject(left[0], right[0]));
}

void Contains::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, setSet(left[0], right[0]));
}

void Contains::runStringRegex(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, stringRegex(left[0], right[0]));
}

icString Contains::toString() {
    return "<<";
}

void Contains::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (Contains::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::ListValue}, {Type::StringValue}}, &Contains::runListString},
        {{{Type::StringValue}, {Type::StringValue}},
         &Contains::runStringString},
        {{{Type::SetValue}, {Type::ObjectValue}}, &Contains::runSetObject},
        {{{Type::SetValue}, {Type::SetValue}}, &Contains::runSetSet},
        {{{Type::StringValue}, {Type::RegexValue}}, &Contains::runStringRegex}};

    runNow<Contains>(operators, left, right);
}

}  // namespace icL::core::ce
