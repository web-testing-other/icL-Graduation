#ifndef core_ce_CastNow
#define core_ce_CastNow

#include "cast.h++"



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT CastNow : public Cast
{
public:
    CastNow(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // CastOperator interface
public:
    int runRank() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_CastNow
