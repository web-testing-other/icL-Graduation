#ifndef core_ce_CompareOperator
#define core_ce_CompareOperator

#include "advanced-operator.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT CompareOperator : public AdvancedOperator
{
public:
    CompareOperator(il::InterLevel * il);

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_CompareOperator
