#ifndef core_ce_LoopKeyword
#define core_ce_LoopKeyword

#include "../main/keyword.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT LoopKeyword : public Keyword
{
public:
    LoopKeyword(il::InterLevel * il);

    // CE interface
protected:
    il::CE * lastToReplace() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_LoopKeyword
