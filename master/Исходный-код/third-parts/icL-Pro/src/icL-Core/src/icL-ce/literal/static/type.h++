#ifndef core_ce_Type
#define core_ce_Type

#include <icL-ce/base/literal/static-literal.h++>

#include <icL-memory/structures/type.h++>



namespace icL::core::ce {

/**
 * @brief The Type class represent a type token - `type`
 */
class icL_core_ce_literal_EXPORT TypeToken : public StaticLiteral
{
public:
    TypeToken(il::InterLevel * il, int type);

    /**
     * @brief getType gets the contined type
     * @return the contained type
     */
    Type getType();

    // CE interface
public:
    icString toString() override;
    int      role() override;

    memory::PackedValueItem packNow() const override;

    void colorize() override;

protected:
    const icSet<int> & acceptedNexts() override;

private:
    /// \brief type is the contianed type
    Type type;
};

}  // namespace icL::core::ce

#endif  // core_ce_Type
