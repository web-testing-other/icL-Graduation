#include "while.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce/operators-ALU/system/context.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

While::While(il::InterLevel * il)
    : LoopKeyword(il) {}

icString While::toString() {
    icString str = "while";

    if (notMod)
        str += "-not";
    if (min != 0)
        str += "-min" % icString::number(min);
    if (max != -1)
        str += "-max" % icString::number(max);

    return str;
}

int While::currentRunRank(bool /*rtl*/) {
    bool whileOk =
      m_prev == nullptr && m_next->next() != nullptr &&
      dynamic_cast<CE *>(m_next->next())->role() == Role::RunContext &&
      m_next->next()->next() == nullptr;
    bool doWhileOk = m_prev != nullptr && m_prev->role() == Role::RunContext &&
                     m_prev->prev() != nullptr &&
                     dynamic_cast<CE *>(m_prev->prev())->role() == Role::Do &&
                     m_next->next() == nullptr;

    if (whileOk || doWhileOk) {
        doWhile = doWhileOk;
        return 9;
    }
    return -1;
}

StepType While::runNow() {
    return transact();
}

int While::role() {
    return Role::While;
}

il::CE * While::firstToReplace() {
    if (doWhile) {
        return dynamic_cast<CE *>(m_prev->prev());
    }

    return this;
}

const icSet<int> & While::acceptedPrevs() {
    static const icSet<int> roles1{Role::NoRole, Role::RunContext};
    static const icSet<int> roles2{Role::NoRole};
    return m_prev != nullptr && m_prev->prev() != nullptr &&
               dynamic_cast<CE *>(m_prev->prev())->role() == Role::Do
             ? roles1
             : roles2;
}

const icSet<int> & While::acceptedNexts() {
    static const icSet<int> roles{Role::ValueContext};
    return roles;
}

void While::initialize() {
    condition = dynamic_cast<Context *>(m_next)->getCode();

    if (doWhile) {
        loopBody = dynamic_cast<Context *>(m_prev)->getCode();
    }
    else {
        loopBody = dynamic_cast<Context *>(m_next->next())->getCode();
    }
}

void While::finalize() {
    m_newContext = il->factory->fromValue(il, executed);
}

void While::giveModifiers(const icStringList & modifiers) {
    static icRegEx minRx = R"(min(\d+))"_rx;
    static icRegEx maxRx = R"(max(\d+))"_rx;

    icRegExMatch match;

    for (auto & mod : modifiers) {
        if (mod == "not") {
            if (notMod) {
                il->vm->cpe_sig("while: `not` modifier setted twince");
            }
            notMod = true;
        }
        else if ((match = minRx.match(mod)).hasMatch()) {
            int _min = match.captured(1).toInt();

            if (_min < 2) {
                il->vm->cpe_sig(
                  "while: The minimal value of `min` modifier is 2");
            }
            else if (min > 0) {
                il->vm->syssig("while: `min` modifier setted twince");
            }
            min = _min;
        }
        else if ((match = maxRx.match(mod)).hasMatch()) {
            int _max = match.captured(1).toInt();

            if (_max < 2) {
                il->vm->cpe_sig(
                  "while: The minimal value of `max` modifier is 2");
            }
            else if (max > 0) {
                il->vm->cpe_sig("while: `max` modifier setted twince");
            }
            max = _max;
        }
        else {
            il->vm->cpe_sig("while: No such modifier: " + mod);
        }
    }
}

il::InterLevel * While::core() {
    return il;
}

}  // namespace icL::core::ce
