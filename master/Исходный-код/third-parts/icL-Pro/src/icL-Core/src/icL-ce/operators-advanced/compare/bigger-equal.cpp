#include "bigger-equal.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

BiggerEqual::BiggerEqual(il::InterLevel * il)
    : CompareOperator(il) {}

void BiggerEqual::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
}

void BiggerEqual::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleDouble(left[0], right[0]));
}

icString BiggerEqual::toString() {
    return ">=";
}

void BiggerEqual::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (BiggerEqual::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue}}, &BiggerEqual::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue}},
                 &BiggerEqual::runDoubleDouble}};

    runNow<BiggerEqual>(operators, left, right);
}

}  // namespace icL::core::ce
