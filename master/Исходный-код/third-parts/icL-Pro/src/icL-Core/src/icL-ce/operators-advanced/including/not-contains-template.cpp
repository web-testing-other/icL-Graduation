#include "not-contains-template.h++"

#include <icL-ce/value-base/base/bool-value.h++>

namespace icL::core::ce {

NotContainsTemplate::NotContainsTemplate(il::InterLevel * il)
    : ContainsTemplate(il) {}

icString NotContainsTemplate::toString() {
    return "!*";
}

void NotContainsTemplate::run(
  const memory::ArgList & left, const memory::ArgList & right) {

    ContainsTemplate::run(left, right);

    auto * value = dynamic_cast<il::PackableValue *>(m_newContext);

    if (value->type() != Type::BoolValue) {
        delete m_newContext;
        m_newContext = nullptr;
        return;
    }


    value->reset(!value->value().toBool());
}

}  // namespace icL::core::ce
