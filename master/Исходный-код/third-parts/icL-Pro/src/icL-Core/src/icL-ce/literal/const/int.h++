#ifndef core_ce_Int
#define core_ce_Int

#include <icL-ce/base/literal/const-literal.h++>



namespace icL::core::ce {

/**
 * @brief The Int class represent and iteger literal `\-?\d+`
 */
class icL_core_ce_literal_EXPORT Int : public ConstLiteral
{
public:
    Int(il::InterLevel * il, const icString & pattern);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;

    // il.CE interface
public:
    void colorize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Int
