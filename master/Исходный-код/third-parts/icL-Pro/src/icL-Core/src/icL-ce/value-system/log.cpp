#include "log.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

Log::Log(il::InterLevel * il)
    : SystemValue(il) {}

void Log::runError(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        error(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Log::runInfo(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        info(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Log::runOut(const memory::ArgList & args) {
    out(args);
    m_newContext = il->factory->fromValue(il, {});
}

void Log::runStack(const memory::ArgList & args) {
    if (args.length() == 1) {
        stack(args[0]);
    }
}

void Log::runState(const memory::ArgList & args) {
    if (args.length() == 1) {
        state(args[0]);
    }
}

Type Log::type() const {
    return Type::Log;
}

icString Log::typeName() {
    return "Log";
}

void Log::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Log::*)(const memory::ArgList &)> methods{
      {"error", &Log::runError},
      {"info", &Log::runInfo},
      {"out", &Log::runOut},
      {"stack", &Log::runStack},
      {"state", &Log::runState}};

    runMethodNow<Log, SystemValue>(methods, name, args);
}

}  // namespace icL::core::ce
