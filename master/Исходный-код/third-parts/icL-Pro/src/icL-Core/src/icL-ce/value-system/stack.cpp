#include "stack.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-own-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/void-value.h++>

#include <icL-memory/state/stackcontainer.h++>
#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

Stack::Stack(il::InterLevel * il, memory::StackContainer * stack)
    : SystemValue(il)
    , service::Stack(stack) {}

il::CE * Stack::property(const icString & name) {
    return il->factory->fromValue(il, stack, name);
}

void Stack::runPropertyLevel2(const icString & name) {
    m_newContext = property(name);
}

void Stack::runAddDescription(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        addDescription(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runBreak(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        break_();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runClear(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runContinue(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        continue_();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runDestroy(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        destroy();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runIgnore(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        ignore();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runListen(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        listen();
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runMarkStep(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        markStep(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runMarkTest(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        markTest(args[0]);
        m_newContext = il->factory->fromValue(il, {});
    }
}

void Stack::runReturn(const memory::ArgList & args) {
    if (checkArgs(args, {Type::AnyValue})) {
        return_(args[0].value);
        m_newContext = il->factory->fromValue(il, {});
    }
}

Type Stack::type() const {
    return Type::Stack;
}

icString Stack::typeName() {
    return "Stack";
}

void Stack::runProperty(Prefix prefix, const icString & name) {
    runOwnPropertyWithPrefixCheck<Stack, SystemValue>(
      &Stack::runPropertyLevel2, prefix, name);
}

void Stack::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Stack::*)(const memory::ArgList &)> methods{
      {"addDescription", &Stack::runAddDescription},
      {"break", &Stack::runBreak},
      {"clear", &Stack::runClear},
      {"continue", &Stack::runContinue},
      {"destroy", &Stack::runDestroy},
      {"ignore", &Stack::runIgnore},
      {"listen", &Stack::runListen},
      {"markStep", &Stack::runMarkStep},
      {"markTest", &Stack::runMarkTest},
      {"return", &Stack::runReturn}};

    runMethodNow<Stack, SystemValue>(methods, name, args);
}

}  // namespace icL::core::ce
