#include "exists.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/base/value/base-value.h++>
#include <icL-ce/operators-ALU/system/context.h++>



namespace icL::core::ce {

Exists::Exists(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Exists::toString() {
    return "exists";
}

int Exists::currentRunRank(bool rtl) {
    bool runnable = m_prev == nullptr || m_prev->role() != Role::If;
    return runnable && !rtl ? 9 : -1;
}

StepType Exists::runNow() {
    return transact();
}

bool Exists::hasValue() {
    return true;
}

int Exists::role() {
    return Role::Exists;
}

const icSet<int> & Exists::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole, Role::If, Role::Assign,
                                   Role::Operator, Role::Comma};
    return roles;
}

const icSet<int> & Exists::acceptedNexts() {
    static const icSet<int> roles{Role::Value, Role::ValueContext};
    return roles;
}

il::CE * Exists::lastToReplace() {
    return m_next;
}

void Exists::initialize() {
    if (m_next->role() == Role::Value) {
        value   = dynamic_cast<il::ReadableValue *>(m_next)->value();
        current = State::ValueCalculed;
    }
    else {
        auto commands =
          il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

        if (commands.length() == 0) {
            il->vm->syssig(
              "exists: Round brackets must contain at last an expression");
        }
        else if (commands.length() <= 2) {
            valueCode = commands[0];
            if (commands.length() > 1) {
                conditionCode = commands[1];
            }
        }
        else {
            il->vm->syssig(
              "exists: Round brackets must contain maximum 2 expressions");
        }

        current = State::Inited;
    }
}

void Exists::finalize() {
    m_newContext = il->factory->fromValue(il, value);
}

}  // namespace icL::core::ce
