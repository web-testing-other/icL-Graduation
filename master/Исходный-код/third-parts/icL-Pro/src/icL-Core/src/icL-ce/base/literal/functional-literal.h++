#ifndef core_ce_FunctionalLiteral
#define core_ce_FunctionalLiteral

#include "../main/literal.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT FunctionalLiteral : public Literal
{
public:
    FunctionalLiteral(il::InterLevel * il);
};

}  // namespace icL::core::ce

#endif  // core_ce_FunctionalLiteral
