#ifndef core_ce_DatetimeValue
#define core_ce_DatetimeValue

#include <icL-service/value-base/complex/datetime-value.h++>

#include <icL-ce/base/value/base-value.h++>



class icDateTime;

namespace icL::core::ce {

/**
 * @brief The DatetimeValue class represents a `icDateTime` value
 */
class icL_core_ce_value_base_EXPORT DatetimeValue
    : public BaseValue
    , public service::DatetimeValue
{
public:
    /// @brief DatetimeValue calls BaseValue(il, container, varName, readonly)
    DatetimeValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief DatetimeValue calls BaseValue(il, rvalue)
    DatetimeValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief DatetimeValue calls BaseValue(value)
    DatetimeValue(BaseValue * value);

private:
    il::CE * runProperty(const std::shared_ptr<il::IValueFixator> & fixator);

public:
    // properties level 1

    /// `[r/w] icDateTime'day : int`
    il::CE * day();

    /// `[r/w] icDateTime'hour : int`
    il::CE * hour();

    /// `[r/w] icDateTime'minute : int `
    il::CE * minute();

    /// `[r/w] icDateTime'month : int`
    il::CE * month();

    /// `[r/w] icDateTime'second : int`
    il::CE * second();

    /// `[r/w] icDateTime'year : int`
    il::CE * year();

    // properties level 2

    /// `icDateTime'day`
    void runDay();

    /// `icDateTime'hour`
    void runHour();

    /// `icDateTime'minute`
    void runMinute();

    /// `icDateTime'month`
    void runMonth();

    /// `icDateTime'second`
    void runSecond();

    /// `icDateTime'valid`
    void runValid();

    /// `icDateTime'year`
    void runYear();

    // methods level 2

    /// `icDateTime.adddDays`
    void runAddDays(const memory::ArgList & args);

    /// `icDateTime.addMonth`
    void runAddMonths(const memory::ArgList & args);

    /// `icDateTime.addSecs`
    void runAddSecs(const memory::ArgList & args);

    /// `icDateTime.addYear`
    void runAddYears(const memory::ArgList & args);

    /// `icDateTime.daysTo`
    void runDaysTo(const memory::ArgList & args);

    /// `icDateTime.secsTo`
    void runSecsTo(const memory::ArgList & args);

    /// `icDateTime.toTimeZone`
    void runToTimeZone(const memory::ArgList & args);

    /// `icDateTime.toUTC`
    void runToUTC(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_DatetimeValue
