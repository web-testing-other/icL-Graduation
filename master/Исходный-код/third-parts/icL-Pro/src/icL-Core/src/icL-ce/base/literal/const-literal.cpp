#include "const-literal.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <utility>

namespace icL::core::ce {

ConstLiteral::ConstLiteral(il::InterLevel * il, const icString & pattern)
    : Literal(il)
    , pattern(pattern) {}

icString ConstLiteral::toString() {
    return pattern;
}

int ConstLiteral::currentRunRank(bool rtl) {
    return rtl ? -1 : 10;
}

StepType ConstLiteral::runNow() {
    m_newContext = il->factory->fromValue(il, getValueOf());
    return StepType::CommandEnd;
}

int ConstLiteral::role() {
    return Role::Value;
}

const icSet<int> & ConstLiteral::acceptedPrevs() {
    static icSet<int> roles = {
      Role::NoRole,   Role::Any,    Role::If,       Role::For,    Role::Range,
      Role::Filter,   Role::Method, Role::Function, Role::Assign, Role::Comma,
      Role::Operator, Role::JsRun,  Role::Emit};
    return roles;
}

const icSet<int> & ConstLiteral::acceptedNexts() {
    static icSet<int> roles = {Role::NoRole,     Role::Assign, Role::Comma,
                                Role::RunContext, Role::Cast,   Role::Operator,
                                Role::Property,   Role::Method};
    return roles;
}

}  // namespace icL::core::ce
