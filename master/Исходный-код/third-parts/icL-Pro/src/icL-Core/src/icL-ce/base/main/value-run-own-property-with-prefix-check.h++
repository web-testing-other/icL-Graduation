#ifndef core_ce_Value_runOwnPropertyWithPrefixcheck
#define core_ce_Value_runOwnPropertyWithPrefixcheck

#include "value.h++"

namespace icL::core::ce {

template <typename This, typename ParentClass>
void Value::runOwnPropertyWithPrefixCheck(
  void (This::*property)(const icString &), Prefix prefix,
  const icString & name) {
    auto this_ = dynamic_cast<This *>(this);

    if (prefix == Prefix::None) {
        (this_->*property)(name);
    }
    else {
        this_->ParentClass::runProperty(prefix, name);
    }
}

}  // namespace icL::core::ce

#endif  // core_ce_Value_runOwnPropertyWithPrefixcheck
