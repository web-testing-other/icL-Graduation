#include "type-of.h++"

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

TypeOf::TypeOf(il::InterLevel * il)
    : CastOperator(il) {}

icString TypeOf::toString() {
    return "::";
}

void TypeOf::runCast(const memory::Argument & left, Type right) {
    bool isTypeOf = left.type == right;

    if (m_newContext == nullptr) {
        m_newContext = il->factory->fromValue(il, isTypeOf);
    }
    else {
        auto * asValue = dynamic_cast<il::PackableValue *>(m_newContext);
        asValue->reset(isTypeOf && asValue->value().toBool());
    }
}

void TypeOf::runCast(
  const memory::ArgList & /*left*/, const memory::ArgList & /*right*/) {
    // `type of` doesn't accepts values on right
}

}  // namespace icL::core::ce
