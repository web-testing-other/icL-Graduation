#include "int.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/signals.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>

namespace icL::core::ce {

Int::Int(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icVariant Int::getValueOf() {
    int  ret;
    bool ok;

    ret = pattern.toInt(ok);

    if (!ok) {
        il->vm->signal({il::Signals::System, "Wrong integer literal"});
    }

    return ret;
}

void Int::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::Number);
}

}  // namespace icL::core::ce
