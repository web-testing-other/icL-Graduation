#include "logic-operator.h++"

namespace icL::core::ce {

LogicOperator::LogicOperator(il::InterLevel * il)
    : AluOperator(il) {}

int LogicOperator::currentRunRank(bool rtl) {
    bool runnable =
      m_next->role() == Role::Value && m_prev->role() == Role::Value;
    return runnable && !rtl ? 2 : -1;
}

}  // namespace icL::core::ce
