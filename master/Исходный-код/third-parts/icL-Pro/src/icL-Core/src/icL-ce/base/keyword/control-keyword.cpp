#include "control-keyword.h++"

namespace icL::core::ce {

ControlKeyword::ControlKeyword(il::InterLevel * il)
    : Keyword(il) {}

il::CE * ControlKeyword::lastToReplace() {
    il::CE * it = this;

    while (it->next() != nullptr) {
        it = it->next();
    }

    return dynamic_cast<CE *>(it);
}

}  // namespace icL::core::ce
