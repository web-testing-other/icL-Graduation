#ifndef core_ce_StaticLiteral
#define core_ce_StaticLiteral

#include "../main/literal.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/factory.h++>

#include <icL-memory/structures/packed-value-item.h++>



namespace icL::core::ce {

class icL_core_ce_base_EXPORT StaticLiteral
    : public Literal
    , public il::PackableValue
{
public:
    StaticLiteral(il::InterLevel * il)
        : Literal(il) {}

    virtual ~StaticLiteral() override = default;

    // CE interface
public:
    int      currentRunRank(bool /*rtl*/) override;
    StepType runNow() override;
    bool     hasValue() override;

    icVariant value() const override;
    void      colorize() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // PackableValue interface
public:
    memory::PackedValueItem packNow() const override;
    il::PackableType        packableType() const override;

    Type type() const override;
    void  reset(const icVariant & value) override;
};



}  // namespace icL::core::ce

#endif  // core_ce_StaticLiteral
