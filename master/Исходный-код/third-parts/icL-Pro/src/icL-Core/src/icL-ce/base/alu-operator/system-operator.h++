#ifndef core_ce_SystemOperator
#define core_ce_SystemOperator

#include "alu-operator.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT SystemOperator : public AluOperator
{
public:
    SystemOperator(il::InterLevel * il);
};

}  // namespace icL::core::ce

#endif  // core_ce_SystemOperator
