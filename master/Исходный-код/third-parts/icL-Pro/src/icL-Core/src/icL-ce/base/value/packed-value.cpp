#include "packed-value.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-memory/state/datacontainer.h++>



namespace icL::core::ce {

PackedValue::PackedValue(
  il::InterLevel * il, il::PackableValue * first, il::PackableValue * second)
    : BaseValue(il, {}) {
    if (first != nullptr) {
        extractDataFrom(first);
    }
    if (second != nullptr) {
        extractDataFrom(second);
    }
    if (first == nullptr && second == nullptr) {
        values.data = std::make_shared<memory::PackedItems>();
    }
    rvalue = values;
}

PackedValue::PackedValue(il::InterLevel * il, const memory::PackedValue & other)
    : BaseValue(il, {}) {
    if (other.data != nullptr) {
        values.data = other.data;
    }
    else {
        values.data = std::make_shared<memory::PackedItems>();
    }
    rvalue = values;
}

PackedValue::PackedValue(il::ValuePack * value)
    : BaseValue(dynamic_cast<BaseValue *>(value)) {
    values = dynamic_cast<PackedValue *>(value)->values;
}

const memory::PackedValue & PackedValue::getValues() {
    return values;
}

void PackedValue::extractDataFrom(il::PackableValue * value) {
    auto * packedValue = dynamic_cast<PackedValue *>(value);

    if (packedValue != nullptr) {
        if (values.data == nullptr) {
            values.data = packedValue->values.data;
        }
        else {
            values.data->append(*packedValue->values.data);
        }
    }
    else {
        if (values.data == nullptr) {
            values.data = std::make_shared<icList<memory::PackedValueItem>>();
        }

        values.data->append(value->packNow());
    }
}

const icSet<int> & PackedValue::acceptedPrevs() {
    static icSet<int> roles{Role::NoRole,   Role::Case,     Role::Assign,
                             Role::Switch,   Role::Comma,    Role::Cast,
                             Role::Operator, Role::Function, Role::Method};
    return roles;
}

const icSet<int> & PackedValue::acceptedNexts() {
    static icSet<int> roles{Role::NoRole, Role::Method, Role::Property,
                             Role::Assign, Role::Comma,  Role::RunContext};
    return roles;
}

Type PackedValue::type() const {
    return Type::PackedValue;
}

icString PackedValue::typeName() {
    return "packed";
}

const memory::PackedItems & PackedValue::getValues() const {
    return *values.data;
}

}  // namespace icL::core::ce
