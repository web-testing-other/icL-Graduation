#ifndef core_ce_BoolValue
#define core_ce_BoolValue

#include <icL-ce/base/value/base-value.h++>



namespace icL::core::ce {

/**
 * @brief The BoolValue class represents a `bool` value
 */
class icL_core_ce_value_base_EXPORT BoolValue : public BaseValue
{
public:
    /// @brief BoolValue calls BaseValue(il, container, varName, readonly)
    BoolValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief BoolValue calls BaseValue(il, rvalue)
    BoolValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief BoolValue calls BaseValue(value)
    BoolValue(BaseValue * value);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_BoolValue
