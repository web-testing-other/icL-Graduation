﻿#ifndef core_ce_Exists
#define core_ce_Exists

#include <icL-service/keyword/control/exists.h++>

#include <icL-ce/base/keyword/control-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT Exists
    : public ControlKeyword
    , public service::Exists
{
public:
    Exists(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    bool hasValue() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    il::CE * lastToReplace() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Exists
