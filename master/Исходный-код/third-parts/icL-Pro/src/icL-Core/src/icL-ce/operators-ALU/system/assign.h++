#ifndef core_ce_Assign
#define core_ce_Assign

#include <icL-ce/base/alu-operator/system-operator.h++>

#include <icL-memory/structures/type.h++>


class icString;
class icStringList;
class icDateTime;
class icRegEx;

template <typename>
class icList;

namespace icL::core {

namespace memory {
struct Argument;
struct Object;
struct Set;
using ArgList = icList<Argument>;
}  // namespace memory

namespace ce {

class icL_core_ce_operators_ALU_EXPORT Assign : public SystemOperator
{
public:
    Assign(il::InterLevel * il);

    // level 1

private:
    /**
     * @brief tryToAssign tries to assign a value with a copy constructor
     * @param arg is the argument to get the container of
     * @param type is the accepted type of value
     * @param value is the value to assign
     */
    void tryToAssign(
      const memory::Argument & arg, short type, const icVariant & value);

public:
    // values which can be setted in stack, state and icObject

    /// `void = bool` & `bool = bool`
    void assignBool(const memory::Argument & arg, bool value);

    /// `void = double` & `double = double`
    void assignDouble(const memory::Argument & arg, double value);

    /// `void = int` & `int = int`
    void assignInt(const memory::Argument & arg, int value);

    /// `void = string` & `string = string`
    void assignString(const memory::Argument & arg, const icString & value);

    /// `void = list` & `list = list`
    void assignList(const memory::Argument & arg, const icStringList & value);

    // values which can be setted in stack and state

    /// `void = icDateTime` & `icDateTime = icDateTime`
    void assignDatetime(const memory::Argument & arg, const icDateTime & value);

    /// `void = regex` & `regex = regex`
    void assignRegex(const memory::Argument & arg, const icRegEx & value);

    /// `void = object` & `object = object`
    void assignObject(
      const memory::Argument & arg, const memory::Object & value);

    /// `void = set` & `set = set`
    void assignSet(const memory::Argument & arg, const memory::Set & value);

    // level 2

    /**
     * @brief assignAny assigns any value
     * @param arg is the argument which will give the value
     * @param value is the argument which will get the value
     */
    void assignAny(
      const memory::Argument & arg, const memory::Argument & value);

    /// `any = any`, `any = (..)` & `(..) = (..)`
    void runAssign(const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    int      role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_Assign
