#include "for-collection.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/operators-ALU/system/context.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

using il::Signals::Signals;

ForCollection::ForCollection(il::InterLevel * il, bool reverse, int maxX)
    : ForBase(il)
    , service::ForCollection(reverse, maxX) {}

StepType ForCollection::runNow() {
    return transact();
}

icString ForCollection::toString() {
    icString str = "for";

    if (reverse)
        str += "-reverse";
    if (maxX != -1)
        str += "-max" % icString::number(maxX);

    return str;
}

void ForCollection::initialize() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    if (commands.length() != 1) {
        il->vm->signal(
          {Signals::System,
           "for-each: round brackets content must contains a command"});
    }
    else {
        collectionCode = commands[0];
        body           = dynamic_cast<Context *>(m_next->next())->getCode();
    }
}

void ForCollection::finalize() {
    m_newContext = il->factory->fromValue(il, retValue);
}

}  // namespace icL::core::ce
