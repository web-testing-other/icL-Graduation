#ifndef core_ce_String
#define core_ce_String

#include <icL-ce/base/literal/const-literal.h++>



namespace icL::core::ce {

/**
 * @brief The string class represent a string literal `".*"`
 */
class icL_core_ce_literal_EXPORT String : public ConstLiteral
{
public:
    String(il::InterLevel * il, const icString & pattern);

    // CE interface
public:
    icString toString() override;
    void     colorize() override;

    // ConstLiteral interface
public:
    icVariant getValueOf() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_String
