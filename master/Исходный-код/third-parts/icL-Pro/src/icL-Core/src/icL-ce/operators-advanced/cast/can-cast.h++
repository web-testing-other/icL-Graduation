#ifndef core_ce_CanCast
#define core_ce_CanCast

#include "cast-operator.h++"


namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT CanCast : public CastOperator
{
public:
    CanCast(il::InterLevel * il);

private:
    template <typename ReturnType, typename ArgType>
    void runAny(
      ReturnType (*func)(il::InterLevel *, const ArgType &),
      const memory::Argument & left);

public:
    /// to be linked to conversions which never fails
    void runAlwaysTrue(const memory::Argument & left);

    /// to be linked to conversions which always fails
    void runAlwaysFalse(const memory::Argument & left);

    /// `string : int`
    void runStringInt(const memory::Argument & left);

    /// `string : double`
    void runStringDouble(const memory::Argument & left);

    /// `string : object`
    void runStringObject(const memory::Argument & left);

    /// `string : set`
    void runStringSet(const memory::Argument & left);

    /// `list : string`
    void runListString(const memory::Argument & left);

    /// `list : set`
    void runListSet(const memory::Argument & left);

    /// `set : object`
    void runSetObject(const memory::Argument & left);

    /// `string : icDateTime`
    void runStringDatetime(const memory::Argument & left);

    // CE interface
public:
    icString toString() override;

    // CastOperator interface
public:
    void runCast(const memory::Argument & left, Type right) override;
    void runCast(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_CanCast
