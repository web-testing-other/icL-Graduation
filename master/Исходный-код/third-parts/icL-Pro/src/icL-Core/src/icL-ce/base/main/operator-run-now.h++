#ifndef core_ce_Operator_runNow
#define core_ce_Operator_runNow

#include "operator.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/args/listify-args-to-list.h++>
#include <icL-service/main/args/listify.h++>

#include <icL-memory/structures/arg-value.h++>

namespace icL::core::memory {

}  // namespace icL::core::memory

namespace icL::core::ce {

template <typename This>
void Operator::runNow(
  const icObject<
    icPair<icList<Type>, icList<Type>>,
    void (This::*)(const memory::ArgList &, const memory::ArgList &)> &
                          operators,
  const memory::ArgList & left, const memory::ArgList & right) {

    auto this_ = dynamic_cast<This *>(this);

    icList<Type> leftTypes  = service::Listify::fromArgs<Type>(left);
    icList<Type> rightTypes = service::Listify::fromArgs<Type>(right);

    auto it = operators.find({leftTypes, rightTypes});

    if (it == operators.end()) {
        il->vm->signal({il::Signals::System, "No such operator"});
    }
    else {
        (this_->*it.value())(left, right);
    }
}

}  // namespace icL::core::ce

#endif  // core_ce_Operator_runNow
