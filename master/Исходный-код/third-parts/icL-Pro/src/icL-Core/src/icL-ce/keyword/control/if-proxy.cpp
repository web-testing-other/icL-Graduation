#include "if-proxy.h++"

#include <if-exists.h++>
#include <if.h++>



namespace icL::core::ce {

IfProxy::IfProxy(il::InterLevel * il)
    : IfBase(il) {}

int IfProxy::currentRunRank(bool rtl) {
    return rtl ? 9 : -1;
}

StepType IfProxy::runNow() {
    if (m_next->role() == Role::Exists) {
        m_newContext = new IfExists{il, notModifier};
    }
    else {
        m_newContext = new If{il, notModifier};
    }

    return StepType::CommandEnd;
}

il::CE * IfProxy::firstToReplace() {
    return this;
}

il::CE * IfProxy::lastToReplace() {
    return this;
}

bool IfProxy::hasNotModifier() {
    return notModifier;
}

void IfProxy::setNotModifier() {
    notModifier = true;
}

}  // namespace icL::core::ce
