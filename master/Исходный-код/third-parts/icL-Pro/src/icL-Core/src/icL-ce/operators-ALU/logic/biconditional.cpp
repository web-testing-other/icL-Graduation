#include "biconditional.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/export/signals.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::core::ce {

Biconditional::Biconditional(il::InterLevel * il)
    : LogicOperator(il) {}

bool Biconditional::run(bool left, bool right) {
    return left == right;
}

int Biconditional::currentRunRank(bool rtl) {
    bool runnable1 = (m_prev == nullptr || m_prev->role() == Role::Comma) &&
                     (m_next == nullptr || m_next->role() == Role::Comma);
    bool runnable2 = (m_prev != nullptr && m_prev->role() == Role::Value) &&
                     (m_next != nullptr && m_next->role() == Role::Value);

    asVoidValue = runnable1 && !runnable2;
    return runnable1 && rtl ? 8 : runnable2 && !rtl ? 2 : -1;
}

il::CE * Biconditional::firstToReplace() {
    return asVoidValue ? this : m_prev;
}

il::CE * Biconditional::lastToReplace() {
    return asVoidValue ? this : m_next;
}

const icSet<int> & Biconditional::acceptedPrevs() {
    static const icSet<int> roles{LogicOperator::acceptedPrevs(),
                                   {Role::Comma, Role::NoRole}};
    return roles;
}

const icSet<int> & Biconditional::acceptedNexts() {
    static const icSet<int> roles{LogicOperator::acceptedNexts(),
                                   {Role::Comma, Role::NoRole}};
    return roles;
}

icString Biconditional::toString() {
    return "~";
}

void Biconditional::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (left.isEmpty() || right.isEmpty()) {
        if (left.isEmpty() && right.isEmpty()) {
            m_newContext = il->factory->fromValue(il, {});
        }
    }
    else if (
      left[0].type == Type::BoolValue && right[0].type == Type::BoolValue) {
        m_newContext = il->factory->fromValue(il, run(bool(left[0]), right[0]));
    }
    else if (left[0].type == right[0].type) {
        m_newContext = il->factory->fromValue(il, right[0].value);
    }
    else {
        il->vm->signal({il::Signals::System, "No such operator"});
    }
}

}  // namespace icL::core::ce
