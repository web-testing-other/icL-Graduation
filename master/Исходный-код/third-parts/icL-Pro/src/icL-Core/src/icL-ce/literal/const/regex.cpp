#include "regex.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>

#include <utility>


namespace icL::core::ce {

RegEx::RegEx(
  il::InterLevel * il, const icString & pattern, const icString & mods)
    : ConstLiteral(il, pattern)
    , mods(mods) {}

icVariant RegEx::getValueOf() {
    icRegEx check = R"(^[fimsux]*$)"_rx;

    auto match = check.match(mods);

    if (!match.hasMatch()) {
        il->vm->signal({il::Signals::System, "Wrong RegEx modifiers"});
        return {};
    }

    return icRegEx{pattern, mods};
}

void RegEx::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::RegEx);
}

}  // namespace icL::core::ce
