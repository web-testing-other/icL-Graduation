#ifndef core_ce_Signal
#define core_ce_Signal

#include <icL-service/value-system/signal.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::core::ce {

class icL_core_ce_value_system_EXPORT Signal
    : public SystemValue
    , public service::Signal
{
public:
    Signal(il::InterLevel * il);

    // properties level 2

    /// `Signal'(string)`
    void runPropertyLevel2(const icString & name);

    // methods level 2

    /// `Signal.add`
    void runAdd(const memory::ArgList & args);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Signal
