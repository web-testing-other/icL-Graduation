#ifndef core_ce_DefaultParameter
#define core_ce_DefaultParameter

#include "parameter.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::core::ce {

class icL_core_ce_literal_EXPORT DefaultParameter : public Parameter
{
public:
    DefaultParameter(
      il::InterLevel * il, const icString & name, const icVariant & value);

    /**
     * @brief getDefaultValue gets the default value of parameter
     * @return the default value of parameter
     */
    const icVariant & getDefaultValue();

    // CE interface
public:
    memory::PackedValueItem packNow() const override;

    icString toString() override;

    // Parameter interface
public:
    bool isDefault() const override;

private:
    /// \brief defaultValue is the default value of parameter
    icVariant defaultValue;
};

}  // namespace icL::core::ce

#endif  // core_ce_DefaultParameter
