#include "smaller-bigger.h++"

#include <icL-il/main/factory.h++>

#include <icL-ce/base/main/operator-run-now.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

SmallerBigger::SmallerBigger(il::InterLevel * il)
    : CompareOperator(il) {}

void SmallerBigger::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext =
      il->factory->fromValue(il, intIntInt(left[0], right[0], right[1]));
}

void SmallerBigger::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(
      il, doubleDoubleDouble(left[0], right[0], right[1]));
}

icString SmallerBigger::toString() {
    return "<>";
}

void SmallerBigger::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (SmallerBigger::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue, Type::IntValue}},
                 &SmallerBigger::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue, Type::DoubleValue}},
                 &SmallerBigger::runDoubleDouble}};

    runNow<SmallerBigger>(operators, left, right);
}

}  // namespace icL::core::ce
