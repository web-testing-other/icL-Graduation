#ifndef core_ce_ForParametric
#define core_ce_ForParametric

#include "for-base.h++"

#include <icL-service/keyword/loop/for-parametric.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT ForParametric
    : public ForBase
    , public service::ForParametric
{
public:
    ForParametric(il::InterLevel * il, bool alt);


    // CE interface
public:
    StepType runNow() override;
    icString toString() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ForParametric
