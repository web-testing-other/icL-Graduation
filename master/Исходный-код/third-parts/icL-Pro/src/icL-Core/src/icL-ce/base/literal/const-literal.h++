#ifndef core_ce_ConstLiteral
#define core_ce_ConstLiteral

#include "../main/literal.h++"

#include <icL-types/replaces/ic-string.h++>



class icVariant;

namespace icL::core::ce {

class icL_core_ce_base_EXPORT ConstLiteral : public Literal
{
public:
    ConstLiteral(il::InterLevel * il, const icString & pattern);

    /**
     * @brief getValueOf gets the value of this literal
     * @return the value of this constant literal
     */
    virtual icVariant getValueOf() = 0;

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    int      role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // fields
protected:
    icString pattern;
};

}  // namespace icL::core::ce

#endif  // core_ce_ConstLiteral
