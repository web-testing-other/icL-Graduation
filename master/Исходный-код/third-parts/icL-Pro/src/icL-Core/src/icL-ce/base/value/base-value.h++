#ifndef core_ce_BaseValue
#define core_ce_BaseValue

#include "../main/value.h++"

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <functional>

namespace icL::core {

namespace memory {
class DataContainer;
}

namespace ce {

using memory::Type;

/**
 * @brief The BaseValue class contains all commom posibilities of values
 */
class icL_core_ce_base_EXPORT BaseValue
    : public Value
    , public il::PackableValue
    , public il::FixableValue
{
public:
    /**
     * @brief BaseValue constructs a L-Value
     * @param container is a DataState pointer
     * @param varName is the name of var in container
     * @param readonly if true restricts assigns
     */
    BaseValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /**
     * @brief BaseValue constucts a R-Value
     * @param rvalue is the value of Value
     */
    BaseValue(il::InterLevel * il, icVariant rvalue);

    /**
     * @brief BaseValue create a copy of
     * @param value is the value to copy
     */
    BaseValue(BaseValue * value);

    /**
     * @brief isLValue checks if this is a left value
     * @return true if is a lvalue, otherwise false
     */
    bool isLValue() const;

    /**
     * @brief isRValue checks if this is a right value
     * @return true if is a rvalue, otherwise false
     */
    bool isRValue() const;

    /**
     * @brief rebind lvalue to the last stack
     */
    void rebind();

    // methods level 1

    /**
     * @brief ensureRValue copies the value from the container
     * @return the new created icL Value
     */
    il::CE * ensureRValue();

    // methods level 2

    /// \brief runTypeId `.ensureRValue` level 2
    void runEnsureRValue(const memory::ArgList & args);

    // CE interface
public:
    bool hasValue() override;

    // CE interface
public:
    icString toString() override;
    int      role() override;

    void colorize() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // PackableValue interface
public:
    memory::PackedValueItem packNow() const override;
    il::PackableType        packableType() const override;

    Type      type() const override;
    icVariant value() const override;
    void      reset(const icVariant & value) override;

    // Value interface
public:
    void runMethod(
      const icString & name, const memory::ArgList & args) override;

    // FixableValue interface
public:
    void installFixator(
      const std::shared_ptr<il::IValueFixator> & fixator) override;

    // class fields

protected:
    // L-Value
    /// \brief container is the container of value
    memory::DataContainer * container = nullptr;
    /// \brief varName is the name of varaible in continer
    icString varName;

    // R-Value
    /// \brief rvalue is the value of a R-Value or last cached JS-Value
    icVariant rvalue;

    // Fixable value
    std::shared_ptr<il::IValueFixator> fixator = nullptr;

    /**
     * @brief The Value enum decribes the type of a value
     */
    enum ValueType {
        L,  ///< L-Value
        R,  ///< R-Value
    };

    int valuetype;  ///< The type of contained value

    /// \brief readonly if true will generate an exception on assign
    bool readonly = false;

    // Fix size warning
    int : 24;
};

}  // namespace ce
}  // namespace icL::core

#endif  // core_ce_BaseValue
