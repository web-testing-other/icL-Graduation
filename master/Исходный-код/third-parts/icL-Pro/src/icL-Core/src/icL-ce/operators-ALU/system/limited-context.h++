#ifndef core_ce_LimitedContext
#define core_ce_LimitedContext

#include "context.h++"

#include <icL-service/operators-ALU/system/limited-context.h++>



namespace icL::core::ce {

/**
 * @brief The LimitedContext class represents a limited context `[]`
 */
class icL_core_ce_operators_ALU_EXPORT LimitedContext
    : public Context
    , public service::LimitedContext
{
public:
    LimitedContext(il::InterLevel * il, const il::CodeFragment & code);

    /**
     * @brief tryToMakeSomething tries to create a value from packed items
     * @param items are the result of parsing the inner context
     */
    void tryToMakeSomething(const memory::PackedItems & items);

    // CE interface
public:
    icString toString() override;
    StepType runNow() override;
    int      role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_LimitedContext
