#include "inequality.h++"

#include <icL-ce/base/value/base-value.h++>

namespace icL::core::ce {

Inequality::Inequality(il::InterLevel * il)
    : Equality(il) {}

icString Inequality::toString() {
    return "!=";
}

void Inequality::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    Equality::run(left, right);

    if (m_newContext == nullptr) {
        return;
    }

    auto * newContext = dynamic_cast<il::WriteableValue *>(m_newContext);

    newContext->reset(!newContext->value().toBool());
}

}  // namespace icL::core::ce
