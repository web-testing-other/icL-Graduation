#include "for-parametric.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/operators-ALU/system/context.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

using il::Signals::Signals;

ForParametric::ForParametric(il::InterLevel * il, bool alt)
    : ForBase(il)
    , service::ForParametric(alt) {}

StepType ForParametric::runNow() {
    return transact();
}

icString ForParametric::toString() {
    icString str = "for";

    if (alt)
        str += "-alt";

    return str;
}

void ForParametric::initialize() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    if (commands.length() != 3) {
        il->vm->signal(
          {Signals::System,
           "for: round brackets content must contains 3 commands"});
    }
    else {
        init      = commands[0];
        condition = commands[1];
        step      = commands[2];
        body      = dynamic_cast<Context *>(m_next->next())->getCode();
    }
}

void ForParametric::finalize() {
    m_newContext = il->factory->fromValue(il, retValue);
}

}  // namespace icL::core::ce
