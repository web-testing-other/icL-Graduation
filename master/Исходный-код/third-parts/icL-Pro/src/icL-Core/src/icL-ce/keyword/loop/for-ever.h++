#ifndef core_ce_ForEver
#define core_ce_ForEver

#include "for-base.h++"

#include <icL-service/keyword/loop/for-ever.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT ForEver
    : public ForBase
    , public service::ForEver
{
public:
    ForEver(il::InterLevel * il, int xTimes);

    // CE interface
public:
    StepType runNow() override;
    icString toString() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ForEver
