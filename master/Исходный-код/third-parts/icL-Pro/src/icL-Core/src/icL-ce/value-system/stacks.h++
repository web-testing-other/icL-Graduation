#ifndef core_ce_Stacks
#define core_ce_Stacks

#include <icL-service/value-system/stacks.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::core::ce {

class Stack;

class icL_core_ce_value_system_EXPORT Stacks
    : public SystemValue
    , public service::Stacks
{
public:
    Stacks(il::InterLevel * il);

    // properties level 2

    /// `Stacks'(string)`
    void runPropertyLevel2(const icString & name);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Stacks
