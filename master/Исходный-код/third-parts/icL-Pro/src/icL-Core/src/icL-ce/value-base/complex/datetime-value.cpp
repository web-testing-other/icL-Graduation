#include "datetime-value.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::core::ce {

class DayFixator : public il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = value.toDateTime().day();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        auto date = value.toDateTime();
        date.setDate(request, date.month(), date.year());
        value = date;
    }
};

class HourFixator : public il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = value.toDateTime().hour();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        auto date = value.toDateTime();
        date.setHMS(request, date.minute(), date.second());
        value = date;
    }
};

class MinuteFixator : public il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = value.toDateTime().minute();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        auto date = value.toDateTime();
        date.setHMS(date.hour(), request, date.second());
        value = date;
    }
};

class MonthFixator : public il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = value.toDateTime().month();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        auto date = value.toDateTime();
        date.setDate(date.day(), request, date.year());
        value = date;
    }
};

class SecondFixator : public il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = value.toDateTime().second();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        auto date = value.toDateTime();
        date.setHMS(date.hour(), date.minute(), request);
        value = date;
    }
};

class YearFixator : public il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = value.toDateTime().year();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        auto date = value.toDateTime();
        date.setDate(date.day(), date.month(), request);
        value = date;
    }
};

DatetimeValue::DatetimeValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DatetimeValue::DatetimeValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DatetimeValue::DatetimeValue(BaseValue * value)
    : BaseValue(value) {}

il::CE * DatetimeValue::runProperty(
  const std::shared_ptr<il::IValueFixator> & fixator) {
    auto * node    = il->factory->clone(this, Type::IntValue);
    auto * fixable = dynamic_cast<il::FixableValue *>(node);

    fixable->installFixator(fixator);
    return node;
}

il::CE * DatetimeValue::day() {
    return runProperty(std::make_shared<DayFixator>());
}

il::CE * DatetimeValue::hour() {
    return runProperty(std::make_shared<HourFixator>());
}

il::CE * DatetimeValue::minute() {
    return runProperty(std::make_shared<MinuteFixator>());
}

il::CE * DatetimeValue::month() {
    return runProperty(std::make_shared<MonthFixator>());
}

il::CE * DatetimeValue::second() {
    return runProperty(std::make_shared<SecondFixator>());
}

il::CE * DatetimeValue::year() {
    return runProperty(std::make_shared<YearFixator>());
}

void DatetimeValue::runDay() {
    m_newContext = day();
}

void DatetimeValue::runHour() {
    m_newContext = hour();
}

void DatetimeValue::runMinute() {
    m_newContext = minute();
}

void DatetimeValue::runMonth() {
    m_newContext = month();
}

void DatetimeValue::runSecond() {
    m_newContext = second();
}

void DatetimeValue::runValid() {
    m_newContext = il->factory->fromValue(il, valid());
}

void DatetimeValue::runYear() {
    m_newContext = year();
}

void DatetimeValue::runAddDays(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addDays(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void DatetimeValue::runAddMonths(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addMonths(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void DatetimeValue::runAddSecs(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addSecs(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void DatetimeValue::runAddYears(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        addYears(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void DatetimeValue::runDaysTo(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DatetimeValue})) {
        m_newContext = il->factory->fromValue(il, daysTo(args[0]));
    }
}

void DatetimeValue::runSecsTo(const memory::ArgList & args) {
    if (checkArgs(args, {Type::DatetimeValue})) {
        m_newContext = il->factory->fromValue(il, secsTo(args[0]));
    }
}

void DatetimeValue::runToTimeZone(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, toTimeZone(args[0], args[1]));
    }
}

void DatetimeValue::runToUTC(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, toUTC());
    }
}

Type DatetimeValue::type() const {
    return Type::DatetimeValue;
}

icString DatetimeValue::typeName() {
    return "icDateTime";
}

void DatetimeValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (DatetimeValue::*)()> properties{
      {"day", &DatetimeValue::runDay},
      {"hour", &DatetimeValue::runHour},
      {"minute", &DatetimeValue::runMinute},
      {"month", &DatetimeValue::runMonth},
      {"second", &DatetimeValue::runSecond},
      {"valid", &DatetimeValue::runValid},
      {"year", &DatetimeValue::runYear}};

    runPropertyWithPrefixCheck<DatetimeValue, BaseValue>(
      properties, prefix, name);
}

void DatetimeValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (DatetimeValue::*)(const memory::ArgList &)>
      methods{{"addDays", &DatetimeValue::runAddDays},
              {"addMonths", &DatetimeValue::runAddMonths},
              {"addSecs", &DatetimeValue::runAddSecs},
              {"addYears", &DatetimeValue::runAddYears},
              {"daysTo", &DatetimeValue::runDaysTo},
              {"secsTo", &DatetimeValue::runSecsTo},
              {"toTimeZone", &DatetimeValue::runToTimeZone},
              {"toUTC", &DatetimeValue::runToUTC}};

    runMethodNow<DatetimeValue, BaseValue>(methods, name, args);
}

}  // namespace icL::core::ce
