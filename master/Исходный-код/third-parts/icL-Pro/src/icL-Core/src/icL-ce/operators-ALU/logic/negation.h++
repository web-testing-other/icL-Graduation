#ifndef core_ce_Negation
#define core_ce_Negation

#include <icL-ce/base/alu-operator/logic-operator.h++>



namespace icL::core::ce {

class icL_core_ce_operators_ALU_EXPORT Negation : public LogicOperator
{
public:
    Negation(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    il::CE * firstToReplace() override;

protected:
    const icSet<int> & acceptedPrevs() override;

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Negation
