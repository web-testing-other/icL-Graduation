#include "system-value.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

namespace icL::core::ce {

SystemValue::SystemValue(il::InterLevel * il)
    : Value(il) {}

icString SystemValue::toString() {
    return typeName();
}

void SystemValue::colorize() {
    il->vms->markToken(m_fragmentData, il::Token::SystemType);
}

}  // namespace icL::core::ce
