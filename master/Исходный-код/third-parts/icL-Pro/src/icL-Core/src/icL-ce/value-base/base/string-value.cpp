#include "string-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-index-check.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::core::ce {

StringValue::StringValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

StringValue::StringValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

StringValue::StringValue(BaseValue * value)
    : BaseValue(value) {}

void StringValue::runEmpty() {
    m_newContext = il->factory->fromValue(il, empty());
}

void StringValue::runLength() {
    m_newContext = il->factory->fromValue(il, length());
}

void StringValue::runLast() {
    m_newContext = il->factory->fromValue(il, last());
}

void StringValue::runAppend(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        append(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void StringValue::runAt(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, at(args[0]));
    }
}

void StringValue::runBeginsWith(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, beginsWith(args[0]));
    }
}

void StringValue::runCompare(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, compare(args[0]));
    }
    else if (checkArgs(args, {Type::StringValue, Type::BoolValue})) {
        m_newContext = il->factory->fromValue(il, compare(args[0], args[1]));
    }
}

void StringValue::runCount(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, count(icString(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = il->factory->fromValue(il, count(icRegEx(args[0])));
    }
}

void StringValue::runEndsWith(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, endsWith(args[0]));
    }
}

void StringValue::runIndexOf(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, indexOf(icString(args[0])));
    }
    else if (checkArgs(args, {Type::StringValue, Type::IntValue})) {
        m_newContext =
          il->factory->fromValue(il, indexOf(icString(args[0]), args[1]));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = il->factory->fromValue(il, indexOf(icRegEx(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue, Type::IntValue})) {
        m_newContext =
          il->factory->fromValue(il, indexOf(icRegEx(args[0]), args[1]));
    }
}

void StringValue::runInsert(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::IntValue})) {
        insert(args[0], args[1]);
        m_newContext = il->factory->clone(this);
    }
}

void StringValue::runLastIndexOf(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext =
          il->factory->fromValue(il, lastIndexOf(icString(args[0])));
    }
    else if (checkArgs(args, {Type::StringValue, Type::IntValue})) {
        m_newContext =
          il->factory->fromValue(il, lastIndexOf(icString(args[0]), args[1]));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext =
          il->factory->fromValue(il, lastIndexOf(icRegEx(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue, Type::IntValue})) {
        m_newContext =
          il->factory->fromValue(il, lastIndexOf(icRegEx(args[0]), args[1]));
    }
}

void StringValue::runLeft(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, left(args[0]));
    }
}

void StringValue::runLeftJustified(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        m_newContext =
          il->factory->fromValue(il, leftJustified(args[0], args[1]));
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::StringValue, Type::BoolValue})) {
        m_newContext =
          il->factory->fromValue(il, leftJustified(args[0], args[1], args[2]));
    }
}

void StringValue::runMid(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, mid(args[0]));
    }
    else if (checkArgs(args, {Type::IntValue, Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, mid(args[0], args[1]));
    }
}

void StringValue::runPrepend(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        prepend(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void StringValue::runRemove(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue})) {
        remove(int(args[0]), args[1]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {Type::StringValue})) {
        remove(icString(args[0]));
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {Type::StringValue, Type::BoolValue})) {
        remove(icString(args[0]), args[1]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        remove(icRegEx(args[0]));
        m_newContext = il->factory->clone(this);
    }
}

void StringValue::runReplace(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue, Type::StringValue})) {
        replace(int(args[0]), args[1], args[2]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {Type::StringValue, Type::StringValue})) {
        replace(icString(args[0]), args[1]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {Type::RegexValue, Type::StringValue})) {
        replace(icRegEx(args[0]), args[1]);
        m_newContext = il->factory->clone(this);
    }
}

void StringValue::runRight(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, right(args[0]));
    }
}

void StringValue::runRightJustified(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        m_newContext =
          il->factory->fromValue(il, rightJustified(args[0], args[1]));
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::StringValue, Type::BoolValue})) {
        m_newContext =
          il->factory->fromValue(il, rightJustified(args[0], args[1], args[2]));
    }
}

void StringValue::runSplit(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, split(icString(args[0])));
    }
    else if (checkArgs(args, {Type::StringValue, Type::BoolValue})) {
        m_newContext =
          il->factory->fromValue(il, split(icString(args[0]), args[1]));
    }
    else if (checkArgs(
               args, {Type::StringValue, Type::BoolValue, Type::BoolValue})) {
        m_newContext = il->factory->fromValue(
          il, split(icString(args[0]), args[1], args[2]));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = il->factory->fromValue(il, split(icRegEx(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue, Type::BoolValue})) {
        m_newContext =
          il->factory->fromValue(il, split(icRegEx(args[0]), args[1]));
    }
}

void StringValue::runSubstring(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, substring(args[0], args[1]));
    }
}

void StringValue::runToLowerCase(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, toLowerCase());
    }
}

void StringValue::runToUpperCase(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, toUpperCase());
    }
}

void StringValue::runTrim(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, trim());
    }
    else if (checkArgs(args, {Type::BoolValue})) {
        m_newContext = il->factory->fromValue(il, trim(args[0]));
    }
}

Type StringValue::type() const {
    return Type::StringValue;
}

icString StringValue::typeName() {
    return "icString";
}

void StringValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (StringValue::*)()> properties{
      {"empty", &StringValue::runEmpty},
      {"length", &StringValue::runLength},
      {"last", &StringValue::runLast}};

    runPropertyWithIndexCheck<StringValue, BaseValue>(
      properties, prefix, name, [this](int i) -> icVariant { return at(i); });
}

void StringValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (StringValue::*)(const memory::ArgList &)>
      methods{{"append", &StringValue::runAppend},
              {"at", &StringValue::runAt},
              {"beginsWith", &StringValue::runBeginsWith},
              {"compare", &StringValue::runCompare},
              {"count", &StringValue::runCount},
              {"endsWith", &StringValue::runEndsWith},
              {"indexOf", &StringValue::runIndexOf},
              {"insert", &StringValue::runInsert},
              {"lastIndexOf", &StringValue::runLastIndexOf},
              {"left", &StringValue::runLeft},
              {"leftJustified", &StringValue::runLeftJustified},
              {"mid", &StringValue::runMid},
              {"prepend", &StringValue::runPrepend},
              {"remove", &StringValue::runRemove},
              {"replace", &StringValue::runReplace},
              {"right", &StringValue::runRight},
              {"rightJustified", &StringValue::runRightJustified},
              {"split", &StringValue::runSplit},
              {"substring", &StringValue::runSubstring},
              {"toLowerCase", &StringValue::runToLowerCase},
              {"toUpperCase", &StringValue::runToUpperCase},
              {"trim", &StringValue::runTrim}};

    runMethodNow<StringValue, BaseValue>(methods, name, args);
}

const icSet<int> & StringValue::acceptedPrevs() {
    static icSet<int> roles = {Role::NoRole, Role::Method, Role::Function,
                                Role::Assign, Role::Comma,  Role::Operator,
                                Role::JsRun,  Role::JsFile};
    return roles;
}

}  // namespace icL::core::ce
