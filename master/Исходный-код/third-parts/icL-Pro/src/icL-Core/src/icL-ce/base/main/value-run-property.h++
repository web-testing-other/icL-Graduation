#ifndef core_ce_Value_RunPropertyNow
#define core_ce_Value_RunPropertyNow

#include "value.h++"

namespace icL::core::ce {

template <typename This, typename ParentClass>
void Value::runPropertyNow(
  const icObject<icString, void (This::*)()> & properties, Prefix prefix,
  const icString & name) {
    auto this_ = dynamic_cast<This *>(this);
    auto it    = properties.find(name);

    if (it != properties.end()) {
        (this_->*it.value())();
    }
    else {
        this_->ParentClass::runProperty(prefix, name);
    }
}

}  // namespace icL::core::ce

#endif  // core_ce_Value_RunPropertyNow
