#ifndef core_ce_Comma
#define core_ce_Comma

#include <icL-ce/base/alu-operator/system-operator.h++>



namespace icL::core::ce {

/**
 * @brief The Comma class represent a comma (`,`) token
 */
class icL_core_ce_operators_ALU_EXPORT Comma : public SystemOperator
{
public:
    Comma(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;

    icString toString() override;
    StepType runNow() override;
    int      role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // Operator interface
public:
    void run(const memory::ArgList &, const memory::ArgList &) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Comma
