#include "base-value.h++"

#include <icL-types/replaces/ic-debug.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/state/datacontainer.h++>
#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>

#include <memory>
#include <utility>

namespace icL::core::ce {

BaseValue::BaseValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : Value(il)
    , container(container)
    , varName(varName)
    , valuetype(ValueType::L)
    , readonly(readonly) {}

BaseValue::BaseValue(il::InterLevel * il, icVariant rvalue)
    : Value(il)
    , rvalue(std::move(rvalue))
    , valuetype(ValueType::R)
    , readonly(true) {}

BaseValue::BaseValue(BaseValue * value)
    : Value(value->il) {
    valuetype = value->valuetype;
    readonly  = value->readonly;

    if (valuetype == L) {
        container = value->container;
        varName   = value->varName;
    }
    else if (valuetype == R) {
        rvalue = value->rvalue;
        fixator = value->fixator;
    }
}

bool BaseValue::isLValue() const {
    return valuetype == L;
}

bool BaseValue::isRValue() const {
    return valuetype == R;
}

void BaseValue::rebind() {
    auto * it = il->mem->stackIt().stack();

    while (!it->isContainer()) {
        it = it->getPrev();
    }

    container = it;
}

il::CE * BaseValue::ensureRValue() {
    return il->factory->fromValue(il, value());
}

void BaseValue::runEnsureRValue(const memory::ArgList & args) {
    if (args.isEmpty()) {
        m_newContext = ensureRValue();
    }
    else {
        Value::runMethod("ensureRValue", args);
    }
}

bool BaseValue::hasValue() {
    return true;
}

icString BaseValue::toString() {
    memory::Argument arg;
    arg.type      = type();
    arg.value     = value();
    arg.varName   = varName;
    arg.container = container;
    return service::Stringify::argConstructor(il, arg, container);
}

int BaseValue::role() {
    return Role::Value;
}

void BaseValue::colorize() {
    if (container != nullptr) {
        if (container->getContainerType() == memory::ContainerType::Stack) {
            il->vms->markToken(m_fragmentData, il::Token::LocalVariable);
        }
        else if (
          container->getContainerType() == memory::ContainerType::State) {
            il->vms->markToken(m_fragmentData, il::Token::GlobalVariable);
        }
    }
}

void BaseValue::runMethod(const icString & name, const memory::ArgList & args) {
    if (name == "ensureRValue") {
        runEnsureRValue(args);
    }
    else {
        Value::runMethod(name, args);
    }
}

void BaseValue::installFixator(
  const std::shared_ptr<il::IValueFixator> & fixator) {
    this->fixator = fixator;
}

const icSet<int> & BaseValue::acceptedPrevs() {
    static icSet<int> roles{
      Role::NoRole,   Role::Any,    Role::If,       Role::For,    Role::Range,
      Role::Filter,   Role::Method, Role::Function, Role::Assign, Role::Comma,
      Role::Operator, Role::JsRun,  Role::Now};
    return roles;
}

const icSet<int> & BaseValue::acceptedNexts() {
    static icSet<int> roles{Role::NoRole,     Role::Assign,  Role::Comma,
                             Role::RunContext, Role::Cast,    Role::Operator,
                             Role::Method,     Role::Property};
    return roles;
}

memory::PackedValueItem BaseValue::packNow() const {
    memory::PackedValueItem ret;

    ret.itemType  = il::PackableType::Value;
    ret.value     = value();
    ret.name      = varName;
    ret.container = container;
    ret.type      = type();

    return ret;
}

il::PackableType BaseValue::packableType() const {
    return il::PackableType::Value;
}

Type BaseValue::type() const {
    return Type::Last;
}

icVariant BaseValue::value() const {
    icVariant value;

    if (valuetype == R) {
        value = rvalue;
    }
    if (valuetype == L) {
        value = container->getValue(varName);
    }

    if (fixator != nullptr) {
        fixator->getter(value);
    }

    return value;
}

void BaseValue::reset(const icVariant & value) {
    if (valuetype == R) {
        if (fixator != nullptr) {
            fixator->setter(rvalue, value);
        }
        else {
            rvalue = value;
        }
    }
    else if (valuetype == L) {
        if (fixator) {
            auto toset = container->getValue(varName);
            fixator->setter(toset, value);
            container->setValue(varName, toset);
        }
        else {
            container->setValue(varName, value);
        }
    }
}

}  // namespace icL::core::ce
