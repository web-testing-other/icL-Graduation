#include "types.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::core::ce {

Types::Types(il::InterLevel * il)
    : SystemValue(il) {}

void Types::runBool() {
    m_newContext = il->factory->fromValue(il, int(Type::BoolValue));
}

void Types::runDouble() {
    m_newContext = il->factory->fromValue(il, int(Type::DoubleValue));
}

void Types::runInt() {
    m_newContext = il->factory->fromValue(il, int(Type::IntValue));
}

void Types::runLambda_icL() {
    m_newContext = il->factory->fromValue(il, int(Type::LambdaValue));
}

void Types::runList() {
    m_newContext = il->factory->fromValue(il, int(Type::ListValue));
}

void Types::runObject() {
    m_newContext = il->factory->fromValue(il, int(Type::ObjectValue));
}

void Types::runSet() {
    m_newContext = il->factory->fromValue(il, int(Type::SetValue));
}

void Types::runString() {
    m_newContext = il->factory->fromValue(il, int(Type::StringValue));
}


void Types::runVoid() {
    m_newContext = il->factory->fromValue(il, int(Type::VoidValue));
}

Type Types::type() const {
    return Type::Types;
}

icString Types::typeName() {
    return "Types";
}

void Types::runProperty(Prefix prefix, const icString & name) {
    static const icObject<icString, void (Types::*)()> properties{
      {"bool", &Types::runBool},     {"double", &Types::runDouble},
      {"int", &Types::runInt},       {"list", &Types::runList},
      {"object", &Types::runObject}, {"set", &Types::runSet},
      {"string", &Types::runString}, {"void", &Types::runVoid},
    };

    runPropertyWithPrefixCheck<Types, SystemValue>(properties, prefix, name);
}

}  // namespace icL::core::ce
