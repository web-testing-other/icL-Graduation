#include "set-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/values/set.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::ce {

using il::Signals::Signals;

SetValue::SetValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

SetValue::SetValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

SetValue::SetValue(BaseValue * value)
    : BaseValue(value) {}

SetValue::SetValue(il::InterLevel * il, const icList<icRect> & rects)
    : BaseValue(il, icVariant{}) {
    memory::Set     set;
    memory::Columns columns{{Type::IntValue, "x"},
                            {Type::IntValue, "y"},
                            {Type::IntValue, "width"},
                            {Type::IntValue, "height"}};
    memory::SetData data;

    for (auto & rect : rects) {
        data.insert({rect.x(), rect.y(), rect.width(), rect.height()});
    }

    set.header  = std::make_shared<memory::Columns>(columns);
    set.setData = std::make_shared<memory::SetData>(data);

    BaseValue::reset(set);
}

void SetValue::runCapacity() {
    m_newContext = il->factory->fromValue(il, capacity());
}

void SetValue::runEmpty() {
    m_newContext = il->factory->fromValue(il, empty());
}

void SetValue::runApplicate(const memory::ArgList & args) {
    bool eachArgIsList = true;

    for (auto & arg : args) {
        eachArgIsList =
          eachArgIsList && !arg.value.isNull() && arg.type == Type::ListValue;
    }

    if (eachArgIsList) {
        icList<icStringList> args_values;

        for (const auto & arg : args) {
            args_values.append(icString(arg));
        }

        applicate(args_values);
        m_newContext = il->factory->clone(this);
    }
    else {
        il->vm->signal({Signals::IncompatibleData, {}});
    }
}

void SetValue::runClear(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = il->factory->clone(this);
    }
}

void SetValue::runClone(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, clone());
    }
}

void SetValue::runGetField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, getField(args[0]));
    }
}

void SetValue::runHasField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, hasField(args[0]));
    }
}

void SetValue::runInsert(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        insert(memory::Object(args[0]));
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args)) {
        insert(service::Set::argsToList(args));
        m_newContext = il->factory->clone(this);
    }
}

void SetValue::runInsertField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::ListValue})) {
        insertField(icString(args[0]), icStringList(args[1]));
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(
               args, {Type::StringValue, Type::ListValue, Type::TypeId})) {
        insertField(icString(args[0]), icStringList(args[1]), args[2].type);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {Type::StringValue, Type::AnyValue})) {
        insertField(args[0], args[1].value);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(
               args, {Type::StringValue, Type::AnyValue, Type::TypeId})) {
        insertField(args[0], args[1].value, args[2].type);
        m_newContext = il->factory->clone(this);
    }
}

void SetValue::runRemove(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        remove(memory::Object(args[0]));
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args)) {
        remove(service::Set::argsToList(args));
        m_newContext = il->factory->clone(this);
    }
}

void SetValue::runRemoveField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        removeField(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

Type SetValue::type() const {
    return Type::SetValue;
}

icString SetValue::typeName() {
    return "icSet";
}

void SetValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (SetValue::*)()> properties{
      {"capacity", &SetValue::runCapacity}, {"empty", &SetValue::runEmpty}};

    runPropertyWithPrefixCheck<SetValue, BaseValue>(properties, prefix, name);
}

void SetValue::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (SetValue::*)(const memory::ArgList &)>
      methods{{"applicate", &SetValue::runApplicate},
              {"clear", &SetValue::runClear},
              {"clone", &SetValue::runClone},
              {"getField", &SetValue::runGetField},
              {"hasField", &SetValue::runHasField},
              {"insert", &SetValue::runInsert},
              {"insertField", &SetValue::runInsertField},
              {"remove", &SetValue::runRemove},
              {"removeField", &SetValue::runRemoveField}};

    runMethodNow<SetValue, BaseValue>(methods, name, args);
}

}  // namespace icL::core::ce
