#ifndef core_ce_ArithmeticalOperator
#define core_ce_ArithmeticalOperator

#include "alu-operator.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT ArithmeticalOperator : public AluOperator
{
public:
    ArithmeticalOperator(il::InterLevel * il);

protected:
    /**
     * @brief runRank returns the run rank of this operator
     * @return the run rank of this operator
     */
    virtual int runRank() = 0;

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_ArithmeticalOperator
