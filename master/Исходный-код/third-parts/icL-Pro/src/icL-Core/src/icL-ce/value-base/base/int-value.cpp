#include "int-value.h++"

namespace icL::core::ce {

IntValue::IntValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

IntValue::IntValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

IntValue::IntValue(BaseValue * value)
    : BaseValue(value) {}

Type IntValue::type() const {
    return Type::IntValue;
}

icString IntValue::typeName() {
    return "int";
}

void IntValue::runMethod(const icString & name, const memory::ArgList & args) {
    BaseValue::runMethod(name, args);
}

}  // namespace icL::core::ce
