#include "negation.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::core::ce {

Negation::Negation(il::InterLevel * il)
    : LogicOperator(il) {}

int Negation::currentRunRank(bool rtl) {
    bool runnable =
      m_next->hasValue() &&
      dynamic_cast<il::PackableValue *>(m_next)->type() == Type::BoolValue;
    return runnable && rtl ? 7 : -1;
}

il::CE * Negation::firstToReplace() {
    return this;
}

const icSet<int> & Negation::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole, Role::Function,
                                   Role::Method, Role::Assign,
                                   Role::Comma,  Role::Operator};
    return roles;
}

icString Negation::toString() {
    return "!";
}

void Negation::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (left.isEmpty()) {
        m_newContext = il->factory->fromValue(il, !right[0].value);
    }
}



}  // namespace icL::core::ce
