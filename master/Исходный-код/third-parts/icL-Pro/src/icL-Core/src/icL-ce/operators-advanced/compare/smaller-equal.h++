#ifndef core_ce_SmallerEqual
#define core_ce_SmallerEqual

#include <icL-service/operators-advanced/compare/smaller-equal.h++>

#include <icL-ce/base/advanced-operator/compare-operator.h++>



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT SmallerEqual
    : public CompareOperator
    , public service::SmallerEqual
{
public:
    SmallerEqual(il::InterLevel * il);

    /// `int <= int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double <= double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_SmallerEqual
