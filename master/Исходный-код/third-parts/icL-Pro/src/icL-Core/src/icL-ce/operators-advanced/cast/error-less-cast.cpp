#include "error-less-cast.h++"

#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel.h++>

#include <icL-service/cast/base/list-cast.h++>
#include <icL-service/cast/base/set-cast.h++>
#include <icL-service/cast/base/string-cast.h++>

#include <icL-ce/value-base/base/double-value.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/base/string-value.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/complex/object-value.h++>
#include <icL-ce/value-base/complex/set-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>
#include <icL-mock/vmlayer.h++>



namespace icL::core::ce {

ErrorLessCast::ErrorLessCast(il::InterLevel * il)
    : Cast(il) {}

template <typename ReturnType, typename ArgType>
void ErrorLessCast::runAny(
  ReturnType (*func)(il::InterLevel *, const ArgType &),
  const memory::Argument & left) {
    il::InterLevel il{};

    il.vm      = new mock::VMLayer();
    auto value = func(&il, left);

    if (il.vm->hasOkState()) {
        m_newContext = this->il->factory->fromValue(this->il, value);
    }
    else {
        m_newContext = this->il->factory->fromValue(this->il, {});
    }

    delete il.vm;
}

void ErrorLessCast::runAlwaysVoid(const memory::Argument & /*left*/) {
    m_newContext = il->factory->fromValue(il, {});
}

icString ErrorLessCast::toString() {
    return ":?";
}

void ErrorLessCast::runUnhandled(const memory::Argument & left) {
    runAlwaysVoid(left);
}

void ErrorLessCast::runStringInt(const memory::Argument & left) {
    runAny<int, icString>(service::StringCast::toInt, left);
}

void ErrorLessCast::runStringDouble(const memory::Argument & left) {
    runAny<double, icString>(service::StringCast::toDouble, left);
}

void ErrorLessCast::runStringObject(const memory::Argument & left) {
    runAny<memory::Object, icString>(service::StringCast::toObject, left);
}

void ErrorLessCast::runStringSet(const memory::Argument & left) {
    runAny<memory::Set, icString>(service::StringCast::toSet, left);
}

void ErrorLessCast::runListString(const memory::Argument & left) {
    runAny<icString, icStringList>(service::ListCast::toString, left);
}

void ErrorLessCast::runListSet(const memory::Argument & left) {
    runAny<memory::Set, icStringList>(service::ListCast::toSet, left);
}

void ErrorLessCast::runSetObject(const memory::Argument & left) {
    runAny<memory::Object, memory::Set>(service::SetCast::toObject, left);
}

}  // namespace icL::core::ce
