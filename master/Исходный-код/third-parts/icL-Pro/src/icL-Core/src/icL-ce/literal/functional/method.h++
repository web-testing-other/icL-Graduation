#ifndef core_ce_Method
#define core_ce_Method

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce/base/literal/functional-literal.h++>



namespace icL::core::ce {

class icL_core_ce_literal_EXPORT Method : public FunctionalLiteral
{
public:
    Method(il::InterLevel * il, const icString & name);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    il::CE * firstToReplace() override;
    il::CE * lastToReplace() override;
    int      role() override;

    void colorize() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // fields
private:
    /// \brief name is the method name
    icString name;
};

}  // namespace icL::core::ce

#endif  // core_ce_Method
