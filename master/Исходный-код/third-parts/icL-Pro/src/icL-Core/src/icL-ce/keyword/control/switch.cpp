#include "switch.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/operators-ALU/system/context.h++>

namespace icL::core::ce {

Switch::Switch(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Switch::toString() {
    return "switch";
}

int Switch::currentRunRank(bool rtl) {
    return !rtl ? 9 : -1;
}

StepType Switch::runNow() {
    return transact();
}

int Switch::role() {
    return Role::Switch;
}

const icSet<int> & Switch::acceptedPrevs() {
    static const icSet<int> roles{Role::NoRole};
    return roles;
}

const icSet<int> & Switch::acceptedNexts() {
    static const icSet<int> roles{Role::Value, Role::ValueContext};
    return roles;
}

void Switch::initialize() {
    bool ok   = true;
    CE * it   = this;
    int  prev = Role::NoRole;

    while (it != nullptr && ok) {
        switch (it->role()) {
        case Role::Switch:
            ok = prev == Role::NoRole;
            break;

        case Role::Value:
        case Role::ValueContext:
            ok = prev == Role::Switch || prev == Role::Case;
            break;

        case Role::RunContext:
            ok = prev == Role::Value || prev == Role::ValueContext;
            break;

        case Role::Case:
            ok = prev == Role::RunContext || prev == Role::Value ||
                 prev == Role::ValueContext;
            break;

        default:
            ok = false;
        }

        prev = it->role();
        it   = dynamic_cast<CE *>(it->next());
    }

    if (!ok || prev != Role::RunContext) {
        il->vm->signal(
          {il::Signals::System, "Syntax error in switch-case statement"});
        return;
    }

    it = dynamic_cast<CE *>(this->next());

    if (it->role() == Role::Value) {
        switchCode = it->fragmentData();
    }
    else {
        switchCode = dynamic_cast<Context *>(it)->getCode();
    }

    it = dynamic_cast<CE *>(it->next());

    while (it != nullptr) {
        CaseData cd;

        auto * case_      = dynamic_cast<CE *>(it);
        auto * case_value = dynamic_cast<CE *>(case_->next());
        auto * case_code  = dynamic_cast<Context *>(case_value->next());

        if (it->role() == Role::Value) {
            cd.values = case_value->fragmentData();
        }
        else {
            cd.values = dynamic_cast<Context *>(case_value)->getCode();
        }

        cd.body = case_code->getCode();

        cases.append(cd);

        it = dynamic_cast<CE *>(case_code->next());
    }
}

void Switch::finalize() {
    m_newContext = il->factory->fromValue(il, returnValue);
}

}  // namespace icL::core::ce
