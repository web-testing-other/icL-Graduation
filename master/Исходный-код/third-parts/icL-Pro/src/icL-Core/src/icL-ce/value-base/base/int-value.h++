#ifndef core_ce_IntValue
#define core_ce_IntValue

#include <icL-ce/base/value/base-value.h++>



namespace icL::core::ce {

/**
 * @brief The IntValue class represents an `int` value
 */
class icL_core_ce_value_base_EXPORT IntValue : public BaseValue
{
public:
    /// @brief IntValue calls BaseValue(il, container, varName, readonly)
    IntValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief IntValue calls BaseValue(il, rvalue)
    IntValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief IntValue calls BaseValue(value)
    IntValue(BaseValue * value);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;

    // Value interface
public:
    void runMethod(
      const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_IntValue
