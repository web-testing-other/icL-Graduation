#ifndef core_ce_If
#define core_ce_If

#include "if-base.h++"

#include <icL-service/keyword/control/if.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT If
    : public IfBase
    , public service::If
{
public:
    If(il::InterLevel * il, bool notMod);

    // CE interface
public:
    StepType runNow() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // IfBase interface
protected:
    bool hasNotModifier() override;
    void setNotModifier() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_If
