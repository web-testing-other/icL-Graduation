#ifndef core_ce_IfBase
#define core_ce_IfBase

#include <icL-service/keyword/main/finite-state-machine.h++>

#include <icL-ce/base/keyword/control-keyword.h++>



namespace icL::core::ce {

class icL_core_ce_keyword_EXPORT IfBase : public ControlKeyword
{
public:
    IfBase(il::InterLevel * il);

protected:
    virtual bool hasNotModifier() = 0;
    virtual void setNotModifier() = 0;

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    icString toString() override;

    // CE interface
public:
    int role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_IfBase
