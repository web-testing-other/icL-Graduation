#ifndef core_ce_Literal
#define core_ce_Literal

#include "ce.h++"



namespace icL::core::ce {

class icL_core_ce_base_EXPORT Literal : public CE
{
public:
    Literal(il::InterLevel * il);
};

}  // namespace icL::core::ce

#endif  // core_ce_Literal
