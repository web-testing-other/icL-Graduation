#ifndef core_ce_DoubleValue
#define core_ce_DoubleValue

#include <icL-ce/base/value/base-value.h++>



namespace icL::core::ce {

/**
 * @brief The DoubleValue class represents a `double` value
 */
class icL_core_ce_value_base_EXPORT DoubleValue : public BaseValue
{
public:
    /// @brief DoubleValue calls BaseValue(il, container, varName, readonly)
    DoubleValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief DoubleValue calls BaseValue(il, rvalue)
    DoubleValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief DoubleValue calls BaseValue(value)
    DoubleValue(BaseValue * value);

    // Value interface
public:
    Type type() const override;
    icString typeName() override;
};

}  // namespace icL::core::ce

#endif  // core_ce_DoubleValue
