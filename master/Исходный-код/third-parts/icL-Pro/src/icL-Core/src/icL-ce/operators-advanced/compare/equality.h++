#ifndef core_ce_Equality
#define core_ce_Equality

#include <icL-service/operators-advanced/compare/equality.h++>

#include <icL-ce/base/advanced-operator/compare-operator.h++>



namespace icL::core::ce {

class icL_core_ce_operators_advanced_EXPORT Equality
    : public CompareOperator
    , public service::Equality
{
public:
    Equality(il::InterLevel * il);

    // level 2

    /// `bool == bool`
    void runBoolBool(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `int == int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double == double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string == string`
    void runStringString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `list == list`
    void runListList(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `object == object`
    void runObjectObject(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set == set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::core::ce

#endif  // core_ce_Equality
