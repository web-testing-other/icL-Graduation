#include "division.h++"

#include <icL-ce/base/main/operator-run-now.h++>
#include <icL-ce/value-base/base/double-value.h++>
#include <icL-ce/value-base/base/int-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::ce {

Division::Division(il::InterLevel * il)
    : ArithmeticalOperator(il) {}

void Division::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, intInt(left[0], right[0]));
}

void Division::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = il->factory->fromValue(il, doubleDouble(left[0], right[0]));
}

icString Division::toString() {
    return "/";
}

void Division::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<Type>, icList<Type>>,
      void (Division::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue}}, &Division::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue}},
                 &Division::runDoubleDouble}};

    runNow<Division>(operators, left, right);
}

int Division::runRank() {
    return 5;
}

}  // namespace icL::core::ce
