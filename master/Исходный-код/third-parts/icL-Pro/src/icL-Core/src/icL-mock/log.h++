#ifndef core_mock_Log
#define core_mock_Log

#include <icL-il/main/log.h++>



namespace icL::core::mock {

class Log : public il::Log
{
    // Log interface
public:
    void logError(const icString & message) override;
    void logInfo(const icString & message) override;
};

}  // namespace icL::core::mock

#endif  // core_mock_Log
