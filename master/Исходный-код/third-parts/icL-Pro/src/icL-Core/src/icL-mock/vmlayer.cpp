#include "vmlayer.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <QThread>

namespace icL::core::mock {

void VMLayer::signal(const il::Signal & signal) {
    m_signal = signal;
}

void VMLayer::syssig(const icString & message) {
    signal({il::Signals::System, message});
}

void VMLayer::cp_sig(const icString & message) {
    syssig(message);
}

void VMLayer::cpe_sig(const icString & message) {
    syssig(message);
}

void VMLayer::sendAssert(const icString &) {
    // Do nothing
}

void VMLayer::sleep(int ms) {
    QThread::msleep(uint64_t(ms));
}

void VMLayer::addDescription(const icString &) {
    // Do nothing
}

void VMLayer::markStep(const icString &) {
    // Do nothing
}

void VMLayer::markTest(const icString &) {
    // Do nothing
}

void VMLayer::break_() {
    // Do nothing
}

void VMLayer::continue_() {
    // Do nothing
}

void VMLayer::return_(const icVariant &) {
    // Do nothing
}

bool VMLayer::hasOkState() {
    return m_signal.code == il::Signals::NoError;
}

void VMLayer::finalize() {
    // Do nothing
}

icVariant VMLayer::get(int /*ext*/) {
    return {};
}

icVariant VMLayer::set(int /*ext*/, const icVariant & /*value*/) {
    return {};
}

il::VMLayer * VMLayer::parent() {
    return nullptr;
}

}  // namespace icL::core::mock
