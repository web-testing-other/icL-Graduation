#ifndef core_mock_VMLayer
#define core_mock_VMLayer

#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>



namespace icL::core::mock {

class icL_core_mock_EXPORT VMLayer : public il::VMLayer
{
public:
    VMLayer() = default;

    // VMLayer interface
public:
    void signal(const il::Signal & signal) override;
    void syssig(const icString & message) override;
    void cp_sig(const icString & message) override;
    void cpe_sig(const icString & message) override;
    void sendAssert(const icString & message) override;
    void sleep(int ms) override;
    void addDescription(const icString & description) override;
    void markStep(const icString & name) override;
    void markTest(const icString & name) override;
    void break_() override;
    void continue_() override;
    void return_(const icVariant & value) override;
    bool hasOkState() override;
    void finalize() override;

    icVariant get(int ext) override;
    icVariant set(int ext, const icVariant & value) override;

    il::VMLayer * parent() override;

private:
    il::Signal m_signal;
};

}  // namespace icL::core::mock

#endif  // core_mock_VMLayer
