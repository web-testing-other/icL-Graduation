#include "log.h++"

#include <icL-types/replaces/ic-debug.h++>
#include <icL-types/replaces/ic-string.h++>

namespace icL::core::mock {

void Log::logError(const icString & message) {
    icDebug() << "output error:" << message;
}

void Log::logInfo(const icString & message) {
    icDebug() << "output info:" << message;
}

}  // namespace icL::core::mock
