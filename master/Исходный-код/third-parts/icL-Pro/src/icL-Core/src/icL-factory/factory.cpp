#include "factory.h++"

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce/base/value/packed-value.h++>
#include <icL-ce/value-base/base/bool-value.h++>
#include <icL-ce/value-base/base/double-value.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/base/string-value.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/complex/datetime-value.h++>
#include <icL-ce/value-base/complex/list-value.h++>
#include <icL-ce/value-base/complex/object-value.h++>
#include <icL-ce/value-base/complex/regex-value.h++>
#include <icL-ce/value-base/complex/set-value.h++>

#include <icL-memory/state/datacontainer.h++>
#include <icL-memory/structures/set.h++>
#include <icL-memory/structures/type.h++>



namespace icL::core::factory {

using memory::Type;

memory::ArgList Factory::listify(
  il::InterLevel * il, il::PackableValue * value) {
    memory::ArgList ret;

    auto * packed = dynamic_cast<il::ValuePack *>(value);

    if (packed != nullptr) {
        for (auto & value : packed->getValues()) {
            memory::Argument arg;

            if (
              value.itemType == il::PackableType::Value ||
              value.itemType == il::PackableType::Type) {
                arg.type      = value.type;
                arg.varName   = value.name;
                arg.value     = value.value;
                arg.container = value.container;
            }
            else {
                il->vm->syssig("Wrong arguments list");
                break;
            }

            ret.append(arg);
        }
    }
    else {
        memory::Argument        arg;
        memory::PackedValueItem packedValue = value->packNow();

        if (
          packedValue.type != Type::VoidValue ||
          packedValue.container != nullptr) {

            arg.type      = packedValue.type;
            arg.varName   = packedValue.name;
            arg.value     = packedValue.value;
            arg.container = packedValue.container;

            ret.append(arg);
        }
    }

    return ret;
}

il::CE * Factory::fromValue(il::InterLevel * il, const icVariant & value) {
    ce::CE * ret = nullptr;

    switch (value.type()) {
    case icType::Bool:
        ret = new ce::BoolValue{il, value};
        break;

    case icType::Int:
        ret = new ce::IntValue{il, value};
        break;

    case icType::Double:
        ret = new ce::DoubleValue{il, value};
        break;

    case icType::String:
        ret = new ce::StringValue{il, value};
        break;

    case icType::StringList:
        ret = new ce::ListValue{il, value};
        break;

    case icType::RegEx:
        ret = new ce::RegexValue{il, value};
        break;

    case icType::DateTime:
        ret = new ce::DatetimeValue{il, value};
        break;

    case icType::Set:
        ret = new ce::SetValue{il, value};
        break;

    case icType::Object:
        ret = new ce::ObjectValue{il, value};
        break;

    case icType::Packed:
        ret = new ce::PackedValue{il, value};
        break;

    case icType::Initial:
        ret = new ce::VoidValue{il};
        break;

    default:
        ret = new ce::VoidValue{il};
        break;
    }

    return ret;
}

il::CE * Factory::fromValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & var) {

    ce::CE * ret = nullptr;

    switch (container->getType(var)) {
        using namespace memory::TypeNM;

    case BoolValue:
        ret = new ce::BoolValue{il, container, var};
        break;

    case IntValue:
        ret = new ce::IntValue{il, container, var};
        break;

    case DoubleValue:
        ret = new ce::DoubleValue{il, container, var};
        break;

    case StringValue:
        ret = new ce::StringValue{il, container, var};
        break;

    case ListValue:
        ret = new ce::ListValue{il, container, var};
        break;

    case RegexValue:
        ret = new ce::RegexValue{il, container, var};
        break;

    case DatetimeValue:
        ret = new ce::DatetimeValue{il, container, var};
        break;

    case SetValue:
        ret = new ce::SetValue{il, container, var};
        break;

    case ObjectValue:
        ret = new ce::ObjectValue{il, container, var};
        break;

    default:
        ret = new ce::VoidValue{il, container, var};
        break;
    }

    return ret;
}

il::CE * Factory::clone(il::ValuePack * pack) {
    return new ce::PackedValue{pack};
}

il::CE * Factory::clone(il::PackableValue * value, short type) {

    ce::CE * ret       = nullptr;
    auto     baseValue = dynamic_cast<ce::BaseValue *>(value);

    if (type == -1) {
        type = value->type();
    }

    switch (type) {
        using namespace memory::TypeNM;

    case BoolValue:
        ret = new ce::BoolValue{baseValue};
        break;

    case IntValue:
        ret = new ce::IntValue{baseValue};
        break;

    case DoubleValue:
        ret = new ce::DoubleValue{baseValue};
        break;

    case StringValue:
        ret = new ce::StringValue{baseValue};
        break;

    case ListValue:
        ret = new ce::ListValue{baseValue};
        break;

    case RegexValue:
        ret = new ce::RegexValue{baseValue};
        break;

    case DatetimeValue:
        ret = new ce::DatetimeValue{baseValue};
        break;

    case SetValue:
        ret = new ce::SetValue{baseValue};
        break;

    case ObjectValue:
        ret = new ce::ObjectValue{baseValue};
        break;

    default:
        ret = new ce::VoidValue{baseValue};
        break;
    }

    return ret;
}

Type Factory::variantToType(const icVariant & var) {
    using namespace memory::TypeNM;
    using namespace icTypeNM;

    static const icObject<icType, Type> map{
      {Void, VoidValue},       {Bool, BoolValue},     {Int, IntValue},
      {Double, DoubleValue},   {String, StringValue}, {DateTime, DatetimeValue},
      {StringList, ListValue}, {RegEx, RegexValue},   {Object, ObjectValue},
      {Set, SetValue},         {Packed, PackedValue}};

    return map.value(var.type(), Type::VoidValue);
}

icType Factory::typeToVarType(const Type type) {
    using namespace memory::TypeNM;
    using namespace icTypeNM;

    static const icObject<Type, icType> map{
      {VoidValue, Void},       {BoolValue, Bool},     {IntValue, Int},
      {DoubleValue, Double},   {StringValue, String}, {DatetimeValue, DateTime},
      {ListValue, StringList}, {RegexValue, RegEx},   {ObjectValue, Object},
      {SetValue, Set},         {PackedValue, Packed}};

    return map.value(type, icType::Initial);
}

icString Factory::typeToString(const short & type) {
    using namespace memory::TypeNM;

    static const icObject<int, icString> names{
      {BoolValue, "bool"},     {DatetimeValue, "datetime"},
      {DoubleValue, "double"}, {IntValue, "int"},
      {ListValue, "list"},     {ObjectValue, "object"},
      {RegexValue, "regex"},   {SetValue, "set"},
      {StringValue, "string"}, {VoidValue, "void"},
      {PackedValue, "packed"}, {LambdaValue, "lambda-icl"}};

    return names.value(type, "unknown");
}

}  // namespace icL::core::factory
