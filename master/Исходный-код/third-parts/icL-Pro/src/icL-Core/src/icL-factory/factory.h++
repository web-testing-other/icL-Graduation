#ifndef core_factory_Factory
#define core_factory_Factory

#include <icL-il/export/global.h++>
#include <icL-il/main/factory.h++>



namespace icL::core::factory {

class icL_core_factory_EXPORT Factory : public il::Factory
{
    // Factory interface
public:
    memory::ArgList listify(
      il::InterLevel * il, il::PackableValue * value) override;

    il::CE * fromValue(il::InterLevel * il, const icVariant & value) override;
    il::CE * fromValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & var) override;
    il::CE * clone(il::ValuePack * pack) override;
    il::CE * clone(il::PackableValue * value, short type) override;

    memory::Type variantToType(const icVariant & var) override;
    icType       typeToVarType(const memory::Type type) override;
    icString typeToString(const short & type) override;
};

}  // namespace icL::core::factory


#endif  // core_factory_Factory
