#include "stateful-server.h++"

#include <icL-il/export/signals.h++>


namespace icL::core::shared {

using il::Signals::Signals;

StatefulServer::StatefulServer()
    : map({{"System", Signals::System},
           {"Exit", Signals::Exit},
           {"FieldNotFound", Signals::FieldNotFound},
           {"FieldAlreadyExists", Signals::FieldAlreadyExists},
           {"OutOfBounds", Signals::OutOfBounds},
           {"EmptyString", Signals::EmptyString},
           {"EmptyList", Signals::EmptyList},
           {"MultiList", Signals::MultiList},
           {"EmptySet", Signals::EmptySet},
           {"MultiSet", Signals::MultiSet},
           {"InvalidArgument", Signals::InvalidArgument},
           {"IncompatibleRoot", Signals::IncompatibleRoot},
           {"IncompatibleData", Signals::IncompatibleData},
           {"IncompatibleObject", Signals::IncompatibleObject},
           {"ParsingFailed", Signals::ParsingFailed},
           {"WrongDelimiter", Signals::WrongDelimiter},
           {"ComplexField", Signals::ComplexField},
           {"Timeout", Signals::Timeout}})
    , lastSignalCode(Signals::Last) {}

bool StatefulServer::registerSignal(const icString & name) {
    if (map.contains(name)) {
        return false;
    }

    map.insert(name, lastSignalCode++);
    return true;
}

int StatefulServer::getSignal(const icString & name) {
    return map.value(name, Signals::System - 1);
}

void StatefulServer::loadDefaults(StatefulServer::Mode mode) {
    switch (mode) {
    case Mode::SelfTesting:
        selfTesting = true;
        break;

    default:;
    }
}

bool StatefulServer::catchSystem() {
    return selfTesting;
}

}  // namespace icL::core::shared
