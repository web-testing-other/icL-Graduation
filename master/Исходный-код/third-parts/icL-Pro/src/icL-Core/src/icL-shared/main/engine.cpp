#include "engine.h++"

#include <icL-cp/cp/cp.h++>
#include <icL-cp/cp/factory.h++>
#include <icL-cp/source/source-server.h++>
#include <icL-factory/factory.h++>
#include <icL-memory/state/memory.h++>
#include <icL-mock/log.h++>
#include <icL-vm/vmstack.h++>

#include <stateful-server.h++>



namespace icL::core::shared {

void Engine::setCpFactory(il::CpFactory * cpf) {
    m_core.cpfactory = cpf;
}

void Engine::setMemory(memory::Memory * mem) {
    m_core.mem = mem;
}

void Engine::setVMStack(il::VMStack * vms) {
    m_core.vms = vms;
}

void Engine::setSourceServer(il::SourceServer * source) {
    m_core.source = source;
}

void Engine::setStatefulServer(il::StatefulServer * stateful) {
    m_core.stateful = stateful;
}

void Engine::setLogServer(il::Log * log) {
    m_core.log = log;
}

void Engine::setFactoryServer(il::Factory * factory) {
    m_core.factory = factory;
}

void Engine::finalize() {
    if (m_core.cpfactory == nullptr) {
        m_core.cpfactory = new cp::Factory{};
    }
    if (m_core.source == nullptr) {
        m_core.source = new cp::SourceServer{&m_core};
    }
    if (m_core.mem == nullptr) {
        m_core.mem = new memory::Memory{&m_core};
    }
    if (m_core.vms == nullptr) {
        m_core.vms = new vm::VMStack{&m_core};
    }
    if (m_core.stateful == nullptr) {
        m_core.stateful = new StatefulServer{};
    }
    if (m_core.log == nullptr) {
        m_core.log = new mock::Log{};
    }
    if (m_core.factory == nullptr) {
        m_core.factory = new factory::Factory{};
    }

    m_core.stateful->loadDefaults(StatefulServer::Mode::SelfTesting);
}

il::InterLevel * Engine::core() {
    return &m_core;
}

}  // namespace icL::core::shared
