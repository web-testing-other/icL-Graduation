#ifndef core_shared_StatefulServer
#define core_shared_StatefulServer

#include <icL-types/replaces/ic-string.h++>

// magic here
#include <icL-types/replaces/ic-object.h++>

#include <icL-il/main/stateful-server.h++>



namespace icL::core::shared {

class icL_core_shared_EXPORT StatefulServer : public il::StatefulServer
{
protected:
    /// \brief map is used to store signals
    icObject<icString, int> map;

    /// \brief lastSignalCode gets a new code for each new signal
    int lastSignalCode;

    /// \brief selfTesting mode
    bool selfTesting = false;

public:
    StatefulServer();

    // StatefulServer interface
public:
    bool registerSignal(const icString & name) override;
    int  getSignal(const icString & name) override;
    void loadDefaults(Mode mode) override;
    bool catchSystem() override;
};

}  // namespace icL::core::shared

#endif  // core_shared_StatefulServer
