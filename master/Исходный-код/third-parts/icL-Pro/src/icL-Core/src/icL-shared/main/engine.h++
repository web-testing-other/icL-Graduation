#ifndef core_shared_Engine
#define core_shared_Engine

#include <icL-il/export/global.h++>
#include <icL-il/main/interlevel.h++>



namespace icL::core::shared {

/**
 * @brief The Engine class represent a icL engine ready for use
 */
class icL_core_shared_EXPORT Engine
{
protected:
    /// \brief m_il represent the interlevel node
    il::InterLevel m_core;

public:
    void setCpFactory(il::CpFactory * cpf);

    /**
     * @brief setMemory sets the memory service
     * @param mem is the pointer to memory service
     */
    void setMemory(memory::Memory * mem);

    /**
     * @brief setVMStack sets the vm stack (factory)
     * @param vms is the VM stack
     */
    void setVMStack(il::VMStack * vms);

    /**
     * @brief setSourceServer sets the source service
     * @param source is the source service to set
     */
    void setSourceServer(il::SourceServer * source);

    /**
     * @brief setStatefulServer sets the stateful service
     * @param stateful is the stateful service to set
     */
    void setStatefulServer(il::StatefulServer * stateful);

    void setLogServer(il::Log * log);

    void setFactoryServer(il::Factory * factory);

    /**
     * @brief finalize initilize unitialized services
     */
    void finalize();

    /**
     * @brief il gets a pointer to main interlevel node
     * @return a pointer to main interserver node
     */
    il::InterLevel * core();
};

}  // namespace icL::core::shared

#endif  // core_shared_Engine
