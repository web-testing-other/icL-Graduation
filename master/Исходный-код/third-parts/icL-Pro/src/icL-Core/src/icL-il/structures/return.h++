#ifndef core_il_Return
#define core_il_Return

#include "signal.h++"

#include <icL-types/replaces/ic-variant.h++>



namespace icL::core::il {

/**
 * @brief The Return class is a result of code executing
 */
struct icL_core_il_EXPORT Return
{
    Return() = default;

    /// \brief returnValue is the value returned by function
    icVariant returnValue = icVariant::makeVoid();

    /// \brief consoleValue is the value returned by last line of code
    icVariant consoleValue = icVariant::makeVoid();

    /// \brief signal is the genereted error
    Signal signal;
};

}  // namespace icL::core::il

#endif  // core_il_Return
