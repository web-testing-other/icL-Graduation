#ifndef core_il_Node
#define core_il_Node

#include "../export/global.h++"



namespace icL::core::il {

struct InterLevel;

/**
 * @brief The Node class is a unit of contextual system
 */
class icL_core_il_EXPORT Node
{
public:
    Node(InterLevel * il)
        : il(il) {}

protected:
    /// \brief il is a pointer to inter-level interface
    InterLevel * il;
};

}  // namespace icL::core::il

#endif  // core_il_Node
