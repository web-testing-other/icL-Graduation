#ifndef core_memory_Signals
#define core_memory_Signals

namespace icL::core::il {

/**
 * The Signals namespace block the distribution of Signals items to memory
 */
namespace Signals {

/**
 * @brief The Signals enum contains all predefined icL signals
 */
enum Signals {
    System  = -2,        ///< Syntax or semantic error.
    Exit    = -1,        ///< Error.
    NoError = 0,         ///< No error
    FieldAlreadyExists,  ///< Field already exists.
    InvalidArgument,     ///< invalid argument
    FieldNotFound,       ///< Field not found
    OutOfBounds,         ///< Out of collection bound.
    EmptyString,         ///< The string is empty.
    EmptyList,           ///< The list is empty.
    MultiList,           ///< The list contains some strings.
    EmptySet,            ///< The set is empty.
    MultiSet,            ///< The set contains some items.
    IncompatibleRoot,    ///< Wrong root of JSON.
    IncompatibleData,    ///< Wrong input data.
    IncompatibleObject,  ///< Wrong input object.
    WrongDelimiter,      ///< Wrong delimiter.
    ComplexField,        ///< The field is complex.
    Timeout,             ///< Timeout.
    ParsingFailed,       ///< (JSON) Parsing failed
    Last
};
}  // namespace Signals

}  // namespace icL::core::il

#endif  // core_memory_Signals
