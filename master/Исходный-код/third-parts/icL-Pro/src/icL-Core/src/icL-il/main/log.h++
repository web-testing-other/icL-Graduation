#ifndef core_il_Log
#define core_il_Log

#include "../export/global.h++"

class icString;

namespace icL::core::il {

class icL_core_il_EXPORT Log
{
public:
    virtual void logError(const icString & message) = 0;
    virtual void logInfo(const icString & message)  = 0;
};

}  // namespace icL::core::il

#endif  // core_il_Log
