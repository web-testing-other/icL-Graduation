#ifndef core_il_StepType
#define core_il_StepType

namespace icL::core::il {

namespace StepTypeNM {

enum StepType {        ///< types of steps in icL command processor
    None       = 0x0,  ///< no step was relased (error occurrer)
    MiniStep   = 0x1,  ///< operation is not completed yet
    CommandEnd = 0x2,  ///< operation completed
    CommandIn  = 0x3,  ///< was opened a new stack layer via interrupt
    CommandOut = 0x4,  ///< a stack layer was closed
    ReadyNow   = 0x5,  ///< command is ready to execute
    Any        = 0xF   ///< just in arg: run to end of file
};

}  // namespace StepTypeNM

using StepTypeNM::StepType;

}  // namespace icL::core::il

#endif  // core_il_StepType
