#ifndef core_il_SourceServer
#define core_il_SourceServer

#include <icL-il/export/global.h++>

#include <memory>


class icString;

namespace icL::core::il {

class SourceOfCode;

class icL_core_il_EXPORT SourceServer
{
public:
    virtual ~SourceServer() = default;

    /**
     * @brief getSource gets a source object of
     * @param path is the path of file in project
     * @return a pointer to source or nullptr if no such file
     */
    virtual std::shared_ptr<SourceOfCode> getSource(const icString & path) = 0;

    /**
     * @brief closeSource closes and remove file from hash
     * @param path is the path to file on HDD
     */
    virtual void closeSource(const icString & path) = 0;
};

}  // namespace icL::core::il

#endif  // core_il_SourceServer
