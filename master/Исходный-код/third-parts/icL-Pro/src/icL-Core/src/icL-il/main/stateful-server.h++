#ifndef core_il_StatefulServer
#define core_il_StatefulServer

#include <icL-il/export/global.h++>



class icString;

namespace icL::core::il {

class icL_core_il_EXPORT StatefulServer
{
public:
    virtual ~StatefulServer() = default;

    /**
     * @brief registerSignal register a new signal
     * @param name is the name of new signal
     * @return false if so signal exists, otherwise true
     */
    virtual bool registerSignal(const icString & name) = 0;

    /**
     * @brief getSignal gets the integer code of signal
     * @param name is the name of signal
     * @return the integer code of signal, otherwise returns -1
     */
    virtual int getSignal(const icString & name) = 0;

    enum class Mode {  ///< Default running modes
        SelfTesting,   ///< self-testing mode
        Automation,    ///< automation mode
        Testing        ///< testing mode
    };

    /**
     * @brief loadDefaults loads default settings
     * @param mode is the requested mode
     */
    virtual void loadDefaults(Mode mode) = 0;

    /**
     * @brief catchSystem catch the signal 'System'
     * @return true if system must be catched
     */
    virtual bool catchSystem() = 0;
};

}  // namespace icL::core::il

#endif  // core_il_StatefulServer
