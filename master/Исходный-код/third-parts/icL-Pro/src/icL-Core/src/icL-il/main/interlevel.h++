#ifndef core_il_InterLevel
#define core_il_InterLevel

#include "../export/global.h++"



namespace std {
class any;
}

namespace icL::core {

namespace memory {
class Memory;
}

namespace il {

class CP;
class CpFactory;
class VMLayer;
class VMStack;
class SourceServer;
class StatefulServer;
class Log;
class Factory;

namespace ModeNM {
enum Mode {   ///< modes of interpreteur running
    Run = 0,  ///<< Run the program
    Check,    ///< Check the program
    Last      ///< Extendenble
};
}

namespace SubmodeNM {
enum Submode {  ///< modes of submodule
    Default,    ///< default for all modes
    Colorize,   ///< used in check mode
    Last        ///< Extendable
};
}

using ModeNM::Mode;
using SubmodeNM::Submode;

/**
 * @brief The InterLevel struct contains interfaces to each level
 */
struct icL_core_il_EXPORT InterLevel
{
    ~InterLevel() {}

    /// \brief mode is the current run mode of command processor
    int mode = Mode::Run;

    /// \brief submode extends the mode posibilities
    int submode = Submode::Default;

    /// \brief mem is a pointer to memory level
    memory::Memory * mem = nullptr;

    /// \brief cpu is a pointer to command processor level
    CP * cpu = nullptr;

    /// \brief cpfactory is a pointer to command proccessor factory
    CpFactory * cpfactory = nullptr;

    /// \brief vm is a pointer to virtual machine layer
    VMLayer * vm = nullptr;

    /// \brief vms is a pointer to highest level - virtual machine stack
    VMStack * vms = nullptr;

    /// \brief source is a pointer to the source server
    SourceServer * source = nullptr;

    /// \brief stateful is a pointer to the setateful server
    StatefulServer * stateful = nullptr;

    /// \brief log is a logging service
    Log * log = nullptr;

    /// \brief factory is a network factory
    Factory * factory = nullptr;

    /// \brief extend the standart funtionality
    std::any * ext = nullptr;
};

}  // namespace il
}  // namespace icL::core


#endif  // core_il_InterLevel
