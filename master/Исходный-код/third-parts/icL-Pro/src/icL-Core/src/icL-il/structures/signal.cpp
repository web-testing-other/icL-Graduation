#include "signal.h++"

#include <utility>

namespace icL::core::il {

Signal::Signal(int code, icString message)
    : code(code)
    , message(std::move(message)) {}

}  // namespace icL::core::il
