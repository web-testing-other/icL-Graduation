#ifndef core_il_CpFactory
#define core_il_CpFactory

namespace icL::core::il {

class CP;
struct CodeFragment;
struct InterLevel;

/**
 * @brief The CpFactory class is a command processor factory
 */
class CpFactory
{
public:
    /**
     * @brief create creates a new command processor
     * @param il is the interlevel node
     * @param code is the code fragment to parse and run
     * @return the created command processor
     */
    virtual CP * create(il::InterLevel * il, const CodeFragment & code) = 0;
};

}  // namespace icL::core::il

#endif  // core_il_CpFactory
