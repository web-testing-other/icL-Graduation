#ifndef core_il_CP
#define core_il_CP

#include <icL-il/export/global.h++>



class icString;
template <typename>
class icList;

namespace icL::core::il {

class CE;
struct CodeFragment;

class icL_core_il_EXPORT CP
{
public:
    virtual ~CP() = default;

    /**
     * @brief parseNext parses the next logical symbol
     * @return the pointer to created block
     */
    virtual CE * parseNext() = 0;

    /**
     * @brief lastKey returns the last catched key
     * @return the last catched key as string
     */
    virtual icString lastKey() = 0;

    /**
     * @brief getFilePath gets the path of current file
     * @return path to current file
     */
    virtual icString getFilePath() = 0;

    /**
     * @brief getFilePathLineChar gets the path to current file with line nr
     * @return the path to current file with line & char number
     */
    virtual icString getFilePathLineChar() = 0;

    /**
     * @brief lastTokenPosition gets the postion of last token
     * @return "filename:line:symbol: "
     */
    virtual icString lastTokenPosition() = 0;

    /**
     * @brief reset resets flayers position and range
     * @param code is the code to execute
     * @note used in keep alive layers
     */
    virtual void reset(const CodeFragment & code) = 0;

    /**
     * @brief repeat forces to repeat the last token
     */
    virtual void repeat() = 0;

    /**
     * @brief splitCommands split code to commands
     * @param code is the code to split
     * @return a list of commands
     */
    virtual icList<CodeFragment> splitCommands(const CodeFragment & code) = 0;

    // virtual inter::Flayer & getFlayer() = 0;
};

}  // namespace icL::core::il

#endif  // core_il_CP
