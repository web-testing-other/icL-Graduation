#ifndef core_il_Factory
#define core_il_Factory

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-types-enum.h++>

#include <icL-memory/structures/type.h++>

#include <memory>



class icString;
class icVariant;

namespace std {
template <typename>
class core_shared_ptr;
}

namespace icL::core {

namespace memory {
struct Argument;
using ArgList = icList<Argument>;
class DataContainer;
struct PackedValueItem;
using PackedItems = icList<PackedValueItem>;
}  // namespace memory

namespace il {

struct InterLevel;
class CE;

enum class PackableType {  ///< Types of packeable items
    Value,                 ///< it's a value (value, name & container)
    Parameter,             ///< it's a parameter (name & type)
    DefaultParameter,      ///< it's a parameter (name, type & value)
    Field,                 ///< it's a field (name & value)
    Column,                ///< it's a set column (name & type)
    Type                   ///< it's a type (type only)
};

struct TypeableValue
{
    /**
     * @brief type gets the value type
     * @return the value type
     */
    virtual enum memory::Type type() const = 0;

    virtual ~TypeableValue() = default;
};

struct ReadableValue : virtual public TypeableValue
{
    /**
     * @brief value get the value as variant
     * @return the value as variant
     */
    virtual icVariant value() const = 0;
};

struct WriteableValue : virtual public ReadableValue
{
    /**
     * @brief reset resets value of variable
     * @param value is the new value
     */
    virtual void reset(const icVariant & value) = 0;
};

struct PackableValue : virtual public WriteableValue
{
    /**
     * @brief packNow pack the node value
     * @return the packed node value
     */
    virtual memory::PackedValueItem packNow() const = 0;

    /**
     * @brief packableType return the packable type of node
     * @return the packable type of node
     */
    virtual PackableType packableType() const = 0;
};

struct IValueFixator
{
    /**
     * @brief getter fixes value on read/get
     * @param value is the value to fix
     */
    virtual void getter(icVariant & value) const = 0;

    /**
     * @brief setter fixes value on write/set
     * @param value is the value to fix
     */
    virtual void setter(icVariant & value, const icVariant & request) const = 0;

    /// virtual desctructor
    virtual ~IValueFixator() = default;
};

struct FixableValue
{
    /**
     * @brief makeFunctional makes this value icL value
     * @param fgetter is the functional getter of value
     * @param fsetter is the functional setter of value
     */
    virtual void installFixator(
      const std::shared_ptr<il::IValueFixator> & fixator) = 0;
};

struct ValuePack
{
    /**
     * @brief getValues extract values from a pack
     * @return the pack node values
     */
    virtual const memory::PackedItems & getValues() const = 0;
};

class Factory
{
public:
    /**
     * @brief toArgList cast a value to a args list
     * @param il is the inter-level node
     * @param value is the value to cast
     * @return an arg icList created from given value
     */
    virtual memory::ArgList listify(
      il::InterLevel * il, PackableValue * value) = 0;

    /**
     * @brief fromValue creates a new CE node from a icVariant
     * @param il is the inter-level node
     * @param value is the value of new CE node
     * @return a pointer to the new created node
     */
    virtual CE * fromValue(il::InterLevel * il, const icVariant & value) = 0;

    /**
     * @brief fromValue creates a new CE node from a container variable
     * @param il is the inter-level node
     * @param container is the container of variable
     * @param var is the name of variable in container
     * @return a pointer to the new created node
     */
    virtual CE * fromValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & var) = 0;

    /**
     * @brief clone clones a pack
     * @param pack is the pack to clone
     * @return a new pack with the same values
     */
    virtual CE * clone(ValuePack * pack) = 0;

    /**
     * @brief clone clpne a value
     * @param value is the value to clone
     * @return a new value abstraction with the same value
     */
    virtual CE * clone(PackableValue * value, short type = -1) = 0;

    /**
     * @brief variantToType gets type of icVariant value
     * @param var is the variant to extract type
     * @return icL type of icVariant
     */
    virtual enum memory::Type variantToType(const icVariant & var) = 0;

    /**
     * @brief typeToVarType converts a type to a icType
     * @param type is the type to convert
     * @return the icType equivalent
     */
    virtual icType typeToVarType(const memory::Type type) = 0;

    /**
     * @brief typeToString convert a value type to a string
     * @param type is the type to convert to string
     * @return the type name using icL specification
     */
    virtual icString typeToString(const short & type) = 0;
};

}  // namespace il
}  // namespace icL::core

#endif  // core_il_Factory
