#ifndef core_il_Signal
#define core_il_Signal

#include <icL-types/replaces/ic-string.h++>

#include <icL-il/export/global.h++>
#include <icL-il/export/signals.h++>



namespace icL::core::il {

/**
 * @brief The Signal class is an icL specific signal
 */
class icL_core_il_EXPORT Signal
{
public:
    Signal() = default;

    /**
     * @brief Signal create an inited signal
     * @param code is the code of signal
     * @param message is the message of signal
     */
    Signal(int code, icString message);

    /// \brief code is the integer code of signal
    int code = 0;

    /// \brief message is the message of signal
    icString message;

    /// \brief printed elude repeat stack trace
    bool printed = false;
};

}  // namespace icL::core::il

#endif  // core_il_Signal
