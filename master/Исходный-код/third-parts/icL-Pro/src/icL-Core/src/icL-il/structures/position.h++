#ifndef core_il_Position
#define core_il_Position

#include <icL-il/export/global.h++>

#include <inttypes.h>



class icVariant;

namespace icL::core::il {

/**
 * @brief The Position struct represent a position of a cursor in a file
 */
struct icL_core_il_EXPORT Position
{
    /// \brief byte number
    int64_t byte{};
    /// \brief absolute is the absolute position
    int absolute{};
    /// \brief line is the line number
    short line{};
    /// \brief relative is the relative position to line begin
    short relative{};

    Position() = default;
    Position(const icVariant & variant);
    Position(int64_t byte, int absolute, short line, short relative);

    Position operator+(int i) const;
    Position operator-(int i) const;

    /**
     * @brief operator icVariant casts the position to a variant
     */
    operator icVariant() const;
};

}  // namespace icL::core::il

#endif  // core_il_Position
