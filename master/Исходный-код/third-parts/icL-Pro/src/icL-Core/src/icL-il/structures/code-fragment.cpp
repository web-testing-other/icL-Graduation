#include "code-fragment.h++"

#include "../auxiliary/source-of-code.h++"

#include <icL-types/replaces/ic-char.h++>



namespace icL::core::il {

icString CodeFragment::getCode() const {

    SourceOfCode * code = source->fragment(begin, end);
    icString ret;

    do {
        code->next();
        ret += code->current();
    } while (!code->atEnd());

    delete code;
    return ret;
}

}  // namespace icL::core::il
