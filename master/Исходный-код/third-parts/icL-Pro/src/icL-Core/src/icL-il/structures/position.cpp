#include "position.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <cassert>



namespace icL::core::il {

Position::Position(const icVariant & variant) {
    assert(variant.is(icType::Position));
    *this = to<Position>(variant);
}

Position::Position(int64_t byte, int absolute, short line, short relative)
    : byte(byte)
    , absolute(absolute)
    , line(line)
    , relative(relative) {}

Position Position::operator+(int i) const {
    assert(i == 1);
    Position ret = *this;

    ret.relative += 1;
    ret.absolute += 1;

    return ret;
}

Position Position::operator-(int i) const {
    assert(i == 1);
    Position ret = *this;

    ret.relative -= 1;
    ret.absolute -= 1;

    return ret;
}

icL::core::il::Position::operator icVariant() const {
    return icVariant::fromValueTemplate(*this, icType::Position);
}

}  // namespace icL::core::il
