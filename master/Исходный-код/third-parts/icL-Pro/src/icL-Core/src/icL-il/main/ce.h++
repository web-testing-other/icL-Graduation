#ifndef core_il_CE
#define core_il_CE

#include <icL-il/export/global.h++>
#include <icL-il/structures/steptype.h++>



class icString;
class icVariant;

namespace icL::core {

namespace cp {
struct FlyResult;
}

using il::StepType;

namespace il {

struct CodeFragment;

class icL_core_il_EXPORT CE
{
public:
    virtual ~CE() = default;

    /**
     * @brief checkPrev checks the previous entity for compability
     * @param ce is the contextual entity to check
     * @return true if compatible, otherwise false
     */
    virtual bool checkPrev(CE * ce) = 0;

    /**
     * @brief checkNext checks the next entity for compability
     * @param ce is the contextual entity to check
     * @return true if compatible, otherwise false
     */
    virtual bool checkNext(CE * ce) = 0;

    /**
     * @brief currentRunRank is the current rank of entity in current context
     * @param rtl is true if the check is right-to-left, otherwise false
     * @return the rank of entity for copatible context, or -1
     */
    virtual int currentRunRank(bool rtl) = 0;

    /**
     * @brief runNow runs the entity function right now
     * @return the type of the current executed command
     */
    virtual enum StepType runNow() = 0;

    /**
     * @brief release replace the nodes in linked list
     */
    virtual void release() = 0;

    /**
     * @brief prev gets a pointer to previous contextual entity
     * @return a pointer to previous contextual entity
     */
    virtual CE *& prev() = 0;

    /**
     * @brief next gets a pointer to next contextual entity
     * @return a pointer to next contextual entity
     */
    virtual CE *& next() = 0;

    /**
     * @brief firstToReplace gets the first context entity to replace
     * @return the first context entity to be replaced by the new
     */
    virtual il::CE * firstToReplace() = 0;

    /**
     * @brief lastToReplace gets the last context entity to replace
     * @return the last context entity to be replaced by the new
     */
    virtual il::CE * lastToReplace() = 0;

    /**
     * @brief newCE return the new node create by this
     * @return the new node, created by this
     */
    virtual il::CE * newCE() = 0;

    /**
     * @brief links a node after current
     */
    virtual void linkNode(CE * node) = 0;

    /**
     * @brief replace a node
     */
    virtual void replaceNode(CE * node) = 0;

    /**
     * @brief replace repalces an interval of nodes
     */
    virtual void replaceNodes(CE * begin, CE * end) = 0;

    /**
     * @brief fragmentData gets the data of token in code
     * @return the data about position of token in code
     */
    virtual CodeFragment & fragmentData() = 0;

    /**
     * @brief hasValue checks if that type of token contains a value
     * @return true if this type of token contains a value, otherwise false
     */
    virtual bool hasValue() = 0;

    /**
     * @brief toString generate a string to create a token like this
     * @return a string to create a token like this
     */
    virtual icString toString() = 0;

    /**
     * @brief role gets the role of the node
     * @return the role of the node
     */
    virtual int role() = 0;

    /**
     * @brief colorize mark the syntax hightlight
     */
    virtual void colorize() = 0;
};

}  // namespace il
}  // namespace icL::core

#endif  // core_il_CE
