#ifndef icL_core_Global
#define icL_core_Global

#include <icL-types/global/icl-types.h++>

// clang-format off

#if defined(icL_core_ce_base_LIBRARY)
    #define icL_core_ce_base_EXPORT icL_Export
#else
    #define icL_core_ce_base_EXPORT icL_Import
#endif

#if defined(icL_core_ce_keyword_LIBRARY)
    #define icL_core_ce_keyword_EXPORT icL_Export
#else
    #define icL_core_ce_keyword_EXPORT icL_Import
#endif

#if defined(icL_core_ce_literal_LIBRARY)
    #define icL_core_ce_literal_EXPORT icL_Export
#else
    #define icL_core_ce_literal_EXPORT icL_Import
#endif

#if defined(icL_core_ce_operators_advanced_LIBRARY)
    #define icL_core_ce_operators_advanced_EXPORT icL_Export
#else
    #define icL_core_ce_operators_advanced_EXPORT icL_Import
#endif

#if defined(icL_core_ce_operators_ALU_LIBRARY)
    #define icL_core_ce_operators_ALU_EXPORT icL_Export
#else
    #define icL_core_ce_operators_ALU_EXPORT icL_Import
#endif

#if defined(icL_core_ce_value_base_LIBRARY)
    #define icL_core_ce_value_base_EXPORT icL_Export
#else
    #define icL_core_ce_value_base_EXPORT icL_Import
#endif

#if defined(icL_core_ce_value_system_LIBRARY)
    #define icL_core_ce_value_system_EXPORT icL_Export
#else
    #define icL_core_ce_value_system_EXPORT icL_Import
#endif

#if defined(icL_core_cp_LIBRARY)
    #define icL_core_cp_EXPORT icL_Export
#else
    #define icL_core_cp_EXPORT icL_Import
#endif

#if defined(icL_core_factory_LIBRARY)
    #define icL_core_factory_EXPORT icL_Export
#else
    #define icL_core_factory_EXPORT icL_Import
#endif

#if defined(icL_core_il_LIBRARY)
    #define icL_core_il_EXPORT icL_Export
#else
    #define icL_core_il_EXPORT icL_Import
#endif

#if defined(icL_core_memory_LIBRARY)
    #define icL_core_memory_EXPORT icL_Export
#else
    #define icL_core_memory_EXPORT icL_Import
#endif

#if defined(icL_core_mock_LIBRARY)
    #define icL_core_mock_EXPORT icL_Export
#else
    #define icL_core_mock_EXPORT icL_Import
#endif

#if defined(icL_core_service_cast_LIBRARY)
    #define icL_core_service_cast_EXPORT icL_Export
#else
    #define icL_core_service_cast_EXPORT icL_Import
#endif

#if defined(icL_core_service_keyword_LIBRARY)
    #define icL_core_service_keyword_EXPORT icL_Export
#else
    #define icL_core_service_keyword_EXPORT icL_Import
#endif

#if defined(icL_core_service_main_LIBRARY)
    #define icL_core_service_main_EXPORT icL_Export
#else
    #define icL_core_service_main_EXPORT icL_Import
#endif

#if defined(icL_core_service_operators_advanced_LIBRARY)
    #define icL_core_service_operators_advanced_EXPORT icL_Export
#else
    #define icL_core_service_operators_advanced_EXPORT icL_Import
#endif

#if defined(icL_core_service_operators_ALU_LIBRARY)
    #define icL_core_service_operators_ALU_EXPORT icL_Export
#else
    #define icL_core_service_operators_ALU_EXPORT icL_Import
#endif

#if defined(icL_core_service_value_base_LIBRARY)
    #define icL_core_service_value_base_EXPORT icL_Export
#else
    #define icL_core_service_value_base_EXPORT icL_Import
#endif

#if defined(icL_core_service_value_system_LIBRARY)
    #define icL_core_service_value_system_EXPORT icL_Export
#else
    #define icL_core_service_value_system_EXPORT icL_Import
#endif

#if defined(icL_core_shared_LIBRARY)
    #define icL_core_shared_EXPORT icL_Export
#else
    #define icL_core_shared_EXPORT icL_Import
#endif

#if defined(icL_core_vm_LIBRARY)
    #define icL_core_vm_EXPORT icL_Export
#else
    #define icL_core_vm_EXPORT icL_Import
#endif


// clang-format on

#endif
