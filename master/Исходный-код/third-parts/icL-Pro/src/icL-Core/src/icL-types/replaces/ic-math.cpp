#include "ic-math.h++"

#include <QtMath>



constexpr double pi = 3.14159265358979323846;

double icPow(double v, double pow) {
    return qPow(v, pow);
}

double icSqrt(double v) {
    return qSqrt(v);
}

double icAcos(double v) {
    return qAcos(v);
}

double icAsin(double v) {
    return qAsin(v);
}

double icAtan(double v) {
    return qAtan(v);
}

int icCeil(double v) {
    return qCeil(v);
}

double icCos(double v) {
    return qCos(v);
}

double icDegreesToRadians(double v) {
    return v * pi / 180.;
}

double icExp(double v) {
    return qExp(v);
}

int icFloor(double v) {
    return qFloor(v);
}

double icLn(double v) {
    return qLn(v);
}

double icRadiansToDegrees(double v) {
    return v * 180. / pi;
}

int icRound(double v) {
    return qRound(v);
}

double icSin(double v) {
    return qSin(v);
}

double icTan(double v) {
    return qTan(v);
}
