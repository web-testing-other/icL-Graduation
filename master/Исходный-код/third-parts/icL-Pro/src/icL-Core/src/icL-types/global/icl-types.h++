#ifndef types_Global
#define types_Global

#ifdef Q_OS_WIN
#/**/ define icL_Export __declspec(dllexport)
#/**/ define icL_Import __declspec(dllimport)
#else
#/**/ define icL_Export __attribute__((visibility("default")))
#/**/ define icL_Import __attribute__((visibility("default")))
#endif

#if defined(icL_types_LIBRARY)
#/**/ define icL_Core_types_Share icL_Export
#else
#/**/ define icL_Core_types_Share icL_Import
#endif

#endif  // types_Global
