#ifndef ICIMAGE_H
#define ICIMAGE_H

#include <icL-types/global/icl-types.h++>



class icString;

class icL_Core_types_Share icImage
{
public:
    icImage();

    static void saveData(const icString & base64, const icString & path);
};

#endif  // ICIMAGE_H
