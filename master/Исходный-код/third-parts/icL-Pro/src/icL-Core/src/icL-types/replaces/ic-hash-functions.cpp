#include "ic-hash-functions.h++"

#include "ic-list.h++"
#include "ic-string.h++"
#include "ic-variant.h++"

#include <ic-char.h++>

#include <QHash>



uint qHash(const icVariant & var, uint seed) {
    switch (var.type()) {
    case icType::Void:
        return 0;

    case icType::Bool:
        return qHash(var.toBool(), seed);

    case icType::Int:
        return qHash(var.toInt(), seed);

    case icType::Double:
        return qHash(var.toDouble(), seed);

    case icType::String:
        return qHash(var.toString(), seed);

    case icType::StringList:
        return qHash(var.toStringList(), seed);

    default:
        return 0;
    }
}

uint qHash(const icString & str, uint seed) {
    return qHash(str.getData(), seed);
}

uint qHash(const icChar & ch, uint seed) {
    return qHash(QChar(ch), seed);
}
