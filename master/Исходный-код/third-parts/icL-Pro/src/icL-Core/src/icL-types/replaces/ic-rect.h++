#ifndef types_icRect
#define types_icRect

#include <icL-types/global/icl-types.h++>



class icL_Core_types_Share icRect
{
public:
    icRect() = default;

    icRect(double x, double y, double width, double height) {
        m_x      = x;
        m_y      = y;
        m_width  = width;
        m_height = height;
    }

    double x() const {
        return m_x;
    }
    void setX(double value) {
        m_x = value;
    }
    double y() const {
        return m_y;
    }
    void setY(double value) {
        m_y = value;
    }
    double width() const {
        return m_width;
    }
    void setWidth(double value) {
        m_width = value;
    }
    double height() const {
        return m_height;
    }
    void setHeight(double value) {
        m_height = value;
    }

private:
    double m_x, m_y, m_width, m_height;
};

#endif  // types_icRect
