#include "ic-char.h++"

#include <ic-string.h++>

icChar::icChar(const char ch)
    : d(ch) {}

icChar::icChar(int ch)
    : d(ch) {}

icChar::icChar(const QChar ch)
    : d(ch) {}

bool icChar::isLetter() const {
    return d.isLetter();
}

bool icChar::isDigit() const {
    return d.isDigit();
}

bool icChar::isLetterOrDigit() const {
    return d.isLetterOrNumber();
}

bool icChar::isWhite() const {
    return d.isSpace();
}

icChar icChar::operator+(const int step) const {
    return d.toLatin1() + char(step);
}

icChar::operator char() const {
    return d.toLatin1();
}

icChar::operator QChar() const {
    return d;
}

icString icChar::operator%(const icString & str) const {
    icString ret = str;

    ret.insert(0, *this);
    return ret;
}

bool icChar::operator<(const icChar & other) const {
    return d < other.d;
}

bool icChar::operator==(const icChar & other) const {
    return d == other.d;
}

bool icChar::operator==(char ch) const {
    return d == ch;
}

bool icChar::operator!=(char ch) const {
    return d != ch;
}

bool icChar::operator!=(const icChar & other) const {
    return d != other.d;
}
