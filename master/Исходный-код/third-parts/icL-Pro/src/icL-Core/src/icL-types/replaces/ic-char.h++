#ifndef types_Char
#define types_Char

#include <icL-types/global/icl-types.h++>

#include <QChar>


class icString;

/**
 * @brief The icChar class describes a charater
 */
class icL_Core_types_Share icChar
{
public:
    // contructors

    /**
     * @brief icChar constructs a 0x0 char
     */
    icChar() = default;

    /**
     * @brief icChar constructs a character from a char value
     * @param ch is the value of character
     */
    icChar(char ch);

    icChar(int ch);

    /**
     * @brief icChar constructs a character from an unicode code
     * @param ch is the unicode code of caracter
     */
    icChar(QChar ch);

    // immutable members

    bool isLetter() const;

    bool isDigit() const;

    bool isLetterOrDigit() const;

    bool isWhite() const;

    // casting functions

    /**
     * @brief operator QChar casts value to QChar
     */
    operator QChar() const;

    /**
     * @brief operator char casts value to char
     */
    operator char() const;

    // operators

    /**
     * @brief operator + returns a ASCII char genereated by a offset
     * @param step is the needed offset
     * @return a new char
     */
    icChar operator+(const int step) const;

    /**
     * @brief operator % build a string from a char and a string
     * @param str is the string to be inserted to the end
     * @return a new string
     */
    icString operator%(const icString & str) const;

    bool operator<(const icChar & other) const;

    /**
     * @brief operator == checks if chars are equal
     * @param other is the char to compare with
     * @return `true` if chars are the same, otherwise `false`
     */
    bool operator==(const icChar & other) const;
    bool operator==(char ch) const;
    bool operator!=(char ch) const;

    /**
     * @brief operator != checks if chars are not equal
     * @param other is the char to compare with
     * @return `false` if chars are equal, otherwise `true`
     */
    bool operator!=(const icChar & other) const;

private:
    /**
     * @brief d contains the data of char (a UChar32 value)
     */
    QChar d = 0;
};

#endif  // types_Char
