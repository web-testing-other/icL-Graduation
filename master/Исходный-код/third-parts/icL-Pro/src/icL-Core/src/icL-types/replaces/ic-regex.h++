#ifndef types_RegEx
#define types_RegEx

#include "ic-hash-functions.h++"
#include "ic-string.h++"

#include <QRegularExpression>



class icRegExMatch;
class icStringList;

class icL_Core_types_Share icRegEx
{
    QRegularExpression d;

public:
    icRegEx();
    icRegEx(const icString & pattern, uint flags = 0x0);
    icRegEx(const icString & pattern, const icString & flags);

    enum Option {
        InvertedGreedinessOption    = 0x01,
        CaseInsensitiveOption       = 0x02,
        MultilineOption             = 0x04,
        DotMatchesEverythingOption  = 0x08,
        UseUnicodePropertiesOption  = 0x10,
        ExtendedPatternSyntaxOption = 0x20
    };

    const icString pattern() const;

    void setPattern(const icString & pattern);

    uint patternOptions() const;
    void setPatternOptions(uint flags);

    icRegExMatch match(const icString & str) const;

    icStringList namedCaptureGroups() const;

    operator QRegularExpression();
    operator const QRegularExpression() const;

    static icRegEx fromWildcard(const icString & in);

    bool operator==(const icRegEx & other) const;

private:
    static QFlags<QRegularExpression::PatternOption> castFlags(uint flags);
    static QFlags<QRegularExpression::PatternOption> castFlags(
      const icString & mods);

    static uint castFlagsBack(uint flags);
};

icRegEx operator""_rx(const char * pattern, unsigned long size);

class icRegExMatch
{
    QRegularExpressionMatch d;

public:
    icRegExMatch() = default;
    icRegExMatch(const QRegularExpressionMatch & match);

    bool hasMatch();

    icString captured(int id) const;
    icString captured(const icString & name) const;

    icStringList capturedTexts() const;
};

#endif  // types_RegEx
