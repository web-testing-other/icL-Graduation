#include "ic-debug.h++"

#include "ic-string.h++"

#include <iostream>



ICDebug & ICDebug::instance() {
    static ICDebug ret;
    return ret;
}

ICDebug & ICDebug::operator<<(const icString & str) {
    std::cout << str.toString() << std::endl;
    return *this;
}
