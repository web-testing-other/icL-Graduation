#ifndef types_List
#define types_List

#include <icL-types/global/icl-types.h++>

#include <QList>

#include <cassert>

template <typename T>
class icL_Core_types_Share icList
{
public:
    icList() = default;
    icList(std::initializer_list<T> list)
        : d(list) {}
    icList(const T & value, const icList<T> & list) {
        d.append(value);
        d.append(list);
    }
    icList(const icList<T> & list, const T & value) {
        d.append(list);
        d.append(value);
    }
    icList(const icList<T> & list1, const icList<T> & list2) {
        d.append(list1);
        d.append(list2);
    }

    using Iterator      = typename QList<T>::iterator;
    using ConstIterator = typename QList<T>::const_iterator;

    // properties

    bool isEmpty() const {
        return d.isEmpty();
    }
    int length() const {
        return d.length();
    }

    // immutable members

    bool contains(const T & value) const {
        return d.contains(value);
    }

    const T & at(int i) const {
        return d[i];
    }

    const T & first() const {
        return d.first();
    }
    const T & last() const {
        return d.last();
    }

    int indexOf(const T & value, int start = 0) const {
        return d.indexOf(value, start);
    }

    // muttable members

    icList & append(const T & value) {
        d.append(value);
        return *this;
    }
    icList & append(const icList<T> & values) {
        for (auto & item : values) {
            d.append(item);
        }
        return *this;
    }

    icList & removeFirst() {
        d.removeFirst();
        return *this;
    }
    icList & removeLast() {
        d.removeLast();
        return *this;
    }
    icList & removeAt(int index) {
        d.removeAt(index);
        return *this;
    }
    bool removeOne(const T & value) {
        return d.removeOne(value);
    }

    // operators

    bool operator==(const icList & other) const {
        return d == other.d;
    }
    bool operator!=(const icList & other) const {
        return d != other.d;
    }

    T & operator[](const int i) {
        return d[i];
    }
    const T & operator[](const int i) const {
        return d[i];
    }

    icList & operator+=(const T & value) {
        d.append(value);
        return *this;
    }

    bool operator<(const icList<T> & other) const {
        return d < other.d;
    }

    operator QList<T>() const {
        return d;
    }

    // iterators

    Iterator begin() {
        return d.begin();
    }
    ConstIterator begin() const {
        return d.cbegin();
    }

    Iterator end() {
        return d.end();
    }
    ConstIterator end() const {
        return d.cend();
    }

protected:
    QList<T> d;
};

#endif  // types_List
