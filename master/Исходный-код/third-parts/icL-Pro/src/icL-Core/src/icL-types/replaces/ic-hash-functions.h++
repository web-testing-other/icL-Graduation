#ifndef types_icHashFunctions
#define types_icHashFunctions

#include "ic-char.h++"
#include "ic-list.h++"
#include "ic-pair.h++"
#include "ic-types-enum.h++"
#include "ic-variant.h++"

uint qHash(const icVariant & var, uint seed = 0);
uint qHash(const icString & str, uint seed = 0);
uint qHash(const icChar & ch, uint seed = 0);

template <typename T>
uint qHash(const icList<T> & list, uint seed = 0) {
    uint ret = 0x0;

    for (auto & var : list) {
        ret = ret ^ qHash(var, seed);
    }
    return ret;
}

template <typename T>
uint qHash(const icPair<T, T> & pair, uint seed = 0) {
    return qHash(pair.first(), seed) ^ qHash(pair.second(), seed);
}

#endif  // types_icHashFunctions
