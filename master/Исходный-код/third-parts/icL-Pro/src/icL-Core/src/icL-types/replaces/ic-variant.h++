#ifndef types_Variant
#define types_Variant

#include "ic-types-enum.h++"

#include <icL-types/global/icl-types.h++>

#include <any>



class icString;
class icDateTime;
class icStringList;
class icRegEx;
class jsArray;
class jsObject;

namespace icL {

}  // namespace icL

class icL_Core_types_Share icVariant
{

    std::any d;

    icType m_type = icType::Void;

public:
    icVariant();
    icVariant(const bool value);
    icVariant(const int & value);
    icVariant(const double & value);
    icVariant(const icString & string);
    icVariant(const icDateTime & dt);
    icVariant(const icStringList & list);
    icVariant(const icRegEx & re);
    icVariant(const jsArray & array);
    icVariant(const jsObject & object);

    static icVariant makeVoid();

    icType type() const;

    bool isBool() const;
    bool isInt() const;
    bool isDouble() const;
    bool isString() const;
    bool isDateTime() const;
    bool isStringList() const;
    bool isRegEx() const;
    bool isJsObject() const;
    bool isJsArray() const;
    bool is(int type) const;

    bool isNull() const;
    bool isVoid() const;
    bool isValid() const;

    const std::any & toAny() const;

    template <typename T>
    static icVariant fromValueTemplate(const T & value, icType type) {
        icVariant ret;
        ret.d      = value;
        ret.m_type = type;
        return ret;
    }

    static icVariant fromValue(const bool & value);
    static icVariant fromValue(const int & value);
    static icVariant fromValue(const double & value);
    static icVariant fromValue(const icString & string);
    static icVariant fromValue(const icDateTime & dt);
    static icVariant fromValue(const icStringList & list);
    static icVariant fromValue(const icRegEx & re);
    static icVariant fromValue(const jsArray & array);
    static icVariant fromValue(const jsObject & object);

    const bool &         toBool() const;
    const int &          toInt() const;
    const double &       toDouble() const;
    const icString &     toString() const;
    const icDateTime &   toDateTime() const;
    const icStringList & toStringList() const;
    const icRegEx &      toRegEx() const;
    const jsArray &      toArray() const;
    const jsObject &     toJsObject() const;

    icVariant & clear();

    bool operator==(const icVariant & other) const;

    operator bool() const;
    operator int() const;
    operator double() const;
    operator icString() const;
    operator icDateTime() const;
    operator icStringList() const;
    operator icRegEx() const;
};

template <typename T>
const T & to(const icVariant & val) {
    return std::any_cast<const T &>(val.toAny());
}

#endif  // types_Variant
