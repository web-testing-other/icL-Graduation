#ifndef type_TypesEnum
#define type_TypesEnum

namespace icTypeNM {

enum icType {
    Initial = 0,
    Void,
    Bool,
    Int,
    Double,
    String,
    DateTime,
    StringList,
    RegEx,
    Object,
    Set,
    Packed,
    JsArray,
    JsObject,
    Position,
    Lambda,
    Last
};
}

using icTypeNM::icType;

#endif  // type_TypesEnum
