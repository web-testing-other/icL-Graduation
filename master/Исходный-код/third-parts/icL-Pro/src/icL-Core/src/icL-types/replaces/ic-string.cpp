#include "ic-string.h++"

#include <ic-char.h++>
#include <ic-regex.h++>
#include <ic-string-list.h++>

#include <utility>



icString::icString(const char * const cString)
    : d(cString) {}

icString::icString(const icChar & ch)
    : d(ch) {}

icString::icString(QString str)
    : d(std::move(str)) {}

icString::operator QString() const {
    return d;
}

bool icString::isEmpty() const {
    return d.isEmpty();
}

int icString::length() const {
    return d.length();
}

icChar icString::at(int i) const {
    return d.at(i);
}

bool icString::beginsWith(const icString & str) const {
    return d.startsWith(str.d);
}

bool icString::contains(char ch) const {
    return d.contains(ch);
}

bool icString::contains(icChar ch) const {
    return d.contains(ch);
}

bool icString::contains(const char * const substr) const {
    return d.indexOf(substr) >= 0;
}

bool icString::contains(const icString & substr) const {
    return d.indexOf(substr.d) >= 0;
}

bool icString::compare(const icString & other, bool cs) const {
    return cs ? d.compare(other.d, Qt::CaseSensitive) == 0
              : d.compare(other.d, Qt::CaseInsensitive) == 0;
}

int icString::count(const icString & str) const {
    return d.count(str);
}

int icString::count(const icRegEx & regex) const {
    return d.count(regex);
}

bool icString::endsWith(const icString & str) const {
    return d.endsWith(str.d);
}

int icString::indexOf(const icChar & ch, int start) const {
    return d.indexOf(ch, start);
}

int icString::indexOf(const icString & str, int start) const {
    return d.indexOf(str.d, start);
}

int icString::indexOf(const icRegEx & rx, int start) const {
    return d.indexOf(rx, start);
}

int icString::lastIndexOf(const icChar & ch, int start) const {
    return d.lastIndexOf(ch, start);
}

int icString::lastIndexOf(const icString & str, int start) const {
    return start == -1 ? d.lastIndexOf(str.d) : d.lastIndexOf(str.d, start);
}

int icString::lastIndexOf(const icRegEx & rx, int start) const {
    return d.lastIndexOf(rx, start);
}

const QString & icString::getData() const {
    return d;
}

int icString::toInt() const {
    bool ok;
    return toInt(ok);
}

int icString::toInt(bool & ok) const {
    return d.toInt(&ok);
}

double icString::toDouble() const {
    bool ok;
    return toDouble(ok);
}

double icString::toDouble(bool & ok) const {
    try {
        ok = true;
        return std::stod(toString());
    }
    catch (...) {
        ok = false;
        return 0;
    }
}

std::string icString::toString() const {
    return d.isEmpty() ? "" : d.toStdString();
}

icString & icString::append(const icString & str) {
    d.append(str.d);
    return *this;
}

icString & icString::clear() {
    d.clear();
    return *this;
}

icString & icString::insert(int pos, const icString & str) {
    d.insert(pos, str.d);
    return *this;
}

icString & icString::insert(int pos, const icChar & ch) {
    d.insert(pos, ch);
    return *this;
}

icString & icString::prepend(const icString & str) {
    d.insert(0, str.d);
    return *this;
}

icString & icString::replace(int pos, int n, const icString & after) {
    d.replace(pos, n, after.d);
    return *this;
}

icString & icString::replace(const icString & before, const icString & after) {
    d.replace(before.d, after.d);
    return *this;
}

icString & icString::replace(
  const char * const before, const icString & after) {
    return replace(icString(before), after);
}

icString & icString::replace(
  const char * const before, const char * const after) {
    return replace(icString(before), icString(after));
}

icString & icString::replace(const icRegEx & before, const icString & after) {
    d.replace(before, after);
    return *this;
}

icString & icString::remove(int pos, int n) {
    d.remove(pos, n == -1 ? INT32_MAX : n);
    return *this;
}

icString & icString::remove(const icString & what, bool cs) {
    int index = cs ? d.indexOf(what.d) : d.toLower().indexOf(what.toLower().d);
    if (index >= 0) {
        d.remove(index, what.length());
    }
    return *this;
}

icString & icString::remove(const icRegEx & regex) {
    d.remove(regex);
    return *this;
}

icString & icString::toLower() {
    d = d.toLower();
    return *this;
}

icString & icString::toUpper() {
    d = d.toUpper();
    return *this;
}

icString & icString::fixLast() {
    auto ref = d[d.length() - 1];
    if (ref == ',')
        ref = ']';
    else
        d += ']';
    return *this;
}

icString icString::clone() const {
    icString ret;

    ret.d = d;
    return ret;
}

icString icString::toLower() const {
    return d.toLower();
}

icString icString::toUpper() const {
    return d.toUpper();
}

icString icString::left(int n) const {
    return mid(0, n);
}

icString icString::leftJustified(
  int width, const icString & fill, bool truncate) const {
    return d.leftJustified(width, fill[0], truncate);
}

icString icString::mid(int pos, int n) const {
    return d.mid(pos, n);
}

icString icString::right(int n) const {
    return mid(length() - n, n);
}

icString icString::rightJustified(
  int width, const icString & fill, bool truncate) const {
    return d.rightJustified(width, fill[0], truncate);
}

icStringList icString::split(
  const icString & delimiter, bool keepEmptyParts, bool caseSensitive) const {
    return d.split(
      delimiter,
      keepEmptyParts ? QString::KeepEmptyParts : QString::SkipEmptyParts,
      caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive);
}

icStringList icString::split(
  const icRegEx & delimiter, bool keepEmptyParts) const {
    return d.split(
      delimiter,
      keepEmptyParts ? QString::KeepEmptyParts : QString::SkipEmptyParts);
}

icString icString::trimmed() {
    return d.trimmed();
}

bool icString::operator==(const icString & other) const {
    return d == other.d;
}

bool icString::operator!=(const icString & other) const {
    return d != other.d;
}

bool icString::operator<(const icString & other) const {
    return d < other.d;
}

bool icString::operator>(const icString & other) const {
    return d > other.d;
}

bool icString::operator<=(const icString & other) const {
    return d <= other.d;
}

bool icString::operator>=(const icString & other) const {
    return d >= other.d;
}

icString & icString::operator%(const char * const other) {
    d.append(other);
    return *this;
}

icString & icString::operator%(const icString & other) {
    d.append(other.d);
    return *this;
}

icString & icString::operator%(const icChar & other) {
    d.append(other);
    return *this;
}

icString & icString::operator%(const char & other) {
    d.append(other);
    return *this;
}

icString icString::operator%(const char * const other) const {
    auto _clone = clone();
    _clone.d.append(other);
    return _clone;
}

icString icString::operator%(const icString & other) const {
    auto _clone = clone();
    _clone.d.append(other.d);
    return _clone;
}

icString icString::operator%(const icChar & other) const {
    auto _clone = clone();
    _clone.d.append(other);
    return _clone;
}

icString icString::operator%(const char & other) const {
    auto _clone = clone();
    _clone.d.append(other);
    return _clone;
}

icString icString::operator+(const icString & other) const {
    auto _clone = clone();
    _clone.d.append(other.d);
    return _clone;
}

icString & icString::operator+=(const icString & other) {
    d.append(other.d);
    return *this;
}

icString & icString::operator+=(const icChar & other) {
    d.append(other);
    return *this;
}

icString & icString::operator+=(char other) {
    d.append(other);
    return *this;
}

icChar icString::operator[](int index) const {
    return d[index];
}

icString icString::number(int i) {
    return QString::number(i);
}

icString icString::number(double i) {
    return QString::number(i);
}

icString operator+(const char * const s1, const icString & s2) {
    icString ret{s1};

    ret.append(s2);
    return ret;
}

icString operator%(const char * const s1, const icString & s2) {
    icString ret{s1};

    ret.append(s2);
    return ret;
}

icString operator%(char s1, const icString & s2) {
    icString ret{icChar(s1)};

    ret.append(s2);
    return ret;
}

icString operator+(const icString s1, const char * s2) {
    icString ret{s1};

    ret.append(s2);
    return ret;
}

icString operator+(const icString s1, const icString & s2) {
    icString ret{s1};

    ret.append(s2);
    return ret;
}

icString operator""_str(const char * str, size_t) {
    return str;
}
