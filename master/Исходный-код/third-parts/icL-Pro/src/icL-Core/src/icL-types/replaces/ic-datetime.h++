#ifndef types_DateTime
#define types_DateTime

#include "ic-hash-functions.h++"

#include <QDateTime>


class icString;

class icL_Core_types_Share icDateTime
{
    QDateTime d;

public:
    icDateTime() = default;
    icDateTime(QDateTime dt);

    // properties

    bool isValid() const;

    // immutual members

    int year() const;
    int month() const;
    int day() const;
    int hour() const;
    int minute() const;
    int second() const;

    int daysTo(const icDateTime & other) const;
    int secsTo(const icDateTime & other) const;

    // mutual members

    icDateTime & addDays(int days);
    icDateTime & addMonths(int months);
    icDateTime & addSecs(int secs);
    icDateTime & addYears(int years);

    icDateTime & setDate(int year, int month, int day);
    icDateTime & setHMS(int hour, int minute, int second);

    icDateTime & toTimeZone(int seconds);
    icDateTime & toUTC();

    // casting members

    icString toString() const;
    icString toString(const icString & format) const;

    int toSecsSinceEpoch() const;

    // static members

    static icDateTime fromString(const icString & datetime);
    static icDateTime fromString(
      const icString & datetime, const icString & format);
    static icDateTime fromSecsSinceEpoch(int seconds);

    static icDateTime currentDateTime();
    static icDateTime currentDateTimeUtc();

    static int      currentSecsSinceEpoch();
    static uint64_t currentMSecsSinceEpoch();

    bool operator==(const icDateTime & other) const;
};

#endif  // types_DateTime
