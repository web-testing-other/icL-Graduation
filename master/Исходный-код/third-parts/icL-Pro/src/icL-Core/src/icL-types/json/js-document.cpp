#include "js-document.h++"

#include "js-array.h++"
#include "js-object.h++"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

#include <cassert>



jsDocument::jsDocument(jsDocument && doc) {
    m_array  = doc.m_array;
    m_object = doc.m_object;

    doc.m_object = nullptr;
    doc.m_array  = nullptr;
}

jsDocument::~jsDocument() {
    delete m_array;
    delete m_object;
}

bool jsDocument::isObject() {
    return m_object != nullptr;
}

bool jsDocument::isArray() {
    return m_array != nullptr;
}

jsObject & jsDocument::object() {
    return *m_object;
}

jsArray & jsDocument::array() {
    return *m_array;
}

icString jsDocument::toJson() {
    QJsonDocument jdoc;

    if (isObject()) {
        jdoc.setObject(m_object->toJson());
    }
    else if (isArray()) {
        jdoc.setArray(m_array->toJson());
    }

    return QString{jdoc.toJson(QJsonDocument::Compact)};
}

jsDocument & jsDocument::root(jsRoot rootType) {
    if (rootType == jsRoot::Array) {
        delete m_object;
        delete m_array;
        m_array = new jsArray;
    }
    else if (rootType == jsRoot::Object) {
        delete m_array;
        delete m_object;
        m_object = new jsObject;
    }
    return *this;
}

jsDocument & jsDocument::root(jsObject & root) {
    delete m_array;
    delete m_object;
    m_object = new jsObject(root);

    return *this;
}

jsDocument & jsDocument::root(jsArray & root) {
    delete m_array;
    delete m_object;
    m_array = new jsArray(root);

    return *this;
}

jsDocument & jsDocument::insert(const icString & key, const icVariant & value) {
    assert(isObject());

    m_object->insert(key, value);

    return *this;
}

jsDocument jsDocument::fromJson(const icString & str, jsError & error) {
    QJsonParseError qerror;
    QJsonDocument   qjdoc =
      QJsonDocument::fromJson(str.getData().toUtf8(), &qerror);
    jsDocument ret;

    if (qerror.error != QJsonParseError::NoError) {
        error.error       = jsError::ParseError;
        error.errorString = qerror.errorString();
    }
    else if (qjdoc.isArray()) {
        ret.root(jsRoot::Array);
        ret.m_array->fromJson(qjdoc.array());
    }
    else if (qjdoc.isObject()) {
        ret.root(jsRoot::Object);
        ret.m_object->fromJson(qjdoc.object());
    }
    else {
        error.error       = jsError::ParseError;
        error.errorString = "Incompatible root type";
    }

    return ret;
}
