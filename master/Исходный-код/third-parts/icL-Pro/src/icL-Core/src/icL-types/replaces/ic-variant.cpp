#include "ic-variant.h++"

#include "ic-datetime.h++"
#include "ic-file.h++"
#include "ic-regex.h++"
#include "ic-string-list.h++"
#include "ic-string.h++"
#include "js-array.h++"
#include "js-object.h++"

#include <cassert>



icVariant::icVariant() {
    m_type = icType::Initial;
}

icVariant::icVariant(const bool value) {
    d      = value;
    m_type = icType::Bool;
}

icVariant::icVariant(const int & value)
    : icVariant() {
    d      = value;
    m_type = icType::Int;
}

icVariant::icVariant(const double & value)
    : icVariant() {
    d      = value;
    m_type = icType::Double;
}

icVariant::icVariant(const icString & string)
    : icVariant() {
    d      = string;
    m_type = icType::String;
}

icVariant::icVariant(const icDateTime & dt)
    : icVariant() {
    d      = dt;
    m_type = icType::DateTime;
}

icVariant::icVariant(const icStringList & list)
    : icVariant() {
    d      = list;
    m_type = icType::StringList;
}

icVariant::icVariant(const icRegEx & re)
    : icVariant() {
    d      = re;
    m_type = icType::RegEx;
}

icVariant::icVariant(const jsArray & array)
    : icVariant() {
    d      = array;
    m_type = icType::JsArray;
}

icVariant::icVariant(const jsObject & object)
    : icVariant() {
    d      = object;
    m_type = icType::JsObject;
}

icVariant icVariant::makeVoid() {
    icVariant ret;
    ret.m_type = icType::Void;
    return ret;
}

icType icVariant::type() const {
    return m_type;
}

bool icVariant::isBool() const {
    return m_type == icType::Bool;
}

bool icVariant::isInt() const {
    return m_type == icType::Int;
}

bool icVariant::isDouble() const {
    return m_type == icType::Double;
}

bool icVariant::isString() const {
    return m_type == icType::String;
}

bool icVariant::isDateTime() const {
    return m_type == icType::DateTime;
}

bool icVariant::isStringList() const {
    return m_type == icType::StringList;
}

bool icVariant::isRegEx() const {
    return m_type == icType::RegEx;
}

bool icVariant::isJsObject() const {
    return m_type == icType::JsObject;
}

bool icVariant::isJsArray() const {
    return m_type == icType::JsArray;
}

bool icVariant::is(int type) const {
    return m_type == type;
}

bool icVariant::isNull() const {
    return m_type == icType::Initial;
}

bool icVariant::isVoid() const {
    return m_type == icType::Void;
}

bool icVariant::isValid() const {
    return m_type != icType::Void && m_type != icType::Initial;
}

const std::any & icVariant::toAny() const {
    return d;
}

icVariant icVariant::fromValue(const bool & value) {
    return fromValueTemplate(value, icType::Bool);
}

icVariant icVariant::fromValue(const int & value) {
    return fromValueTemplate(value, icType::Int);
}

icVariant icVariant::fromValue(const double & value) {
    return fromValueTemplate(value, icType::Double);
}

icVariant icVariant::fromValue(const icString & string) {
    return fromValueTemplate(string, icType::String);
}

icVariant icVariant::fromValue(const icDateTime & dt) {
    return fromValueTemplate(dt, icType::DateTime);
}

icVariant icVariant::fromValue(const icStringList & list) {
    return fromValueTemplate(list, icType::StringList);
}

icVariant icVariant::fromValue(const icRegEx & re) {
    return fromValueTemplate(re, icType::RegEx);
}

icVariant icVariant::fromValue(const jsArray & array) {
    return fromValueTemplate(array, icType::JsArray);
}

icVariant icVariant::fromValue(const jsObject & object) {
    return fromValueTemplate(object, icType::JsObject);
}

const bool & icVariant::toBool() const {
    assert(isBool());
    return std::any_cast<const bool &>(d);
}

const int & icVariant::toInt() const {
    assert(isInt());
    return std::any_cast<const int &>(d);
}

const double & icVariant::toDouble() const {
    assert(isDouble());
    return std::any_cast<const double &>(d);
}

const icString & icVariant::toString() const {
    assert(isString());
    return std::any_cast<const icString &>(d);
}

const icDateTime & icVariant::toDateTime() const {
    assert(isDateTime());
    return std::any_cast<const icDateTime &>(d);
}

const icStringList & icVariant::toStringList() const {
    assert(isStringList());
    return std::any_cast<const icStringList &>(d);
}

const icRegEx & icVariant::toRegEx() const {
    assert(isRegEx());
    return std::any_cast<const icRegEx &>(d);
}

const jsArray & icVariant::toArray() const {
    assert(isJsArray());
    return std::any_cast<const jsArray &>(d);
}

const jsObject & icVariant::toJsObject() const {
    assert(isJsObject());
    return std::any_cast<const jsObject &>(d);
}

icVariant & icVariant::clear() {
    m_type = icType::Void;
    return *this;
}

bool icVariant::operator==(const icVariant & other) const {
    if (m_type != other.m_type)
        return false;

    switch (m_type) {
    case icType::Initial:
    case icType::Void:
        return true;

    case icType::Bool:
        return toBool() == other.toBool();

    case icType::Int:
        return toInt() == other.toInt();

    case icType::Double:
        return toDouble() == other.toDouble();

    case icType::String:
        return toString() == other.toString();

    case icType::DateTime:
        return toDateTime() == other.toDateTime();

    case icType::StringList:
        return toStringList() == other.toStringList();

    case icType::RegEx:
        return toRegEx() == other.toRegEx();

    case icType::JsArray:
        return toArray() == other.toArray();

    case icType::JsObject:
        return toJsObject() == other.toJsObject();

    default:
        return false;
    }
}

icVariant::operator icRegEx() const {
    assert(isRegEx());
    return std::any_cast<const icRegEx &>(d);
}

icVariant::operator icStringList() const {
    assert(isStringList());
    return std::any_cast<const icStringList &>(d);
}

icVariant::operator icDateTime() const {
    assert(isDateTime());
    return std::any_cast<const icDateTime &>(d);
}

icVariant::operator icString() const {
    assert(isString());
    return std::any_cast<const icString &>(d);
}

icVariant::operator double() const {
    assert(isDouble());
    return std::any_cast<const double &>(d);
}

icVariant::operator int() const {
    assert(isInt());
    return std::any_cast<const int &>(d);
}

icVariant::operator bool() const {
    assert(isBool());
    return std::any_cast<const bool &>(d);
}
