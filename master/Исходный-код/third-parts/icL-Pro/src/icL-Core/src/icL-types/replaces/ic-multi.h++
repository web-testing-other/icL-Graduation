#ifndef types_Multi
#define types_Multi

#include "ic-hash-functions.h++"

#include <QMap>



template <typename Key, typename Value>
class icL_Core_types_Share icMulti
{
    QMultiMap<Key, Value> d;

public:
    icMulti() = default;

    using Iterator      = typename QMultiMap<Key, Value>::Iterator;
    using ConstIterator = typename QMultiMap<Key, Value>::ConstIterator;

    // immutable members

    bool contains(const Key & key) const {
        return d.contains(key);
    }

    bool isEmpty() const {
        return d.isEmpty();
    }

    const Key & lastKey() const {
        return d.lastKey();
    }

    const Value & last() const {
        return d.last();
    }

    // mutable members

    icMulti & insert(const Key & key, const Value & value) {
        d.insert(key, value);
        return *this;
    }

    icMulti & clear() {
        d.clear();
        return *this;
    }

    icMulti & remove(const Key & key, const Value & value) {
        d.remove(key, value);
        return *this;
    }

    // iterators

    Iterator find(const Key & key) {
        return d.find(key);
    }

    ConstIterator find(const Key & key) const {
        return d.find(key);
    }

    Iterator erase(Iterator & it) {
        return d.erase(it);
    }

    Iterator begin() {
        return d.begin();
    }

    ConstIterator begin() const {
        return d.end();
    }

    Iterator end() {
        return d.end();
    }

    ConstIterator end() const {
        return d.end();
    }
};

#endif  // types_Multi
