#ifndef types_Object
#define types_Object

#include "ic-hash-functions.h++"
#include "ic-pair.h++"

#include <QHash>



template <typename Key, typename Value>
class icL_Core_types_Share icObject
{
    QHash<Key, Value> d;

public:
    icObject() = default;

    icObject(std::initializer_list<icPair<Key, Value>> list)
        : d() {
        for (auto & pair : list) {
            d.insert(pair.first(), pair.second());
        }
    }

    icObject(const QHash<Key, Value> & map)
        : d(map) {}

    using Iterator      = typename QHash<Key, Value>::Iterator;
    using ConstIterator = typename QHash<Key, Value>::ConstIterator;

    // immutable members

    int count() const {
        return d.count();
    }

    bool contains(const Key & key) const {
        return d.contains(key);
    }

    Value value(const Key & key, const Value & default_) const {
        return d.value(key, default_);
    }

    // mutable members

    void insert(const Key & key, const Value & value) {
        d.insert(key, value);
    }

    void clear() {
        d.clear();
    }

    void remove(const Key & key) {
        auto it = d.find(key);

        if (it != d.end()) {
            d.erase(it);
        }
    }

    // operators

    bool operator==(const icObject & other) const {
        return d == other.d;
    }

    // iterators

    Iterator begin() {
        return d.begin();
    }

    Iterator end() {
        return d.end();
    }

    ConstIterator begin() const {
        return d.begin();
    }

    ConstIterator end() const {
        return d.end();
    }

    Iterator find(const Key & key) {
        return d.find(key);
    }

    ConstIterator find(const Key & key) const {
        return d.find(key);
    }

    Value & operator[](const Key & key) {
        return d[key];
    }

    Value operator[](const Key & key) const {
        return d[key];
    }
};

#endif  // types_Object
