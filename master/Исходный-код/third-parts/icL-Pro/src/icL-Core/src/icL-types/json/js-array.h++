#ifndef types_jsArray
#define types_jsArray

#include "../replaces/ic-list.h++"
#include "../replaces/ic-variant.h++"



class icL_Core_types_Share jsArray : public icList<icVariant>
{
public:
    jsArray();

    class QJsonArray toJson() const;

    jsArray & fromJson(class QJsonArray json);
};

#endif  // types_jsArray
