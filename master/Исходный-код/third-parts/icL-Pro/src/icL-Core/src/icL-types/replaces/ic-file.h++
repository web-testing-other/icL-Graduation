#ifndef types_icFile
#define types_icFile

#include <icL-types/global/icl-types.h++>

#include <QFile>



class icString;

class icL_Core_types_Share icFile
{
    QFile d;

public:
    icFile();
    icFile(const char * path);
    icFile(const icString & path);

    bool isReadable() const;

    icString getFileName() const;

    bool operator==(const icFile & other) const;

    friend class icTextStream;
};

#endif  // types_icFile
