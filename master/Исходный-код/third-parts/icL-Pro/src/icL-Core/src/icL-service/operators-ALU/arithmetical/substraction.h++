#ifndef core_service_Substraction
#define core_service_Substraction

#include <icL-service/main/values/inode.h++>



namespace icL::core {

namespace memory {
struct Set;
}

namespace service {

class icL_core_service_operators_ALU_EXPORT Subtraction : virtual public INode
{
public:
    Subtraction();

    // level 1

    /// `-int : int`
    int voidInt(int right);

    /// `-double : double`
    double voidDouble(double right);

    /// `int - int : int`
    int intInt(int left, int right);

    /// `double - double : double`
    double doubleDouble(double left, double right);

    /// `set - set : set`
    memory::Set setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Substraction
