#ifndef core_service_Stack
#define core_service_Stack

#include <icL-il/export/global.h++>



class icString;
class icVariant;

namespace icL::core {

namespace memory {
class StackContainer;
}

namespace service {

class icL_core_service_value_system_EXPORT Stack
{
public:
    Stack(memory::StackContainer * stack);

    // methods level 1

    /// `Stack.addDescription (description : string) : void`
    void addDescription(const icString & description);

    /// `Stack.break () : void`
    void break_();

    /// `Stack.clear () : void`
    void clear();

    /// `Stack.continue () : void`
    void continue_();

    /// `Stack.destroy () : void`
    void destroy();

    /// `Stack.ignore () : Stack`
    void ignore();

    /// `Stack.listen () : Stack`
    void listen();

    /// `Stack.markStep (name : string) : Stack`
    void markStep(const icString & name);

    /// `Stack.markTest (name : string) : Stack`
    void markTest(const icString & name);

    /// `Stack.return (value : any) : void`
    void return_(const icVariant & value);

protected:
    memory::StackContainer * stack = nullptr;
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Stack
