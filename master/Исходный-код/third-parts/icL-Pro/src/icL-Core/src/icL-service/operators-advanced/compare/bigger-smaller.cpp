#include "bigger-smaller.h++"



namespace icL::core::service {

bool BiggerSmaller::intIntInt(int left, int begin, int end) {
    return left > begin && left < end;
}

bool BiggerSmaller::doubleDoubleDouble(double left, double begin, double end) {
    return left > begin && left < end;
}

}  // namespace icL::core::service
