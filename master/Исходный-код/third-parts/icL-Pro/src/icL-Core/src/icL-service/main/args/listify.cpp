#include "listify.h++"

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>
#include <icL-memory/structures/parameter.h++>
#include <icL-memory/structures/type.h++>

namespace icL::core::service {

memory::ArgList Listify::toArgList(
  il::InterLevel * il, const icVariant & value) {
    memory::ArgList ret;

    if (value.is(icType::Packed)) {
        for (auto & item : *memory::PackedValue(value).data) {
            memory::Argument arg;

            if (
              item.itemType == il::PackableType::Value ||
              item.itemType == il::PackableType::Type) {
                arg.type      = static_cast<enum memory::Type>(item.type);
                arg.varName   = item.name;
                arg.value     = item.value;
                arg.container = item.container;
            }
            else {
                il->vm->syssig("Wrong arguments list");
                break;
            }

            ret.append(arg);
        }
    }
    else {
        memory::Argument arg;

        arg.type  = il->factory->variantToType(value);
        arg.value = value;

        ret.append(arg);
    }

    return ret;
}

memory::ParamList Listify::toParamList(
  il::InterLevel * il, const icVariant & value) {
    memory::ParamList ret;

    if (value.is(icType::Packed)) {
        for (auto & item : *memory::PackedValue(value).data) {
            memory::Parameter param;

            if (
              item.itemType == il::PackableType::Parameter ||
              item.itemType == il::PackableType::DefaultParameter) {
                param.type  = item.type;
                param.value = item.value;
                param.name  = item.name;
            }
            else {
                il->vm->syssig("Wrong parameters list");
                break;
            }

            ret.append(param);
        }
    }
    else {
        il->vm->syssig("Parameters list is not a packed value");
    }

    return ret;
}

bool Listify::isArgList(il::PackableValue * value) {
    auto * packed = dynamic_cast<il::ValuePack *>(value);

    if (packed == nullptr) {
        return true;
    }

    bool ret = true;

    for (auto & value : packed->getValues()) {
        ret = ret && (value.itemType == il::PackableType::Value ||
                      value.itemType == il::PackableType::Type);
    }

    return ret;
}

bool Listify::isParamList(il::PackableValue * value) {
    auto * packed = dynamic_cast<il::ValuePack *>(value);

    if (packed == nullptr) {
        return value->packableType() == il::PackableType::Parameter ||
               value->packableType() == il::PackableType::DefaultParameter;
    }

    bool ret = true;

    for (auto & value : packed->getValues()) {
        ret = ret && (value.itemType == il::PackableType::Parameter ||
                      value.itemType == il::PackableType::DefaultParameter);
    }

    return ret;
}

}  // namespace icL::core::service
