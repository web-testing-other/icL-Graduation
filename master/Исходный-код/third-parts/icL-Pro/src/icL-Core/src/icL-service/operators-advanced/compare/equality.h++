#ifndef core_service_Equality
#define core_service_Equality


#include <icL-service/main/values/inode.h++>



class icString;
class icStringList;

namespace icL::core {

namespace il {
struct Session;
struct Tab;
struct Window;
}  // namespace il

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace service {

class icL_core_service_operators_advanced_EXPORT Equality : virtual public INode
{
public:
    // level 1

    /// `bool == bool : bool`
    bool boolBool(bool left, bool right);

    /// `int == int : bool`
    bool intInt(int left, int right);

    /// `double == double : bool`
    bool doubleDouble(double left, double right);

    /// `string == string : bool`
    bool stringString(const icString & left, const icString & right);

    /// `list == list : bool`
    bool listList(const icStringList & left, const icStringList & right);

    /// `object == object : bool`
    bool objectObject(
      const memory::Object & left, const memory::Object & right);

    /// `set == set : bool`
    bool setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Equality
