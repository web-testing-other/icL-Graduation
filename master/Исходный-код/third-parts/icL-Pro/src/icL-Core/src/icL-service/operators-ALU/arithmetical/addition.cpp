#include "addition.h++"

#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

Addition::Addition() = default;

int Addition::voidInt(int right) {
    return right >= 0 ? right : -right;
}

double Addition::voidDouble(double right) {
    return right >= 0 ? right : -right;
}

int Addition::intInt(int left, int right) {
    return left + right;
}

double Addition::doubleDouble(double left, double right) {
    return left + right;
}

icString Addition::stringString(const icString & left, const icString & right) {
    return left + right;
}

icStringList Addition::stringList(
  const icString & left, const icStringList & right) {
    return {left, right};
}

icStringList Addition::listString(
  const icStringList & left, const icString & right) {
    return {left, right};
}

icStringList Addition::listList(
  const class icStringList & left, const icStringList & right) {
    return {left, right};
}

memory::Set Addition::setSet(
  const memory::Set & left, const memory::Set & right) {
    return service::Set::unite(core(), left, right);
}

}  // namespace icL::core::service
