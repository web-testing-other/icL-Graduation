#ifndef core_service_Import
#define core_service_Import

#include <icL-service/main/values/inode.h++>



class icString;
class icVariant;
template <typename>
class icList;
template <typename, typename>
class icObject;
using icVariantMap = icObject<icString, icVariant>;

namespace std {
template <typename>
class function;
}

namespace icL::core {

namespace memory {
struct ArgValue;
struct Object;
using ArgValueList = icList<ArgValue>;
class Memory;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_value_system_EXPORT Import : virtual public INode
{
public:
    Import();

    // methods level 1

    /// `Import.all (data = [<>], path : string) : void`
    void all(const memory::Object & obj, const icString & path);

    /// `Import.functions (data = [<>], path : string) : void`
    void functions(const memory::Object & obj, const icString & path);

    /// `Import.none (data = [<>], path : string) : void`
    void none(const memory::Object & obj, const icString & path);

    /// `Import.run (path : string) : void`
    void run(const icString & path);

private:
    /**
     * @brief mapToArgValueList converts a icVariant map to arg-value icList
     * @param map is the map to convert
     * @return a arg-value icList
     */
    memory::ArgValueList mapToArgValueList(const icVariantMap & map);

    /**
     * @brief cloneGlobals copy global variable from dump to current context
     * @param dumpMemory is the dump of a file execution
     */
    void cloneGlobals(memory::Memory * dumpMemory);

    /**
     * @brief cloneFunctions copy functions from dump to current context
     * @param dumpMemory is the dump of a file execution
     */
    void cloneFunctions(memory::Memory * dumpMemory);

    /**
     * @brief import eecutes a file and import data from it
     * @param obj is the icObject to export to file context
     * @param path is the path of file to execute
     * @param feedback is the feedback function after function execution
     */
    void import(
      const memory::Object & obj, const icString & path,
      const std::function<void(memory::Memory *)> & feedback);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Import
