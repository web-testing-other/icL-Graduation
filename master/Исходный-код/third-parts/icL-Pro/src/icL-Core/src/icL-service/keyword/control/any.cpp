#include "any.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>

namespace icL::core::service {

using il::Signals::Signals;

StepType Any::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        core()->vms->pushKeepAliveLayer(il::LayerType::Control);
        current = State::ValueCalculating;
        break;

    case State::ValueCalculating: {
        memory::FunctionCall fcall;

        fcall.code        = valueCode;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "any");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else {
                value   = ret.consoleValue;
                current = State::BodyExecution;
            }
            return false;
        });

        ret = StepType::CommandIn;
        break;
    }
    case State::BodyExecution: {
        memory::FunctionCall fcall;

        fcall.code        = body;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "any");
        fcall.args.append({"@", value});

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else {
                current = State::End;
            }
            return false;
        });

        ret = StepType::CommandIn;
        break;
    }
    case State::End:
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}


}  // namespace icL::core::service
