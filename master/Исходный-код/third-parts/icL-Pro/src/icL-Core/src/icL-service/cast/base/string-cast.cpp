#include "string-cast.h++"

#include "void-cast.h++"

#include <icL-types/json/js-array.h++>
#include <icL-types/json/js-document.h++>
#include <icL-types/json/js-object.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

using il::Signals::Signals;

bool StringCast::toBool(const icString & value) {
    return !value.isEmpty();
}

int StringCast::toInt(il::InterLevel * il, const icString & value) {
    bool ok;
    int  ret;

    ret = value.toInt(ok);

    if (!ok) {
        il->vm->signal({il::Signals::ParsingFailed, ""});
    }

    return ret;
}

double StringCast::toDouble(il::InterLevel * il, const icString & value) {
    bool   ok;
    double ret;

    ret = value.toDouble(ok);

    if (!ok) {
        il->vm->signal({Signals::ParsingFailed, ""});
    }

    return ret;
}

icStringList StringCast::toList(const icString & value) {
    return icStringList() << value;
}

memory::Object StringCast::toObject(il::InterLevel * il, const jsObject obj) {
    memory::Object ret = service::VoidCast::toObject(il);

    for (auto it = obj.begin(); it != obj.end(); it++) {

        switch (it.value().type()) {
        case icType::Bool:
            ret.data->setValue(it.key(), it.value().toBool());
            break;

        case icType::Double:
            ret.data->setValue(it.key(), it.value().toDouble());
            break;

        case icType::String:
            ret.data->setValue(it.key(), it.value().toString());
            break;

        case icType::StringList: {
            icStringList list;

            bool isStringList = true;

            for (const auto & value : it.value().toArray()) {
                isStringList = isStringList && value.isString();

                if (isStringList) {
                    list.append(value.toString());
                }
            }

            if (!isStringList) {
                il->vm->signal({Signals::ComplexField, ""});
            }

            ret.data->setValue(it.key(), list);
        } break;

        default:
            il->vm->signal({Signals::ComplexField, ""});
        }
    }

    return ret;
}

memory::Object StringCast::toObject(
  il::InterLevel * il, const icString & value) {
    jsError        error;
    jsDocument     jdoc = jsDocument::fromJson(value, error);
    memory::Object ret  = service::VoidCast::toObject(il);

    if (error.error != jsError::NoError) {
        il->vm->signal({Signals::ParsingFailed, error.errorString});
    }
    else {
        if (jdoc.isObject()) {
            ret = toObject(il, jdoc.object());
        }
        else {
            il->vm->signal({Signals::IncompatibleRoot, ""});
        }
    }

    return ret;
}

memory::Set StringCast::toSet(il::InterLevel * il, const icString & value) {
    jsError     error;
    jsDocument  jdoc = jsDocument::fromJson(value, error);
    memory::Set ret  = service::VoidCast::toSet();

    if (error.error != jsError::NoError) {
        il->vm->signal({Signals::ParsingFailed, error.errorString});
    }
    else {
        if (jdoc.isArray()) {
            for (const auto & value : jdoc.array()) {
                if (value.isJsObject()) {
                    memory::Object toAdd = toObject(il, value.toJsObject());

                    if (ret.header->isEmpty()) {
                        ret = service::Set::fromObject(toAdd);
                    }
                    else {
                        service::Set::insert(il, ret, toAdd);
                    }
                }
                else {
                    il->vm->signal({Signals::IncompatibleRoot, ""});
                }
            }
        }
        else {
            il->vm->signal({Signals::IncompatibleRoot, ""});
        }
    }

    return ret;
}

}  // namespace icL::core::service
