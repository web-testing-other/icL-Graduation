#ifndef core_service_If
#define core_service_If


#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service/keyword/main/finite-state-machine.h++>
#include <icL-service/main/values/inode.h++>



namespace icL::core::service {

class icL_core_service_keyword_EXPORT If
    : virtual public FiniteStateMachine
    , virtual public INode
{
    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    /**
     * @brief release runs the code and switch to end state
     * @param code is the code to run
     */
    void release(const il::CodeFragment & code);

protected:
    enum class State {      ///< State of if FSM
        Initial,            ///< Initial state
        ConditionChecking,  ///< Checking conditions of ifs
        CodeRunning,        ///< Run the code of current if
        LastResort,         ///< Run the code of last else
        End                 ///< The FMS ends here
    } current = State::Initial;

    // padding
    int : 32;

    struct IfData  ///< describes a `if () {}` sequence
    {
        il::CodeFragment condition;  ///< is the condition code
        il::CodeFragment body;       ///< is the body code

        bool notModifier{};  ///< `if!` or `if:not`
    };

    icList<IfData>           ifs;  ///< list of cascade `if`s
    icList<IfData>::Iterator it;   ///< iterator to current `if` token

    il::CodeFragment elseCode;  ///< is the code of `else {}` token

    icVariant ret;  ///< is the value to be returned as `#`

    bool notModifier = false;  ///< `if!` or `if:not`
};

}  // namespace icL::core::service

#endif  // core_service_If
