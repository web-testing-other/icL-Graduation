#ifndef core_service_DoubleCast
#define core_service_DoubleCast

#include <icL-il/export/global.h++>



class icString;

namespace icL::core::service {

class icL_core_service_cast_EXPORT DoubleCast
{
public:
    /// `double : bool`
    static bool toBool(double value);

    /// `double : int`
    static int toInt(double value);

    /// `double : string`
    static icString toString(double value);
};

}  // namespace icL::core::service

#endif  // core_service_DoubleCast
