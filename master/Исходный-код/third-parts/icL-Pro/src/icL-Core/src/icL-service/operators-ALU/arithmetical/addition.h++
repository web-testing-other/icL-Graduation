#ifndef core_service_Addition
#define core_service_Addition

#include <icL-service/main/values/inode.h++>



class icString;
class icStringList;

namespace icL::core {

namespace memory {
struct Set;
}

namespace service {

class icL_core_service_operators_ALU_EXPORT Addition : virtual public INode
{
public:
    Addition();

    // level 1

    /// `+int : int`
    int voidInt(int right);

    /// `+double : double`
    double voidDouble(double right);

    /// `int + int : int`
    int intInt(int left, int right);

    /// `double + double : double`
    double doubleDouble(double left, double right);

    /// `string + string : string`
    icString stringString(const icString & left, const icString & right);

    /// `string + list : list`
    icStringList stringList(const icString & left, const icStringList & right);

    /// `list + string : list`
    icStringList listString(const icStringList & left, const icString & right);

    /// `list + list : list`
    class icStringList listList(
      const icStringList & left, const icStringList & right);

    /// `set + set : set`
    memory::Set setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Addition
