#ifndef core_service_ForParametric
#define core_service_ForParametric

#include "for.h++"



namespace icL::core::service {

class icL_core_service_keyword_EXPORT ForParametric : public For
{
public:
    ForParametric(bool alt);

    // Loop interface
public:
    void break_() override;
    void continue_() override;

    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {      ///< FMS `for (<init>; <condition>; <step>) {code}`
        Initial,            ///< Initial state of any `for`
        Intiaization,       ///< Initializing parametric `for`
        ConditionChecking,  ///< Checking the condition of loop
        CodeExecution,      ///< Executing the body loop
        StepExecution,      ///< Executing step command
        End                 ///< The FMS ends here
    } current = State::Initial;

    /// alt contains the state of `:alt` modifier
    bool alt = false;

    int : 24;  // padding

    /// init is the initialization code
    il::CodeFragment init;
    /// condition is the condition of loop
    il::CodeFragment condition;
    /// step is the iteration step code
    il::CodeFragment step;
};

}  // namespace icL::core::service

#endif  // core_service_ForParametric
