#ifndef core_service_Iterator
#define core_service_Iterator

#include <icL-il/export/global.h++>



class icVariant;

namespace icL::core::service {

/**
 * @brief The Iterator class describes a interface of a icL iterator
 */
class icL_core_service_keyword_EXPORT Iterator
{
public:
    /**
     * @brief ~Iterator is the virutal destructor for interface
     */
    virtual ~Iterator() = default;

    /**
     * @brief toFirst iterates to first item of collection
     */
    virtual void toFirst() = 0;

    /**
     * @brief toLast iterates to last item of collection
     */
    virtual void toLast() = 0;

    /**
     * @brief toNext iterates to last item of collection
     */
    virtual void toNext() = 0;

    /**
     * @brief toPrev iterates to previous item of collection
     */
    virtual void toPrev() = 0;

    /**
     * @brief atBegin checks if the iterator is placed at the begin
     * @return true if `it == collection.begin`, otherwise false
     */
    virtual bool atBegin() = 0;

    /**
     * @brief atEnd checks if the iterator is placed at end
     * @return true if `it == collection.end`, otherwise false
     */
    virtual bool atEnd() = 0;

    /**
     * @brief atStop checks if the iterator is placed in stop place
     * @return true if `it == stop`, otherwise false
     */
    virtual bool atStop() = 0;

    /**
     * @brief setStopToCurrent sets the position of stop mark to current
     */
    virtual void setStopToCurrent() = 0;

    /**
     * @brief setEndToCurrent sets the end position to current
     */
    virtual void setEndToCurrent() = 0;

    /**
     * @brief getCurrent gets the value of current item
     * @return the value of current item
     */
    virtual icVariant getCurrent() = 0;

    /**
     * @brief getSize gets the size of collection
     * @return the size of collection
     */
    virtual int getSize() = 0;
};

}  // namespace icL::core::service

#endif  // core_service_Iterator
