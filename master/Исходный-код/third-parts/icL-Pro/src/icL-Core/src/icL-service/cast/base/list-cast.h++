#ifndef core_service_ListCast
#define core_service_ListCast

#include <icL-il/export/global.h++>



class icString;
class icStringList;

namespace icL::core {

namespace memory {
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_cast_EXPORT ListCast
{
public:
    /// `list : bool`
    static bool toBool(const icStringList & value);

    /// `list : string`
    static icString toString(il::InterLevel * il, const icStringList & value);

    /// `list : set`
    static memory::Set toSet(il::InterLevel * il, const icStringList & value);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_ListCast
