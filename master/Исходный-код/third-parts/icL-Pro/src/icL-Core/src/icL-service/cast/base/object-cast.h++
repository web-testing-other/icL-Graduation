#ifndef core_service_ObjectCast
#define core_service_ObjectCast

#include <icL-il/export/global.h++>



class icString;
class icStringList;

namespace icL::core {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_cast_EXPORT ObjectCast
{
public:
    /// `object : bool`
    static bool toBool(const memory::Object & value);

    /// `object : string`
    static icString toString(const memory::Object & value);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_ObjectCast
