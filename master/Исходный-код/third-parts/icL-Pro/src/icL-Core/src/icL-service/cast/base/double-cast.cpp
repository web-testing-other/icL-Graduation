#include "double-cast.h++"

#include <icL-types/replaces/ic-string.h++>

namespace icL::core::service {

bool DoubleCast::toBool(double value) {
    return value != 0.0;
}

int DoubleCast::toInt(double value) {
    return int(value);
}

icString DoubleCast::toString(double value) {
    return icString::number(value);
}


}  // namespace icL::core::service
