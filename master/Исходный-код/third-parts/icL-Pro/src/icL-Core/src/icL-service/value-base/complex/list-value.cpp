#include "list-value.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>



namespace icL::core::service {

ListValue::ListValue() = default;

bool ListValue::empty() {
    return _value().isEmpty();
}

int ListValue::length() {
    return _value().length();
}

icString ListValue::last() {
    auto value = _value();

    if (value.isEmpty()) {
        core()->vm->signal({il::Signals::EmptyList, {}});
        return {};
    }

    return value[value.length() - 1];
}

void ListValue::append(const icString & str) {
    auto value = _value();

    value.append(str);
    reset(value);
}

icString ListValue::at(int i) {
    auto value = _value();

    if (i < 0 || i >= value.length()) {
        core()->vm->signal({il::Signals::OutOfBounds, {}});
        return {};
    }

    return value[i];
}

bool ListValue::contains(const icString & str, bool caseSensitive) {
    return _value().contains(str, caseSensitive);
}

void ListValue::clear() {
    reset(icStringList{});
}

int ListValue::count(const icString & what) {
    return _value().count(what);
}

icStringList ListValue::filter(const icString & str, bool caseSensitive) {
    return _value().filter(str, caseSensitive);
}

icStringList ListValue::filter(const icRegEx & re) {
    return _value().filter(re);
}

int ListValue::indexOf(const icString & str, int start) {
    return _value().indexOf(str, start);
}

int ListValue::indexOf(const icRegEx & re, int start) {
    return _value().indexOf(re, start);
}

void ListValue::insert(int index, const icString & str) {
    reset(_value().insert(index, str));
}

icString ListValue::join(const icString & separator) {
    return _value().join(separator);
}

int ListValue::lastIndexOf(const icString & str, int start) {
    return _value().lastIndexOf(str, start);
}

int ListValue::lastIndexOf(const icRegEx & re, int start) {
    return _value().lastIndexOf(re, start);
}

icStringList ListValue::mid(int pos, int n) {
    return _value().mid(pos, n);
}

void ListValue::prepend(const icString & str) {
    reset(_value().prepend(str));
}

void ListValue::move(int from, int to) {
    auto value = _value();

    if (from < 0 || to < 0 || from >= value.length() || to >= value.length()) {
        core()->vm->signal({il::Signals::OutOfBounds, {}});
        return;
    }

    reset(value.move(from, to));
}

void ListValue::removeAll(const icString & str) {
    reset(_value().removeAll(str));
}

void ListValue::removeAt(int i) {
    auto value = _value();

    if (i < 0 || i >= value.length()) {
        core()->vm->signal({il::Signals::OutOfBounds, {}});
        return;
    }

    reset(value.removeAt(i));
}

void ListValue::removeDuplicates() {
    reset(_value().removeDuplicates());
}

void ListValue::removeFirst() {
    auto value = _value();

    if (value.isEmpty()) {
        core()->vm->signal({il::Signals::EmptyList, {}});
        return;
    }

    reset(value.removeFirst());
}

void ListValue::removeLast() {
    auto value = _value();

    if (value.isEmpty()) {
        core()->vm->signal({il::Signals::EmptyList, {}});
        return;
    }

    reset(value.removeLast());
}

void ListValue::removeOne(const icString & str) {
    reset(_value().removeOne(str));
}

icStringList ListValue::replaceInStrings(
  const icString & before, const icString & after) {
    return _value().replaceInStrings(before, after);
}

void ListValue::sort(bool caseSensitive) {
    reset(_value().sort(caseSensitive));
}

icStringList ListValue::_value() {
    return value();
}

}  // namespace icL::core::service
