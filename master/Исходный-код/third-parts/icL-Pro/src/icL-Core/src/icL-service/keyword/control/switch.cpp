#include "switch.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/args/listify.h++>
#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/arg-value.h++>
#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::service {

using il::Signals::Signals;

StepType Switch::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        core()->vms->pushKeepAliveLayer(il::LayerType::Control);
        it      = cases.begin();
        current = State::SwitchChecking;
        break;

    case State::SwitchChecking: {
        switchChecking();
        ret = StepType::CommandIn;
    } break;

    case State::CaseChecking:
        if (somethingExecuted && !lastExecuted) {
            current = State::End;
        }
        else {
            caseChecking();
            ret = StepType::CommandIn;
        }
        break;

    case State::CaseRunning: {
        memory::FunctionCall fcall;

        fcall.code        = it->body;
        fcall.createLayer = false;
        fcall.contextType = memory::ContextType::Run;
        fcall.contextName = Stringify::alternative(fcall.code.name, "case");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            returnValue = ret.consoleValue;
            return false;
        });

        somethingExecuted = true;

        it++;
        current = State::CaseChecking;
        ret     = StepType::CommandIn;
    } break;

    case State::End:
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

using memory::Type;

bool Switch::compare(const memory::Argument & sw, const memory::Argument & cs) {
    if (cs.type == Type::VoidValue) {
        return !somethingExecuted;
    }

    if (cs.type == Type::BoolValue) {
        return cs.value.toBool();
    }

    if (sw.type != cs.type) {
        core()->vm->signal({Signals::System, "Types not matching"});
        return false;
    }

    bool ret;

    switch (sw.type) {
    case Type::IntValue:
        ret = intInt(sw, cs);
        break;

    case Type::DoubleValue:
        ret = doubleDouble(sw, cs);
        break;

    case Type::StringValue:
        ret = stringString(sw, cs);
        break;

    case Type::ListValue:
        ret = listList(sw, cs);
        break;

    case Type::ObjectValue:
        ret = objectObject(sw, cs);
        break;

    case Type::SetValue:
        ret = setSet(sw, cs);
        break;

    default:
        core()->vm->signal(
          {Signals::System, "No equality operator for this value type"});
        ret = false;
    }

    return ret;
}

void Switch::switchChecking() {
    memory::FunctionCall fcall;

    fcall.code        = switchCode;
    fcall.createLayer = false;
    fcall.contextType = memory::ContextType::Value;
    fcall.contextName = Stringify::alternative(fcall.code.name, "switch");

    core()->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code == Signals::NoError) {
            if (
              ret.consoleValue.type() == icType::Bool ||
              !ret.consoleValue.isValid()) {
                core()->vm->signal(
                  {Signals::System,
                   "Switch value must return a valid non bool value"});
            }
            else {
                switchValue =
                  service::Listify::toArgList(core(), ret.consoleValue);

                if (switchValue.isEmpty()) {
                    core()->vm->signal(
                      {Signals::System,
                       "Switch value must return a non void value"});
                }
            }
        }
        else {
            core()->vm->signal(ret.signal);
        }
        return false;
    });

    current = State::CaseChecking;
}

void Switch::caseChecking() {
    if (it != cases.end()) {
        memory::FunctionCall fcall;

        fcall.code        = it->values;
        fcall.createLayer = false;
        fcall.contextType = memory::ContextType::Value;
        fcall.contextName = Stringify::alternative(fcall.code.name, "case");
        fcall.args.append({"#", lastExecuted});

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            memory::ArgList caseValues =
              service::Listify::toArgList(core(), ret.consoleValue);

            if (caseValues.isEmpty()) {
                core()->vm->signal(
                  {Signals::System, "Case must have a value at least"});
            }

            if (caseValues.length() % switchValue.length() != 0) {
                core()->vm->signal(
                  {Signals::System, "Wrong countity of case values"});
            }

            bool needExecution = false;
            bool compatible;

            int step = switchValue.length();

            for (int i = 0; i < caseValues.length(); i += step) {
                compatible = true;

                for (int j = 0; j < step; j++) {
                    compatible =
                      compatible &&
                      compare(switchValue[j], caseValues[i * step + j]);
                }

                needExecution = needExecution || compatible;
            }

            if (needExecution) {
                current = State::CaseRunning;
            }
            else {
                it++;
            }

            lastExecuted = needExecution;
            return false;
        });
    }
    else {
        current = State::End;
    }
}

}  // namespace icL::core::service
