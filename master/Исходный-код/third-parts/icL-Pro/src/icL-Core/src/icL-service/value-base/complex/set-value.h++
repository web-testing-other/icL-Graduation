#ifndef core_service_SetValue
#define core_service_SetValue

#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode.h++>

#include <icL-memory/structures/type.h++>

template <typename>
class icList;
class icStringList;
class icString;
class icVariant;
using icVariantList = icList<icVariant>;

namespace icL::core {

namespace memory {
struct Set;
struct Object;
}  // namespace memory

namespace service {

using memory::Type;

class icL_core_service_value_base_EXPORT SetValue
    : virtual public il::WriteableValue
    , virtual public INode
{
public:
    SetValue();
    ~SetValue() override = default;

    // properties level 1

    /// `[r/o] set'capacity : int`
    int capacity();

    /// `[r/o] set'empty : bool`
    bool empty();

    // methods level 1

    /// `set.applicate (data : list ...) : set`
    void applicate(const icList<icStringList> & data);

    /// `set.clear () : set`
    void clear();

    /// `set.clone () : set`
    memory::Set clone();

    /// `set.getField (name : string) : list`
    icStringList getField(const icString & name);

    /// `set.hasField (name : string) : bool`
    bool hasField(const icString & name);

    /// `set.insert (data : any ...) : set`
    void insert(const icVariantList & data);

    /// `set.insert (obj : object) : set`
    void insert(const memory::Object & obj);

    /// `set.insertField (name : string, value : list, type = string) : set`
    void insertField(
      const icString & name, const icStringList & value,
      Type type = Type::StringValue);

    /// `set.insertField (name : string, value : any, type = void) : set`
    void insertField(
      const icString & name, const icVariant & var,
      Type type = Type::VoidValue);

    /// `set.remove (data : any ...) : set`
    void remove(const icVariantList & data);

    /// `set.remove (obj : object) : set`
    void remove(const memory::Object & obj);

    /// `set.removeField (name : string) : set`
    void removeField(const icString & name);

protected:
    /**
     * @brief _value gets the own value
     * @return the own value as memory.icSet
     */
    memory::Set _value();
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_SetValue
