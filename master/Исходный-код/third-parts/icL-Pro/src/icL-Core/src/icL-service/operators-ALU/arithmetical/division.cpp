#include "division.h++"

#include <icL-types/replaces/ic-variant.h++>



namespace icL::core::service {

Division::Division() = default;

int Division::intInt(int left, int right) {
    return right == 0 ? 0 : left / right;
}

double Division::doubleDouble(double left, double right) {
    return right == 0.0 ? 0.0 : left / right;
}

}  // namespace icL::core::service
