#ifndef core_service_IntCast
#define core_service_IntCast

#include <icL-il/export/global.h++>



class icString;

namespace icL::core::service {

class icL_core_service_cast_EXPORT IntCast
{
public:
    /// `int : bool`
    static bool toBool(int value);

    /// `int : double`
    static double toDouble(int value);

    /// `int : string`
    static icString toString(int value);
};

}  // namespace icL::core::service

#endif  // core_service_IntCast
