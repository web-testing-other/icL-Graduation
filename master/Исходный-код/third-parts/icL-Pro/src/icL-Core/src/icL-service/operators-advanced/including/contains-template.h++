#ifndef core_service_ContainsTemplate
#define core_service_ContainsTemplate

#include <icL-service/main/values/inode.h++>



class icString;
class icStringList;
class icRegEx;

namespace icL::core {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace service {

class icL_core_service_operators_advanced_EXPORT ContainsTemplate
    : virtual public INode
{
public:
    /// `list <* string : bool`
    bool listString(const icStringList & left, const icString & right);

    /// `set <* object : bool`
    bool setObject(const memory::Set & left, const memory::Object & right);

    /// `object <* object : bool`
    bool objectObject(
      const memory::Object & left, const memory::Object & right);

    /// `string <* regex : list`
    icStringList stringRegex(const icString & left, const icRegEx & right);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_ContainsTemplate
