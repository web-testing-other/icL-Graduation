#include "limited-context.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>

namespace icL::core::service {

using memory::Type;

LimitedContext::LimitedContext() = default;

icStringList LimitedContext::makeList(const memory::PackedItems & items) {
    icStringList value;

    for (auto & item : items) {
        if (item.type == Type::StringValue) {
            value.append(item.value.toString());
        }
        else /* item.type == Type::ListValue */ {
            value.append(item.value.toStringList());
        }
    }

    return value;
}

memory::Set LimitedContext::makeSet(const memory::PackedItems & items) {
    memory::Set value;

    value.header  = std::make_shared<memory::Columns>();
    value.setData = std::make_shared<memory::SetData>();

    if (items.isEmpty()) {
        return value;
    }

    bool   ok    = true;
    auto & first = items.first();
    auto & map   = to<memory::Object>(first.value).data->getMap();

    for (auto it = map.begin(); it != map.end(); it++) {
        value.header->append(
          {core()->factory->variantToType(it.value()), it.key()});
    }

    for (auto & item : items) {
        ok = ok && service::Set::checkObject(
                     core(), value, to<memory::Object>(item.value));
    }

    if (ok) {
        for (auto & item : items) {
            service::Set::insert(core(), value, to<memory::Object>(item.value));
        }
    }
    else {
        core()->vm->signal({il::Signals::IncompatibleData,
                           "All objects must have same fields"});
    }

    return value;
}

memory::Object LimitedContext::makeObject(const memory::PackedItems & items) {
    memory::Object value(core());

    value.data = std::make_shared<memory::DataContainer>(core());

    for (auto & item : items) {
        if (!value.data->contains(item.name)) {
            if (item.type != Type::VoidValue) {
                value.data->setValue(item.name, item.value);
            }
        }
        else {
            core()->vm->signal(
              {il::Signals::System, "Repeated icObject field"});
            return value;
        }
    }

    return value;
}

memory::Set LimitedContext::makeEmptySet(const memory::PackedItems & items) {
    memory::Set value;

    value.header  = std::make_shared<memory::Columns>();
    value.setData = std::make_shared<memory::SetData>();

    for (auto & item : items) {
        memory::Column column{item.type, item.name};

        if (!value.header->contains(column)) {
            if (item.type != Type::VoidValue) {
                value.header->append(column);
            }
        }
        else {
            core()->vm->signal(
              {il::Signals::System, "Repeated icSet column"});
            return value;
        }
    }

    return value;
}

bool LimitedContext::checkItems(
  const memory::PackedItems & items, int t1, int t2) {
    bool ret = !items.isEmpty();

    for (auto & item : items) {
        if (item.type != t1 && item.type != t2) {
            ret = false;
        }
    }

    return ret;
}

bool LimitedContext::checkItems(const memory::PackedItems & items, il::PackableType t1,
  il::PackableType t2) {
    bool ret = !items.isEmpty();

    for (auto & item : items) {
        if (item.itemType != t1 && item.itemType != t2) {
            ret = false;
        }
    }

    return ret;
}

}  // namespace icL::core::service
