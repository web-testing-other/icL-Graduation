#ifndef core_service_Any
#define core_service_Any

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service/keyword/main/finite-state-machine.h++>
#include <icL-service/main/values/inode.h++>



namespace icL::core::service {

class icL_core_service_keyword_EXPORT Any
    : public FiniteStateMachine
    , public INode
{
public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {     ///< FMS `for any <value> {<body>}`
        Initial,           ///< Initial state of FMS
        ValueCalculating,  ///< Getting the `value`
        BodyExecution,     ///< Execution of `body`
        End                ///< The FMS ends here
    } current = State::Initial;

    int : 32;  // padding

    /// value is the value of any expression
    icVariant value;

    /// value code is the code to run to get the value
    il::CodeFragment valueCode;
    /// body is the body of any expression
    il::CodeFragment body;
};

}  // namespace icL::core::service

#endif  // core_service_Any
