#ifndef core_service_IteratorFactory
#define core_service_IteratorFactory

#include <icL-il/export/global.h++>



class icVariant;

namespace icL::core {

namespace il {
struct InterLevel;
}

namespace service {

class Iterator;

/**
 * @brief The IteratorFactory class is a abstract factory for iterators
 */
class icL_core_service_keyword_EXPORT IteratorFactory
{
public:
    /**
     * @brief fromValue creates a iterator from a icVariant
     * @param value is the icVariant to create a iterator for
     * @return a pointer to created abstract iterator
     */
    static icL::core::service::Iterator * fromValue(
      il::InterLevel * il, const icVariant & value);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_IteratorFactory
