#include "set-value.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {


SetValue::SetValue() = default;

int SetValue::capacity() {
    return _value().setData->capacity();
}

bool SetValue::empty() {
    return _value().setData->isEmpty();
}

void SetValue::applicate(const icList<icStringList> & data) {
    service::Set::applicate(core(), _value(), data);
}

void SetValue::clear() {
    _value().setData->clear();
}

memory::Set SetValue::clone() {
    return service::Set::clone(_value());
}

icStringList SetValue::getField(const icString & name) {
    return service::Set::getField(core(), _value(), name);
}

bool SetValue::hasField(const icString & name) {
    return service::Set::getFieldIndex(_value(), name) >= 0;
}

void SetValue::insert(const icVariantList & data) {
    service::Set::insert(core(), _value(), data);
}

void SetValue::insert(const memory::Object & obj) {
    service::Set::insert(core(), _value(), obj);
}

void SetValue::insertField(
  const icString & name, const icStringList & values, Type type) {

    memory::Set newSet =
      service::Set::insertField(core(), _value(), name, values, type);

    if (newSet.header != nullptr) {
        reset(newSet);
    }
}

void SetValue::insertField(
  const icString & name, const icVariant & var, Type type) {

    memory::Set newSet =
      service::Set::insertField(core(), _value(), name, var, type);

    if (newSet.header != nullptr) {
        reset(newSet);
    }
}

void SetValue::remove(const icVariantList & data) {
    service::Set::remove(_value(), data);
}

void SetValue::remove(const memory::Object & obj) {
    service::Set::remove(core(), _value(), obj);
}

void SetValue::removeField(const icString & name) {
    memory::Set newSet = service::Set::removeField(core(), _value(), name);

    if (newSet.header != nullptr) {
        reset(newSet);
    }
}

memory::Set SetValue::_value() {
    return value();
}

}  // namespace icL::core::service
