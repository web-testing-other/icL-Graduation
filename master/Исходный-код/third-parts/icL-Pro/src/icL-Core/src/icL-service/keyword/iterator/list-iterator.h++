#ifndef core_service_ListIterator
#define core_service_ListIterator

#include "iterator.h++"

#include <icL-types/replaces/ic-string-list.h++>



namespace icL::core::service {

/**
 * @brief The ListIterator class release iterator for a icList value
 */
class icL_core_service_keyword_EXPORT ListIterator : public Iterator
{
public:
    /**
     * @brief ListIterator builds a icList iterator from a icList value
     * @param icList is the icList to iterate after
     */
    ListIterator(const icStringList & list);

private:
    /**
     * @brief init initializes the class icObject
     */
    void init();

    // Iterator interface
public:
    void toFirst() override;
    void toLast() override;
    void toNext() override;
    void toPrev() override;
    bool atBegin() override;
    bool atEnd() override;
    bool atStop() override;
    void setStopToCurrent() override;
    void setEndToCurrent() override;

    icVariant getCurrent() override;
    int       getSize() override;

private:
    /// icList is the icList to iterate
    icStringList list;
    /// current is the icList active iterator
    icStringList::Iterator current;
    /// stop is the icList stop iterator
    icStringList::Iterator stop;
    /// end is the programmable end of collection
    icStringList::Iterator end;
};

}  // namespace icL::core::service

#endif  // core_service_ListIterator
