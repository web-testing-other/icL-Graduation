#include "root.h++"

#include <icL-types/replaces/ic-math.h++>



namespace icL::core::service {

Root::Root() = default;

int Root::voidInt(int right) {
    return int(icSqrt(right));
}

double Root::voidDouble(double right) {
    return icSqrt(right);
}

int Root::intInt(int left, int right) {
    return int(icPow(right, 1.0 / double(left)));
}

double Root::intDouble(int left, double right) {
    return icPow(right, 1.0 / double(left));
}

double Root::doubleDouble(double left, double right) {
    return icPow(right, 1.0 / left);
}

}  // namespace icL::core::service
