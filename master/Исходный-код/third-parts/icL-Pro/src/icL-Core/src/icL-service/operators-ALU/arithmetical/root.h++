#ifndef core_service_Root
#define core_service_Root

#include <icL-il/export/global.h++>



namespace icL::core::service {

class icL_core_service_operators_ALU_EXPORT Root
{
public:
    Root();

    // level 1

    /// `/' int : int`
    int voidInt(int right);

    /// `/' double : double`
    double voidDouble(double right);

    /// `int /' int : int`
    int intInt(int left, int right);

    /// `int /' double : double`
    double intDouble(int left, double right);

    /// `double /' double : double`
    double doubleDouble(double left, double right);
};

}  // namespace icL::core::service

#endif  // core_service_Root
