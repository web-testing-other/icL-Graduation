#include "smaller.h++"



namespace icL::core::service {

bool Smaller::intInt(int left, int right) {
    return left < right;
}

bool Smaller::doubleDouble(double left, double right) {
    return left < right;
}

}  // namespace icL::core::service
