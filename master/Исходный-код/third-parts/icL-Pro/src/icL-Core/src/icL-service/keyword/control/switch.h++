#ifndef core_service_Switch
#define core_service_Switch

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service/keyword/main/finite-state-machine.h++>
#include <icL-service/main/values/inode.h++>
#include <icL-service/operators-advanced/compare/equality.h++>

#include <icL-memory/structures/argument.h++>


namespace icL::core::service {

/**
 * @brief The Switch class represent a `switch` token
 */
class icL_core_service_keyword_EXPORT Switch
    : public FiniteStateMachine
    , public Equality
    , virtual public INode
{
    // FiniteStateMachine interface
public:
    StepType transact() override;

private:
    /**
     * @brief comapre comapare 2 values
     * @param sw is the switch argument
     * @param cs is the case argument
     * @return true if case argument is void, or case is bool and is true, or
     * case arg == switch arg
     */
    bool compare(const memory::Argument & sw, const memory::Argument & cs);

    /**
     * @brief switchChecking releases the switch value check
     */
    void switchChecking();

    /**
     * @brief caseChecking releases the case value check
     */
    void caseChecking();

protected:
    enum class State {   ///< states of switch case FSM
        Initial,         ///< initial state of FSM
        SwitchChecking,  ///< checking the switch case
        CaseChecking,    ///< state checking conditio of a check
        CaseRunning,     ///< state running a case body
        End              ///< the FSM ends here
    } current = State::Initial;


    bool somethingExecuted = false;  ///< a case was executed
    bool lastExecuted      = false;  ///< the previous case was executed

    int : 16;  // padding

    struct CaseData
    {                             ///< all necessary data to describe a case
        il::CodeFragment values;  ///< code of values icList
        il::CodeFragment body;    ///< body of case
    };

    il::CodeFragment           switchCode;  ///< code of switch value
    icList<CaseData>           cases;       ///< icList of cascades cases
    icList<CaseData>::Iterator it;  ///< iterator to current `case` token

    memory::ArgList switchValue;  ///< value of switch
    icVariant       returnValue;  ///< value to return
};

}  // namespace icL::core::service

#endif  // core_service_Switch
