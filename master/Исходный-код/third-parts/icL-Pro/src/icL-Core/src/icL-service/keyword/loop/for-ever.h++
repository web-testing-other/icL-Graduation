#ifndef core_service_ForEver
#define core_service_ForEver

#include "for.h++"



namespace icL::core::service {

class icL_core_service_keyword_EXPORT ForEver : public For
{
public:
    ForEver(int xTimes);

    // Loop interface
public:
    void break_() override;
    void continue_() override;

    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {      ///< FMS `for:ever (<condition>) {<code>}`
        Initial,            ///< Initial state of any `for`
        ConditionChecking,  ///< Checking the condition of loop
        CodeExecution,      ///< Executing the body loop
        End                 ///< The FMS ends here
    } current = State::Initial;

    /// xTimes is the value of `:Xtimes` modifier
    int xTimes = -1;
    /// condition is used to stop `for:ever` loop
    il::CodeFragment condition;
};

}  // namespace icL::core::service

#endif  // core_service_ForEver
