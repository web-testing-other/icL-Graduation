#include "void-cast.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

bool VoidCast::toBool() {
    return false;
}

int VoidCast::toInt() {
    return 0;
}

double VoidCast::toDouble() {
    return 0.0;
}

icString VoidCast::toString() {
    return "";
}

icStringList VoidCast::toList() {
    return {};
}

memory::Object VoidCast::toObject(il::InterLevel * il) {
    memory::Object ret(il);

    ret.data = std::make_shared<memory::DataContainer>(il);
    return ret;
}

memory::Set VoidCast::toSet() {
    memory::Set ret;

    ret.header  = std::make_shared<memory::Columns>();
    ret.setData = std::make_shared<memory::SetData>();
    return ret;
}

icVariant VoidCast::fromType(il::InterLevel * il, short type) {
    using memory::Type;

    switch (type) {
    case Type::BoolValue:
        return false;

    case Type::DatetimeValue:
        return icDateTime::currentDateTime();

    case Type::DoubleValue:
        return 0.0;

    case Type::IntValue:
        return 0;

    case Type::ListValue:
        return icStringList{};

    case Type::ObjectValue:
        return toObject(il);

    case Type::RegexValue:
        return icRegEx{};

    case Type::SetValue:
        return toSet();

    case Type::StringValue:
        return toString();

    case Type::VoidValue:
        return icVariant::makeVoid();

    default:
        return {};
    }
}


}  // namespace icL::core::service
