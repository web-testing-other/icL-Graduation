#ifndef core_service_Math
#define core_service_Math

#include <icL-il/export/global.h++>



template <typename>
class icList;

namespace icL::core::service {

class icL_core_service_value_system_EXPORT Math
{
public:
    Math();

    // properties level 1

    /// `[r/o] Math'1divPi : double `
    double _1divPi();

    /// `[r/o] Math'1divSqrt2 : double`
    double _1divSqrt2();

    /// `[r/o] Math'2divPi : double`
    double _2divPi();

    /// `[r/o] Math'2divSqrtPi : double`
    double _2divSqrtPi();

    /// `[r/o] Math'e : double`
    double e();

    /// `[r/o] Math'ln2 : double`
    double ln2();

    /// `[r/o] Math'ln10 : double`
    double ln10();

    /// `[r/o] Math'log2e : double`
    double log2e();

    /// `[r/o] Math'log10e : double`
    double log10e();

    /// `[r/o] Math'pi : double`
    double pi();

    /// `[r/o] Math'piDiv2 : double`
    double piDiv2();

    /// `[r/o] Math'piDiv4 : double`
    double piDiv4();

    /// `[r/o] Math'sqrt2 : double`
    double sqrt2();

    // methods level 1

    /// `Math.acos (v : double) : double`
    double acos(double v);

    /// `Math.asin (v : double) : double`
    double asin(double v);

    /// `Math.atan (v : double) : double`
    double atan(double v);

    /// `Math.ceil (v : double) : int`
    int ceil(double v);

    /// `Math.cos (v : double) : double`
    double cos(double v);

    /// `Math.degreesToRadians (v : double) : double`
    double degreesToRadians(double v);

    /// `Math.exp (v : double) : double`
    double exp(double v);

    /// `Math.floor (v : double) : int`
    int floor(double v);

    /// `Math.ln (v : double) : double`
    double ln(double v);

    /// `Math.max (arr : int...) : int`
    int max(icList<int> arr);

    /// `Math.max (arr : double...) : double`
    double max(icList<double> arr);

    /// `Math.min (arr : int...) : int`
    int min(icList<int> arr);

    /// `Math.min (arr : double...) : double`
    double min(icList<double> arr);

    /// `Math.radiansToDegrees (v : double) : double`
    double radiansToDegrees(double v);

    /// `Math.round (v : double) : int`
    int round(double v);

    /// `Math.sin (v : double) : double`
    double sin(double v);

    /// `Math.tan (v : double) : double`
    double tan(double v);
};

}  // namespace icL::core::service

#endif  // core_service_Math
