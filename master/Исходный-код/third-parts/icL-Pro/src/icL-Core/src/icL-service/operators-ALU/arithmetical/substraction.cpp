#include "substraction.h++"

#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

Subtraction::Subtraction() = default;

int Subtraction::voidInt(int right) {
    return -right;
}

double Subtraction::voidDouble(double right) {
    return -right;
}

int Subtraction::intInt(int left, int right) {
    return left - right;
}

double Subtraction::doubleDouble(double left, double right) {
    return left - right;
}

memory::Set Subtraction::setSet(
  const memory::Set & left, const memory::Set & right) {
    return service::Set::symmetricDifference(core(), left, right);
}

}  // namespace icL::core::service
