#ifndef core_service_Remainder
#define core_service_Remainder

#include <icL-service/main/values/inode.h++>



namespace icL::core {

namespace memory {
struct Set;
}

namespace service {

class icL_core_service_operators_ALU_EXPORT Remainder : virtual public INode
{
public:
    Remainder();

    // level 1

    /// `int \ int : int`
    int intInt(int left, int right);

    /// `set \ set : set`
    memory::Set setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Remainder
