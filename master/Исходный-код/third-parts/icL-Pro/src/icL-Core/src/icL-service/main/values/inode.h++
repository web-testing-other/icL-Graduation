#ifndef core_service_INode
#define core_service_INode

#include <icL-il/export/global.h++>



namespace icL::core {

namespace il {
struct InterLevel;
}

namespace service {

/**
 * @brief The INode class represent an interface for a node
 *
 * This interface is special for classes which will become nodes later and the
 * inherance of Node twince will be a serious problem
 */
class icL_core_service_main_EXPORT INode
{
public:
    virtual ~INode() = default;

public:
    virtual il::InterLevel * core() = 0;
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_INode
