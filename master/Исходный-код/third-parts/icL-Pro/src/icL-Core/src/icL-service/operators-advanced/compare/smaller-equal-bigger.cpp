#include "smaller-equal-bigger.h++"



namespace icL::core::service {

bool SmallerEqualBigger::intIntInt(int left, int begin, int end) {
    return left <= begin || left >= end;
}

bool SmallerEqualBigger::doubleDoubleDouble(
  double left, double begin, double end) {
    return left <= begin || left >= end;
}

}  // namespace icL::core::service
