#ifndef core_service_BoolCast
#define core_service_BoolCast

#include <icL-il/export/global.h++>



class icString;

namespace icL::core::service {

class icL_core_service_cast_EXPORT BoolCast
{
public:
    /// `bool : int`
    static int toInt(bool value);

    /// `bool : double`
    static double toDouble(bool value);

    /// `bool : string`
    static icString toString(bool value);
};

}  // namespace icL::core::service

#endif  // core_service_BoolCast
