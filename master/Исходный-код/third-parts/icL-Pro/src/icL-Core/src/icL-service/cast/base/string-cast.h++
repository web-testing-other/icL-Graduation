#ifndef core_service_StringCast
#define core_service_StringCast

#include <icL-types/json/js-object.h++>

#include <icL-il/export/global.h++>


class icString;
class icStringList;

namespace icL::core {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_cast_EXPORT StringCast
{
public:
    /// `string : bool`
    static bool toBool(const icString & value);

    /// `string : int`
    static int toInt(il::InterLevel * il, const icString & value);

    /// `string : double`
    static double toDouble(il::InterLevel * il, const icString & value);

    /// `string : list`
    static icStringList toList(const icString & value);

    /**
     * @brief toObject creates a icObject from a JSON-icObject
     * @param obj is the icObject to cast to icL icObject
     * @return a icL icObject with the same field as JSON-icObject
     */
    static memory::Object toObject(il::InterLevel * il, jsObject obj);

    /// `string : object`
    static memory::Object toObject(il::InterLevel * il, const icString & value);

    /// `string : set`
    static memory::Set toSet(il::InterLevel * il, const icString & value);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_StringCast
