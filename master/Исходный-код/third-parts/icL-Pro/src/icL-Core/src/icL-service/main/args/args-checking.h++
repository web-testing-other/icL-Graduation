#ifndef core_service_ArgsChecking
#define core_service_ArgsChecking

#include <icL-il/export/global.h++>

#include <icL-memory/structures/type.h++>



template <typename>
class icList;

namespace icL::core {

namespace memory {
struct Argument;
using ArgList = icList<Argument>;
}  // namespace memory

namespace service {

class icL_core_service_main_EXPORT ArgsChecking
{
public:
    /**
     * @brief checkArg check the compability of arg with type
     * @param arg is the argument to check
     * @param type is the type to match
     * @return true if arg martches the type, otherwise false
     */
    static bool checkArg(const memory::Argument & arg, short type);

    /**
     * @brief checksArg checks the compability of args with standard
     * @param args are the arguments of method call
     * @param standard is the wanted arguments icList
     * @return true if compatible, otherwise false
     */
    static bool checkArgs(
      const memory::ArgList & args, const icList<int> & standard);

    /**
     * @brief checksArg checks the compability of each arg with type
     * @param args are the arguments of method call
     * @param type is the wanted arguments type
     * @return true if compatible, otherwise false
     */
    static bool checkArgs(short type, const memory::ArgList & args);

    /**
     * @brief checkArgs check if all aguments are valid
     * @param args are the arguments of method call
     * @return true if valid, otherwise false
     */
    static bool checkArgs(const memory::ArgList & args);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_ArgsChecking
