#ifndef core_service_Datetime
#define core_service_Datetime

#include <icL-il/export/global.h++>



class icDateTime;

namespace icL::core::service {

class icL_core_service_value_system_EXPORT DateTime
{
public:
    DateTime();

    // methods level 1

    /// `icDateTime.current () : icDateTime`
    icDateTime current();

    /// `icDateTime.currentUTC () : icDateTime`
    icDateTime currentUTC();
};

}  // namespace icL::core::service

#endif  // core_service_Datetime
