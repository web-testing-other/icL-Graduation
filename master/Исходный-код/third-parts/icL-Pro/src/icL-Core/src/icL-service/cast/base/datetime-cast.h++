#ifndef core_service_DatetimeCast
#define core_service_DatetimeCast

#include <icL-il/export/global.h++>



class icDateTime;
class icString;

namespace icL::core::service {

class icL_core_service_cast_EXPORT DatetimeCast
{
public:
    /// `icDateTime : string`
    static icString toString(const icDateTime & value);

    /// `icDateTime : (string, format)`
    static icString toString(const icDateTime & value, const icString & format);

    /// `string : icDateTime`
    static icDateTime toDatetime(const icString & value);

    /// `string : (icDateTime, format)`
    static icDateTime toDatetime(
      const icString & value, const icString & format);
};

}  // namespace icL::core::service

#endif  // core_service_DatetimeCast
