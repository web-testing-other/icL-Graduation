#ifndef core_service_Emitter
#define core_service_Emitter

#include "../main/finite-state-machine.h++"
#include "slot.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/structures/code-fragment.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/values/inode.h++>



namespace icL::core::service {

class icL_core_service_keyword_EXPORT Emitter
    : public FiniteStateMachine
    , virtual public INode
{
public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

    // Own data
protected:
    enum class State {  ///< Finite state machine for `emitter`
        Initial,        ///< Initial state of FMS
        Emitting,       ///< Executing code and give the emitted signal
        Catching,       ///< Finding a compatible slot
        End             ///< The FMS ends here
    } current = State::Initial;

    int : 24;  // padding

    /// \brief alive is the alive odifier state
    bool alive = false;

    /// \brief errorCode is chatched error code
    il::Signal error = {0, ""};

    /// \brief emmiter is the code of emitter
    il::CodeFragment emmiter;

    /// \brief slotsPtr contains pointer to slots
    icList<Slot *> slotsPtr;

    /// \brief slotsCode contians codes of slots
    icList<il::CodeFragment> slotsCode;
};

}  // namespace icL::core::service

#endif  // core_service_Emitter
