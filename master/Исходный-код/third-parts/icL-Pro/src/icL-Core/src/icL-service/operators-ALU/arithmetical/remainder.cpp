#include "remainder.h++"

#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

Remainder::Remainder() = default;

int Remainder::intInt(int left, int right) {
    return left % right;
}

memory::Set Remainder::setSet(
  const memory::Set & left, const memory::Set & right) {
    return service::Set::complement(core(), left, right);
}

}  // namespace icL::core::service
