#ifndef core_service_Signal
#define core_service_Signal

#include <icL-service/main/values/inode.h++>



class icString;

namespace icL::core {

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_value_system_EXPORT Signal : virtual public INode
{
public:
    Signal();

    // properties level 1

    /// `Signal'(name : string) : int`
    int property(const icString & name);

    // methods level 1

    /// `Signal.add (name : string) : void`
    void add(const icString & name);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Signal
