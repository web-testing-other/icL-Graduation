#include "for-collection.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/keyword/iterator/iterator-factory.h++>
#include <icL-service/keyword/iterator/iterator.h++>
#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::core::service {

using il::Signals::Signals;

ForCollection::ForCollection(bool reverse, int maxX)
    : reverse(reverse) {
    this->maxX = maxX;
}

void ForCollection::break_() {
    current = State::End;
}

void ForCollection::continue_() {
    reverse ? it->toPrev() : it->toNext();
}

StepType ForCollection::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        core()->vms->pushKeepAliveLayer(il::LayerType::Loop);
        current = State::ValueExtraction;
        break;

    case State::ValueExtraction: {
        memory::FunctionCall fcall;

        fcall.code        = collectionCode;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "for");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else {
                it = IteratorFactory::fromValue(core(), ret.consoleValue);

                if (it == nullptr) {
                    core()->vm->signal(
                      {Signals::System, "For loop iterates just collections"});
                }
                else {
                    current = State::CodeExecution;

                    if (reverse) {
                        it->toLast();
                    }
                }
            }
            return false;
        });

        retValue = executed;
        ret      = StepType::CommandIn;
        break;
    }
    case State::CodeExecution: {
        memory::FunctionCall fcall;

        if (it->atEnd() || (maxX >= 1 && executed >= maxX)) {
            current = State::End;
            break;
        }

        fcall.code        = body;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "for");
        fcall.args.append({"@", it->getCurrent()});

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else {
                if (reverse && it->atBegin()) {
                    current = State::End;
                }
                reverse ? it->toPrev() : it->toNext();
            }
            return false;
        });

        retValue = ++executed;
        ret      = StepType::CommandIn;
        break;
    }
    case State::End:
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

}  // namespace icL::core::service
