#ifndef core_service_Smaller
#define core_service_Smaller

#include <icL-il/export/global.h++>



namespace icL::core::service {

class icL_core_service_operators_advanced_EXPORT Smaller
{
public:
    /// `int < int : bool`
    bool intInt(int left, int right);

    /// `double < double : bool`
    bool doubleDouble(double left, double right);
};

}  // namespace icL::core::service

#endif  // core_service_Smaller
