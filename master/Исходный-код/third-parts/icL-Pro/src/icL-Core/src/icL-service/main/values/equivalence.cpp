#include "equivalence.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

namespace icL::core::service {

void Equivalence::pair(Equivalence::Key & key1, Equivalence::Key & key2) {
    if (!key1.paired && !key2.paired && key1.word == key2.word) {
        key1.paired = key2.paired = true;
        key1.top.insert(key1.word.length(), &key2);

        for (auto it = key2.top.begin(); it != key2.top.end(); ++it) {
            it.value()->top.remove(it.key(), &key2);
        }

        key2.top.clear();
        key2.top.insert(key2.word.length(), &key1);
    }
}

Equivalence::FindResult Equivalence::find(
  const icString & str1, int index1, const icString & str2, int index2) {
    int index  = index1;
    int length = 0;
    int i      = index;

    while (i < str1.length()) {
        if (str1[i] == str2[index2]) {
            int j = 0;
            while (j + i < str1.length() && index2 + j < str2.length() &&
                   str1[i + j] == str2[index2 + j]) {
                j++;
            }

            if (j > length) {
                length = j;
                index  = i;
            }
        }

        i++;
    }

    return {index, length};
}

int Equivalence::calcMark(const icString & str1, const icString & str2) {
    int i1   = 0;
    int i2   = 0;
    int mark = 0;

    while (i1 < str1.length() && i2 < str2.length()) {
        auto result1 = find(str1, i1, str2, i2);
        auto result2 = find(str2, i2, str1, i1);

        if (result1.legnth == 0 && result2.legnth == 0) {
            break;
        }

        if (result1.legnth > result2.legnth) {
            mark += result1.legnth;
            i1 = result1.index + result1.legnth;
            i2 += result1.legnth;
        }
        else {
            mark += result2.legnth;
            i2 = result2.index + result2.legnth;
            i1 += result2.legnth;
        }
    }

    return mark;
}

void Equivalence::join(Equivalence::Key & key1, Equivalence::Key & key2) {
    int mark = calcMark(key1.word, key2.word);

    if (mark > 0) {
        key1.top.insert(mark, &key2);
        key2.top.insert(mark, &key1);
    }
}

void Equivalence::normalize(icList<Equivalence::Key> & keys) {
    int max;

    do {
        max = 0;

        for (Key & key : keys) {
            int nmax = key.top.isEmpty() ? 0 : key.top.lastKey();

            if (nmax > max && !key.paired) {
                max = nmax;
            }
        }

        if (max == 0) {
            break;
        }

        for (Key & key : keys) {
            int commom = key.top.isEmpty() ? 0 : key.top.lastKey();

            if (commom == max && !key.paired) {
                Key * last = key.top.last();

                for (auto it = key.top.begin(); it != key.top.end();) {
                    if (it.value() != last) {
                        it.value()->top.remove(it.key(), &key);
                        it = key.top.erase(it);
                    }
                    else {
                        ++it;
                    }
                }

                last->paired = true;
                key.paired   = true;
            }
        }
    } while (max > 0);
}

void Equivalence::mesh(
  icList<Equivalence::Key> & keys1, icList<Equivalence::Key> & keys2) {
    for (Key & key1 : keys1) {
        for (Key & key2 : keys2) {
            pair(key1, key2);
        }

        if (key1.paired) {
            continue;
        }

        for (Key & key2 : keys2) {
            if (!key2.paired) {
                join(key1, key2);
            }
        }
    }

    normalize(keys2);
}

double Equivalence::calculate(
  const icStringList & l1, const icStringList & l2) {
    icList<Key> k1;
    icList<Key> k2;

    for (const auto & str : l1) {
        k1.append(Key{str});
    }

    for (const auto & str : l2) {
        k2.append(Key{str});
    }

    mesh(k1, k2);

    int catched1 = 0;
    int sum1     = 0;
    int catched2 = 0;
    int sum2     = 0;

    for (Key & key : k1) {
        if (!key.top.isEmpty()) {
            catched1 += key.top.lastKey();
        }
        sum1 += key.word.length();
    }

    for (Key & key : k2) {
        if (!key.top.isEmpty()) {
            catched2 += key.top.lastKey();
        }
        sum2 += key.word.length();
    }

    return (double(catched1) / double(sum1) + double(catched2) / double(sum2)) /
           2;
}

double Equivalence::calculate(const icString & str1, const icString & str2) {
    static icRegEx icRegEx{"[^\\w\\d]+", icRegEx::UseUnicodePropertiesOption};

    icString s1 = str1.toLower();
    icString s2 = str2.toLower();

    icStringList l1 = s1.split(icRegEx, true);
    icStringList l2 = s2.split(icRegEx, true);

    return calculate(l1, l2);
}


}  // namespace icL::core::service
