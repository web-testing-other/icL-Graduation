#ifndef core_service_SetCast
#define core_service_SetCast

#include <icL-il/export/global.h++>



class icString;
class icStringList;

namespace icL::core {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_cast_EXPORT SetCast
{
public:
    /// `set : string`
    static icString toString(il::InterLevel *il, const memory::Set & value);

    /// `set : object`
    static memory::Object toObject(
      il::InterLevel * il, const memory::Set & value);

    /// `set : list`
    static icStringList toList(il::InterLevel *il, const memory::Set & value);

    /// `set : bool`
    static bool toBool(const memory::Set & value);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_SetCast
