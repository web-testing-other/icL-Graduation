#include "stack.h++"

#include <icL-memory/state/stackcontainer.h++>

namespace icL::core::service {

Stack::Stack(memory::StackContainer * stack)
    : stack(stack) {}

void Stack::addDescription(const icString & description) {
    stack->addDescription(description);
}

void Stack::break_() {
    stack->break_();
}

void Stack::clear() {
    stack->clear();
}

void Stack::continue_() {
    stack->continue_();
}

void Stack::destroy() {
    stack->destroy();
}

void Stack::ignore() {
    stack->ignore();
}

void Stack::listen() {
    stack->listen();
}

void Stack::markStep(const icString & name) {
    stack->markStep(name);
}

void Stack::markTest(const icString & name) {
    stack->markTest(name);
}

void Stack::return_(const icVariant & value) {
    stack->return_(value);
}

}  // namespace icL::core::service
