#include "assert.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>


namespace icL::core::service {

StepType icL::core::service::Assert::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        core()->vms->pushKeepAliveLayer(il::LayerType::Control);
        current = State::ConditionChecking;
        break;

    case State::ConditionChecking: {
        memory::FunctionCall fcall;

        fcall.code        = condition;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "assert");

        core()->vms->interrupt(fcall, [this](const il::Return result) {
            if (result.signal.code != il::Signals::NoError) {
                core()->vm->signal(result.signal);
            }
            else if (result.consoleValue.isBool()) {
                current = result.consoleValue.toBool() != notMod
                            ? State::End
                            : State::ValueExtracting;
            }
            else {
                core()->vm->syssig("assert: Condition must return a bool value");
            }
            return false;
        });

        ret = StepType::CommandIn;
        break;
    }
    case State::ValueExtracting:
        if (message.source == nullptr) {
            core()->vm->sendAssert(
              (notMod ? "(false expected) " : "(true expected) ") +
              condition.getCode().replace("\n", " "));
            current = State::End;
        }
        else {
            memory::FunctionCall fcall;

            fcall.code        = message;
            fcall.createLayer = false;
            fcall.contextName =
              Stringify::alternative(fcall.code.name, "assert");

            core()->vms->interrupt(fcall, [this](const il::Return & result) {
                if (result.signal.code != il::Signals::NoError) {
                    core()->vm->signal(result.signal);
                }
                else if (result.consoleValue.isString()) {
                    core()->vm->sendAssert(result.consoleValue.toString());
                    current = State::End;
                }
                else {
                    core()->vm->syssig(
                      "assert: message code must return a string value");
                }
                return false;
            });

            ret = StepType::CommandIn;
        }
        break;

    case State::End:
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
    }

    return ret;
}

}  // namespace icL::core::service
