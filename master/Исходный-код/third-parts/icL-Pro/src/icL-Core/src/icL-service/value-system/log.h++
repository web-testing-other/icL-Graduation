#ifndef core_service_Log
#define core_service_Log

#include <icL-service/main/values/inode.h++>



class icString;
template <typename>
class icList;

namespace icL::core {

namespace memory {
struct Argument;
using ArgList = icList<Argument>;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_value_system_EXPORT Log : virtual public INode
{
public:
    Log();

    // methods level 1

    /// `Log.error (message : string) : void`
    void error(const icString & message);

    /// `Log.info (message : string) : void`
    void info(const icString & message);

    /// `Log.out (args : any ...) : void`
    void out(const memory::ArgList & args);

    /// `Log.stack (var : any) : void`
    void stack(const memory::Argument & arg);

    /// `Log.state (var : any) : void`
    void state(const memory::Argument & arg);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Log
