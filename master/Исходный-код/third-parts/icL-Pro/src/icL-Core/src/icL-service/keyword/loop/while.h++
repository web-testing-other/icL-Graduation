#ifndef core_service_While
#define core_service_While

#include "../main/finite-state-machine.h++"
#include "loop.h++"

#include <icL-il/structures/code-fragment.h++>

#include <icL-service/main/values/inode.h++>



namespace icL::core::service {

class icL_core_service_keyword_EXPORT While
    : public FiniteStateMachine
    , public Loop
    , public INode
{
    // Loop interface
public:
    void break_() override;
    void continue_() override;

public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {       ///< Finite state machine for `while`
        Initial,             ///< Initial state of FSM
        CondtitionChecking,  ///< Checking the confition of loop
        CodeExecution,       ///< Execution of loop body
        End                  ///< The FMS ends here
    } current = State::Initial;

    int : 32;  // padding

    il::CodeFragment condition;  ///< `while (<here>) {..}`
    il::CodeFragment loopBody;   ///< `while (..) {<here>}`

    int  executed = 0;      ///< currently number of executions
    int  min      = 0;      ///< `:minX`
    int  max      = -1;     ///< `:maxX`
    bool doWhile  = false;  ///< `while` vs `do-while`
    bool notMod   = false;  ///< `:not`

    int : 16;  // padding
};

}  // namespace icL::core::service

#endif  // core_service_While
