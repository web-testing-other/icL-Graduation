#include "contains.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-service/main/values/set.h++>



namespace icL::core::service {

bool Contains::listString(const icStringList & left, const icString & right) {
    return left.contains(right);
}

bool Contains::stringString(const icString & left, const icString & right) {
    return left.contains(right);
}

bool Contains::setObject(
  const memory::Set & left, const memory::Object & right) {
    return service::Set::contains(core(), left, right);
}

bool Contains::setSet(const memory::Set & left, const memory::Set & right) {
    return service::Set::contains(core(), left, right);
}

bool Contains::stringRegex(const icString & left, const icRegEx & right) {
    auto match = right.match(left);
    return match.hasMatch();
}

}  // namespace icL::core::service
