#ifndef core_service_IfExists
#define core_service_IfExists

#include "../main/finite-state-machine.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service/main/values/inode.h++>



namespace icL::core::service {

class icL_core_service_keyword_EXPORT IfExists
    : public FiniteStateMachine
    , virtual public INode
{
public:
    IfExists(bool notMod);

    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    /**
     * @brief release runs the code and switch to end state
     * @param code is the code to run
     */
    void release(const il::CodeFragment & code);

protected:
    enum class State {   ///< State of if-exists FMS
        Initial,         ///< Initial state
        ExistsChecking,  ///< Executing exists block
        CodeRunning,     ///< Run the current if
        LastResort,      ///< Run the last end
        End              ///< The FMS ends here
    } current = State::Initial;

    bool notModifier = false;  ///< `if!` or `if:not`

    int : 24;  // padding

    struct IfData  ///< describes a `if exists() {}` sequence
    {
        FiniteStateMachine * exists;
        il::CodeFragment     body;

        bool notModifer{};  ///< `if!` or `if:not`
    };

    icList<IfData>           ifs;  ///< list of cascade `if`s
    icList<IfData>::Iterator it;   ///< iterator to current `if` token

    il::CodeFragment elseCode;  ///< is the code of `else {}` token

    icVariant value;  ///< value to send to run block
};

}  // namespace icL::core::service

#endif  // core_service_IfExists
