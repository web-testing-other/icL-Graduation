#include "args-checking.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::core::service {


bool ArgsChecking::checkArg(const memory::Argument & arg, const short type) {
    bool ret;

    using memory::Type;

    switch (type) {
    case Type::VoidValue:
        ret = arg.type == type || arg.value.isNull();
        break;

    case Type::TypeId:
        ret = arg.value.isNull();
        break;

    case Type::AnyValue:
        ret = !arg.value.isNull();
        break;

    case Type::Identifier:
        ret = arg.type == Type::Identifier;
        break;

    default:
        ret = arg.type == type && !arg.value.isNull();
    }

    return ret;
}

bool ArgsChecking::checkArgs(const memory::ArgList & args, const icList<int> &standard) {
    if (args.length() != standard.length()) {
        return false;
    }

    bool ret = true;

    for (int i = 0; i < standard.length(); i++) {
        ret = ret && checkArg(args[i], standard[i]);
    }

    return ret;
}

bool ArgsChecking::checkArgs(const short type, const memory::ArgList & args) {
    bool ret = !args.isEmpty();

    for (auto & arg : args) {
        ret = ret && checkArg(arg, type);
    }

    return ret;
}

bool ArgsChecking::checkArgs(const memory::ArgList & args) {
    bool ret = true;

    for (const auto & arg : args) {
        ret = ret && !arg.value.isNull();
    }

    return ret;
}

}  // namespace icL::core::service
