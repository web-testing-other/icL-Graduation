#include "contains-template.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-service/main/values/set.h++>

namespace icL::core::service {

bool ContainsTemplate::listString(
  const icStringList & left, const icString & right) {
    bool ret = false;

    for (auto & str : left) {
        ret = ret || str.contains(right);
    }

    return ret;
}

bool ContainsTemplate::setObject(
  const memory::Set & left, const memory::Object & right) {
    return Set::containsTemplate(core(), left, right);
}

bool ContainsTemplate::objectObject(
  const memory::Object & left, const memory::Object & right) {
    return Set::containsTemplate(core(), left, right);
}

icStringList ContainsTemplate::stringRegex(
  const icString & left, const icRegEx & right) {
    auto match = right.match(left);
    return match.capturedTexts();
}


}  // namespace icL::core::service
