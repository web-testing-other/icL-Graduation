#ifndef core_service_SmallerBigger
#define core_service_SmallerBigger

#include <icL-il/export/global.h++>



namespace icL::core::service {

class icL_core_service_operators_advanced_EXPORT SmallerBigger
{
public:
    /// `int <> (int, int) : bool`
    bool intIntInt(int left, int begin, int end);

    /// `double <> (double, double) : bool`
    bool doubleDoubleDouble(double left, double begin, double end);
};

}  // namespace icL::core::service

#endif  // core_service_SmallerBigger
