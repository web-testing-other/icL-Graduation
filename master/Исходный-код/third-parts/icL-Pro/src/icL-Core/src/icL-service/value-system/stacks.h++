#ifndef core_service_Stacks
#define core_service_Stacks

#include <icL-service/main/values/inode.h++>



class icString;

namespace icL::core {

namespace memory {
class StackContainer;
}

namespace il {
struct InterLevel;
}

namespace service {

class icL_core_service_value_system_EXPORT Stacks : virtual public INode
{
public:
    Stacks();

    // properties level 1

    /// `[r/o] Stacks (name : string) : Stack`
    memory::StackContainer * property(const icString & name);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Stacks
