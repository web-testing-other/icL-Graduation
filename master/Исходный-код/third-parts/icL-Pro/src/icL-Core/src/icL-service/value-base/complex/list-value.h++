#ifndef core_service_ListValue
#define core_service_ListValue

#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode.h++>



class icString;
class icStringList;
class icRegEx;

namespace icL::core::service {

class icL_core_service_value_base_EXPORT ListValue
    : virtual public il::WriteableValue
    , virtual public INode
{
public:
    ListValue();
    ~ListValue() override = default;

    // properties level 1

    /// `[r/o] list'empty : bool`
    bool empty();

    /// `[r/o] list'length : int`
    int length();

    /// `[r/o] list'last : string`
    icString last();

    // methods level 1

    /// `list.append (str : string) : list`
    void append(const icString & str);

    /// `list.at (i : int) : string`
    icString at(int i);

    /// `list.contains (str : string, caseSensitive = true) : bool`
    bool contains(const icString & str, bool caseSensitive = true);

    /// `list.clear () : list`
    void clear();

    /// `list.count (what : string) : int`
    int count(const icString & what);

    /// `list.filter (str : string, caseSensitive = true) : bool`
    icStringList filter(const icString & str, bool caseSensitive = true);

    /// ``icList.filter (re : icRegEx) : icList
    icStringList filter(const icRegEx & re);

    /// `list.indexOf (str : string, start = 0) : int`
    int indexOf(const icString & str, int start = 0);

    /// `list.indexOf (re : regex, start = 0) : int`
    int indexOf(const icRegEx & re, int start = 0);

    /// `list.insert (index : int, str : string) : list`
    void insert(int index, const icString & str);

    /// `list.join (separator : string) : string`
    icString join(const icString & separator);

    /// `list.lastIndexOf (str : string, start = -1) : int`
    int lastIndexOf(const icString & str, int start = -1);

    /// `list.lastIndexOf (re : regex, start = -1) : int`
    int lastIndexOf(const icRegEx & re, int start = -1);

    /// `list.mid (pos : int, n = -1) : list`
    icStringList mid(int pos, int n = -1);

    /// `list.prepend (str : string) : list`
    void prepend(const icString & str);

    /// `list.move (from : int, to : int) : list`
    void move(int from, int to);

    /// `list.removeAll (str : string) : list`
    void removeAll(const icString & str);

    /// `list.removeAt (i : int) : list`
    void removeAt(int i);

    /// `list.removeDuplicates () : list`
    void removeDuplicates();

    /// `list.removeFirst () : list`
    void removeFirst();

    /// `list.removeLast () : list`
    void removeLast();

    /// `list.removeOne (str : string) : bool`
    void removeOne(const icString & str);

    /// `list.replaceInStrings (before : string, after : string) : list`
    icStringList replaceInStrings(
      const icString & before, const icString & after);

    /// `list.sort (caseSensitive = true) : list`
    void sort(bool caseSensitive = true);

protected:
    /**
     * @brief _value gets the own value
     * @return the own values as icStringList
     */
    icStringList _value();
};

}  // namespace icL::core::service

#endif  // core_service_ListValue
