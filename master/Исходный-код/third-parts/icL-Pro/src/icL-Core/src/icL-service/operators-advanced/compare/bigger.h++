#ifndef core_service_Bigger
#define core_service_Bigger

#include <icL-il/export/global.h++>



namespace icL::core::service {

class icL_core_service_operators_advanced_EXPORT Bigger
{
public:
    /// `int > int : bool`
    bool intInt(int left, int right);

    /// `double > double : bool`
    bool doubleDouble(double left, double right);
};

}  // namespace icL::core::service

#endif  // core_service_Bigger
