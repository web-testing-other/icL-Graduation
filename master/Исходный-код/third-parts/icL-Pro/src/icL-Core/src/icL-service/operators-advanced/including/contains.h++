#ifndef core_service_Contains
#define core_service_Contains

#include <icL-service/main/values/inode.h++>



class icString;
class icStringList;
class icRegEx;

namespace icL::core {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace service {

class icL_core_service_operators_advanced_EXPORT Contains : virtual public INode
{
public:
    // level 1

    /// `list << string : bool`
    bool listString(const icStringList & left, const icString & right);

    /// `string << string : bool`
    bool stringString(const icString & left, const icString & right);

    /// `set << object : bool`
    bool setObject(const memory::Set & left, const memory::Object & right);

    /// `set << set : bool`
    bool setSet(const memory::Set & left, const memory::Set & right);

    /// `string << regex : bool`
    bool stringRegex(const icString & left, const icRegEx & right);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Contains
