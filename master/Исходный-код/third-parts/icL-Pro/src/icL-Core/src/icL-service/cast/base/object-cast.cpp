#include "object-cast.h++"

#include "void-cast.h++"

#include <icL-types/json/js-document.h++>
#include <icL-types/json/js-object.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

bool ObjectCast::toBool(const memory::Object & value) {
    return value.data->countVariables() > 0;
}

icString ObjectCast::toString(const memory::Object & value) {
    jsDocument object;
    auto &     dataMap = value.data->getMap();

    object.root(jsRoot::Object);

    for (auto it = dataMap.begin(); it != dataMap.end(); it++) {
        object.insert(it.key(), it.value());
    }

    return object.toJson();
}

}  // namespace icL::core::service
