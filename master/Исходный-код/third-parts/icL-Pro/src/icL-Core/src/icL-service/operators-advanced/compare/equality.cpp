#include "equality.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

bool Equality::boolBool(bool left, bool right) {
    return left == right;
}

bool Equality::intInt(int left, int right) {
    return left == right;
}

bool Equality::doubleDouble(double left, double right) {
    double diff = left - right;
    return -1e-10 <= diff && diff <= 1e-10;
}

bool Equality::stringString(const icString & left, const icString & right) {
    return left == right;
}

bool Equality::listList(const icStringList & left, const icStringList & right) {
    return left.toSet() == right.toSet();
}

bool Equality::objectObject(
  const memory::Object & left, const memory::Object & right) {
    return left.data->getMap() == right.data->getMap();
}

bool Equality::setSet(const memory::Set & left, const memory::Set & right) {
    return Set::compare(core(), left, right);
}

}  // namespace icL::core::service
