#include "bool-cast.h++"

#include <icL-types/replaces/ic-string.h++>

namespace icL::core::service {

int BoolCast::toInt(bool value) {
    return value ? 1 : 0;
}

double BoolCast::toDouble(bool value) {
    return value ? 1.0 : 0.0;
}

icString BoolCast::toString(bool value) {
    return value ? "true" : "false";
}

}  // namespace icL::core::service
