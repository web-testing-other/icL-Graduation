#include "emitter.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>

namespace icL::core::service {

StepType Emitter::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        current = State::Emitting;
        break;

    case State::Emitting: {
        memory::FunctionCall fcall;

        fcall.code        = emmiter;
        fcall.contextName = Stringify::alternative(fcall.code.name, "emitter");

        core()->vms->interrupt(fcall, [this](const il::Return & result) {
            if (result.signal.code != il::Signals::NoError) {
                error   = result.signal;
                current = State::Catching;
                return false;
            }
            else {
                current = State::End;
                return true;
            }
        });

        ret = StepType::CommandIn;
        break;
    }

    case State::Catching:
        for (int i = 0; i < slotsPtr.length(); i++) {
            if (slotsPtr[i]->check(error.code)) {
                memory::FunctionCall fcall;

                fcall.code = slotsCode[i];
                fcall.args.append({"@", error.message});
                fcall.contextName =
                  Stringify::alternative(fcall.code.name, "slot");

                core()->vms->interrupt(fcall, [this](const il::Return & result) {
                    if (result.signal.code != il::Signals::NoError) {
                        core()->vm->signal(result.signal);
                    }
                    return false;
                });

                current = State::End;
                ret     = StepType::CommandIn;
                break;
            }
        }

        if (current == State::Catching) {
            // compatible slot not found
            core()->vm->signal(error);
            current = State::End;
        }
        break;

    case State::End:
        finalize();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

}  // namespace icL::core::service
