#include "log.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/log.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/argument.h++>



namespace icL::core::service {

Log::Log() = default;

void Log::error(const icString & message) {
    core()->log->logError(message);
}

void Log::info(const icString & message) {
    core()->log->logInfo(message);
}

void Log::out(const memory::ArgList & args) {
    for (auto & arg : args) {
        info(service::Stringify::arg(core(), arg, arg.container));
    }
}

void Log::stack(const memory::Argument & arg) {
    if (arg.varName.isEmpty()) {
        core()->vm->signal({il::Signals::System, "Wrong argument"});
        return;
    }

    memory::StackContainer * it = core()->mem->stackIt().stack();

    while (it != nullptr) {
        if (it->contains(arg.varName)) {
            info(service::Stringify::arg(core(), arg, it));
        }
    }
}

void Log::state(const memory::Argument & arg) {
    if (arg.varName.isEmpty()) {
        core()->vm->signal({il::Signals::System, "Wrong argument"});
        return;
    }

    memory::StateContainer * it = core()->mem->stateIt().state();

    while (it->getPrev() != nullptr) {
        it = it->getPrev();
    }

    while (it != nullptr) {
        if (it->contains(arg.varName)) {
            info(service::Stringify::arg(core(), arg, it));
        }
    }
}

}  // namespace icL::core::service
