#ifndef core_service_SmallerEqualBigger
#define core_service_SmallerEqualBigger

#include <icL-il/export/global.h++>



namespace icL::core::service {

class icL_core_service_operators_advanced_EXPORT SmallerEqualBigger
{
public:
    /// `int <=> (int, int) : bool`
    bool intIntInt(int left, int begin, int end);

    /// `double <=> (double, double) : bool`
    bool doubleDoubleDouble(double left, double begin, double end);
};

}  // namespace icL::core::service

#endif  // core_service_SmallerEqualBigger
