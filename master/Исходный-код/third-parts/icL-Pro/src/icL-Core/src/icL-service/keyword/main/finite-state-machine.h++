#ifndef core_service_FiniteStateMachine
#define core_service_FiniteStateMachine

#include <icL-il/export/global.h++>
#include <icL-il/structures/steptype.h++>



class icString;
using icL::core::il::StepType;

namespace icL::core::service {

class icL_core_service_keyword_EXPORT FiniteStateMachine
{
public:
    virtual ~FiniteStateMachine() = default;

    /**
     * @brief initialize initializes the finite-state machine
     */
    virtual void initialize() = 0;

    /**
     * @brief transact make a state transation
     */
    virtual StepType transact() = 0;

    /**
     * @brief finalize finalizes the work of finite-state machine
     */
    virtual void finalize() = 0;
};

}  // namespace icL::core::service

#endif  // core_service_FiniteStateMachine
