#ifndef core_service_Multiplication
#define core_service_Multiplication

#include <icL-service/main/values/inode.h++>



class icString;
class icStringList;

namespace icL::core {

namespace memory {
struct Set;
}

namespace service {

class icL_core_service_operators_ALU_EXPORT Multiplication
    : virtual public INode
{
public:
    Multiplication();

    // level 1

    /// `int * int : int`
    int intInt(int left, int right);

    /// `double * double : double`
    double doubleDouble(double left, double right);

    /// `string * string : bool`
    bool stringString(const icString & left, const icString & right);

    /// `list * string : bool`
    bool listString(const icStringList & left, const icString & right);

    /// `list * list : bool`
    bool listList(const icStringList & left, const icStringList & right);

    /// `set * set : set`
    memory::Set setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_Multiplication
