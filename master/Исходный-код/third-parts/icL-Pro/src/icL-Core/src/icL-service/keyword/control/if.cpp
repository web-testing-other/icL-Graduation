#include "if.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::core::service {

using il::Signals::Signals;

StepType If::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        core()->vms->pushKeepAliveLayer(il::LayerType::Control);
        it      = ifs.begin();
        current = State::ConditionChecking;
        break;

    case State::ConditionChecking:
        if (it != ifs.end()) {
            memory::FunctionCall fcall;

            fcall.code        = it->condition;
            fcall.createLayer = false;
            fcall.contextName = Stringify::alternative(fcall.code.name, "if");

            core()->vms->interrupt(fcall, [this](const il::Return & ret) {
                if (ret.signal.code == Signals::NoError) {
                    if (ret.consoleValue.type() != icType::Bool) {
                        core()->vm->signal(
                          {Signals::System,
                           "Expression of if must return a bool value"});
                    }
                    else {
                        if (ret.consoleValue.toBool() ^ it->notModifier) {
                            current = State::CodeRunning;
                        }
                        else {
                            it++;
                        }
                    }
                }
                else {
                    core()->vm->signal(ret.signal);
                }
                return false;
            });
        }
        else {
            current =
              elseCode.source == nullptr ? State::End : State::LastResort;
        }
        break;

    case State::CodeRunning:
        release(it->body);
        ret     = StepType::CommandIn;
        current = State::End;
        break;

    case State::LastResort:
        release(elseCode);
        ret     = StepType::CommandIn;
        current = State::End;
        break;

    case State::End:
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

void If::release(const il::CodeFragment & code) {
    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.createLayer = false;
    fcall.contextName = Stringify::alternative(fcall.code.name, "if");

    core()->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code == Signals::NoError) {
            this->ret = ret.consoleValue;
        }
        else {
            core()->vm->signal(ret.signal);
        }
        return false;
    });
}

}  // namespace icL::core::service
