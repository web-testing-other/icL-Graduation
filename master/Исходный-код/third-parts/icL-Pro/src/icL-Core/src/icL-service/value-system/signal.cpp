#include "signal.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/stateful-server.h++>

namespace icL::core::service {

Signal::Signal() = default;

int Signal::property(const icString & name) {
    return core()->stateful->getSignal(name);
}

void Signal::add(const icString & name) {
    core()->stateful->registerSignal(name);
}

}  // namespace icL::core::service
