#ifndef core_service_Slot
#define core_service_Slot

#include <icL-il/export/global.h++>



namespace icL::core::service {

class icL_core_service_keyword_EXPORT Slot
{
public:
    virtual ~Slot() = default;

    /**
     * @brief check ckecks if this error must be handled by this slot
     * @param code is the error code
     * @return true if this slot must handle this error, otherwise false
     */
    virtual bool check(int code) = 0;
};

}  // namespace icL::core::service

#endif  // core_service_Slot
