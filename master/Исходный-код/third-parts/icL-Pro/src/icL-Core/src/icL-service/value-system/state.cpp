#include "state.h++"

#include <icL-il/main/interlevel.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/set.h++>

namespace icL::core::service {

State::State() = default;

void State::clear() {
    core()->mem->stateIt().clear();
}

void State::delete_() {
    core()->mem->stateIt().delete_();
}

void State::new_(const memory::Object & data) {
    core()->mem->stateIt().appendNewAfter(data.data.get());
}

void State::newAtEnd(const memory::Object & data) {
    core()->mem->stateIt().appendNewAtEnd(data.data.get());
}

void State::toFirst() {
    core()->mem->stateIt().iterateToFirst();
}

void State::toLast() {
    core()->mem->stateIt().iterateToLast();
}

void State::toNext() {
    core()->mem->stateIt().iterateToNext();
}

void State::toPrev() {
    core()->mem->stateIt().iterateToPrev();
}

}  // namespace icL::core::service
