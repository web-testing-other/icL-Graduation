#include "for-parametric.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::core::service {

using il::Signals::Signals;

ForParametric::ForParametric(bool alt)
    : alt(alt) {}

void ForParametric::break_() {
    current = State::End;
}

void ForParametric::continue_() {
    current = State::ConditionChecking;
}

StepType ForParametric::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        core()->vms->pushKeepAliveLayer(il::LayerType::Loop);
        current = State::Intiaization;
        break;

    case State::Intiaization: {
        memory::FunctionCall fcall;

        fcall.code        = init;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "for");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else {
                current = alt ? State::CodeExecution : State::ConditionChecking;
            }
            return false;
        });

        ret      = StepType::CommandIn;
        retValue = executed;
        break;
    }

    case State::ConditionChecking: {
        memory::FunctionCall fcall;

        fcall.code        = condition;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "for");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else if (ret.consoleValue.type() != icType::Bool) {
                core()->vm->signal(
                  {Signals::System, "For: contition must return a bool value"});
            }
            else {
                if (ret.consoleValue.toBool()) {
                    current = State::CodeExecution;
                }
                else {
                    current = State::End;
                }
            }
            return false;
        });

        ret = StepType::CommandIn;
        break;
    }

    case State::CodeExecution: {
        memory::FunctionCall fcall;

        fcall.code        = body;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "for");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else {
                current = State::StepExecution;
            }
            return false;
        });

        retValue = ++executed;
        ret      = StepType::CommandIn;
        break;
    }

    case State::StepExecution: {
        memory::FunctionCall fcall;

        fcall.code        = step;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "for");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                core()->vm->signal(ret.signal);
            }
            else {
                current = State::ConditionChecking;
            }
            return false;
        });

        ret = StepType::CommandIn;
        break;
    }

    case State::End:
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

}  // namespace icL::core::service
