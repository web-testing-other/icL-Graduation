#include "iterator-factory.h++"

#include "list-iterator.h++"
#include "set-iterator.h++"

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-memory/structures/type.h++>



namespace icL::core::service {

using memory::Type;

Iterator * IteratorFactory::fromValue(
  il::InterLevel * il, const icVariant & value) {
    short      type = il->factory->variantToType(value);
    Iterator * ret  = nullptr;

    switch (type) {
    case Type::ListValue:
        ret = new ListIterator(value.toStringList());
        break;

    case Type::SetValue:
        ret = new SetIterator(il, value);
        break;

    default:
        ret = nullptr;
    }

    return ret;
}

}  // namespace icL::core::service
