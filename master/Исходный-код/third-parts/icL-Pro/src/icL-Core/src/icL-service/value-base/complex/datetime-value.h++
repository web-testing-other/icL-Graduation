#ifndef core_service_DatetimeValue
#define core_service_DatetimeValue

#include <icL-il/export/global.h++>
#include <icL-il/main/factory.h++>



class icDateTime;

namespace icL::core::service {

class icL_core_service_value_base_EXPORT DatetimeValue
    : virtual public il::WriteableValue
{
public:
    DatetimeValue();
    ~DatetimeValue() override = default;

    /// `[r/o] icDateTime'valid : bool`
    bool valid();

    // methods level 1

    /// `icDateTime.addDays (days : int) : icDateTime`
    void addDays(int days);

    /// `icDateTime.addMonths (months : int) : icDateTime`
    void addMonths(int months);

    /// `icDateTime.addSecs (secs : int) : icDateTime`
    void addSecs(int secs);

    /// `icDateTime.addYears (months : int) : icDateTime`
    void addYears(int years);

    /// `icDateTime.daysTo (dt : icDateTime) : int`
    int daysTo(const icDateTime & dt);

    /// `icDateTime.secsTo (dt : icDateTime) : int`
    int secsTo(const icDateTime & dt);

    /// `icDateTime.toTimeZone (hours : int, minutes : int) : icDateTime`
    icDateTime toTimeZone(int hours, int minutes);

    /// `icDateTime.toUTC () : icDateTime`
    icDateTime toUTC();

protected:
    /**
     * @brief _value gets own value
     * @return own values as icDateTime
     */
    icDateTime _value();
};

}  // namespace icL::core::service

#endif  // core_service_DatetimeValue
