#ifndef core_service_BiggerEqual
#define core_service_BiggerEqual

#include <icL-il/export/global.h++>


namespace icL::core::service {

class icL_core_service_operators_advanced_EXPORT BiggerEqual
{
public:
    /// `int >= int : bool`
    bool intInt(int left, int right);

    /// `double >= double : bool`
    bool doubleDouble(double left, double right);
};

}  // namespace icL::core::service

#endif  // core_service_BiggerEqual
