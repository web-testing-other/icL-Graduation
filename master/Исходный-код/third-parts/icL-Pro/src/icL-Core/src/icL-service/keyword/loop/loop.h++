#ifndef core_service_Loop
#define core_service_Loop

#include <icL-il/export/global.h++>



namespace icL::core::service {

struct icL_core_service_keyword_EXPORT Loop
{
    virtual ~Loop() = default;

    virtual void break_()    = 0;
    virtual void continue_() = 0;
};

}  // namespace icL::core::service

#endif  // core_service_Loop
