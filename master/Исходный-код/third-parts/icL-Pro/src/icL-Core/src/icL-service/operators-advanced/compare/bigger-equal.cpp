#include "bigger-equal.h++"



namespace icL::core::service {

bool BiggerEqual::intInt(int left, int right) {
    return left >= right;
}

bool BiggerEqual::doubleDouble(double left, double right) {
    return left >= right;
}

}  // namespace icL::core::service
