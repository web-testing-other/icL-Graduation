#ifndef core_service_Assert
#define core_service_Assert

#include "../main/finite-state-machine.h++"

#include <icL-il/structures/code-fragment.h++>

#include <icL-service/main/values/inode.h++>



namespace icL::core::service {

class icL_core_service_keyword_EXPORT Assert
    : public FiniteStateMachine
    , virtual public INode
{
public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

    enum class State {      ///< Finite state machine for `assert`
        Initial,            ///< Initial state of FMS
        ConditionChecking,  ///< Checking assert condition
        ValueExtracting,    ///< extract value of assert string
        End                 ///< The FMS ends here
    } current = State::Initial;

    bool notMod = false;

    il::CodeFragment condition;
    il::CodeFragment message;
};

}  // namespace icL::core::service

#endif  // core_service_Assert
