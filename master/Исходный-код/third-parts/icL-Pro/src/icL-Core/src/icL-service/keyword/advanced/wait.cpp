#include "wait.h++"

#include <icL-types/replaces/ic-datetime.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::core::service {

StepType Wait::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();

        core()->vms->pushKeepAliveLayer(il::LayerType::Loop);
        if (ms == -1) {
            if (s == -1) {
                ms = 1000;
            }
            else {
                ms = s * 1000;
                s  = -1;
            }
        }

        begin = icDateTime::currentMSecsSinceEpoch();
        end   = begin + uint64_t(ms);

        current = State::Checking;
        break;

    case State::Checking:
        current = State::Sleeping;

        if (!ajax && condition.source != nullptr) {
            memory::FunctionCall fcall;

            fcall.code        = condition;
            fcall.createLayer = false;

            core()->vms->interrupt(fcall, [this](const il::Return & result) {
                if (result.signal.code != il::Signals::NoError) {
                    core()->vm->signal(result.signal);
                }
                else if (result.consoleValue.isBool()) {
                    if (result.consoleValue.toBool()) {
                        current = State::End;
                    }
                }
                else {
                    core()->vm->syssig(
                      "Wait condition must return a bool or elements value");
                }
                return false;
            });

            ret = StepType::CommandIn;
        }
        else if (!requests.isEmpty()) {
            memory::FunctionCall fcall;

            fcall.code        = condition;
            fcall.createLayer = false;
            fcall.contextName = Stringify::alternative(fcall.code.name, "wait");
            fcall.args.append({"response", requests.first().response});
            fcall.args.append({"count", requests.first().count});

            requests.removeFirst();

            core()->vms->interrupt(fcall, [this](const il::Return & result) {
                if (result.signal.code != il::Signals::NoError) {
                    if (
                      result.signal.code != il::Signals::System &&
                      result.signal.code != il::Signals::Exit) {
                        core()->vm->signal(result.signal);
                    }
                }
                else if (result.consoleValue.isBool()) {
                    if (result.consoleValue.toBool()) {
                        current = State::End;
                    }
                }
                else {
                    core()->vm->syssig(
                      "Wait:ajax condition must return a bool value");
                }
                return false;
            });

            ret = StepType::CommandIn;
        }
        break;

    case State::Sleeping: {
        uint64_t currentTime = icDateTime::currentMSecsSinceEpoch();

        if (currentTime >= end) {
            if (!try_) {
                core()->vm->signal(
                  {il::Signals::Timeout,
                   "(wait) Time expired: " + icString::number(ms) + "ms"});
                ret = StepType::CommandEnd;
            }
            else {
                current = State::End;
            }
        }
        else {
            uint64_t elapsed = currentTime - begin;
            int      wait;

            if (elapsed < 100) {
                wait = 1;
            }
            else if (elapsed < 1000) {
                wait = 10;
            }
            else if (elapsed < 10000) {
                wait = 100;
            }
            else {
                wait = 1000;
            }

            if (currentTime + uint64_t(wait) > end) {
                wait = int(end - currentTime);
            }

            core()->vm->sleep(wait);
            current = State::Checking;
        }
        break;
    }
    case State::End:
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;

        break;
    }

    return ret;
}

}  // namespace icL::core::service
