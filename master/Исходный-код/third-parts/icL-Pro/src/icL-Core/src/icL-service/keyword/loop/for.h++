#ifndef core_service_For
#define core_service_For

#include "../main/finite-state-machine.h++"
#include "loop.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service/main/values/inode.h++>



namespace icL::core {

namespace il {
struct InterLevel;
}

namespace service {

class Iterator;

class icL_core_service_keyword_EXPORT For
    : public FiniteStateMachine
    , public Loop
    , virtual public INode
{
protected:
    /// body is the loop body
    il::CodeFragment body;
    /// maxX is the value of `:maxX` modifier
    int maxX = -1;
    /// number of executed loops
    int executed = 0;
    /// \brief retValue is the console value of last executed loop
    icVariant retValue;
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_For
