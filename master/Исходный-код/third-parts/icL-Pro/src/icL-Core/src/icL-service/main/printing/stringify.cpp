#include "stringify.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-memory/state/stackcontainer.h++>
#include <icL-memory/state/statecontainer.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>
#include <icL-memory/structures/set.h++>



namespace icL::core::service {

icString Stringify::fromArgs(
  il::InterLevel * il, const memory::ArgList & args) {
    icString ret;

    for (auto & arg : args) {
        ret += fromVariant(il, arg.value) % " : " %
               il->factory->typeToString(arg.type) % ", ";
    }

    // remove ", " from end of icString
    ret.remove(ret.length() - 2, 2);

    return ret;
}

icString Stringify::fromVariant(il::InterLevel * il, const icVariant & value) {
    memory::Argument arg;
    arg.type  = il->factory->variantToType(value);
    arg.value = value;

    return argConstructor(il, arg, nullptr);
}

icString Stringify::fromList(const icStringList & icList) {
    icString ret = "[";

    for (auto str : icList) {
        ret += "\"" %
               str.replace("\\", "\\\\")
                 .replace("\t", "\\t")
                 .replace("\n", "\\n")
                 .replace("\b", "\\b")
                 .replace("\"", "\\\"") %
               "\",";
    }

    return ret.fixLast();
}

icString Stringify::formRegex(const icRegEx & regEx) {
    auto flags = regEx.patternOptions();

    icString pattern = regEx.pattern();
    icChar   innerSymbol;

    if (!pattern.contains('/')) {
        innerSymbol = '/';
    }
    else if (!pattern.contains(':')) {
        innerSymbol = ':';
    }
    else {
        innerSymbol = '`';
    }

    icString ret = '/' % innerSymbol % pattern % innerSymbol % '/';

    if (flags & icRegEx::CaseInsensitiveOption) {
        ret += 'i';
    }

    if (flags & icRegEx::DotMatchesEverythingOption) {
        ret += 's';
    }

    if (flags & icRegEx::MultilineOption) {
        ret += 'm';
    }

    if (flags & icRegEx::ExtendedPatternSyntaxOption) {
        ret += 'x';
    }

    if (flags & icRegEx::UseUnicodePropertiesOption) {
        ret += 'u';
    }

    if (flags & icRegEx::InvertedGreedinessOption) {
        ret += 'f';
    }

    return ret;
}

icString Stringify::fromSet(il::InterLevel * il, const memory::Set & icSet) {
    const auto & header = *icSet.header.get();

    icString ret = "[";

    for (const auto & column : header) {
        ret +=
          column.name % " : " % il->factory->typeToString(column.type) % ", ";
    }

    // replace ", " from end with "]"
    if (ret.length() > 1)
        ret.remove(ret.length() - 1);
    ret.fixLast();

    ret += 'x' % icString::number(icSet.setData->count());

    return ret;
}

icString Stringify::fromPacked(
  il::InterLevel * il, const memory::PackedValue & packed) {
    icString ret = "[";

    for (auto & item : *packed.data) {

        memory::Argument arg;

        switch (item.itemType) {
        case il::PackableType::Type:
            ret += il->factory->typeToString(item.type) % ',';
            break;

        case il::PackableType::Field:
            arg.value = item.value;
            ret += "field[" % item.name % " = " %
                   argConstructor(il, arg, nullptr) % "],";
            break;

        case il::PackableType::Value:
            arg.value     = item.value;
            arg.varName   = item.name;
            arg.container = item.container;
            ret += argConstructor(il, arg, item.container) % ',';
            break;

        case il::PackableType::Column:
            ret += "column[" % item.name % " : " %
                   il->factory->typeToString(item.type) % "],";
            break;

        case il::PackableType::Parameter:
            ret += "parameter[" % item.name % " : " %
                   il->factory->typeToString(item.type) % "],";
            break;

        case il::PackableType::DefaultParameter:
            arg.value = item.value;
            ret += "patameter[" % item.name % " = " %
                   argConstructor(il, arg, nullptr) % "],";
            break;
        }
    }

    return ret.fixLast();
}

icString Stringify::fromObject(
  il::InterLevel * il, const memory::Object & icObject) {
    auto         dataContainer = icObject.data;
    const auto & map           = dataContainer->getMap();

    icString ret = "[";

    for (auto it = map.begin(); it != map.end(); it++) {
        ret += it.key() % " = " % fromVariant(il, it.value()) % ", ";
    }

    // replace ", " from end with "]"
    if (ret.length() > 1)
        ret.remove(ret.length() - 1);

    return ret.fixLast();
}

icString Stringify::arg(
  il::InterLevel * il, const memory::Argument & arg,
  const memory::DataContainer * container) {
    return argSource(container) % " -> " % argConstructor(il, arg, container);
}

icString Stringify::argSource(const memory::DataContainer * container) {
    icString ret;

    if (container == nullptr) {
        ret = "rvalue";
    }
    else {
        switch (container->getContainerType()) {
        case memory::ContainerType::State: {
            auto state =
              dynamic_cast<const memory::StateContainer *>(container);

            ret = "State@[" % icString::number(state->getOrderNumber()) % ']';
            break;
        }
        case memory::ContainerType::Stack: {
            auto stack =
              dynamic_cast<const memory::StackContainer *>(container);

            ret = "Stack@[" % stack->getName() % ']';
            break;
        }
        default:
            ret = "[anonymous icObject]";
        }
    }

    return ret;
}

icString Stringify::argConstructor(
  il::InterLevel * il, const memory::Argument & arg,
  const memory::DataContainer * container) {
    icString  ret = "";
    icVariant value;
    icString  varName = "";

    if (container != nullptr) {
        varName += container->getContainerType() == memory::ContainerType::Stack
                     ? '@'
                     : '#';
        varName += arg.varName;
        value = container->getValue(arg.varName);
    }
    else {
        value = arg.value;
    }

    switch (arg.value.type()) {
    case icType::Bool:
        ret = "bool" % varName % "[" %
              icString(arg.value.toBool() ? "true" : "false") % ']';
        break;

    case icType::Int:
        ret +=
          "int" % varName % "[" % icString::number(arg.value.toInt()) % ']';
        break;

    case icType::Double:
        ret += "double" % varName % "[" %
               icString::number(arg.value.toDouble()) % ']';
        break;

    case icType::String:
        ret += "string" % varName % "[\"" % arg.value.toString() % "\"]";
        break;

    case icType::StringList:
        ret += "list" % varName % fromList(arg.value.toStringList());
        break;

    case icType::RegEx:
        ret += "regex" % varName % formRegex(arg.value.toRegEx());
        break;

    case icType::DateTime:
        ret +=
          "datetime" % varName % "[" % arg.value.toDateTime().toString() % ']';
        break;

    case icType::Set:
        ret += "set" % varName % fromSet(il, arg.value);
        break;

    case icType::Object:
        ret += "object" % varName % fromObject(il, arg);
        break;

    case icType::Void:
    case icType::Initial:
        ret += "void" % varName % "[~]";
        break;

    case icType::Packed:
        ret += "packed" % varName % fromPacked(il, arg.value);
        break;

    default:
        ret += "unknown[]";
    }

    return ret;
}

const icString & Stringify::alternative(
  const icString & str1, const icString & str2) {
    if (str1.isEmpty()) {
        return str2;
    }
    else {
        return str1;
    }
}

}  // namespace icL::core::service
