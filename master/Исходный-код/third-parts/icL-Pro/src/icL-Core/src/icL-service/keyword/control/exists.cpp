#include "exists.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>
#include <icL-memory/structures/type.h++>



namespace icL::core::service {

void Exists::checkByDefaultCondition() {
    using memory::Type;

    switch (core()->factory->variantToType(value)) {
    case Type::BoolValue:
        if (!value.toBool()) {
            value.clear();
        }
        break;

    case Type::IntValue:
        if (value.toInt() == 0) {
            value.clear();
        }
        break;

    case Type::DoubleValue:
        if (value.toDouble() == 0.0) {
            value.clear();
        }
        break;

    case Type::StringValue:
        if (value.toString().isEmpty()) {
            value.clear();
        }
        break;

    case Type::ListValue:
        if (value.toStringList().isEmpty()) {
            value.clear();
        }
        break;

    case Type::ObjectValue:
        if (memory::Object(value).data->countVariables() == 0) {
            value.clear();
        }
        break;

    case Type::SetValue:
        if (memory::Set(value).setData->empty()) {
            value.clear();
        }
        break;

    case Type::DatetimeValue:
        if (!value.toDateTime().isValid()) {
            value.clear();
        }
        break;

    default:
        value.clear();
    }
}

StepType Exists::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial: {
        initialize();
        core()->vms->pushKeepAliveLayer(il::LayerType::Control);
        break;
    }
    case State::Inited: {
        memory::FunctionCall fcall;

        fcall.code        = valueCode;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "exists");

        core()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code == il::Signals::NoError) {
                value = ret.consoleValue;
            }
            else if (ret.signal.code == il::Signals::System) {
                core()->vm->signal(ret.signal);
            }
            return false;
        });

        current = State::ValueCalculed;
        ret     = StepType::CommandIn;
        break;
    }

    case State::ValueCalculed: {
        if (conditionCode.source == nullptr) {
            checkByDefaultCondition();
        }
        else {
            memory::FunctionCall fcall;

            fcall.code        = conditionCode;
            fcall.createLayer = false;
            fcall.contextName =
              Stringify::alternative(fcall.code.name, "exists");
            fcall.args.append({"#", value});

            core()->vms->interrupt(fcall, [this](const il::Return & ret) {
                if (ret.signal.code == il::Signals::NoError) {
                    if (ret.consoleValue.type() != icType::Bool) {
                        core()->vm->signal(
                          {il::Signals::System,
                           "Condition expression must return a bool value"});
                    }
                    else if (!ret.consoleValue.toBool()) {
                        value.clear();
                    }
                }
                else {
                    core()->vm->signal(ret.signal);
                }
                return false;
            });
        }

        current = State::ConditionChecked;
        ret     = StepType::CommandIn;
        break;
    }

    case State::ConditionChecked: {
        finalize();
        core()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }
    }

    return ret;
}

}  // namespace icL::core::service
