#ifndef core_service_ForCollection
#define core_service_ForCollection

#include "for.h++"



namespace icL::core::service {

class icL_core_service_keyword_EXPORT ForCollection : public For
{
public:
    ForCollection(bool reverse, int maxX);

public:
    // Loop interface
public:
    void break_() override;
    void continue_() override;

    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {    ///< FMS `for (<collection>) {<code>}`
        Initial,          ///< Initial state of any `for`
        ValueExtraction,  ///< Calculating the value of collection
        CodeExecution,    ///< Executing the body loop
        End               ///< The FMS ends here
    } current = State::Initial;

    /// reverse contains the value of `:reverse` modifier
    bool reverse = false;

    int : 24;  // padding

    /// it is the iterator of collection
    Iterator * it = nullptr;

    /// collectionCode is the code of collection value
    il::CodeFragment collectionCode;
};

}  // namespace icL::core::service

#endif  // core_service_ForCollection
