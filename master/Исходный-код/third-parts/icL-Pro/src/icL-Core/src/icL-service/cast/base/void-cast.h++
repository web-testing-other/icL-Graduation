#ifndef core_service_VoidCast
#define core_service_VoidCast

#include <icL-il/export/global.h++>

#include <icL-memory/structures/type.h++>



class icString;
class icStringList;
class icVariant;

namespace icL::core {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct Element;
struct InterLevel;
}

namespace service {

class icL_core_service_cast_EXPORT VoidCast
{
public:
    /// `void : bool`
    static bool toBool();

    /// `void : int`
    static int toInt();

    /// `void : double`
    static double toDouble();

    /// `void : string`
    static icString toString();

    /// `void : list`
    static icStringList toList();

    /// `void : object`
    static memory::Object toObject(il::InterLevel * il);

    /// `void : set`
    static memory::Set toSet();

    /// `new var : type`
    static icVariant fromType(il::InterLevel * il, short type);
};

}  // namespace service
}  // namespace icL::core

#endif  // core_service_VoidCast
