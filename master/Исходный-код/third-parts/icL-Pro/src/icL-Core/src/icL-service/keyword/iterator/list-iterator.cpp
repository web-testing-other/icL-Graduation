#include "list-iterator.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::core::service {

ListIterator::ListIterator(const icStringList & list) {
    this->list = list;
    stop = current = this->list.begin();
    init();
}



void ListIterator::init() {
    current = list.begin();
    stop    = list.begin();
    end     = list.end();
}

void ListIterator::toFirst() {
    current = list.begin();
}

void ListIterator::toLast() {
    current = list.end();
    current--;
}

void ListIterator::toNext() {
    current++;
}

void ListIterator::toPrev() {
    current--;
}

bool ListIterator::atBegin() {
    return current == list.begin();
}

bool ListIterator::atEnd() {
    return current == end;
}

bool ListIterator::atStop() {
    return current == stop;
}

void ListIterator::setStopToCurrent() {
    stop = current;
}

void ListIterator::setEndToCurrent() {
    end = current;
}

icVariant ListIterator::getCurrent() {
    return icString{*current};
}

int ListIterator::getSize() {
    return list.length();
}

}  // namespace icL::core::service
