#ifndef core_service_Division
#define core_service_Division

#include <icL-il/export/global.h++>



namespace icL::core::service {

class icL_core_service_operators_ALU_EXPORT Division
{
public:
    Division();

    // level 1

    /// `int / int : int`
    int intInt(int left, int right);

    /// `double / double : double`
    double doubleDouble(double left, double right);
};

}  // namespace icL::core::service

#endif  // core_service_Division
