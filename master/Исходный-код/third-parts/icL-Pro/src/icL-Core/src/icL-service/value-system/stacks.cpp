#include "stacks.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/memory.h++>



namespace icL::core::service {

Stacks::Stacks() = default;

memory::StackContainer * Stacks::property(const icString & name) {
    auto * it = core()->mem->stackIt().stack();

    while (it != nullptr && !it->isNamed(name)) {
        it = it->getPrev();
    }

    if (it == nullptr) {
        core()->vm->signal({il::Signals::System, "No such signal"});
    }

    return it;
}

}  // namespace icL::core::service
