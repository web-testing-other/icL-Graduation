#ifndef core_service_Listify_argsToList
#define core_service_Listify_argsToList

#include "listify.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::core::service {

template <typename T>
icList<T> Listify::fromArgs(const memory::ArgList & args) {
    icList<T> ret;

    for (auto & arg : args) {
        ret.append(T(arg));
    }

    return ret;
}

}  // namespace icL::core::service

#endif  // core_service_Listify_argsToList
