#include "set-iterator.h++"

#include <icL-service/main/values/set.h++>



namespace icL::core::service {

SetIterator::SetIterator(il::InterLevel * il, const memory::Set & icSet)
    : Node(il) {
    this->set = icSet;
    init();
}

void SetIterator::init() {
    current = set.setData->begin();
    stop    = set.setData->begin();
    end     = set.setData->end();
}

void SetIterator::toFirst() {
    current = set.setData->begin();
}

void SetIterator::toLast() {
    current = set.setData->end();
}

void SetIterator::toNext() {
    current++;
}

void SetIterator::toPrev() {
    current--;
}

bool SetIterator::atBegin() {
    return current == set.setData->begin();
}

bool SetIterator::atEnd() {
    return current == end;
}

bool SetIterator::atStop() {
    return current == stop;
}

void SetIterator::setStopToCurrent() {
    stop = current;
}

void SetIterator::setEndToCurrent() {
    end = current;
}

icVariant SetIterator::getCurrent() {
    return Set::rawToObject(il, set, *current);
}

int SetIterator::getSize() {
    return set.setData->count();
}

}  // namespace icL::core::service
