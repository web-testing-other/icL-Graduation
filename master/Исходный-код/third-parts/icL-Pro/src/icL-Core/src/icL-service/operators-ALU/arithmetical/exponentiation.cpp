#include "exponentiation.h++"

#include <icL-types/replaces/ic-math.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-service/main/values/equivalence.h++>
#include <icL-service/main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::core::service {

Exponentiation::Exponentiation() = default;

int Exponentiation::intVoid(int left) {
    return left * left;
}

double Exponentiation::doubleVoid(double left) {
    return left * left;
}

int Exponentiation::intInt(int left, int right) {
    return int(icPow(left, right));
}

double Exponentiation::doubleInt(double left, int right) {
    return icPow(left, right);
}

double Exponentiation::doubleDouble(double left, double right) {
    return icPow(left, right);
}

double Exponentiation::stringString(
  const icString & left, const icString & right) {
    return service::Equivalence::calculate(left, right);
}

double Exponentiation::listList(
  const icStringList & left, const icStringList & right) {
    return service::Equivalence::calculate(left, right);
}

bool Exponentiation::setSet(
  const memory::Set & left, const memory::Set & right) {
    return service::Set::intersects(core(), left, right);
}

memory::Object Exponentiation::stringRegex(
  const icString & left, const icRegEx & right) {
    memory::Object ret(core());

    ret.data = std::make_shared<memory::DataContainer>(core());

    auto match = right.match(left);

    if (!match.hasMatch()) {
        return ret;
    }

    icStringList names = right.namedCaptureGroups();

    for (auto & name : names) {
        if (!name.isEmpty()) {
            ret.data->setValue(name, match.captured(name));
        }
    }

    return ret;
}

}  // namespace icL::core::service
