#include "datetime-value.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-variant.h++>



namespace icL::core::service {

DatetimeValue::DatetimeValue() = default;

bool DatetimeValue::valid() {
    return _value().isValid();
}

void DatetimeValue::addDays(int days) {
    auto value = _value();

    value = value.addDays(days);
    reset(value);
}

void DatetimeValue::addMonths(int months) {
    auto value = _value();

    value = value.addMonths(months);
    reset(value);
}

void DatetimeValue::addSecs(int secs) {
    auto value = _value();

    value = value.addSecs(secs);
    reset(value);
}

void DatetimeValue::addYears(int years) {
    auto value = _value();

    value = value.addYears(years);
    reset(value);
}

int DatetimeValue::daysTo(const icDateTime & dt) {
    return static_cast<int>(_value().daysTo(dt));
}

int DatetimeValue::secsTo(const icDateTime & dt) {
    return static_cast<int>(_value().secsTo(dt));
}

icDateTime DatetimeValue::toTimeZone(int hours, int minutes) {
    return _value().toTimeZone((hours * 60 + minutes) * 60);
}

icDateTime DatetimeValue::toUTC() {
    return _value().toUTC();
}

icDateTime DatetimeValue::_value() {
    return value();
}

}  // namespace icL::core::service
