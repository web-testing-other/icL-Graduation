#ifndef core_vm_VMStack
#define core_vm_VMStack

#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/node.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/steptype.h++>


namespace icL::core {

namespace il {
class SourceOfCode;
struct Position;
}  // namespace il

namespace vm {

class VMLayer;

struct icL_core_vm_EXPORT Highlighter
{
    virtual ~Highlighter() = default;

    /**
     * @brief switchSource swithes to other source of code
     * @param sourceOfCode is the source of code to swith to
     */
    virtual void switchSource(il::SourceOfCode * sourceOfCode) = 0;

    /**
     * @brief switchRegion switches the highlighted region
     * @param begin is the begin of region
     * @param end is the end of region
     */
    virtual void switchRegion(
      const il::Position & begin, const il::Position & end) = 0;

    /**
     * @brief switchColor switches the highlighting color
     * @param color is the is the needed color
     */
    virtual void switchColor(il::SelectionColor color) = 0;
};

class icL_core_vm_EXPORT VMStack
    : public il::VMStack
    , public il::Node
{
    VMLayer *     current  = nullptr;
    il::VMLayer * toRemove = nullptr;
    icString      rootDir;
    Highlighter * highlighter = nullptr;

public:
    VMStack(il::InterLevel * il);

    /**
     * @brief step steps to the next command by executing the current
     * @return the type of maked step
     */
    il::StepType step();

    void init(const icString & file) override;

    // VMStack interface
public:
    void interrupt(
      const memory::FunctionCall &            fcall,
      std::function<bool(const il::Return &)> feedback) override;

    il::VMLayer * pushKeepAliveLayer(il::LayerType type) override;
    void          popKeepAliveLayer() override;

    icString getRootDir() override;
    icString getResourceDir(const icString & projectName) override;
    icString getLibraryDir(const icString & projectName) override;

    void highlight(
      const il::Position & pos1, const il::Position & pos2) override;
    void setSColor(il::SelectionColor scolor) override;

    il::VMLayer * getCurrentLayer() override;

    bool run() override;
    bool debug() override;
    bool stepInto() override;
    bool stepOver() override;
    bool stepOut() override;
    void stop() override;

    void removeLayers(il::VMLayer * last) override;

    icStringList stackTrace() override;

    void markError(
      const il::CodeFragment & code, const icString & error) override;
    void markToken(const il::CodeFragment & code, int color) override;
};

}  // namespace vm
}  // namespace icL::core

#endif  // core_vm_VMStack
