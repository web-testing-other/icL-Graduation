#include "vmlayer.h++"

#include "vmstack.h++"

#include <icL-types/replaces/ic-debug.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/cp-factory.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/log.h++>

#include <icL-service/keyword/loop/loop.h++>

#include <icL-ce/base/main/ce.h++>

#include <icL-memory/state/memory.h++>

#include <QThread>

#include <iostream>



namespace icL::core::vm {

VMLayer::VMLayer(
  il::InterLevel * il, VMLayer * parent,
  std::function<bool(const il::Return &)> feedback,
  const il::CodeFragment & code, int type, int level)
    : il::Node(new il::InterLevel{})
    , m_parent(parent)
    , feedback(feedback)
    , cp(il->cpfactory->create(this->il, code))
    , type(type)
    , level(level) {
    *(this->il)   = *(parent != nullptr ? parent->il : il);
    this->il->vm  = this;
    this->il->cpu = cp;

    active = code.source != nullptr;
}

VMLayer::~VMLayer() {
    il->mem->stackIt().closeStack();
    delete il;
    delete cp;
}

StepType VMLayer::step() {
    // To be removed later
    icDebug() << contextesToString();

    if (state == State::Parsing) {
        il::CE * new_ = cp->parseNext();

        if (new_ == nullptr) {
            if (lastCE == nullptr) {
                finalize();
                return StepType::None;
            }
            else if (lastCE->checkNext(new_)) {
                state = lastCE == nullptr ? State::Parsing : State::Executing;
            }
            else {
                cp_sig(
                  lineInfoForEnd(lastCE) % " Unexpected end of command: " %
                  contextesToString());
            }
        }
        else {
            if (lastCE == nullptr) {
                if (new_->checkPrev(lastCE)) {
                    lastCE = new_;
                }
                else {
                    cp_sig(lineInfoFor(new_) % " Unexpected " % cp->lastKey());
                }
            }
            else {
                if (new_->checkPrev(lastCE) && lastCE->checkNext(new_)) {
                    lastCE->linkNode(new_);
                    lastCE = new_;
                }
                else {
                    if (lastCE->role() == ce::Role::RunContext) {
                        state = State::Executing;
                        cp->repeat();
                    }
                    else {
                        cp_sig(
                          lineInfoFor(new_) % " Unexpected " % cp->lastKey() %
                          ", after command: " % contextesToString());
                    }
                }
            }

            if (il->submode == il::Submode::Colorize) {
                new_->colorize();
            }
        }

        if (state == State::Executing) {
            auto * it = lastCE;

            while (it->prev() != nullptr) {
                it = it->prev();
            }

            il->vms->highlight(
              it->fragmentData().begin, it->fragmentData().end);
            il->vms->setSColor(il::SelectionColor::Executing);

            return StepType::ReadyNow;
        }

        if (lastCE != nullptr) {
            il->vms->highlight(
              lastCE->fragmentData().begin, lastCE->fragmentData().end);
            il->vms->setSColor(il::SelectionColor::Parsing);
        }

        return StepType::MiniStep;
    }

    // find executable

    if (executable == nullptr) {
        if (!findExecutable()) {
            il->vms->setSColor(il::SelectionColor::Destroying);
            return state == State::Parsing ? StepType::MiniStep
                                           : StepType::None;
        }
    }

    // run excutable

    StepType step = executable->runNow();

    il->vms->highlight(
      executable->fragmentData().begin, executable->fragmentData().end);

    // replace blocks

    if (step == StepType::CommandEnd && hasOkState()) {
        executable->release();

        auto first = executable->firstToReplace();
        auto last  = executable->lastToReplace();
        auto newCE = executable->newCE();

        if (lastCE == last) {
            lastCE = newCE;
        }

        if (first == last) {
            newCE->replaceNode(first);
        }
        else {
            newCE->replaceNodes(first, last);
        }

        if (il->submode == il::Submode::Colorize) {
            newCE->colorize();
        }

        executable = nullptr;
    }

    return step;
}

void VMLayer::makeKeepAlive() {
    keepAlive = true;
}

bool VMLayer::isKeepAlive() {
    return keepAlive;
}

icString VMLayer::contextesToString() {
    auto * it = lastCE;

    if (it == nullptr)
        return "";

    while (it->prev() != nullptr) {
        it = it->prev();
    }

    icString str;

    while (it != nullptr) {
        str += it->toString();
        str += ' ';
        it = it->next();
    }

    return str.remove(str.length() - 1);
}

void VMLayer::reset(
  const il::CodeFragment &                code,
  std::function<bool(const il::Return &)> feedback, int ctype) {
    this->feedback = feedback;
    cp->reset(code);
    active = true;
    type   = ctype;
}

VMLayer * VMLayer::nextActive() {
    return active ? this : m_parent->nextActive();
}

int VMLayer::getLevel() {
    return level;
}

void VMLayer::markAsFunctionCall(int returnType) {
    this->returnType = returnType;
}

icString VMLayer::stackTraceLine() {
    return icString::number(level) % ") " %
           (executable != nullptr
              ? lineInfoFor(executable) % ' ' %
                  executable->fragmentData().getCode()
              : cp->getFilePathLineChar() % ' ' % "<cursor position>");
}

void VMLayer::signal(const il::Signal & signal) {
    if (hasOkState() /*&& il->vms->getCurrentLayer() == this*/) {
        m_return.signal = signal;

        if (
          executable != nullptr && !signal.message.isEmpty() &&
          signal.message[0] != '/') {
            m_return.signal.message =
              lineInfoFor(executable) % ' ' % signal.message;
        }

        if (!m_return.signal.printed) {
            il->log->logError(
              m_return.signal.message + "\nstack trace:\n" +
              il->vms->stackTrace().join("\n"));
            m_return.signal.printed = true;
        }

        finalize();
    }
}

void VMLayer::syssig(const icString & message) {
    signal({il::Signals::System, message});
}

void VMLayer::cp_sig(const icString & message) {
    signal({il::Signals::System, message});
}

void VMLayer::cpe_sig(const icString & message) {
    signal({il::Signals::System, il->cpu->lastTokenPosition() % message});
}

void VMLayer::sendAssert(const icString & message) {
    il->log->logError(
      lineInfoFor(executable) % " assertion failed: " % message);
}

void VMLayer::sleep(int ms) {
    QThread::msleep(uint64_t(ms));
}

void VMLayer::addDescription(const icString & /*description*/) {
    // there is no an assertion service
}

void VMLayer::markStep(const icString & /*name*/) {
    // there is no an assertion service
}

void VMLayer::markTest(const icString & /*name*/) {
    // there is no an assertion service
}

void VMLayer::break_() {
    if (m_parent == nullptr) {
        il->vm->syssig("No loop found");
    }
    else {
        auto loop = dynamic_cast<service::Loop *>(m_parent->executable);

        if (loop != nullptr) {
            finalize();
            loop->break_();
        }
        else {
            m_parent->break_();
        }
    }
}

void VMLayer::continue_() {
    if (m_parent == nullptr) {
        il->vm->syssig("No loop found");
    }
    else {
        auto loop = dynamic_cast<service::Loop *>(m_parent->executable);

        if (loop != nullptr) {
            finalize();
            loop->continue_();
        }
        else {
            m_parent->continue_();
        }
    }
}

void VMLayer::return_(const icVariant & value) {
    if (m_parent == nullptr) {
        il->vm->syssig("No function found");
    }
    else {
        if (returnType != icType::Initial) {
            if (returnType != value.type()) {
                il->vm->syssig("Return value does not match the return type");
            }

            m_return.returnValue  = value;
            m_return.consoleValue = il->mem->stackIt().stack()->getValue("#");
            finalize();
        }
        else {
            m_parent->return_(value);
        }
    }
}

bool VMLayer::hasOkState() {
    return m_return.signal.code == il::Signals::NoError;
}

void VMLayer::finalize() {
    active = false;

    if (!feedback(m_return)) {
        if (!keepAlive)
            il->vms->removeLayers(m_parent);
        else
            il->vms->removeLayers(this);
    }
}

il::VMLayer * VMLayer::parent() {
    return m_parent;
}

icVariant VMLayer::get(int ext) {
    return ext == 0 ? int(type) : 0;
}

icVariant VMLayer::set(int /*ext*/, const icVariant & /*value*/) {
    return {};
}

icString VMLayer::lineInfoFor(il::CE * ce) {
    return cp->getFilePath() % ':' %
           icString::number(ce->fragmentData().begin.line) % ':' %
           icString::number(ce->fragmentData().begin.relative) % ':';
}

icString VMLayer::lineInfoForEnd(il::CE * ce) {
    return cp->getFilePath() % ':' %
           icString::number(ce->fragmentData().end.line) % ':' %
           icString::number(ce->fragmentData().end.relative) % ':';
}

bool VMLayer::findExecutable() {
    auto *   it       = lastCE;
    int      lastRank = -1;
    il::CE * runCE    = nullptr;

    while (true) {
        int currentRank = it->currentRunRank(true);
        if (currentRank >= 0 && currentRank > lastRank) {
            runCE    = it;
            lastRank = currentRank;
        }

        if (it->prev() == nullptr) {
            break;
        }
        else {
            it = it->prev();
        }
    }

    auto * first = it;

    while (it != nullptr) {
        int currentRank = it->currentRunRank(false);

        if (currentRank >= 0 && currentRank > lastRank) {
            runCE    = it;
            lastRank = currentRank;
        }

        it = it->next();
    }

    if (runCE == nullptr) {
        if (lastCE->prev() == nullptr && lastCE->hasValue()) {
            auto value = dynamic_cast<il::ReadableValue *>(lastCE);

            if (value != nullptr) {
                m_return.consoleValue = value->value();
            }

            delete lastCE;
            lastCE = nullptr;

            state = State::Parsing;
        }
        else {
            cp_sig(
              lineInfoFor(first) % " Expression can not be solved: " +
              contextesToString());
        }
        return false;
    }

    executable = dynamic_cast<ce::CE *>(runCE);
    return true;
}

}  // namespace icL::core::vm
