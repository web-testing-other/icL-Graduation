#ifndef core_vm_VMLayer
#define core_vm_VMLayer

#include <icL-il/main/ce.h++>
#include <icL-il/main/cp.h++>
#include <icL-il/main/node.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/return.h++>
#include <icL-il/structures/signal.h++>
#include <icL-il/structures/steptype.h++>

#include <icL-memory/structures/function-call.h++>


namespace icL::core {

namespace il {
struct CodeFragment;
}

namespace vm {

class VMStack;

class icL_core_vm_EXPORT VMLayer
    : public il::VMLayer
    , public il::Node
{
public:
    /**
     * @brief VMLayer is the default constructor
     * @param il is the inter-level pointer
     * @param feedback is a function which will be called on stack destroy
     * @param code is the code to run
     */
    VMLayer(
      il::InterLevel * il, VMLayer * parent,
      std::function<bool(const il::Return &)> feedback,
      const il::CodeFragment & code, int type, int level);

    ~VMLayer() override;

    /**
     * @brief step steps to the next command by executing the current
     * @return the type of maked step
     */
    il::StepType step();

    /**
     * @brief makeKeepAlive makes this layer keep alive
     */
    void makeKeepAlive();

    /**
     * @brief isKeepAlive ckecks if the layer is keep alive
     * @return true if layer is keep alive, otherwise false
     */
    bool isKeepAlive();

    /**
     * @brief contextesToString apply some reverse engineering
     * @return current expession revesed to code
     */
    icString contextesToString();

    /**
     * @brief reset resets flayers position and range
     * @param code is the code to execute
     * @note used in keep alive layers
     */
    void reset(
      const il::CodeFragment &                code,
      std::function<bool(const il::Return &)> feedback, int ctype);

    /**
     * @brief nextActive return this node if it is active, otherwise the parent
     * @return return this node if it is active, otherwise the parent
     */
    VMLayer * nextActive();

    /**
     * @brief getLevel gets the fold level of virtual machine
     * @return the fold level of virtual machine
     */
    int getLevel();

    /**
     * @brief markAsFunctionCall marks this layer as a function ca;;
     * @param returnType is the type of data which must be returned by function
     */
    void markAsFunctionCall(int returnType);

    /**
     * @brief stackTraceLine the line for stack trace
     * @return level) /path/to/file:line:char: stack name
     */
    icString stackTraceLine();

    // VMLayer interface
public:
    void signal(const il::Signal & signal) override;
    void syssig(const icString & message) override;
    void cp_sig(const icString & message) override;
    void cpe_sig(const icString & message) override;

    void sendAssert(const icString & message) override;
    void sleep(int ms) override;
    void addDescription(const icString & description) override;
    void markStep(const icString & name) override;
    void markTest(const icString & name) override;

    void break_() override;
    void continue_() override;
    void return_(const icVariant & value) override;

    bool hasOkState() override;
    void finalize() override;

    il::VMLayer * parent() override;

    icVariant get(int ext) override;
    icVariant set(int ext, const icVariant & value) override;

private:
    /**
     * @brief lineInfoFor returns a line info for a CE node
     * @param ce is the node which was generated the error
     * @return `filename:line:char: `
     */
    icString lineInfoFor(il::CE * ce);

    /**
     * @brief lineInfoFor returns a line info for an end of CE node
     * @param ce is the node which was generated the error
     * @return `filename:line:char: `
     */
    icString lineInfoForEnd(il::CE * ce);

    /**
     * @brief findExecutable finds the executable CE in current expression
     * @return true on success, otherwise false
     */
    bool findExecutable();

private:
    /// \brief m_parent is the pointer to the upper level in stack
    VMLayer * m_parent;

    /// \brief m_return contains the return data of vm layer
    il::Return m_return;

    /// \brief feedback is called on stack destroy
    std::function<bool(const il::Return &)> feedback;

    /// \brief cp is the command processor
    il::CP * cp;

    /// \brief lastCE is the last parsed CE
    il::CE * lastCE = nullptr;

    /// \brief executable is the executable node found
    il::CE * executable = nullptr;

    enum class State {  ///< Represent a very simple FMS for virtual machine
        Parsing,        ///< We are working with CP object
        Executing       ///< We are working with CE objects
    };

    /// \brief state is the current state of FMS (initially - Parsing)
    State state = State::Parsing;

    /// \brief keepAlive defines the keep alive state of layer
    bool keepAlive = false;

    /// \brief type is the layer type
    int type;

    /// \brief active defines if this layer is ready to run code
    bool active;

    /// \brief isFunction defines if this layer was created by a funtion call
    int returnType = icType::Initial;

    /// \brief defines the fold level of virtual machine
    int level;
};

}  // namespace vm
}  // namespace icL::core

#endif  // core_vm_VMLayer
