#include "vmstack.h++"

#include "vmlayer.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/log.h++>

#include <icL-cp/source/source-server.h++>
#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/function-call.h++>

#include <QDir>

#include <iostream>



namespace icL::core::vm {

VMStack::VMStack(il::InterLevel * il)
    : Node(il) {}

StepType VMStack::step() {
    if (current == nullptr) {
        return StepType::None;
    }

    if (toRemove != nullptr) {
        il::VMLayer * parent;

        while (toRemove != current) {
            parent = toRemove->parent();
            delete toRemove;
            toRemove = parent;
        }

        toRemove = nullptr;
    }

    StepType step = current->nextActive()->step();

    return step;
}

void VMStack::init(const icString & file) {
    il::CodeFragment code;
    code.source = il->source->getSource(file);
    code.begin  = {0, 0, 1, 1};
    code.end    = {-1, -1, -1, -1};
    code.name   = "/root";

    il->vm = current =
      new VMLayer{il,
                  nullptr,
                  [this](const il::Return & ret) {
                      if (ret.signal.code != il::Signals::NoError) {
                          il->vm->signal(ret.signal);
                      }
                      return false;
                  },
                  code,
                  memory::ContextType::Run,
                  0};
    il->mem->stackIt().openNewStack(current, true);
    il->mem->stackIt().stack()->makeFinally();

    auto path = file.mid(0, file.lastIndexOf('/', file.lastIndexOf('/') - 1));

    rootDir = path;
}

void VMStack::interrupt(
  const memory::FunctionCall &                 fcall,
  std::function<bool(const icL::core::il::Return &)> feedback) {
    VMLayer * layer = current;

    // std::cout << "interrupt: " << fcall.code.begin.line << ':'
    // << fcall.code.begin.relative << ' ' << fcall.code.end.line
    // << ':' << fcall.code.end.relative << std::endl;

    setSColor(il::SelectionColor::NewStack);
    highlight(fcall.code.begin, fcall.code.end);

    // Layer creating
    if (fcall.createLayer) {
        layer = new VMLayer{il,
                            layer,
                            feedback,
                            fcall.code,
                            fcall.contextType,
                            dynamic_cast<VMLayer *>(current)->getLevel() + 1};
        il->mem->stackIt().openNewStack(
          layer, fcall.contextType == memory::ContextType::Run);
        il->mem->stackIt().stack()->setName(fcall.contextName);

        if (fcall.isolateLocalVariables) {
            il->mem->stackIt().stack()->makeFinally();
        }

        if (fcall.returnType != icType::Initial) {
            layer->markAsFunctionCall(fcall.returnType);
        }
    }
    else {
        current->reset(fcall.code, feedback, fcall.contextType);
        il->mem->stackIt().stack()->setName(fcall.contextName);
    }

    // Set arguments
    auto * stack = il->mem->stackIt().stack();
    for (auto & arg : fcall.args) {
        stack->setValue(arg.name, arg.value);
    }

    // Move iterator
    current = layer;
}

il::VMLayer * VMStack::pushKeepAliveLayer(il::LayerType /*type*/) {
    if (!current->hasOkState())
        return nullptr;

    auto * layer =
      new VMLayer{il,
                  current,
                  [](const il::Return &) { return false; },
                  il::CodeFragment{},
                  memory::ContextType::Run,
                  dynamic_cast<VMLayer *>(current)->getLevel() + 1};

    layer->makeKeepAlive();
    il->mem->stackIt().openNewStack(layer, true);

    current = layer;
    return layer;
}

void VMStack::popKeepAliveLayer() {
    if (!current->isKeepAlive()) {
        il->log->logError("Non keep alive layer gets destroyed");
    }

    auto * new_current = current->parent();
    delete current;
    current = dynamic_cast<VMLayer *>(new_current);
}

icString VMStack::getRootDir() {
    return rootDir;
}

icString VMStack::getResourceDir(const icString & projectName) {
    return rootDir + '/' + projectName + "/res/";
}

icString VMStack::getLibraryDir(const icString & projectName) {
    return rootDir + '/' + projectName + "/lib/";
}

void VMStack::highlight(const il::Position & pos1, const il::Position & pos2) {
    if (highlighter != nullptr) {
        highlighter->switchRegion(pos1, pos2);
    }
}

void VMStack::setSColor(il::SelectionColor scolor) {
    if (highlighter != nullptr) {
        highlighter->switchColor(scolor);
    }
}

il::VMLayer * VMStack::getCurrentLayer() {
    return current;
}

bool VMStack::run() {
    while (current != nullptr && current->nextActive()->hasOkState())
        step();
    return true;
}

bool VMStack::debug() {
    if (current != nullptr && current->hasOkState()) {
        /*auto lastStep =*/step();
        //        if (lastStep == StepType::CommandEnd)
        //            break;
    }
    return true;
}

bool VMStack::stepInto() {
    while (current != nullptr && current->hasOkState()) {
        auto lastStep = step();
        if (lastStep == StepType::ReadyNow)
            break;
    }
    return true;
}

bool VMStack::stepOver() {
    int level = current != nullptr ? current->getLevel() : 0;

    while (current != nullptr && current->hasOkState()) {
        auto lastStep = step();
        if (lastStep == StepType::ReadyNow && level >= current->getLevel())
            break;
    }
    return true;
}

bool VMStack::stepOut() {
    int level = current != nullptr ? current->getLevel() - 1 : 0;

    while (current != nullptr && current->hasOkState()) {
        auto lastStep = step();
        if (lastStep == StepType::ReadyNow && level >= current->getLevel())
            break;
    }
    return true;
}

void VMStack::stop() {}

void VMStack::removeLayers(il::VMLayer * last) {
    if (last == current || current == nullptr) {
        return;
    }

    toRemove = current;

    while (toRemove->parent() != last) {
        toRemove = toRemove->parent();
    }

    current = dynamic_cast<vm::VMLayer *>(last);
}

icStringList VMStack::stackTrace() {
    icStringList ret;

    il::VMLayer * it = current;

    while (it != nullptr) {
        ret.append(dynamic_cast<VMLayer *>(it)->stackTraceLine());
        it = it->parent();
    }

    return ret;
}

void VMStack::markError(
  const il::CodeFragment & /*code*/, const icString & /*error*/) {
    return;
}

void VMStack::markToken(const il::CodeFragment & /*code*/, int /*color*/) {
    return;
}

}  // namespace icL::core::vm
