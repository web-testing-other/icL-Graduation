#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-shared/main/engine.h++>

#include <QDir>



int main() {
    using namespace icL::core;

    shared::Engine engine;

    engine.finalize();

    engine.core()->vms->init(QDir::homePath() + "/.icL/test/core/main.icL");
    engine.core()->mode = il::Mode::Check;
    engine.core()->vms->run();

    return 0;
}
