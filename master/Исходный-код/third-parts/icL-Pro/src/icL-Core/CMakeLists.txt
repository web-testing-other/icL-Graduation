cmake_minimum_required(VERSION 3.8.0)

project(icL-Core)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin/ark)

add_subdirectory(src/icL-ce)
add_subdirectory(src/icL-cp)
add_subdirectory(src/icL-il)
add_subdirectory(src/icL-memory)
add_subdirectory(src/test)
add_subdirectory(src/icL-service)
add_subdirectory(src/icL-types)
add_subdirectory(src/icL-vm)
add_subdirectory(src/icL-shared)
add_subdirectory(src/icL-factory)
add_subdirectory(src/icL-mock)

set(IGNORE_QT_QMAKE_EXECUTABLE ${QT_QMAKE_EXECUTABLE})
