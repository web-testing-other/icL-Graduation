#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-shared/main/engine-pro.h++>

#include <QDir>

#include <iostream>



int main(int argc, char ** argv) {
    using namespace icL;

    icStringList args;

    for (int i = 0; i < argc; i++) {
        args.append(icString(argv[i]));
    }

    if (argc <= 1) {
        std::cout << "Usage:\n icL-Pro [--test-core] <file-path>\n"
                  << std::endl;
        return 0;
    }

    icString path = args[args.length() - 1];

    if (args.contains("--test-core")) {
        core::shared::Engine engine;

        engine.finalize();
        engine.core()->vms->init(path);
        engine.core()->vms->run();
    }
    else {
        shared::Engine engine;

        engine.finalize();
        engine.core()->vms->init(path);
        engine.core()->vms->run();
    }

    return 0;
}
