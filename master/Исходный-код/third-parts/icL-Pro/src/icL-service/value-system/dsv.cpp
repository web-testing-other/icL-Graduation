#include "dsv.h++"

#include <icL-il/main/file-server.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/file-target.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

DSV::DSV() = default;

void DSV::append(const il::File & file, const core::memory::Set & icSet) {
    pro()->file->pushTarget(*file.target);
    pro()->file->append(icSet);
    pro()->file->popTarget();
}

void DSV::append(const il::File & file, const core::memory::Object & obj) {
    pro()->file->pushTarget(*file.target);
    pro()->file->append(obj);
    pro()->file->popTarget();
}

core::memory::Set DSV::load(
  const icString & delimiter, const il::File & file,
  const core::memory::Set & base) {
    core::memory::Set ret;

    pro()->file->pushTarget(*file.target);
    ret = pro()->file->load(delimiter, base);
    pro()->file->popTarget();

    return ret;
}

core::memory::Set DSV::loadCSV(
  const il::File & file, const core::memory::Set & base) {
    core::memory::Set ret;

    pro()->file->pushTarget(*file.target);
    ret = pro()->file->loadCSV(base);
    pro()->file->popTarget();

    return ret;
}

core::memory::Set DSV::loadTSV(
  const il::File & file, const core::memory::Set & base) {
    core::memory::Set ret;

    pro()->file->pushTarget(*file.target);
    ret = pro()->file->loadTSV(base);
    pro()->file->popTarget();

    return ret;
}

void DSV::sync(const il::File & file, const core::memory::Set & icSet) {
    pro()->file->pushTarget(*file.target);
    pro()->file->sync(icSet);
    pro()->file->popTarget();
}

void DSV::write(const il::File & file, const core::memory::Set & icSet) {
    pro()->file->pushTarget(*file.target);
    pro()->file->write(icSet);
    pro()->file->popTarget();
}

}  // namespace icL::service
