#include "file.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using namespace il::FileTypesNM;

File::File() = default;

int File::csv() {
    return CSV;
}

int File::none() {
    return None;
}

int File::tsv() {
    return TSV;
}

}  // namespace icL::service
