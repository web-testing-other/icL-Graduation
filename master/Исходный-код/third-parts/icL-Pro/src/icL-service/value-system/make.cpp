#include "make.h++"

#include <icL-types/replaces/ic-image.h++>



namespace icL::service {

Make::Make() = default;

void Make::image(const icString & base64, const icString & path) {
    icImage::saveData(base64, path);
}

}  // namespace icL::service
