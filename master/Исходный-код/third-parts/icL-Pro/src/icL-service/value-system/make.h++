#ifndef service_Make
#define service_Make

#include <icL-il/export/global-pro.h++>



class icString;

namespace icL::service {

class icL_pro_service_value_system_EXPORT Make
{
public:
    Make();

    // methods level 1

    /// `Make.image (base64 : string, path : string) : void`
    void image(const icString & base64, const icString & path);
};

}  // namespace icL::service

#endif  // service_Make
