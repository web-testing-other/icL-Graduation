#include "request.h++"

#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/request.h++>
#include <icL-il/structures/target-data.h++>



namespace icL::service {

void Request::confirm(const icString & text) {
    pro()->request->confirm(text);
}

bool Request::ask(const icString & question) {
    return pro()->request->ask(question);
}

int Request::int_(const icString & text) {
    return pro()->request->int_(text);
}

double Request::double_(const icString & text) {
    return pro()->request->double_(text);
}

icString Request::string(const icString & text) {
    return pro()->request->string(text);
}

icStringList Request::list(const icString & text) {
    return pro()->request->list(text);
}

il::Tab Request::tab(const icString & text) {
    return pro()->request->tab(text);
}

}  // namespace icL::service
