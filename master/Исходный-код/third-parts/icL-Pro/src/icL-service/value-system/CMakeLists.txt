cmake_minimum_required(VERSION 3.8.0)

project(-icL-pro-service-value-system LANGUAGES CXX)

file(GLOB HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/*.h++")
file(GLOB SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

add_library(${PROJECT_NAME} SHARED
  ${HEADERS}
  ${SOURCES}
)

target_compile_definitions(${PROJECT_NAME}
    PRIVATE icL_pro_service_value_base_LIBRARY)

target_include_directories(${PROJECT_NAME}
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/../..
)

target_link_libraries(${PROJECT_NAME}
  PUBLIC
    -icL-types
)
