#include "files.h++"

#include <icL-il/main/file-server.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/file-target.h++>



namespace icL::service {

Files::Files() = default;

il::File Files::create(const icString & path) {
    il::File ret;

    ret.target = std::make_shared<il::FileTarget>(pro()->file->create(path));
    return ret;
}

void Files::createDir(const icString & path) {
    pro()->file->createDir(path);
}

void Files::createPath(const icString & path) {
    pro()->file->createPath(path);
}

il::File Files::open(const icString & path) {
    il::File ret;

    ret.target = std::make_shared<il::FileTarget>(pro()->file->open(path));
    return ret;
}

}  // namespace icL::service
