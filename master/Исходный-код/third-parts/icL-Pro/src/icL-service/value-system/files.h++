#ifndef service_Files
#define service_Files

#include <icL-il/export/global-pro.h++>

#include <icL-service/main/values/inode-pro.h++>



class icString;

namespace icL {

namespace il {
struct File;
struct InterLevel;
}  // namespace il

namespace service {

class icL_pro_service_value_system_EXPORT Files : virtual public INode
{
public:
    Files();

    // methods level 1

    /// `Files.create (path : string) : file`
    il::File create(const icString & path);

    /// `Files.createDir (path : string) : void`
    void createDir(const icString & path);

    /// `Files.createPath (path : string) : void`
    void createPath(const icString & path);

    /// `Files.open (path : string) : file`
    il::File open(const icString & path);
};

}  // namespace service
}  // namespace icL

#endif  // service_Files
