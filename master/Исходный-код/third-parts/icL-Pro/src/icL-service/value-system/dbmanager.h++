#ifndef service_DBManager
#define service_DBManager

#include <icL-il/export/global-pro.h++>

#include <icL-service/main/values/inode-pro.h++>



class icString;

namespace icL {

namespace il {
struct DB;
struct InterLevel;
}  // namespace il

namespace service {

class icL_pro_service_value_system_EXPORT DBManager : virtual public INode
{
public:
    DBManager();

    // methods level 1

    /// `DBManager.connect (server : string) : db`
    il::DB connect(const icString & server);

    /// `DBManager.connect (server : icString, user : icString, password :
    /// icString) : db`
    il::DB connect(
      const icString & server, const icString & user,
      const icString & password);

    /// `DBManager.openSQLite (path : string) : db`
    il::DB openSQLite(const icString & path);
};

}  // namespace service
}  // namespace icL

#endif  // service_DBManager
