#include "listener-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/listen.h++>
#include <icL-il/structures/listen-handler.h++>



namespace icL::service {

il::Listener ListenerValue::create(
  core::il::InterLevel * il, const icString & server) {
    return pro(il)->listen->listen(server);
}

il::Handler ListenerValue::handle(
  const icList<core::memory::Parameter> & params) {
    return pro()->listen->handle(_value(), params);
}

il::Listener ListenerValue::_value() {
    return value();
}

}  // namespace icL::service
