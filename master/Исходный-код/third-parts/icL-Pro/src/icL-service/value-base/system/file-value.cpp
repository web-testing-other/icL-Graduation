#include "file-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/file-server.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/file-target.h++>

namespace icL::service {

FileValue::FileValue() = default;

int FileValue::format() {
    int ret;

    pro()->file->pushTarget(*_value().target);
    ret = pro()->file->getFormat();
    pro()->file->popTarget();

    return ret;
}

void FileValue::close() {
    pro()->file->pushTarget(*_value().target);
    pro()->file->close();
    pro()->file->popTarget();
}

void FileValue::delete_() {
    pro()->file->pushTarget(*_value().target);
    pro()->file->delete_();
    pro()->file->popTarget();
}

il::File FileValue::_value() {
    return value();
}

}  // namespace icL::service
