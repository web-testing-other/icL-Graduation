#include "handler-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/listen.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/lambda-target.h++>
#include <icL-il/structures/listen-handler.h++>



namespace icL::service {

il::Handler HandlerValue::setup(const il::LambdaTarget & lambda) {
    auto value = _value();
    if (lambda.target->paramList.isEmpty()) {
        pro()->listen->setupHandler(value, lambda.target->body);
    }
    else {
        core()->vm->syssig("handler.setup: lambda must have no parameters");
    }
    return value;
}

il::Handler HandlerValue::activate() {
    auto value = _value();
    pro()->listen->activateHandler(value);
    return value;
}

il::Handler HandlerValue::deactivate() {
    auto value = _value();
    pro()->listen->deactivateHandler(value);
    return value;
}

void HandlerValue::kill() {
    pro()->listen->killHandler(_value());
}

il::Handler HandlerValue::_value() {
    return value();
}

}  // namespace icL::service
