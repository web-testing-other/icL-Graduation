#include "js-file-value.h++"

#include <icL-types/replaces/ic-file.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-text-stream.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/signals-pro.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/signal.h++>



namespace icL::service {

il::JsFile JsFileValue::load(icString path) {
    icFile file(path);
    auto   value = _value();

    if (!file.isReadable()) {
        core()->vm->signal(
          {il::Signals::FileNotFound, "JavaScript file not found: " + path});
    }
    else {
        value.target->fileName = path;
    }

    return value;
}

icVariant JsFileValue::run(icVariantList args) {
    return pro()->server->executeSync(getFileContent(), args);
}

void JsFileValue::runAsync(icVariantList args) {
    pro()->server->executeAsync(getFileContent(), args);
}

il::JsFile JsFileValue::setAsUserScript() {
    pro()->server->setUserScript(getFileContent());
    return _value();
}

il::JsFile JsFileValue::setAsPersistentUserScript() {
    pro()->server->setAlwaysScript(getFileContent());
    return _value();
}

icString JsFileValue::getFileContent() {
    icFile       file(_value().target->fileName);
    icTextStream stream(&file);

    return stream.readAll();
}

}  // namespace icL::service
