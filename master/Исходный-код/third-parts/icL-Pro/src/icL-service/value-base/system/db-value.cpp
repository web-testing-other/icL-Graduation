#include "db-value.h++"

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/db-target.h++>

namespace icL::service {

DBValue::DBValue() = default;

void DBValue::close() {
    pro()->db->close();
}

il::Query DBValue::query(const icString & code) {
    il::Query query;

    query.target = std::make_shared<il::DBTarget>(pro()->db->query(code));
    return query;
}

}  // namespace icL::service
