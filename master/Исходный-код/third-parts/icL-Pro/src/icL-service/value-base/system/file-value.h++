#ifndef service_FileValue
#define service_FileValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode-pro.h++>



namespace icL {

namespace il {
struct File;
}

namespace service {

class icL_pro_service_value_base_EXPORT FileValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    FileValue();

    // properties level 1

    /// `[r/w] file'format : int`
    int format();

    // methods level 1

    /// `file.close () : void`
    void close();

    /// `file.delete () : void`
    void delete_();

protected:
    /**
     * @brief _value gets the own value
     * @return the own value as file value
     */
    il::File _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_FileValue
