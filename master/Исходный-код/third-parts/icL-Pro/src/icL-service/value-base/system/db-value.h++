#ifndef service_DBValue
#define service_DBValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode-pro.h++>



class icString;

namespace icL {

namespace il {
struct Query;
}

namespace service {

class icL_pro_service_value_base_EXPORT DBValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    DBValue();

    // methods level 1

    /// `db.close () : void`
    void close();

    /// `db.query (q : Code) : Query`
    il::Query query(const icString & code);
};

}  // namespace service
}  // namespace icL

#endif  // service_DBValue
