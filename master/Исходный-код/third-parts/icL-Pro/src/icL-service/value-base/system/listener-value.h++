#ifndef service_ListenerValue
#define service_ListenerValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode-pro.h++>



class icString;
template <typename>
class icList;

namespace icL {

namespace il {
struct Handler;
struct Listener;
struct InterLevel;
}  // namespace il

namespace core::memory {
struct Parameter;
}

namespace service {

/**
 * @brief The ListenerValue class represent a value of `listener` type
 */
class icL_pro_service_value_base_EXPORT ListenerValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    /// `listen <serverAddress> : listener`
    static il::Listener create(
      core::il::InterLevel * il, const icString & server);

    /// `listener.handle (params ...) : handler`
    il::Handler handle(const icList<core::memory::Parameter> & params);

protected:
    /**
     * @brief _value returns the own value
     * @return own value as listener target
     */
    il::Listener _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_ListenerValue
