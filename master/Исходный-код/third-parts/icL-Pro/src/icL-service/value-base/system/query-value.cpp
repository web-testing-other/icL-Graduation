#include "query-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/signal.h++>



namespace icL::service {

QueryValue::QueryValue() = default;

bool QueryValue::exec() {
    return pro()->db->exec();
}

bool QueryValue::first() {
    return pro()->db->first();
}

icVariant QueryValue::get(const icString & field) {
    return pro()->db->get(field);
}

icString QueryValue::getError() {
    return pro()->db->getError();
}

int QueryValue::getLength() {
    return pro()->db->getLength();
}

int QueryValue::getRowsAffected() {
    return pro()->db->getRowsAffected();
}

bool QueryValue::last() {
    return pro()->db->last();
}

bool QueryValue::next() {
    return pro()->db->next();
}

bool QueryValue::previous() {
    return pro()->db->previous();
}

bool QueryValue::seek(int i, bool relative) {
    return pro()->db->seek(i, relative);
}

void QueryValue::icSet(const icString & field, const icVariant & value) {
    return pro()->db->set(field, value);
}

il::Query QueryValue::_value() {
    return value();
}

}  // namespace icL::service
