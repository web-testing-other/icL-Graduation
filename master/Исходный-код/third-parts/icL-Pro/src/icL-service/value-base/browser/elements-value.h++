#ifndef service_ElementsValue
#define service_ElementsValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode-pro.h++>



class icRect;
class icString;
class icStringList;

template <typename>
class icList;
using icRects = icList<icRect>;

namespace icL {

namespace il {
struct Element;
struct Elements;
}  // namespace il

namespace service {

class icL_pro_service_value_base_EXPORT ElementsValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    ElementsValue();
    ~ElementsValue() override = default;

    /// `[r/o] elements'attr-* : list`
    icStringList attr(const icString & name);

    /// `[r/o] elements'empty : bool`
    bool empty();

    /// `[r/o] elements'length : int`
    int length();

    /// `[r/o] elements'prop-* : list`
    icVariant prop(const icString & name);

    /// `[r/o] elements'rects : set`
    icRects rects();

    /// `[r/o] elements'tags : list`
    icStringList tags();

    /// `[r/o] element'texts : list`
    icStringList texts();

    // methods level 1

    /// `elements.add (other : element) : elements`
    il::Elements add_m1(const il::Element & el);

    /// `elements.add (other : elements) : elements`
    il::Elements add_mm(const il::Elements & el);

    /// `elements.copy () : elements`
    il::Elements copy();

    /// `elements.filter (cssSelector : string) : elements`
    il::Elements filter(const icString & cssSelector);

    /// `elements.get (i : int) : element`
    il::Element get(int i);

protected:
    /**
     * @brief _value gets the own value
     * @return the own value as il.Element
     */
    il::Elements _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_ElementsValue
