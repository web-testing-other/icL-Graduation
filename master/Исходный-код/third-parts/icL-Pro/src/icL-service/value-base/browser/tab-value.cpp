#include "tab-value.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/signal.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-vm/mock.h++>



namespace icL::service {

TabValue::TabValue() = default;

icString TabValue::screenshot() {
    return pro()->server->screenshot();
}

icString TabValue::source() {
    return pro()->server->source();
}

icString TabValue::title() {
    return pro()->server->title();
}

void TabValue::back() {
    pro()->server->back();
}

void TabValue::close() {
    pro()->server->closeWindow();
}

void TabValue::focus() {
    pro()->server->focusWindow();
}

void TabValue::forward() {
    pro()->server->forward();
}

bool TabValue::get(const icString & url) {
    auto il = *core();
    il.vm   = new vm::Mock();

    pro()->server->load(url);
    bool ret = il.vm->hasOkState();
    delete il.vm;

    return ret;
}

void TabValue::load(const icString & url) {
    icRegEx      rx    = R"(\A([\w\-\_]+)/([\w\-\_]+\.\w+)\z)"_rx;
    icRegExMatch match = rx.match(url);

    if (match.hasMatch()) {
        pro()->server->load(
          "file:///" + core()->vms->getResourceDir(match.captured(1)) +
          match.captured(2));
    }
    else {
        pro()->server->load(url);
    }
}

il::Tab TabValue::_value() {
    return value();
}

}  // namespace icL::service
