#ifndef service_DocumentValue
#define service_DocumentValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-service/main/values/inode-pro.h++>



namespace icL {

namespace il {
struct ClickData;
struct HoverData;
struct MouseData;
struct Element;
struct Elements;
}  // namespace il

namespace service {

class icL_pro_service_value_base_EXPORT DocumentValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    DocumentValue() = default;

    // properties level 1

    /// `[r/o] document'tab : tab`
    il::Tab tab();

    // methods level 1

    /// `document.click (data : object) : Doc`
    void click(const il::ClickData & data);

    /// `document.hover (data : object) : Doc`
    void hover(const il::HoverData & data);

    /// `document.mouseDown (data : object) : Doc`
    void mouseDown(const il::MouseData & data);

    /// `document.mouseUp (data : object) : Doc`
    void mouseUp(const il::MouseData & data);

    /// `document.type (text : string) : Doc`
    void typeMethod(const icString & text);

protected:
    /**
     * @brief _value gets the own value as a sesion pointer
     * @return the own value as session pointer
     */
    il::Document _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_DocumentValue
