#include "document-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/enums.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/signal.h++>



namespace icL::service {

il::Tab DocumentValue::tab() {
    il::Tab ret;
    ret.data = _value().data;
    return ret;
}

void DocumentValue::click(const il::ClickData & data) {
    pro()->server->click(nullptr, data);
}

void DocumentValue::hover(const il::HoverData & data) {
    pro()->server->hover(nullptr, data);
}

void DocumentValue::mouseDown(const il::MouseData & data) {
    pro()->server->mouseDown(nullptr, data);
}

void DocumentValue::mouseUp(const il::MouseData & data) {
    pro()->server->mouseUp(nullptr, data);
}

void DocumentValue::typeMethod(const icString & text) {
    pro()->server->sendKeys(nullptr, 0, text);
}

il::Document DocumentValue::_value() {
    return value();
}

}  // namespace icL::service
