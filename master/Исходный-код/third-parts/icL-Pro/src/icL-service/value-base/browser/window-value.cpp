#include "window-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/element.h++>



namespace icL::service {

WindowValue::WindowValue() = default;

void WindowValue::close() {
    pro()->server->closeWindow();
}

void WindowValue::focus() {
    pro()->server->focusWindow();
}

void WindowValue::fullscreen() {
    pro()->server->fullscreen();
}

void WindowValue::maximize() {
    pro()->server->maximize();
}

void WindowValue::minimize() {
    pro()->server->minimize();
}

void WindowValue::restore() {
    pro()->server->restore();
}

void WindowValue::switchToFrame(int i) {
    pro()->server->switchToFrame(i);
}

void WindowValue::switchToFrame(const il::Element & el) {
    pro()->server->switchToFrame(el);
}

void WindowValue::switchToParent() {
    pro()->server->switchToParent();
}

il::Window WindowValue::_value() {
    return value();
}

}  // namespace icL::service
