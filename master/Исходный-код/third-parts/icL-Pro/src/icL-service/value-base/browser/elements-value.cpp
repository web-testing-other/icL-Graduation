#include "elements-value.h++"

#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

ElementsValue::ElementsValue() = default;

icStringList ElementsValue::attr(const icString & name) {
    return pro()->server->attr(_value(), name);
}

bool ElementsValue::empty() {
    return pro()->server->empty(_value());
}

int ElementsValue::length() {
    return pro()->server->length(_value());
}

icVariant ElementsValue::prop(const icString & name) {
    return pro()->server->prop(_value(), name);
}

icRects ElementsValue::rects() {
    return pro()->server->rect(_value());
}

icStringList ElementsValue::tags() {
    return pro()->server->name(_value());
}

icStringList ElementsValue::texts() {
    return pro()->server->text(_value());
}

il::Elements ElementsValue::add_m1(const il::Element & el) {
    return pro()->server->add_m1(_value(), el);
}

il::Elements ElementsValue::add_mm(const il::Elements & el) {
    return pro()->server->add_mm(_value(), el);
}

il::Elements ElementsValue::copy() {
    return pro()->server->copy(_value());
}

il::Elements ElementsValue::filter(const icString & cssSelector) {
    return pro()->server->filter(_value(), cssSelector);
}

il::Element ElementsValue::get(int i) {
    return pro()->server->get(_value(), i);
}

il::Elements ElementsValue::_value() {
    return value();
}

}  // namespace icL::service
