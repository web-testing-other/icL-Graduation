#ifndef service_SessionValue
#define service_SessionValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode-pro.h++>



class icString;

namespace icL {

namespace il {
struct Session;
}

namespace service {

class icL_pro_service_value_base_EXPORT SessionValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    SessionValue();

    /// `[r/o] session'source : string`
    icString source();

    /// `[r/o] session'title : string`
    icString title();

    // methods level 1

    /// `session.back () : session`
    void back();

    /// `session.close () : void`
    void close();

    /// `session.forward () : session`
    void forward();

    /// `session.refresh () : session`
    void refresh();

    /// `session.screenshot () : string`
    icString screenshot();

    /// `session.switchTo () : session`
    void switchTo();

protected:
    /**
     * @brief _value gets the own value as a sesion pointer
     * @return the own value as session pointer
     */
    il::Session _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_SessionValue
