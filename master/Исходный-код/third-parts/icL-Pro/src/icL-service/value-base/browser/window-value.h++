#ifndef service_WindowValue
#define service_WindowValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode-pro.h++>



namespace icL {

namespace il {
struct Element;
struct Window;
}  // namespace il

namespace service {

class icL_pro_service_value_base_EXPORT WindowValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    WindowValue();

    // methods level 1

    /// `window.close () : void`
    void close();

    /// `window.focus () : window`
    void focus();

    /// `window.fullscreen () : window`
    void fullscreen();

    /// `window.maximize () : window`
    void maximize();

    /// `window.minimize () : window`
    void minimize();

    /// `window.restore () : window`
    void restore();

    /// `window.switchToFrame (i : int) : window`
    void switchToFrame(int i);

    /// `window.switchToFrame (el : element) : window`
    void switchToFrame(const il::Element & el);

    /// `window.switchToParent () : window`
    void switchToParent();

protected:
    /**
     * @brief _value gets the own value
     * @return the own value as window pointer
     */
    il::Window _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_WindowValue
