#include "element-value.h++"

#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

ElementValue::ElementValue() = default;

icString ElementValue::attr(const icString & name) {
    return pro()->server->attr(_value(), name);
}

bool ElementValue::clickable() {
    return pro()->server->clickable(_value());
}

icString ElementValue::css(const icString & name) {
    return pro()->server->css(_value(), name);
}

icVariant ElementValue::prop(const icString & name) {
    return pro()->server->prop(_value(), name);
}

il::Document ElementValue::document() {
    il::Document document;
    document.data = std::make_shared<il::TargetData>(_value().target);
    return document;
}

bool ElementValue::enabled() {
    return pro()->server->enabled(_value());
}

icRect ElementValue::rect() {
    return pro()->server->rect(_value());
}

bool ElementValue::selected() {
    return pro()->server->selected(_value());
}

icString ElementValue::tag() {
    return pro()->server->name(_value());
}

icString ElementValue::text() {
    return pro()->server->text(_value());
}

bool ElementValue::visible() {
    return pro()->server->visible(_value());
}

il::Element ElementValue::child(int index) {
    return pro()->server->child(_value(), index);
}

il::Elements ElementValue::children() {
    return pro()->server->children(_value());
}

void ElementValue::clear() {
    return pro()->server->clear(_value());
}

void ElementValue::click(const il::ClickData & data) {
    auto value = _value();
    return pro()->server->click(&value, data);
}

il::Element ElementValue::closest(const icString & cssSelector) {
    return pro()->server->closest(_value(), cssSelector);
}

il::Element ElementValue::copy() {
    return pro()->server->copy(_value());
}

void ElementValue::fastType(const icString & text) {
    pro()->server->fastType(_value(), text);
}

void ElementValue::forceClick(const il::ClickData & data) {
    pro()->server->forceClick(_value(), data);
}

void ElementValue::forceType(const icString & text) {
    pro()->server->forceType(_value(), text);
}

void ElementValue::hover(const il::HoverData & data) {
    auto value = _value();
    return pro()->server->hover(&value, data);
}

void ElementValue::keyDown(int modifiers, const icString & key) {
    auto value = _value();
    return pro()->server->keyDown(&value, modifiers, key);
}

void ElementValue::keyPress(int modifiers, int delay, const icString & keys) {
    auto value = _value();
    return pro()->server->keyPress(&value, modifiers, delay, keys);
}

void ElementValue::keyUp(int modifiers, const icString & key) {
    auto value = _value();
    return pro()->server->keyUp(&value, modifiers, key);
}

void ElementValue::mouseDown(const il::MouseData & data) {
    auto value = _value();
    return pro()->server->mouseDown(&value, data);
}

void ElementValue::mouseUp(const il::MouseData & data) {
    auto value = _value();
    return pro()->server->mouseUp(&value, data);
}

il::Element ElementValue::next() {
    return pro()->server->next(_value());
}

il::Element ElementValue::parent() {
    return pro()->server->parent(_value());
}

void ElementValue::paste(const icString & text) {
    return pro()->server->paste(_value(), text);
}

il::Element ElementValue::prev() {
    return pro()->server->prev(_value());
}

icString ElementValue::screenshot() {
    return pro()->server->screenshot(_value());
}

void ElementValue::sendKeys(int modifiers, const icString & text) {
    auto value = _value();
    return pro()->server->sendKeys(&value, modifiers, text);
}

void ElementValue::superClick() {
    pro()->server->superClick(_value());
}

il::Element ElementValue::_value() {
    return value();
}

}  // namespace icL::service
