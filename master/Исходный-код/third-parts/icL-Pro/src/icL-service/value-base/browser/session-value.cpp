#include "session-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/target-data.h++>



namespace icL::service {

SessionValue::SessionValue() = default;

icString SessionValue::source() {
    return pro()->server->source();
}

icString SessionValue::title() {
    return pro()->server->title();
}

void SessionValue::back() {
    pro()->server->back();
}

void SessionValue::close() {
    pro()->server->closeSession();
}

void SessionValue::forward() {
    pro()->server->forward();
}

void SessionValue::refresh() {
    pro()->server->refresh();
}

icString SessionValue::screenshot() {
    return pro()->server->screenshot();
}

void SessionValue::switchTo() {
    pro()->server->switchSession();
}

il::Session SessionValue::_value() {
    return value();
}

}  // namespace icL::service
