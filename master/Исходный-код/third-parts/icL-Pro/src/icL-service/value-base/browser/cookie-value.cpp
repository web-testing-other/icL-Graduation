#include "cookie-value.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/cookie-data.h++>



namespace icL::service {

CookieValue::CookieValue() = default;

void CookieValue::add(
  int years, int months, int days, int hours, int minutes, int seconds) {
    _value().data->expiry +=
      seconds +
      60 * (minutes + 60 * (hours + 24 * (days + 30 * (months + 365 * years))));
}

void CookieValue::load() {
    auto value = _value();

    pro()->server->pushTarget(value.data->target);
    *value.data = pro()->server->loadCookie(value.data->name);
    pro()->server->popTarget();
}

void CookieValue::resetTime() {
    _value().data->expiry = icDateTime::currentSecsSinceEpoch();
}

void CookieValue::save() {
    pro()->server->pushTarget(_value().data->target);
    pro()->server->saveCookie(*_value().data);
    pro()->server->popTarget();
}

il::Cookie CookieValue::_value() {
    return value();
}

}  // namespace icL::service
