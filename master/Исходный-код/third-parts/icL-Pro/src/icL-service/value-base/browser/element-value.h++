#ifndef service_ElementValue
#define service_ElementValue

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/factory.h++>

#include <icL-service/main/values/inode-pro.h++>



class icRect;
class icString;

namespace icL {

namespace il {
struct Element;
struct Elements;
struct MouseData;
struct HoverData;
struct ClickData;
struct Document;
}  // namespace il

namespace service {

class icL_pro_service_value_base_EXPORT ElementValue
    : virtual public INode
    , virtual public core::il::WriteableValue
{
public:
    ElementValue();
    ~ElementValue() override = default;

    /// `[r/o] element'attr-* : string`
    icString attr(const icString & name);

    /// `[r/o] element'clickable : bool`
    bool clickable();

    /// `[r/o] element'css-* : string`
    icString css(const icString & name);

    /// `element'document : Document`
    il::Document document();

    /// `[r/o] element'enabled : bool`
    bool enabled();

    /// `[r/o] element'prop-* : any`
    icVariant prop(const icString & name);

    /// `[r/o] element'rect : object`
    icRect rect();

    /// `[r/o] element'selected : bool`
    bool selected();

    /// `[r/o] element'tag : string`
    icString tag();

    /// `[r/o] element'text : string`
    icString text();

    /// `[r/o] element'visible : bool`
    bool visible();

    // methods level 1

    /// `element.child (index : int) : element`
    il::Element child(int index);

    /// `element.children () : element`
    il::Elements children();

    /// `element.clear () : element`
    void clear();

    /// `element.click (data : object) : element`
    void click(const il::ClickData & data);

    /// `element.closest (cssSelector : string) : element`
    il::Element closest(const icString & cssSelector);

    /// `element.copy () : element`
    il::Element copy();

    /// `element.fastType (text : string) : element`
    void fastType(const icString & text);

    /// `element.forceClick (data : object) : element`
    void forceClick(const il::ClickData & data);

    /// `element.forceType (text : string) : element`
    void forceType(const icString & text);

    /// `element.hover (data : object) : element`
    void hover(const il::HoverData & data);

    /// `element.keyDown (modifiers : int, key : string) : element`
    void keyDown(int modifiers, const icString & key);

    /// `element.keyPress (modifiers : int, delay : int, keys : icString) :
    /// element`
    void keyPress(int modifiers, int delay, const icString & keys);

    /// `element.keyUp (modifiers : int, key : string) : element`
    void keyUp(int modifiers, const icString & key);

    /// `element.mouseDown (data : object) : element`
    void mouseDown(const il::MouseData & data);

    /// `element.mouseUp (data : object) : element`
    void mouseUp(const il::MouseData & data);

    /// `element.next () : element`
    il::Element next();

    /// `element.parent () : element`
    il::Element parent();

    /// `element.paste (text : string) : element`
    void paste(const icString & text);

    /// `element.prev () : element`
    il::Element prev();

    /// `element.screenshot () : string`
    icString screenshot();

    /// `element.sendKeys (modifiers : int, text : string) : element`
    void sendKeys(int modifiers, const icString & text);

    /// `element.superClick () : element`
    void superClick();

protected:
    /**
     * @brief _value gets the own value
     * @return the own value as il.Element
     */
    il::Element _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_ElementValue
