#include "move.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using namespace il::MoveTypesNM;

Move::Move() = default;

int Move::bezier() {
    return Bezier;
}

int Move::cubic() {
    return Cubic;
}

int Move::linear() {
    return Linear;
}

int Move::quadratic() {
    return Quadratic;
}

int Move::teleport() {
    return Teleport;
}

}  // namespace icL::service
