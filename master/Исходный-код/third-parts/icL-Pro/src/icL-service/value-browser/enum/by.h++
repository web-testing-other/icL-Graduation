#ifndef service_By
#define service_By

#include <icL-il/export/global-pro.h++>



namespace icL::service {

class icL_pro_service_value_browser_EXPORT By
{
public:
    By();

    // properties level 1

    /// `[r/o] By'cssSelector : 1`
    int cssSelector();

    /// `[r/o] By'linkText : 2`
    int linkText();

    /// `[r/o] By'partialLinkText : 3`
    int partialLinkText();

    /// `[r/o] By'tagName : 4`
    int tagName();

    /// `[r/o] By'xPath : 5`
    int xPath();
};

}  // namespace icL::service

#endif  // service_By
