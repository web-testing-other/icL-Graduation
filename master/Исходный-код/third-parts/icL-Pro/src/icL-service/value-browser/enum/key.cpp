#include "key.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using namespace il::KeysNM;

Key::Key() = default;

int Key::alt() {
    return Alt;
}

int Key::ctrl() {
    return Ctrl;
}

int Key::shift() {
    return Shift;
}

}  // namespace icL::service
