#include "by.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using namespace il::SelectorsNM;

By::By() = default;

int By::cssSelector() {
    return CssSelector;
}

int By::linkText() {
    return LinkText;
}

int By::partialLinkText() {
    return PartialLinkText;
}

int By::tagName() {
    return TagName;
}

int By::xPath() {
    return XPath;
}

}  // namespace icL::service
