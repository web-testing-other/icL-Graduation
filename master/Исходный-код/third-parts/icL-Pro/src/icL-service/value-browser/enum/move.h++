#ifndef service_Move
#define service_Move

#include <icL-il/export/global-pro.h++>



namespace icL::service {

class icL_pro_service_value_browser_EXPORT Move
{
public:
    Move();

    /// `[r/o] Move'bezier : 5`
    int bezier();

    /// `[r/o] Move'cubic : 4`
    int cubic();

    /// `[r/o] Move'linear : 2`
    int linear();

    /// `[r/o] Move'quadratic : 3`
    int quadratic();

    /// `[r/o] Move'teleport : 1`
    int teleport();
};

}  // namespace icL::service

#endif  // service_Move
