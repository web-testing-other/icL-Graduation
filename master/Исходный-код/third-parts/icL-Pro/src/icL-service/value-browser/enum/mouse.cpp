#include "mouse.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using namespace il::MouseButtonsNM;

Mouse::Mouse() = default;

int Mouse::left() {
    return Left;
}

int Mouse::middle() {
    return Middle;
}

int Mouse::right() {
    return Right;
}

}  // namespace icL::service
