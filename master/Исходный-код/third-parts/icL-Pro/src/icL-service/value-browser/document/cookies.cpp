#include "cookies.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/structures/cookie-data.h++>

namespace icL::service {

Cookies::Cookies(il::TargetData target)
    : target(std::move(target)) {}

il::Tab Cookies::tab() {
    il::Tab ret;

    ret.data = std::make_shared<il::TargetData>(target);
    return ret;
}

void Cookies::deleteAll() {
    pro()->server->deleteAllCookies();
}

il::Cookie Cookies::get(const icString & name) {
    il::Cookie ret;

    ret.data =
      std::make_shared<il::CookieData>(pro()->server->loadCookie(name));
    return ret;
}

il::Cookie Cookies::new_() {
    il::Cookie ref;

    ref.data = std::make_shared<il::CookieData>();
    return ref;
}

}  // namespace icL::service
