#ifndef service_Tabs
#define service_Tabs

#include <icL-il/structures/target-data.h++>

#include <icL-service/main/values/inode-pro.h++>


class icRegEx;

namespace std {
template <typename>
class function;
}

namespace icL::service {

class icL_pro_service_value_browser_EXPORT Tabs : virtual public INode
{
public:
    Tabs(il::TargetData target);

    // properties level 1

    /// `[r/o] Tabs'current : Tab`
    il::Tab current();

    /// `[r/o] Tabs'first : Tab`
    il::Tab first();

    /// `[r/o] Tabs'last : Tab`
    il::Tab last();

    /// `[r/o] Tabs'length : int`
    int length();

    /// `[r/o] Tabs'next : Tab`
    il::Tab next();

    /// `[r/o] Tabs'previous : Tab`
    il::Tab previous();

    /// `[r/o] Tabs'session : session`
    il::Session session();

    // methods level 1

    /// `Tabs.close (template : string) : int`
    int close(const icString & template_);

    /// `Tabs.close (url : regex) : int`
    int close(const icRegEx & url);

    /// `Tabs.closeByTitle (template : string) : int`
    int closeByTitle(const icString & template_);

    /// `Tabs.closeByTitle (title : regex) : int`
    int closeByTitle(const icRegEx & title);

    /// `Tabs.closeOthers () : int`
    int closeOthers();

    /// `Tabs.closeToLeft () : int`
    int closeToLeft();

    /// `Tabs.closeToRight () : int`
    int closeToRight();

    /// `Tabs.find (template : string) : Tab`
    il::Tab find(const icString & template_);

    /// `Tabs.find (url : regex) : Tab`
    il::Tab find(const icRegEx & url);

    /// `Tabs.findByTitle (template : string) : Tab`
    il::Tab findByTitle(const icString & template_);

    /// `Tabs.findByTitle (title : regex) : Tab`
    il::Tab findByTitle(const icRegEx & title);

    /// `Tabs.get (i : int) : tab`
    il::Tab get(int i);

    /// `Tabs.new () : tab`
    il::Tab new_();

private:
    /**
     * @brief closeBy closes tabs by a icRegEx and a icString value with match
     * @param icRegEx is the regular expression to check
     * @param getField gets a icString field of current tab
     * @return the number of closed tabs
     */
    int closeBy(
      const icRegEx & icRegEx, const std::function<icString()> & getField);

    /**
     * @brief closeBy closes tabs with matches condition
     * @param cond is the condition to check
     * @return the number of closed tabs
     */
    int closeBy(const std::function<bool()> & cond);

    /**
     * @brief findBy finds a tab by a icRegEx and a icString value to match
     * @param icRegEx is the regular expression to check
     * @param getField gets a icString field of current tab
     * @return the first tab which matches the expression
     */
    il::Tab findBy(
      const icRegEx & icRegEx, const std::function<icString()> & getField);

    /**
     * @brief findBy finds a tab with matches the condition
     * @param cond is the conditon to match
     * @return the first tab which matches the condition
     */
    il::Tab findBy(const std::function<bool()> & cond);

protected:
    /// \brief target data about current webview of tabs
    il::TargetData target;
};

}  // namespace icL::service

#endif  // service_Tabs
