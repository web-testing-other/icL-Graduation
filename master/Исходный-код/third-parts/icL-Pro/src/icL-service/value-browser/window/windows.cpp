#include "windows.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>



namespace icL::service {

using core::il::Signals::Signals;

Windows::Windows(il::TargetData target)
    : target(std::move(target)) {}

il::Window Windows::current() {
    il::Window window;

    window.data =
      std::make_shared<il::TargetData>(pro()->server->getCurrentTarget());
    return window;
}

int Windows::length() {
    return pro()->server->getWindows().length();
}

il::Session Windows::session() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(pro()->server->getCurrentTarget());
    return session;
}

il::Window Windows::get(int i) {
    icList<il::TargetData> windows = pro()->server->getWindows();
    il::Window             window;

    if (i < 0 || i >= windows.length()) {
        core()->vm->signal({Signals::OutOfBounds, {}});
    }
    else {
        window.data = std::make_shared<il::TargetData>(windows[i]);
    }

    return window;
}

}  // namespace icL::service
