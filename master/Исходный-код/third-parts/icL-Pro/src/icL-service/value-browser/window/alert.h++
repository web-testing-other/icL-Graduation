#ifndef service_Alert
#define service_Alert

#include <icL-il/structures/target-data.h++>

#include <icL-service/main/values/inode-pro.h++>



namespace icL::service {

class icL_pro_service_value_browser_EXPORT Alert : virtual public INode
{
public:
    Alert(il::TargetData target);

    /// `[r/o] Alert'session : session`
    il::Session session();

    /// `[r/o] Alert'text : string`
    icString text();

    /// `Alert.accept () : void`
    void accept();

    /// `Alert.dismiss () : void`
    void dismiss();

    // properties/methods level 1

    /// `Alert.sendKeys (keys : string) : void`
    void sendKeys(const icString & text);

protected:
    /// \brief target data about the webview of alert
    il::TargetData target;
};

}  // namespace icL::service

#endif  // service_Alert
