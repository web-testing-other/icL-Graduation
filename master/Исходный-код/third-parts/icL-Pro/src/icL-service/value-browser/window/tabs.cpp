#include "tabs.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/export/signals-pro.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <functional>
#include <utility>



namespace icL::service {

using il::SignalsNM::Signals;

Tabs::Tabs(il::TargetData target)
    : target(std::move(target)) {}

il::Tab Tabs::current() {
    il::Tab tab;

    tab.data =
      std::make_shared<il::TargetData>(pro()->server->getCurrentTarget());
    return tab;
}

il::Tab Tabs::first() {
    il::Tab                tab;
    icList<il::TargetData> tabs = pro()->server->getWindows();


    tab.data = std::make_shared<il::TargetData>(tabs.first());
    return tab;
}

il::Tab Tabs::last() {
    il::Tab                tab;
    icList<il::TargetData> tabs = pro()->server->getWindows();

    tab.data = std::make_shared<il::TargetData>(tabs.last());
    return tab;
}

int Tabs::length() {
    return pro()->server->getWindows().length();
}

il::Tab Tabs::next() {
    il::Tab                tab;
    icList<il::TargetData> tabs    = pro()->server->getWindows();
    il::TargetData         current = pro()->server->getCurrentTarget();

    int currentPos = tabs.indexOf(current);

    if (currentPos >= tabs.length() - 1 || currentPos == -1) {
        core()->vm->signal({Signals::NoSuchWindow, {}});
    }
    else {
        tab.data = std::make_shared<il::TargetData>(tabs[currentPos + 1]);
    }

    return tab;
}

il::Tab Tabs::previous() {
    il::Tab                tab;
    icList<il::TargetData> tabs    = pro()->server->getWindows();
    il::TargetData         current = pro()->server->getCurrentTarget();

    int currentPos = tabs.indexOf(current);

    if (currentPos < 1 /*|| currentPos == -1*/) {
        core()->vm->signal({Signals::NoSuchWindow, {}});
    }
    else {
        tab.data = std::make_shared<il::TargetData>(tabs[currentPos - 1]);
    }

    return tab;
}

il::Session Tabs::session() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(pro()->server->getCurrentTarget());
    return session;
}

int Tabs::close(const icString & template_) {
    return close(icRegEx::fromWildcard(template_));
}

int Tabs::close(const icRegEx & url) {
    return closeBy(url, [this]() -> icString { return pro()->server->url(); });
}

int Tabs::closeByTitle(const icString & template_) {
    return closeByTitle(icRegEx::fromWildcard(template_));
}

int Tabs::closeByTitle(const icRegEx & title) {
    return closeBy(
      title, [this]() -> icString { return pro()->server->title(); });
}

int Tabs::closeOthers() {
    return closeBy(
      [this]() -> bool { return pro()->server->getCurrentTarget() != target; });
}

int Tabs::closeToLeft() {
    icList<il::TargetData> sessions = pro()->server->getSessions();

    int current = sessions.indexOf(target);

    return closeBy([&sessions, &current, this]() -> bool {
        return sessions.indexOf(pro()->server->getCurrentTarget()) < current;
    });
}

int Tabs::closeToRight() {
    icList<il::TargetData> sessions = pro()->server->getSessions();

    int current = sessions.indexOf(target);

    return closeBy([&sessions, &current, this]() -> bool {
        return sessions.indexOf(pro()->server->getCurrentTarget()) > current;
    });
}

il::Tab Tabs::find(const icString & template_) {
    return find(icRegEx::fromWildcard(template_));
}

il::Tab Tabs::find(const icRegEx & url) {
    return findBy(url, [this]() -> icString { return pro()->server->url(); });
}

il::Tab Tabs::findByTitle(const icString & template_) {
    return findByTitle(icRegEx::fromWildcard(template_));
}

il::Tab Tabs::findByTitle(const icRegEx & title) {
    return findBy(title, [this]() { return pro()->server->title(); });
}

il::Tab Tabs::get(int i) {
    icList<il::TargetData> tabs = pro()->server->getWindows();
    il::Tab                tab;

    if (i < 0 || i >= tabs.length()) {
        core()->vm->signal({core::il::Signals::OutOfBounds, {}});
    }
    else {
        tab.data = std::make_shared<il::TargetData>(tabs[i]);
    }

    return tab;
}

il::Tab Tabs::new_() {
    il::Tab tab;
    tab.data = std::make_shared<il::TargetData>(pro()->server->newWindow());
    return tab;
}

int Tabs::closeBy(
  const icRegEx & icRegEx, const std::function<icString()> & getField) {
    return closeBy([&icRegEx, &getField]() -> bool {
        auto match = icRegEx.match(getField());
        return match.hasMatch();
    });
}

int Tabs::closeBy(const std::function<bool()> & cond) {
    il::TargetData         current  = pro()->server->getCurrentTarget();
    icList<il::TargetData> tabs     = pro()->server->getWindows();
    icList<il::TargetData> sessions = pro()->server->getSessions();
    icList<il::TargetData> removed;

    for (auto & tab : tabs) {
        pro()->server->pushTarget(tab);

        if (cond()) {
            pro()->server->closeWindow();
            removed += tab;
        }

        pro()->server->popTarget();
    }

    if (removed.contains(current)) {
        if (removed.length() == tabs.length()) {
            int sessionIndex = sessions.indexOf(current);

            // delete current closed session from sessions
            sessions.removeOne(current);

            if (sessionIndex >= 0 && sessionIndex < sessions.length()) {
                current = sessions[sessionIndex];
            }
            else if (sessionIndex == sessions.length()) {
                current = sessions.last();
            }
            else if (!sessions.isEmpty()) {
                current = sessions[0];
            }
            else {
                current = il::TargetData();
            }
        }
        else {
            for (auto & session : sessions) {
                if (!removed.contains(session)) {
                    current = session;
                    break;
                }
            }
        }

        pro()->server->focusSession(current);
    }

    return removed.length();
}

il::Tab Tabs::findBy(
  const icRegEx & icRegEx, const std::function<icString()> & getField) {
    return findBy([&icRegEx, &getField]() -> bool {
        auto match = icRegEx.match(getField());
        return match.hasMatch();
    });
}

il::Tab Tabs::findBy(const std::function<bool()> & cond) {
    il::TargetData         ret;
    icList<il::TargetData> tabs = pro()->server->getWindows();

    for (auto & tab : tabs) {
        pro()->server->pushTarget(tab);

        if (cond() && tab.sessionId.isEmpty()) {
            ret = tab;
        }

        pro()->server->popTarget();
    }

    il::Tab tab;
    tab.data = std::make_shared<il::TargetData>(ret);
    return tab;
}

}  // namespace icL::service
