#include "alert.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>



namespace icL::service {

Alert::Alert(il::TargetData target)
    : target(std::move(target)) {}

il::Session Alert::session() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(pro()->server->getCurrentTarget());
    return session;
}

icString Alert::text() {
    return pro()->server->alertText();
}

void Alert::accept() {
    pro()->server->alertAccept();
}

void Alert::dismiss() {
    pro()->server->alertDimiss();
}

void Alert::sendKeys(const icString & text) {
    pro()->server->alertSendText(text);
}

}  // namespace icL::service
