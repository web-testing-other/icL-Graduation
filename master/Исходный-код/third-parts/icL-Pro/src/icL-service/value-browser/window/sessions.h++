#ifndef service_Sessions
#define service_Sessions

#include <icL-il/export/global-pro.h++>

#include <icL-service/main/values/inode-pro.h++>



namespace icL {

namespace il {
struct Session;
}  // namespace il

namespace service {

class icL_pro_service_value_browser_EXPORT Sessions : virtual public INode
{
public:
    Sessions();

    // properties / methods level 1

    /// `[r/o] Sessions'current : session`
    il::Session current();

    /// `[r/o] Sessions'length : int`
    int length();

    /// `Sessions.closeAll () : void`
    void closeAll();

    /// `Sessions.get (i : int) : session`
    il::Session get(int i);

    /// `Sessions.new () : session`
    il::Session new_();
};

}  // namespace service
}  // namespace icL

#endif  // service_Sessions
