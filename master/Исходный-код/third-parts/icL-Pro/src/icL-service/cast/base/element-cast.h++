#ifndef service_ElementCast
#define service_ElementCast

#include <icL-il/export/global-pro.h++>



class icString;

namespace icL {

namespace il {
struct Element;
struct Elements;
}  // namespace il

namespace service {

class icL_pro_service_cast_EXPORT ElementCast
{
private:
    ElementCast() = default;

public:
    /// `element : bool`
    static bool toBool(const icL::il::Element & value);

    /// `element : string`
    static icString toString(const icL::il::Element & value);
};

}  // namespace service
}  // namespace icL

#endif  // service_ElementCast
