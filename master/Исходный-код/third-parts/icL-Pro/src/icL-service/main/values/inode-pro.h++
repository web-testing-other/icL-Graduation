#ifndef pro_service_INode
#define pro_service_INode

#include <icL-il/export/global-pro.h++>

#include <icL-service/main/values/inode.h++>



namespace icL {

namespace core::il {
struct InterLevel;
}

namespace il {
struct InterLevel;
}

namespace service {

/**
 * @brief The INode class represent an interface for a node
 *
 * This interface is special for classes which will become nodes later and the
 * inherance of Node twince will be a serious problem
 */
class icL_pro_service_main_EXPORT INode : virtual public core::service::INode
{
public:
    virtual ~INode() = default;

protected:
    il::InterLevel * pro();

    static il::InterLevel * pro(core::il::InterLevel * il);
};

}  // namespace service
}  // namespace icL

#endif  // pro_service_INode
