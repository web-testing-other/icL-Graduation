#include "inode-pro.h++"

#include <icL-il/main/interlevel.h++>

#include <any>



namespace icL::service {

il::InterLevel * INode::pro() {
    return pro(core());
}

il::InterLevel * INode::pro(core::il::InterLevel * il) {
    return std::any_cast<il::InterLevel *>(*il->ext);
}

}  // namespace icL::service
