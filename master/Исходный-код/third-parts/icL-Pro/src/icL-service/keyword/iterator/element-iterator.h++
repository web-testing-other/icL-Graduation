#ifndef service_ElementIterator
#define service_ElementIterator

#include <icL-il/structures/element.h++>

#include <icL-service/keyword/iterator/iterator.h++>
#include <icL-service/main/values/inode-pro.h++>



namespace icL {

namespace il {
struct InterLevel;
}

namespace service {

/**
 * @brief The ElementIterator class describes an iterator for `element` value
 */
class icL_pro_service_keyword_EXPORT ElementIterator
    : public core::service::Iterator
    , virtual public INode
{
public:
    /**
     * @brief ElementIterator
     * @param element
     */
    ElementIterator(il::Elements element, core::il::InterLevel * il);

private:
    /**
     * @brief init initializes the class icObject
     */
    void init();

    // Iterator interface
public:
    void toFirst() override;
    void toLast() override;
    void toNext() override;
    void toPrev() override;
    bool atBegin() override;
    bool atEnd() override;
    bool atStop() override;
    void setStopToCurrent() override;
    void setEndToCurrent() override;

    icVariant getCurrent() override;
    int       getSize() override;

private:
    /// pointer to interlevel interface
    core::il::InterLevel * il;

    /// element is the collection to iterate
    il::Elements element;

    /// current is the index of current item
    int current = 0;
    /// stop is the index of stop item
    int stop = 0;
    /// count is the count of elements in collection
    int count{};
    /// end is the programmable end element
    int end{};

    // INode interface
protected:
    core::il::InterLevel * core() override;
};

}  // namespace service
}  // namespace icL

#endif  // service_ElementIterator
