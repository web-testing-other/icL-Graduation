#include "element-iterator.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>

#include <utility>

namespace icL::service {

ElementIterator::ElementIterator(
  il::Elements element, core::il::InterLevel * il)
    : il(il)
    , element(std::move(element)) {
    init();
}

void ElementIterator::init() {
    end = count = pro(il)->server->length(element);
}

void ElementIterator::toFirst() {
    current = 0;
}

void ElementIterator::toLast() {
    current = count > 0 ? count - 1 : 0;
}

void ElementIterator::toNext() {
    if (current < count)
        current++;
}

void ElementIterator::toPrev() {
    if (current > 0)
        current--;
}

bool ElementIterator::atBegin() {
    return current == 0;
}

bool ElementIterator::atEnd() {
    return current == end;
}

bool ElementIterator::atStop() {
    return current == stop;
}

void ElementIterator::setStopToCurrent() {
    stop = current;
}

void ElementIterator::setEndToCurrent() {
    end = current;
}

icVariant ElementIterator::getCurrent() {
    return pro(il)->server->get(element, current);
}

int ElementIterator::getSize() {
    return count;
}

core::il::InterLevel * ElementIterator::core() {
    return il;
}

}  // namespace icL::service
