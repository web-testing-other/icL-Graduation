#include <icL-types/replaces/ic-variant.h++>

#include <icL-vm/mock.h++>

#include <QThread>



namespace icL::vm {

void Mock::signal(const core::il::Signal & signal) {
    m_signal = signal;
}

void Mock::syssig(const icString & message) {
    signal({core::il::Signals::System, message});
}

void Mock::cp_sig(const icString & message) {
    syssig(message);
}

void Mock::cpe_sig(const icString & message) {
    syssig(message);
}

void Mock::sendAssert(const icString &) {
    // Do nothing
}

void Mock::sleep(int ms) {
    QThread::msleep(uint64_t(ms));
}

void Mock::addDescription(const icString &) {
    // Do nothing
}

void Mock::markStep(const icString &) {
    // Do nothing
}

void Mock::markTest(const icString &) {
    // Do nothing
}

void Mock::break_() {
    // Do nothing
}

void Mock::continue_() {
    // Do nothing
}

void Mock::return_(const icVariant &) {
    // Do nothing
}

bool Mock::hasOkState() {
    return m_signal.code == core::il::Signals::NoError;
}

void Mock::finalize() {
    // Do nothing
}

icVariant Mock::get(int /*ext*/) {
    return icVariant::makeVoid();
}

icVariant Mock::set(int /*ext*/, const icVariant & /*value*/) {
    return icVariant::makeVoid();
}

core::il::VMLayer * Mock::parent() {
    return nullptr;
}

}  // namespace icL::vm
