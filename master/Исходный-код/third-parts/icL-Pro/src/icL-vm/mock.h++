#ifndef vm_Mock
#define vm_Mock

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>



namespace icL::vm {

class icL_pro_vm_EXPORT Mock : public core::il::VMLayer
{
public:
    Mock() = default;

    // VMLayer interface
public:
    void signal(const core::il::Signal & signal) override;
    void syssig(const icString & message) override;
    void cp_sig(const icString & message) override;
    void cpe_sig(const icString & message) override;
    void sendAssert(const icString & message) override;
    void sleep(int ms) override;
    void addDescription(const icString & description) override;
    void markStep(const icString & name) override;
    void markTest(const icString & name) override;
    void break_() override;
    void continue_() override;
    void return_(const icVariant & value) override;
    bool hasOkState() override;
    void finalize() override;

    icVariant get(int ext) override;
    icVariant set(int ext, const icVariant & value) override;

    core::il::VMLayer * parent() override;

private:
    core::il::Signal m_signal;
};

}  // namespace icL::vm

#endif  // vm_Mock
