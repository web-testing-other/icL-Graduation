#include "keyword-packer-pro.h++"

#include "icL-pro.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/lambda-target.h++>

#include <icL-ce/literal/const/element.h++>
#include <icL-ce/literal/functional-js/js-file.h++>
#include <icL-ce/literal/static/type.h++>
#include <icL-ce/value-base/lambda/lambda-js-value.h++>
#include <icL-ce/value-browser/document/cookies.h++>
#include <icL-ce/value-browser/document/document.h++>
#include <icL-ce/value-browser/enum/key.h++>
#include <icL-ce/value-browser/enum/mouse.h++>
#include <icL-ce/value-browser/enum/move.h++>
#include <icL-ce/value-browser/settings/icl.h++>
#include <icL-ce/value-browser/window/alert.h++>
#include <icL-ce/value-browser/window/session.h++>
#include <icL-ce/value-browser/window/sessions.h++>
#include <icL-ce/value-browser/window/tab.h++>
#include <icL-ce/value-browser/window/tabs.h++>
#include <icL-ce/value-browser/window/window.h++>
#include <icL-ce/value-browser/window/windows.h++>
#include <icL-ce/value-system/file.h++>
#include <icL-ce/value-system/files.h++>
#include <icL-ce/value-system/make.h++>
#include <icL-ce/value-system/request.h++>

#include <icL-cp/result/structs.h++>
#include <icL-memory/state/memory.h++>



namespace icL::cp {

KeywordPacker::KeywordPacker() {
    keys.insert("listen", &KeywordPacker::pack_listen);
    keys.insert("notify", &KeywordPacker::pack_notify);
    keys.insert("Alert", &KeywordPacker::pack_Alert);
    keys.insert("By", &KeywordPacker::pack_By);
    keys.insert("Cookies", &KeywordPacker::pack_Cookies);
    keys.insert("DB", &KeywordPacker::pack_DB);
    keys.insert("DBManager", &KeywordPacker::pack_DBManager);
    keys.insert("Document", &KeywordPacker::pack_Document);
    keys.insert("DSV", &KeywordPacker::pack_DSV);
    keys.insert("File", &KeywordPacker::pack_File);
    keys.insert("Files", &KeywordPacker::pack_Files);
    keys.insert("icL", &KeywordPacker::pack_icL);
    keys.insert("Key", &KeywordPacker::pack_Key);
    keys.insert("Make", &KeywordPacker::pack_Make);
    keys.insert("Mouse", &KeywordPacker::pack_Mouse);
    keys.insert("Move", &KeywordPacker::pack_Move);
    keys.insert("Request", &KeywordPacker::pack_Request);
    keys.insert("Query", &KeywordPacker::pack_Query);
    keys.insert("Session", &KeywordPacker::pack_Session);
    keys.insert("Sessions", &KeywordPacker::pack_Sessions);
    keys.insert("Tab", &KeywordPacker::pack_Tab);
    keys.insert("Tabs", &KeywordPacker::pack_Tabs);
    keys.insert("View", &KeywordPacker::pack_View);
    keys.insert("Window", &KeywordPacker::pack_Window);
    keys.insert("Windows", &KeywordPacker::pack_Windows);
    keys.insert("cookie", &KeywordPacker::pack_cookie);
    keys.insert("database", &KeywordPacker::pack_database);
    keys.insert("document", &KeywordPacker::pack_document);
    keys.insert("element", &KeywordPacker::pack_element);
    keys.insert("file", &KeywordPacker::pack_file);
    keys.insert("handler", &KeywordPacker::pack_handler);
    keys.insert("lambda-js", &KeywordPacker::pack_lambda_js);
    keys.insert("lambda-sql", &KeywordPacker::pack_lambda_sql);
    keys.insert("listener", &KeywordPacker::pack_listener);
    keys.insert("query", &KeywordPacker::pack_query);
    keys.insert("session", &KeywordPacker::pack_session);
    keys.insert("tab", &KeywordPacker::pack_tab);
    keys.insert("window", &KeywordPacker::pack_window);
    keys.insert("js", &KeywordPacker::pack_js_literal);
    keys.insert("css", &KeywordPacker::pack_css_literal);
    keys.insert("xpath", &KeywordPacker::pack_xpath_literal);
    keys.insert("link", &KeywordPacker::pack_link_literal);
    keys.insert("links", &KeywordPacker::pack_links_literal);
    keys.insert("tag", &KeywordPacker::pack_tag_literal);
    keys.insert("tags", &KeywordPacker::pack_tags_literal);
    keys.insert("input", &KeywordPacker::pack_input_literal);
    keys.insert("field", &KeywordPacker::pack_field_literal);
}

core::il::CE * KeywordPacker::pack_listen(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_notify(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_Alert(const PackData & pd) {
    return new ce::Alert{pd.il};
}

core::il::CE * KeywordPacker::pack_By(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_Cookies(const PackData & pd) {
    return new ce::Cookies{pd.il};
}

core::il::CE * KeywordPacker::pack_DB(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_DBManager(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_Document(const PackData & pd) {
    return new ce::Document{pd.il};
}

core::il::CE * KeywordPacker::pack_DSV(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_File(const PackData & pd) {
    return new ce::File{pd.il};
}

core::il::CE * KeywordPacker::pack_Files(const PackData & pd) {
    return new ce::Files{pd.il};
}

core::il::CE * KeywordPacker::pack_icL(const PackData & pd) {
    return new ce::icL{pd.il};
}

core::il::CE * KeywordPacker::pack_Key(const PackData & pd) {
    return new ce::Key{pd.il};
}

core::il::CE * KeywordPacker::pack_Make(const PackData & pd) {
    return new ce::Make{pd.il};
}

core::il::CE * KeywordPacker::pack_Mouse(const PackData & pd) {
    return new ce::Mouse{pd.il};
}

core::il::CE * KeywordPacker::pack_Move(const PackData & pd) {
    return new ce::Move{pd.il};
}

core::il::CE * KeywordPacker::pack_Request(const PackData & pd) {
    return new ce::Request{pd.il};
}

core::il::CE * KeywordPacker::pack_Query(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_Session(const PackData & pd) {
    return new ce::Session{pd.il};
}

core::il::CE * KeywordPacker::pack_Sessions(const PackData & pd) {
    return new ce::Sessions{pd.il};
}

core::il::CE * KeywordPacker::pack_Tab(const PackData & pd) {
    return new ce::Tab{pd.il};
}

core::il::CE * KeywordPacker::pack_Tabs(const PackData & pd) {
    return new ce::Tabs{pd.il};
}

core::il::CE * KeywordPacker::pack_View(const PackData & /*pd*/) {
    return nullptr;
}

core::il::CE * KeywordPacker::pack_Window(const PackData & pd) {
    return new ce::Window{pd.il};
}

core::il::CE * KeywordPacker::pack_Windows(const PackData & pd) {
    return new ce::Windows{pd.il};
}

core::il::CE * KeywordPacker::pack_cookie(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::CookieValue};
}

core::il::CE * KeywordPacker::pack_database(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::DatabaseValue};
}

core::il::CE * KeywordPacker::pack_document(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::DocumentValue};
}

core::il::CE * KeywordPacker::pack_element(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::ElementValue};
}

core::il::CE * KeywordPacker::pack_file(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::FileValue};
}

core::il::CE * KeywordPacker::pack_handler(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::HandlerValue};
}

core::il::CE * KeywordPacker::pack_lambda_js(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::JavaScriptLambdaValue};
}

core::il::CE * KeywordPacker::pack_lambda_sql(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::SqlLambdaValue};
}

core::il::CE * KeywordPacker::pack_listener(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::ListenerValue};
}

core::il::CE * KeywordPacker::pack_query(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::QueryValue};
}

core::il::CE * KeywordPacker::pack_session(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::SessionValue};
}

core::il::CE * KeywordPacker::pack_tab(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::TabValue};
}

core::il::CE * KeywordPacker::pack_window(const PackData & pd) {
    return new core::ce::TypeToken{pd.il, memory::Type::WindowValue};
}

core::il::CE * KeywordPacker::pack_js_value(const PackData & pd) {
    core::il::CodeFragment getter, setter;

    getter.source = pd.fly->source();
    getter.begin  = pd.result.data["getter-begin"];
    getter.end    = pd.result.data["getter-end"];

    if (pd.result.data.contains("setter-begin")) {
        setter.source = pd.fly->source();
        setter.begin  = pd.result.data["setter-begin"];
        setter.end    = pd.result.data["setter-end"];

        // return pd.il->factory->fromValue(
        // pd.il, getter.getCode(), setter.getCode());
    }

    // return pd.il->factory->fromValue(il, getter.getCode(), {});
    return nullptr;
}

core::il::CE * KeywordPacker::pack_js_file(const PackData & pd) {
    return new ce::JsFile(pd.il, pd.result.data["lit-str"]);
}

core::il::CE * KeywordPacker::pack_js_code(const PackData & pd) {
    il::JsLambda value;

    value.target          = std::make_shared<core::il::CodeFragment>();
    value.target->source  = pd.fly->source();
    value.target->begin   = pd.result.data["begin"];
    value.target->end     = pd.result.data["end"];
    value.data            = std::make_shared<il::TargetData>();
    value.data->sessionId = pd.result.data["target-session"];
    value.data->windowId  = pd.result.data["target-window"];

    return new ce::LambdaJsValue{pd.il, value};
}

core::il::CE * KeywordPacker::pack_js_literal(const PackData & pd) {
    core::cp::FlyResult res = pd.result;

    dynamic_cast<icL *>(pd.fly)->flyJs(res);
    auto npd = PackData{pd.il, pd.fly, res};

    switch (res.type) {
    case ResultType::JsCode:
        return pack_js_code(npd);

    case ResultType::JsFile:
        return pack_js_file(npd);

    case ResultType::JsValue:
        return pack_js_value(npd);

    default:
        assert(false);
        return nullptr;
    }
}

core::il::CE * KeywordPacker::pack_element_literal(const PackData & pd) {
    icString                pattern;
    icString                      searchMode = pd.result.key;
    icStringList            modifiers;
    core::memory::DataContainer * container;
    icString                varName;
    core::il::CodeFragment        code;

    auto copy = pd.result;
    dynamic_cast<icL *>(pd.fly)->flyElement(copy);

    if (pd.il->vm->hasOkState()) {
        code.source = pd.fly->source();
        code.begin  = copy.data["code-begin"];
        code.end    = copy.data["code-end"];
        pattern     = code.getCode();
        modifiers   = copy.data["modifiers"];

        icString scope = copy.data["scope"];

        if (scope.isEmpty()) {
            container = nullptr;
        }
        else if (scope.length() == 1) {
            varName   = scope;
            container = pd.il->mem->stackIt().getContainerForVariable("@");
        }
        else {
            varName = scope.mid(1, -1);

            if (scope[0] == '#') {
                container = pd.il->mem->stateIt().state();
            }
            else {
                container =
                  pd.il->mem->stackIt().getContainerForVariable(varName);
            }
        }
    }
    else {
        container = nullptr;
    }

    return new ce::Element{pd.il,     pattern, searchMode, modifiers,
                           container, varName, code};
}

core::il::CE * KeywordPacker::pack_css_literal(const PackData & pd) {
    return pack_element_literal(pd);
}

core::il::CE * KeywordPacker::pack_xpath_literal(const PackData & pd) {
    return pack_element_literal(pd);
}

core::il::CE * KeywordPacker::pack_link_literal(const PackData & pd) {
    return pack_element_literal(pd);
}

core::il::CE * KeywordPacker::pack_links_literal(const PackData & pd) {
    return pack_element_literal(pd);
}

core::il::CE * KeywordPacker::pack_tag_literal(const PackData & pd) {
    return pack_element_literal(pd);
}

core::il::CE * KeywordPacker::pack_tags_literal(const PackData & pd) {
    return pack_element_literal(pd);
}

core::il::CE * KeywordPacker::pack_input_literal(const PackData & pd) {
    return pack_element_literal(pd);
}

core::il::CE * KeywordPacker::pack_field_literal(const PackData & pd) {
    return pack_element_literal(pd);
}


}  // namespace icL::cp
