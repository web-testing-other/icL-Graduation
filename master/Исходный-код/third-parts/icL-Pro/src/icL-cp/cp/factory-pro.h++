#ifndef ICL_CP_FACTORY_H
#define ICL_CP_FACTORY_H

#include <icL-il/main/cp-factory.h++>



namespace icL::cp {

class Factory : public core::il::CpFactory
{
    // CpFactory interface
public:
    core::il::CP * create(
      core::il::InterLevel * il, const core::il::CodeFragment & code) override;
};

}  // namespace icL::cp

#endif // ICL_CP_FACTORY_H
