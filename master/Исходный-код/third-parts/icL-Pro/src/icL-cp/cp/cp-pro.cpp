#include "cp-pro.h++"

#include "keyword-packer-pro.h++"

#include <icL-il/main/interlevel.h++>

#include <icL-ce/literal/functional/property-pro.h++>
#include <icL-ce/literal/functional/property.h++>



namespace icL::cp {

CP::CP(
  core::il::InterLevel * il, IFlyer * flyer,
  const core::il::CodeFragment & code)
    : core::cp::CP(il, flyer ? flyer : new icL(il, code), code) {
    using core::cp::ResultType::ResultType;

    proxies.insert(ResultType::Key, &CP::pack_key);
    proxies.insert(ResultType::Property, &CP::pack_property);
}

core::il::CE * CP::pack_key(const PackData & pd) {
    static KeywordPacker kp;
    return kp.pack_key(pd);
}

core::il::CE * CP::pack_property(const PackData & pd) {
    return new ce::Property{pd.il, ce::proPrefix::None, pd.result.data["name"]};
}

}  // namespace icL::cp
