#ifndef KEYWORDPACKER_H
#define KEYWORDPACKER_H

#include <icL-il/main/ce.h++>

#include <icL-ce/base/main/ce.h++>

#include <icL-cp/cp/keyword-packer.h++>



namespace icL::cp {

using core::cp::PackData;

class KeywordPacker : public core::cp::KeywordPacker
{
public:
    KeywordPacker();

    static core::il::CE * pack_listen(const PackData & pd);
    static core::il::CE * pack_notify(const PackData & pd);

    // singletons and commands

    static core::il::CE * pack_Alert(const PackData & pd);
    static core::il::CE * pack_By(const PackData & pd);
    static core::il::CE * pack_Cookies(const PackData & pd);
    static core::il::CE * pack_DB(const PackData & pd);
    static core::il::CE * pack_DBManager(const PackData & pd);
    static core::il::CE * pack_Document(const PackData & pd);
    static core::il::CE * pack_DSV(const PackData & pd);
    static core::il::CE * pack_File(const PackData & pd);
    static core::il::CE * pack_Files(const PackData & pd);
    static core::il::CE * pack_icL(const PackData & pd);
    static core::il::CE * pack_Key(const PackData & pd);
    static core::il::CE * pack_Make(const PackData & pd);
    static core::il::CE * pack_Mouse(const PackData & pd);
    static core::il::CE * pack_Move(const PackData & pd);
    static core::il::CE * pack_Request(const PackData & pd);
    static core::il::CE * pack_Query(const PackData & pd);
    static core::il::CE * pack_Session(const PackData & pd);
    static core::il::CE * pack_Sessions(const PackData & pd);
    static core::il::CE * pack_Tab(const PackData & pd);
    static core::il::CE * pack_Tabs(const PackData & pd);
    static core::il::CE * pack_View(const PackData & pd);
    static core::il::CE * pack_Window(const PackData & pd);
    static core::il::CE * pack_Windows(const PackData & pd);

    // types

    static core::il::CE * pack_cookie(const PackData & pd);
    static core::il::CE * pack_database(const PackData & pd);
    static core::il::CE * pack_document(const PackData & pd);
    static core::il::CE * pack_element(const PackData & pd);
    static core::il::CE * pack_file(const PackData & pd);
    static core::il::CE * pack_handler(const PackData & pd);
    static core::il::CE * pack_lambda_js(const PackData & pd);
    static core::il::CE * pack_lambda_sql(const PackData & pd);
    static core::il::CE * pack_listener(const PackData & pd);
    static core::il::CE * pack_query(const PackData & pd);
    static core::il::CE * pack_session(const PackData & pd);
    static core::il::CE * pack_tab(const PackData & pd);
    static core::il::CE * pack_window(const PackData & pd);

    // other literal

    static core::il::CE * pack_js_value(const PackData & pd);
    static core::il::CE * pack_js_file(const PackData & pd);
    static core::il::CE * pack_js_code(const PackData & pd);
    static core::il::CE * pack_js_literal(const PackData & pd);
    static core::il::CE * pack_element_literal(const PackData & pd);
    static core::il::CE * pack_css_literal(const PackData & pd);
    static core::il::CE * pack_xpath_literal(const PackData & pd);
    static core::il::CE * pack_link_literal(const PackData & pd);
    static core::il::CE * pack_links_literal(const PackData & pd);
    static core::il::CE * pack_tag_literal(const PackData & pd);
    static core::il::CE * pack_tags_literal(const PackData & pd);
    static core::il::CE * pack_input_literal(const PackData & pd);
    static core::il::CE * pack_field_literal(const PackData & pd);
};

}  // namespace icL::cp

#endif // KEYWORDPACKER_H
