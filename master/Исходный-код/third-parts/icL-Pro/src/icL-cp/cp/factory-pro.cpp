#include "factory-pro.h++"

#include "cp-pro.h++"



namespace icL::cp {

core::il::CP * Factory::create(
  core::il::InterLevel * il, const core::il::CodeFragment & code) {
    return new CP(il, nullptr, code);
}

}  // namespace icL::cp
