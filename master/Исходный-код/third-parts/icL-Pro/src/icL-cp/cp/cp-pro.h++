#ifndef cp_CP
#define cp_CP

#include "../flyer/icL-pro.h++"
#include "../result/structs-pro.h++"

#include <icL-il/auxiliary/source-of-code.h++>
#include <icL-il/main/node.h++>
#include <icL-il/structures/code-fragment.h++>

#include <icL-ce/base/main/ce.h++>

#include <icL-cp/cp/cp.h++>



namespace icL {

namespace ce {
class Keyword;
}

namespace cp {

using core::cp::IFlyer;
using core::cp::PackData;

/**
 * @brief The CP class represents a command processor
 */
class icL_pro_cp_EXPORT CP : public core::cp::CP
{
public:
    CP(
      core::il::InterLevel * il, IFlyer * flyer,
      const core::il::CodeFragment & code);

    /**
     * @brief pack_key replaces the original pack_key a swith to custom keyword
     * packer
     * @param pd is the pack data
     * @return a new created network node
     */
    static core::il::CE * pack_key(const PackData & pd);

    /**
     * @brief pack_property replaces to property with prefix support
     * @param pd is the pack data
     * @return a new created network node
     */
    static core::il::CE * pack_property(const PackData & pd);
};

}  // namespace cp
}  // namespace icL

#endif  // cp_CP
