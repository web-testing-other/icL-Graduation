#ifndef cp_FlyResult
#define cp_FlyResult

#include "js-type.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/export/global-pro.h++>
#include <icL-il/structures/position.h++>

#include <icL-cp/result/structs.h++>
#include <icL-cp/result/type.h++>



namespace icL::cp {

struct JsFlyResult : public core::cp::BaseFlyResult
{
    /// \brief type is the type of result
    JsResultType type = JsResultType::CommandEnd;
};


}  // namespace icL::cp

#endif  // cp_FlyResult
