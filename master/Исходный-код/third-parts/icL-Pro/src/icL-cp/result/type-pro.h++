#ifndef cp_ResultType
#define cp_ResultType

#include <icL-cp/result/type.h++>

namespace icL::cp {

namespace ResultTypeNM {
/**
 * @brief The ResultType enum represents a result type of a icL fly
 */
enum ResultType {
    First = core::cp::ResultType::Last,
    Element,  ///< it's an element literal
    JsValue,  ///< it's a js:value{..} literal
    JsCode,   ///< it's a js{..} literal
    JsFile,   ///< it's a js:file[..] literal
};
}  // namespace ResultTypeNM

using ResultTypeNM::ResultType;

}  // namespace icL::cp

#endif  // cp_ResultType
