#ifndef cp_Flyer_icL
#define cp_Flyer_icL

#include "../result/type-pro.h++"
#include "js.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/structures/position.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-service/main/values/inode-pro.h++>

#include <icL-cp/flyer/flyer.h++>



namespace icL::core::cp {
struct FlyResult;
}

namespace icL::cp {

using core::cp::FlyResult;

/**
 * @brief The Flyer class represents an flying cursor
 */
class icL_pro_cp_EXPORT icL
    : public core::cp::Flyer
    , virtual public JS
    , virtual public service::INode
{
public:
    icL(core::il::InterLevel * il, const core::il::CodeFragment & code);

    /// `js...`
    void flyJs(FlyResult & result);

    /// `method:modifier[selector]`
    void flyElement(FlyResult & result);

private:
    /**
     * @brief flyToNext flies to next not-white character
     * @return the next non-white character or 0x0 at end of source
     */
    icChar flyToNext();

    /// `js:value{..}`
    void flyJsValue(FlyResult & result);

    /// `js@target{..}` or `js{..}`
    void flyJsCode(FlyResult & result, const il::TargetData & target);

    /// `js:file[..]`
    void flyJsFile(FlyResult & result);

    /**
     * @brief toTarget extract target data from variant
     * @param value is the target container
     * @return the target data extracted from container
     */
    static il::TargetData toTarget(const icVariant & value);
};

}  // namespace icL::cp

#endif  // cp_Flyer_icL
