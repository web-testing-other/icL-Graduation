#ifndef cp_Flayer_JS
#define cp_Flayer_JS

#include <icL-il/export/global-pro.h++>

#include <icL-service/main/values/inode.h++>

#include <icL-cp/flyer/i-flyer.h++>
#include <icL-cp/result/structs-pro.h++>


namespace icL::cp {

class icL_pro_cp_EXPORT JS : virtual public core::cp::IFlyer
{
    JsFlyResult previous;

public:
    JS();

    // IFlyer interface
public:
    JsFlyResult jsFlyNext(bool packContexts = true);

private:
    /**
     * @brief flyToNext flies to next not-white character
     * @return the next non-white character or 0x0 at end of source
     */
    icChar flyToNext();

    /**
     * @brief finishContext finish a context parsing
     * @param result is the fly result to evaluate
     */
    void finishContext(JsFlyResult & result);

    void flySlash(JsFlyResult & result);
    void flyIdentifier(JsFlyResult & result);
    void flyString(JsFlyResult & result);
    void flyRoundOp(JsFlyResult & result);
    void flyRoundCl(JsFlyResult & result);
    void flySquareOp(JsFlyResult & result);
    void flySquareCl(JsFlyResult & result);
    void flyCurlyOp(JsFlyResult & result);
    void flyCurlyCl(JsFlyResult & result);
    void flyCommandEnd(JsFlyResult & result);
    void flyToken(JsFlyResult & result);
};

}  // namespace icL::cp

#endif  // cp_Flayer_JS
