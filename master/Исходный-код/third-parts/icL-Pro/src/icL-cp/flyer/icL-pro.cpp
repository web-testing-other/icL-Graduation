#include "icL-pro.h++"

#include "structs-pro.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/auxiliary/source-of-code.h++>
#include <icL-il/export/types.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-memory/state/memory.h++>

//#include <iostream>



namespace icL::cp {

icL::icL(core::il::InterLevel * il, const core::il::CodeFragment & code)
    : core::cp::Flyer(il, code) {}

void icL::flyJs(FlyResult & result) {
    icChar sibling = source()->next();

    if (sibling == '-') {
        core::il::Position modPos = source()->getPosition();
        icString           mod    = flyName(this);

        if (mod == "value") {
            flyJsValue(result);
        }
        else if (mod == "file") {
            flyJsFile(result);
        }
        else {
            core()->vm->cp_sig(
              issueWithPath(modPos) % "Unknown modifier `" % mod %
              "`, expected `value` or `file`");
        }
    }
    else if (sibling == '{') {
        flyJsCode(result, pro()->server->getCurrentTarget());
    }
    else if (sibling == '@' || sibling == '#') {
        icString name = sibling == '#' ? "#" : flyName(this);

        if (name.isEmpty()) {
            name = "@";
        }

        if (source()->next() == '{') {
            flyJsCode(
              result,
              toTarget(
                core()->mem->stackIt().getContainerForVariable(name)->getValue(
                  name)));
        }
        else {
            core()->vm->cp_sig(
              issueWithPath(source()->getPosition()) % "Unexpected `" %
              source()->current() % "`, expected '{'");
        }
    }
    else {
        core()->vm->cp_sig(
          issueWithPath(source()->getPosition()) % "Unexpected `" % sibling %
          "`, expected `-` or `{`");
        source()->prev();
    }
}

void icL::flyElement(FlyResult & result) {
    result.type              = ResultType::Element;
    result.data["modifiers"] = icL::getModifiers(this);

    icChar sibling = source()->next();

    if (sibling == '#' || sibling == '@') {
        auto name = flyName(this);

        result.data["scope"] = sibling % name;

        sibling = source()->next();
    }
    else {
        result.data["scope"] = icString{};
    }

    if (sibling == '[') {
        auto beginPos = source()->getPosition();

        FlyResult next = flyNext();
        while (next.type != core::cp::ResultType::LimitedCl &&
               !source()->atEnd() && core()->vm->hasOkState()) {
            next = flyNext();
        }

        source()->prev();
        auto endPos = source()->getPosition();
        source()->next();

        if (beginPos.line != endPos.line) {
            core()->vm->syssig(
              "Unexpected new line character in element literal");
        }
        else {
            result.data.insert("code-begin", beginPos);
            result.data.insert("code-end", endPos);
        }
    }
    else {
        core()->vm->syssig(
          "Unexpected `" % sibling % "` character, expected `[`");
    }

    result.end = source()->getPosition();
}

void icL::flyJsValue(core::cp::FlyResult & result) {
    icChar       sibling = source()->next();
    core::il::Position start   = source()->getPosition();

    if (sibling == '{') {
        result.data.insert("getter-begin", source()->getPosition() + 1);

        JsFlyResult current;

        while ((current = jsFlyNext()).type != JsResultType::CurlyCl &&
               !source()->atEnd() && core()->vm->hasOkState()) {
            if (current.type == JsResultType::CommandEnd) {
                if (result.data.contains("setter-begin")) {
                    core()->vm->cp_sig(
                      issueWithPath(source()->getPosition()) %
                      "JavaScript value can contains just a getter and maybe a "
                      "setter");
                }
                else {
                    result.data.insert("getter-end", source()->getPosition());
                    result.data.insert(
                      "setter-begin", source()->getPosition() + 1);
                }
            }
        }

        if (current.type != JsResultType::CurlyCl) {
            core()->vm->cp_sig(
              issueWithPath(start) % "End of js:value was not found");
        }

        if (result.data.contains("setter-begin")) {
            result.data.insert("setter-end", source()->getPosition());
        }
        else {
            result.data.insert("getter-end", source()->getPosition());
        }

        result.type = ResultType::JsValue;
    }
    else {
        core()->vm->cp_sig(
          issueWithPath(source()->getPosition()) % "Unexpected `" %
          source()->current() % "`, expected `{`");
    }
}

void icL::flyJsCode(
  core::cp::FlyResult & result, const il::TargetData & target) {
    result.type = ResultType::JsCode;

    if (target.sessionId.isEmpty()) {
        core()->vm->cp_sig(
          issueWithPath(result.begin) %
          "Wrong target, a compatible target is a document, tab, window or "
          "session");
    }
    else {
        core::il::Position curlyOpenBracketPosition = source()->getPosition();

        source()->next();
        result.data.insert("begin", source()->getPosition());
        source()->prev();

        result.data.insert("target-session", target.sessionId);
        result.data.insert("target-window", target.windowId);

        JsFlyResult current;

        while ((current = jsFlyNext()).type != JsResultType::CurlyCl &&
               !source()->atEnd() && core()->vm->hasOkState())
            ;

        if (current.type != JsResultType::CurlyCl) {
            core()->vm->cp_sig(
              issueWithPath(curlyOpenBracketPosition) %
              "End of js code was not found");
        }

        source()->prev();
        result.data.insert("end", source()->getPosition());
        source()->next();
    }
}

void icL::flyJsFile(core::cp::FlyResult & result) {
    result.type = ResultType::JsFile;

    if (source()->next() == '[') {
        flyLiteral(this, result);
    }
    else {
        core()->vm->cp_sig(
          issueWithPath(source()->getPosition()) % "Unexpected `" %
          source()->current() % "`, expected `[`");
    }
}

il::TargetData icL::toTarget(const icVariant & value) {
    if (value.is(proType::Document)) {
        return *to<il::Document>(value).data;
    }
    else if (value.is(proType::Tab)) {
        return *to<il::Tab>(value).data;
    }
    else if (value.is(proType::Window)) {
        return *to<il::Window>(value).data;
    }
    else if (value.is(proType::Session)) {
        return *to<il::Session>(value).data;
    }

    return {};
}

}  // namespace icL::cp
