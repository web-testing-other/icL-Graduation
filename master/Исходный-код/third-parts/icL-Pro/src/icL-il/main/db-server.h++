#ifndef il_DBServer
#define il_DBServer

#include <icL-il/export/global-pro.h++>



class icString;
class icVariant;

namespace icL {

namespace ce {
class CE;
}

namespace il {

struct DBTarget;

/**
 * @brief The DBServer class is a interface of a data base realization
 */
class icL_pro_il_EXPORT DBServer
{
public:
    virtual ~DBServer() = default;

    /// `DBManager.connect (server : string) : db`
    virtual DBTarget connect(const icString & server) = 0;

    /// `DBManager.connect (server : string, user : string, password : string) :
    /// db`
    virtual DBTarget connect(
      const icString & server, const icString & user,
      const icString & password) = 0;

    /// `DBManager.openSQLite (path : string) : db`
    virtual DBTarget openSQLite(const icString & path) = 0;

    /**
     * @brief pushTarget push a new target to state stack
     * @param target is the target of current object (db or query)
     */
    virtual void pushTarget(const DBTarget & target) = 0;

    /**
     * @brief popTarget pops the last pushed target
     */
    virtual void popTarget() = 0;

    /**
     * @brief getCurrentTarget gets the current database and query target
     * @return the current database and query target
     */
    virtual DBTarget getCurrentTarget() = 0;

    /// `db.close () : void`
    virtual void close() = 0;

    /// `db.query (q : Code) : Query`
    virtual DBTarget query(const icString & query) = 0;

    /// `query.exec () : bool`
    virtual bool exec() = 0;

    /**
     * @brief isExecuted checks if current target was executed
     * @return true if current target was executed, otherwise false
     */
    virtual bool isExecuted() = 0;

    /// `query.first () : bool`
    virtual bool first() = 0;

    /// `query.get (field : string) : any`
    virtual icVariant get(const icString & field) = 0;

    /// `query.getError () : string`
    virtual icString getError() = 0;

    /// `query.getLength () : int`
    virtual int getLength() = 0;

    /// `query.getRowsAffected () : int`
    virtual int getRowsAffected() = 0;

    /// `query.last () : bool`
    virtual bool last() = 0;

    /// `query.next () : bool`
    virtual bool next() = 0;

    /// `query.previous () : bool`
    virtual bool previous() = 0;

    /// `query.seek (i : int, relative = false) : bool`
    virtual bool seek(int i, bool relative = false) = 0;

    /// `query.set (field : string, value : any) : void`
    virtual void set(const icString & field, const icVariant & value) = 0;
};

}  // namespace il
}  // namespace icL

#endif  // il_DBServer
