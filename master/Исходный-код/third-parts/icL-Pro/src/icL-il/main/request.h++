#ifndef pro_il_Request
#define pro_il_Request

#include <icL-il/export/global-pro.h++>



class icString;
class icStringList;

namespace icL::il {

struct Tab;

/**
 * @brief The Request struct represent a `Request` singleton
 */
class icL_pro_il_EXPORT Request
{
public:
    /// `Request.confirm (text : string) : void`
    virtual void confirm(const icString & text) = 0;

    /// `Request.ask (question : string) : bool`
    virtual bool ask(const icString & question) = 0;

    /// `Request.int (text : string) : int`
    virtual int int_(const icString & text) = 0;

    /// `Request.double (text : string) : double`
    virtual double double_(const icString & text) = 0;

    /// `Request.string (text : string) : string`
    virtual icString string(const icString & text) = 0;

    /// `Request.list (text : string) : list`
    virtual icStringList list(const icString & text) = 0;

    /// `Request.tab (text : string) : tab`
    virtual Tab tab(const icString & text) = 0;

    /// `notify-Xs "message" : void`
    virtual void notify(int secunds, const icString & message) = 0;
};
}  // namespace icL::il

#endif  // pro_il_Request
