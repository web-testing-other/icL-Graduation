#ifndef icL_pro_il_InterLevel
#define icL_pro_il_InterLevel

namespace icL::il {

class FrontEnd;
class DBServer;
class FileServer;
class Listen;
class Request;

struct InterLevel
{
    ~InterLevel() {}

    /// \brief server is a pointer to synchronization server
    FrontEnd * server = nullptr;

    /// \brief db is the pointer to database server
    DBServer * db = nullptr;

    /// \brief file is a pointer to the file service
    FileServer * file = nullptr;

    /// \brief listen is a syncronization server
    Listen * listen = nullptr;

    /// \brief request is a "requesting" server
    Request * request = nullptr;
};

}  // namespace icL::il

#endif  // icL_pro_il_InterLevel
