#ifndef il_Enums
#define il_Enums

namespace icL::il {

namespace SelectorsNM {   ///< \brief available selectors for web elements
enum Selectors {          ///< \brief available selectors for web elements
    CssSelector     = 1,  ///< `css selector`
    LinkText        = 2,  ///< `link text`
    PartialLinkText = 3,  ///< `partial link text`
    TagName         = 4,  ///< `tag name`
    XPath           = 5,  ///< `xpath`
    Input           = 6,  ///< `input`
    Field           = 7   ///< `field`
};
}  // namespace SelectorsNM

namespace KeysNM {  ///< \brief The namespace Keys contains the key modifiers
enum Keys {         ///< \brief The Keys enum enums key modifiers
    Ctrl  = 1,      ///< Control modifier
    Shift = 2,      ///< Shift modifier
    Alt   = 4       ///< Alt modifier
};
}  // namespace KeysNM

namespace MouseButtonsNM {  ///< \brief mouse buttons
enum MouseButtons {         ///< \brief mouse buttons
    Left   = 1,             ///< the left button
    Middle = 2,             ///< the middle button
    Right  = 3              ///< the right button
};
}

namespace MoveTypesNM {  ///< \brief move types
enum MoveTypes {         ///< \brief move types
    Teleport  = 1,       ///< instant move
    Linear    = 2,       ///< linear intepolation move (x)
    Quadratic = 3,       ///< quadratic interpolation move (x*x)
    Cubic     = 4,       ///< qubic interpolation move (x*x*x)
    Bezier    = 5        ///< cubic bezier curve move
};
}

namespace FileTypesNM {  ///< \brief file types
enum FileTypes {         ///< \brief file types
    None = 1,            ///< none (invalid)
    CSV  = 1,            ///< .csv (CSV)
    TSV  = 2             ///< .tsv (TSV)
};
}

using FileTypesNM::FileTypes;
using KeysNM::Keys;
using MouseButtonsNM::MouseButtons;
using MoveTypesNM::MoveTypes;
using SelectorsNM::Selectors;

}  // namespace icL::il

#endif  // il_Enums
