#ifndef il_AjaxHandler
#define il_AjaxHandler

#include <icL-il/export/global-pro.h++>



namespace icL {

namespace memory {
class Object;
}

namespace il {

/**
 * @brief The AjaxHandler class define an interface for an AJAX handling class
 */
class icL_pro_il_EXPORT AjaxHandler
{
public:
    virtual ~AjaxHandler() = default;

    /**
     * @brief handle handle a change in AJAX requests
     * @param response is the response of request
     * @param count is the count of active requests
     */
    virtual void handle(const memory::Object & response, int count) = 0;
};

}  // namespace il
}  // namespace icL

#endif  // il_AjaxHandler
