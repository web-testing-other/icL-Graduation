#ifndef il_FrontEnd
#define il_FrontEnd

#include <icL-il/export/global-pro.h++>



class icRect;
class icVariant;
class QJsonObject;
class icString;
class icStringList;
class icRegEx;

template <typename T>
class icList;

using icVariantList = icList<icVariant>;
using icRects       = icList<icRect>;

namespace icL::il {

class CE;
struct Signal;

struct MouseData;
struct ClickData;
struct HoverData;
struct CookieData;
struct TargetData;
struct Element;
struct Elements;

class AjaxHandler;

/**
 * @brief The FrontEnd class represents an interface to a icL or W3C driver
 */
class icL_pro_il_EXPORT FrontEnd
{
public:
    virtual ~FrontEnd() = default;

    // Working with targets (pairs of session-id and window-id)

    /**
     * @brief pushTarget pusher a target to server
     * @param target is the target to push
     */
    virtual void pushTarget(const TargetData & target) = 0;

    /**
     * @brief popTarget pops the last added target
     */
    virtual void popTarget() = 0;

    /**
     * @brief getCurrentTarget get a target with current session and window
     * @return a target with current session and window
     */
    virtual TargetData getCurrentTarget() = 0;

    // Session functions

    /**
     * @brief newSession releases POST /session
     */
    virtual void newSession() = 0;

    /**
     * @brief setCurrentSession switches current session
     * @param target is the session target to switch to
     */
    virtual void switchSession() = 0;

    /**
     * @brief deleteSession releases DELETE /session/{session id}
     */
    virtual void closeSession() = 0;

    /**
     * @brief implicitTimeout releases GET /session/{session id}/timeouts
     * @return {returned object}.implicit
     */
    virtual int implicitTimeout() = 0;

    /**
     * @brief pageLoadTimeout releases GET /session/{session id}/timeouts
     * @return {returned object}.pageLoad
     */
    virtual int pageLoadTimeout() = 0;

    /**
     * @brief scriptTimeout releases GET /session/{session id}/timeouts
     * @return {returned object}.script
     */
    virtual int scriptTimeout() = 0;

    /**
     * @brief setImplicitTimeout releases POST /session/{session id}/timeouts
     * @param ms - {sended object}.implicit
     */
    virtual void setImplicitTimeout(int ms) = 0;

    /**
     * @brief setPageLoadTimeOut releases POST /session/{session id}/timeouts
     * @param ms - {sended object}.pageLoad
     */
    virtual void setPageLoadTimeOut(int ms) = 0;

    /**
     * @brief setScriptTimeout releases POST /session/{session id}/timeouts
     * @param ms - {sended object}.script
     */
    virtual void setScriptTimeout(int ms) = 0;

    // icL sessions functions

    /**
     * @brief switchSessionTo switches current session
     * @param sessionId is the session id to focus
     */
    virtual void focusSession(const TargetData & sessionId) = 0;

    /**
     * @brief sessions gets the list of current active sessions
     * @return a list of current active sessions
     */
    virtual icList<TargetData> getSessions() = 0;

    // Navigation functions

    /**
     * @brief setUrl releases POST /session/{session id}/url
     * @param url is {sended object}.url
     */
    virtual void load(const icString & url) = 0;

    /**
     * @brief url releases GET /session/{session id}/url
     * @return {returned object}.value
     */
    virtual icString url() = 0;

    /**
     * @brief back releases POST /session/{session id}/back
     */
    virtual void back() = 0;

    /**
     * @brief forward releases POST /session/{session id}/forward
     */
    virtual void forward() = 0;

    /**
     * @brief refresh releases POST /session/{session id}/refresh
     */
    virtual void refresh() = 0;

    /**
     * @brief title releases GET /session/{session id}/title
     * @return {returned object}.value
     */
    virtual icString title() = 0;

    // Windows and frames

    /**
     * @brief window releases GET /session/{session id}/window
     * @return {returned object}.value
     */
    virtual icString window() = 0;

    /**
     * @brief newWindow opens a new window
     * @return the target to a new window
     */
    virtual TargetData newWindow() = 0;

    /**
     * @brief closeWindow releases DELETE /session/{session id}/window
     */
    virtual void closeWindow() = 0;

    /**
     * @brief focusWindow releases POST /session/{session id}/window
     * @param id is {sended object}.handle
     */
    virtual void focusWindow() = 0;

    /**
     * @brief windows releases GET /session/{session id}/window/handles
     * @return {returned object}.value
     */
    virtual icList<TargetData> getWindows() = 0;

    /**
     * @brief switchToFrame releases POST /session/{session id}/frame
     * @param id is {sended object}.id
     */
    virtual void switchToFrame(int id) = 0;

    /**
     * @brief switchToFrame releases POST /session/{session id}/frame
     * @param id is {sended object}.id
     */
    virtual void switchToFrame(const Element & el) = 0;
    /**
     * @brief switchToParent releases POST /session/{session id}/frame/parent
     */
    virtual void switchToParent() = 0;

    // Window move and resize

    /**
     * @brief windowRect releases GET /session/{session id}/window/rect
     * @return {returned object}.value
     */
    virtual icRect getWindowRect() = 0;

    /**
     *
     * @brief setWindowRect releases POST /session/{session id}/window/rect
     * @param rect is {sended object}
     */
    virtual icRect setWindowRect(const icRect & rect) = 0;

    /**
     * @brief maximize releases POST /session/{session id}/window/maximize
     */
    virtual void maximize() = 0;

    /**
     * @brief minimize releases POST /session/{session id}/window/minimize
     */
    virtual void minimize() = 0;

    /**
     * @brief fullscreen releases POST /session/{session id}/window/fullscreen
     */
    virtual void fullscreen() = 0;

    /**
     * @brief restore restores window from maximized / minimised / fullscreen
     * states
     */
    virtual void restore() = 0;

    // Find elements

    /**
     * @brief The SearchElement struct respresents a search of a element
     */
    struct SearchElement
    {
        /// represents a selector type
        int selectorType;
        /// repesent the search time in ms
        int searchTime = -1;
        /// `-try` modifier presence
        bool tryModifier = false;
        ///< parent element
        const Element * element = nullptr;
        ///< the name of selector
        icString * tagName;
        ///< Fixing the proximity interval
        double minP = -0.5, maxP = -0.5;
    };

    /**
     * @brief query releases POST /session/{session id}/element or POST
     * /session/{session id}/element/{element id}/element
     * @param element is element to find in or nullptr
     * @param str is selector data
     * @return {returned object}.value
     */
    virtual Element query(SearchElement & search, const icString & str) = 0;
    virtual Element query(SearchElement & search, const icRegEx & rex)  = 0;

    /**
     * @brief The SearchElement struct respresents a search of some elements
     */
    struct SearchElements
    {
        ///< will use the same fileds
        SearchElement & se;

        ///< minimum number of elements
        int min = -1;
        ///< maximum number of elements
        int max = -1;
    };

    /**
     * @brief queryAll releases POST /session/{session id}/element or POST
     * /session/{session id}/element/{element id}/element
     * @param selectorType is selector type in icL format
     * @param element is element to find in or nullptr
     * @param str is selector data
     * @return {returned object}.value
     */
    virtual Elements queryAll(
      const SearchElements & search, const icString & str) = 0;
    virtual Elements queryAll(
      const SearchElements & search, const icRegEx & rex) = 0;

    // Elements manipulation

    /**
     * @brief empty checks if element is empty
     * @param el is element to check
     * @return true if element is empty, otherwise false
     */
    virtual bool empty(const Elements & el) = 0;

    /**
     * @brief length gets the count of elements in collection
     * @param el is the collection of elements
     * @return count of elements in collection
     */
    virtual int length(const Elements & el) = 0;

    /**
     * @brief active releases GET /session/{session id}/element/active
     * @return {returned object}.value
     */
    virtual Element active() = 0;

    /**
     * @brief clickable checks if a element is clickable
     * @param el is the element to check
     * @return true if element is clickable, otherwise false
     */
    virtual bool clickable(const Element & el) = 0;

    /**
     * @brief visible checks if a element is visible
     * @param el is the element to check
     * @return true if it is visible, otherwise false
     */
    virtual bool visible(const Element & el) = 0;

    /**
     * @brief selected releases GET /session/{session id}/element/{element
     * id}/selected
     * @param el is {element id}
     * @return {returned object}.value
     */
    virtual bool selected(const Element & el) = 0;

    /**
     * @brief attribute releases GET /session/{session id}/element/{element
     * id}/attribute/{name}
     * @param el is {element id}
     * @param name is {name}
     * @return {returned object}.value
     */
    virtual icString     attr(const Element & el, const icString & name)  = 0;
    virtual icStringList attr(const Elements & el, const icString & name) = 0;

    /**
     * @brief property releases GET /session/{session id}/element/{element
     * id}/property/{name}
     * @param el is {element id}
     * @param name is {name}
     * @return {returned object}.value
     */
    virtual icVariant prop(const Element & el, const icString & name)  = 0;
    virtual icVariant prop(const Elements & el, const icString & name) = 0;

    /**
     * @brief css releases GET /session/{session id}/element/{element
     * id}/css/{property name}
     * @param el is {element id}
     * @param name is {property name}
     * @return {returned object}.value
     */
    virtual icString css(const Element & el, const icString & name) = 0;

    /**
     * @brief text releases GET /session/{session id}/element/{element id}/text
     * @param el is {element id}
     * @return {returned object}.value
     */
    virtual icString     text(const Element & el)  = 0;
    virtual icStringList text(const Elements & el) = 0;

    /**
     * @brief name releases GET /session/{session id}/element/{element id}/name
     * @param el is {element id}
     * @return {returned object}.value
     */
    virtual icString     name(const Element & el)  = 0;
    virtual icStringList name(const Elements & el) = 0;

    /**
     * @brief rect releases GET /session/{session id}/element/{element id}/rect
     * @param el is {element id}
     * @return {returned object}.value
     */
    virtual icRect  rect(const Element & el)  = 0;
    virtual icRects rect(const Elements & el) = 0;

    /**
     * @brief enabled releases GET /session/{session id}/element/{element
     * id}/enabled
     * @param el is {element id}
     * @return {returned object}.value
     */
    virtual bool enabled(const Element & el) = 0;

    /**
     * @brief add adds elements form `toAdd` to `el`
     * @param el is the target collection
     * @param toAdd is the source collection
     * @return a new collection with all elements of `el` and `toAdd`
     */
    virtual Elements add_11(const Element & el, const Element & toAdd)   = 0;
    virtual Elements add_m1(const Elements & el, const Element & toAdd)  = 0;
    virtual Elements add_mm(const Elements & el, const Elements & toAdd) = 0;

    /**
     * @brief click releases POST /session/{session id}/element/{element
     * id}/click
     * @param el is {element id}
     */
    virtual void click(const Element * el, const ClickData & data) = 0;

    /**
     * @brief clear releases POST /session/{session id}/element/{element
     * id}/clear
     * @param el is {element id}
     */
    virtual void clear(const Element & el) = 0;

    /**
     * @brief copy copies the collection to a new one
     * @param el is the collection to copy
     * @return a new collection
     */
    virtual Element  copy(const Element & el)  = 0;
    virtual Elements copy(const Elements & el) = 0;

    /**
     * @brief fastType types text as fast as possible
     * @param el is element to focus
     * @param text is the text to type
     */
    virtual void fastType(const Element & el, const icString & text) = 0;

    /**
     * @brief forceClick simulates a click without validation
     * @param el is the element to click
     * @param data is data about click simulation
     */
    virtual void forceClick(const Element & el, const ClickData & data) = 0;

    /**
     * @brief superClick simulates a click whatever the element is not visible
     * @param el is the element to click
     */
    virtual void superClick(const il::Element & el) = 0;

    /**
     * @brief forceType add value to field using JavaScript
     * @param el is the element to focus in
     * @param text is the text to type
     */
    virtual void forceType(const Element & el, const icString & text) = 0;

    /**
     * @brief hover moves mouse over element
     * @param el is the element to hover
     * @param hover is data about mouse move
     */
    virtual void hover(const Element * el, const HoverData & hover) = 0;

    /**
     * @brief keyDown simutates key down event
     * @param el is the element to focus before
     * @param modifiers is modifiers to simulate
     * @param keys is keys to simulate
     */
    virtual void keyDown(
      const Element * el, int modifiers, const icString & keys) = 0;

    /**
     * @brief keyPress simulate a key down and up event
     * @param el is the element to focus before
     * @param modifiers is modifiers to simulate
     * @param delay is the delay of key press
     * @param keys is keys to simulate
     */
    virtual void keyPress(
      const Element * el, int modifiers, int delay, const icString & keys) = 0;

    /**
     * @brief keyUp simulate a key up event
     * @param el is the element to focus before
     * @param modifiers is modifiers to simulate
     * @param keys is keys to simulate
     */
    virtual void keyUp(
      const Element * el, int modifiers, const icString & keys) = 0;

    /**
     * @brief mouseDown simulate a mouse down event on element
     * @param el is the element to calculate screen coodinate
     * @param data is the data of mouse event
     */
    virtual void mouseDown(const Element * el, const MouseData & data) = 0;

    /**
     * @brief mouseUp simulates a mouse up event on element
     * @param el is the element to calculate screen coodinate
     * @param data is the data of mouse event
     */
    virtual void mouseUp(const Element * el, const MouseData & data) = 0;

    /**
     * @brief paste focus the element and simulate a Ctrl+V
     * @param el is the element to focus
     * @param text is the text to copy to clipboard first
     */
    virtual void paste(const Element & el, const icString & text) = 0;

    /**
     * @brief value releases POST /session/{session id}/element/{element
     * id}/value
     * @param el is {element id}
     * @param modifiers are modifiers to simulate
     * @param keys - {sended object}.value
     */
    virtual void sendKeys(
      const Element * el, int modifiers, const icString & keys) = 0;

    // document

    /**
     * @brief source releases GET /session/{session id}/source
     * @return {returned object}.value
     */
    virtual icString source() = 0;

    /**
     * @brief executeSync releases POST /session/{session id}/execute/sync
     * @param code is {sended object}.script
     * @param args is {sended object}.args
     * @return {returned object}.value
     */
    virtual icVariant executeSync(
      const icString & code, const icVariantList & args) = 0;

    /**
     * @brief executeAsync releases POST /session/{session id}/execute/async
     * @param code is {sended object}.script
     * @param args is {sended object}.args
     */
    virtual void executeAsync(
      const icString & code, const icVariantList & args) = 0;

    /**
     * @brief setUserScript sets a user script for current tab
     * @param code is the code of user script
     */
    virtual void setUserScript(const icString & code) = 0;

    /**
     * @brief setAlwaysScript set a user script for all tabs in sessions
     * (inluding new)
     * @param code is the code of user script
     */
    virtual void setAlwaysScript(const icString & code) = 0;

    // cookie

    /**
     * @brief cookies releases GET /session/{session id}/cookie
     * @return {returned object}.value
     */
    virtual icStringList getCookies() = 0;

    /**
     * @brief cookie releases GET /session/{session id}/cookie/{name}
     * @param name is {name}
     * @return {returned object}.value
     */
    virtual CookieData loadCookie(const icString & name) = 0;

    /**
     * @brief udpCookie releases POST /session/{session id}/cookie
     * @param obj is {sended object}.cookie
     */
    virtual void saveCookie(const CookieData & data) = 0;

    /**
     * @brief deleteCookie releases DELETE /session/{session id}/cookie/{name}
     * @param name is {name}
     */
    virtual void deleteCookie(const icString & name) = 0;

    /**
     * @brief deleteAllCookies releases DELETE /session/{session id}/cookie
     */
    virtual void deleteAllCookies() = 0;

    // Alert

    /**
     * @brief alertDimiss releases POST /session/{session id}/alert/dismiss
     */
    virtual void alertDimiss() = 0;

    /**
     * @brief alertAccept releases POST /session/{session id}/alert/accept
     */
    virtual void alertAccept() = 0;

    /**
     * @brief alertText releases GET /session/{session id}/alert/text
     * @return {returned object}.value
     */
    virtual icString alertText() = 0;

    /**
     * @brief alertSendText releases POST /session/{session id}/alert/text
     * @param text is {sended object}.text
     */
    virtual void alertSendText(const icString & text) = 0;

    // Screenshots

    /**
     * @brief screenshot releases GET /session/{session id}/screenshot
     * @return {returned object}.value
     */
    virtual icString screenshot() = 0;

    /**
     * @brief screenshot releases GET /session/{session id}/element/{element
     * id}/screenshot
     * @param el is {element id}
     * @return {returned object}.value
     */
    virtual icString screenshot(const Element & el) = 0;

    // icL additional methods

    /**
     * @brief at gets nth element
     * @param el is the elements container
     * @param n is the element order number
     * @return a new element object
     */
    virtual Element get(const Elements & el, int n) = 0;

    /**
     * @brief filter filters elements from continer
     * @param el is the elements container
     * @param selector is the selector to match
     * @return a new element object
     */
    virtual Elements filter(const Elements & el, const icString & selector) = 0;

    /**
     * @brief contains filters elements by content
     * @param el is the elements container
     * @param substring is the string to check innerText
     * @return a new element object
     */
    virtual Elements contains(
      const Elements & el, const icString & substring) = 0;

    /**
     * @brief next gets next sibling element
     * @param el is the elements container
     * @return a new element object
     */
    virtual Element next(const Element & el) = 0;

    /**
     * @brief prev gets previous sibling element
     * @param el is the elements container
     * @return a new element object
     */
    virtual Element prev(const Element & el) = 0;

    /**
     * @brief parent gets parent node
     * @param el is the elements container
     * @return a new element object
     */
    virtual Element parent(const Element & el) = 0;

    /**
     * @brief child gets child by index
     * @param el is the elements container
     * @param n is the index of child
     * @return a new element object
     */
    virtual Element child(const Element & el, int n) = 0;

    /**
     * @brief children gets the children of element
     * @param el is the elements container
     * @return a new element object
     */
    virtual Elements children(const Element & el) = 0;

    /**
     * @brief closest gets closest element
     * @param el is elements container
     * @param selector is the selector to match
     * @return a new element object
     */
    virtual Element closest(const Element & el, const icString & selector) = 0;

    // settings

    /**
     * @brief getClickTime gets click time
     * @return the click time
     */
    virtual int getClickTime() = 0;

    /**
     * @brief getMoveTime gets the move time for 100px
     * @return the move time of mouse for hover
     */
    virtual int getMoveTime() = 0;

    /**
     * @brief getPressTime gets the time of a key press
     * @return the time of a key press
     */
    virtual int getPressTime() = 0;

    /**
     * @brief getFlashMode gets the flash mode enabled
     * @return the flash mode flag state
     */
    virtual bool getFlashMode() = 0;

    /**
     * @brief getHumanMode gets the human mode enabled
     * @return the human mode flag state
     */
    virtual bool getHumanMode() = 0;

    /**
     * @brief getSilentMode gets the silent mode enabled
     * @return the silent mode flag state
     */
    virtual bool getSilentMode() = 0;

    /**
     * @brief setClickTime sets the time of click
     * @param clickTime is the new time of click
     */
    virtual void setClickTime(int clickTime) = 0;

    /**
     * @brief setMoveTime sets the time of moving mouse by 100px
     * @param moveTime is the new time of moving mouse by 100px
     */
    virtual void setMoveTime(int moveTime) = 0;

    /**
     * @brief setPressTime sets the time of a key press
     * @param pressTime is the new time of key press
     */
    virtual void setPressTime(int pressTime) = 0;

    /**
     * @brief setFlashMode sets the flash mode
     * @param flashMode is the new value, true will enable it
     */
    virtual void setFlashMode(bool flashMode) = 0;

    /**
     * @brief setHumanMode sets the human mode
     * @param humanMode is the new value, true will enable it
     */
    virtual void setHumanMode(bool humanMode) = 0;

    /**
     * @brief setSilentMode sets the silent mode
     * @param silentMode is the new value, true will enable it
     */
    virtual void setSilentMode(bool silentMode) = 0;

    // log

    /**
     * @brief logError prints a error to error file
     * @param error is the text of error
     */
    virtual void logError(const icString & error) = 0;

    /**
     * @brief logInfo prints a information to info file
     * @param info is the text of information
     */
    virtual void logInfo(const icString & info) = 0;

    // AJAX handling

    /**
     * @brief setAjaxHandler sets up an AJAX handler
     * @param handler is a pointer to a handler
     */
    virtual void setAjaxHandler(AjaxHandler * handler) = 0;

    /**
     * @brief resetAjaxHandler reset AJAX handler pointer to nnullptr
     */
    virtual void resetAjaxHandler() = 0;
};

}  // namespace icL::il

#endif  // il_FrontEnd
