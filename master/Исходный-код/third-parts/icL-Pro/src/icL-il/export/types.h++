#ifndef icL_pro_il_Types
#define icL_pro_il_Types

#include <icL-types/replaces/ic-types-enum.h++>



namespace proTypeNM {
enum proType {  ///< enums icL-Pro specific types
    First = icType::Last,
    Cookie,        ///< a browser cookie desctiption
    DB,            ///< a database target
    Query,         ///< a query to a database
    Element,       ///< a single web element
    Elements,      ///< a set of elements
    File,          ///< a file target
    JsFile,        ///< a js file from resources
    ResourceFile,  ///< a resource file target
    Lambda,        ///< a lambda expression or function
    JsLambda,      ///< a js code fragment
    Listener,      ///< a sync listener
    Handler,       ///< a sync handler
    Session,       ///< a browser session
    Tab,           ///< a browser tab
    Window,        ///< a browser window
    Document,      ///< a dom document
    Last           ///< make it extendable
};
}

using proTypeNM::proType;

#endif  // icL_pro_il_Types
