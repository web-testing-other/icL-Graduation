#ifndef icL_pro_Global
#define icL_pro_Global

#include <icL-types/global/icl-types.h++>

// clang-format off

#if defined(icL_pro_ce_base_LIBRARY)
    #define icL_pro_ce_base_EXPORT icL_Export
#else
    #define icL_pro_ce_base_EXPORT icL_Import
#endif

#if defined(icL_pro_ce_literal_LIBRARY)
    #define icL_pro_ce_literal_EXPORT icL_Export
#else
    #define icL_pro_ce_literal_EXPORT icL_Import
#endif

#if defined(icL_pro_ce_value_base_LIBRARY)
    #define icL_pro_ce_value_base_EXPORT icL_Export
#else
    #define icL_pro_ce_value_base_EXPORT icL_Import
#endif

#if defined(icL_pro_ce_value_browser_LIBRARY)
    #define icL_pro_ce_value_browser_EXPORT icL_Export
#else
    #define icL_pro_ce_value_browser_EXPORT icL_Import
#endif

#if defined(icL_pro_ce_value_system_LIBRARY)
    #define icL_pro_ce_value_system_EXPORT icL_Export
#else
    #define icL_pro_ce_value_system_EXPORT icL_Import
#endif

#if defined(icL_pro_cp_LIBRARY)
    #define icL_pro_cp_EXPORT icL_Export
#else
    #define icL_pro_cp_EXPORT icL_Import
#endif

#if defined(icL_pro_il_LIBRARY)
    #define icL_pro_il_EXPORT icL_Export
#else
    #define icL_pro_il_EXPORT icL_Import
#endif

#if defined(icL_pro_service_cast_LIBRARY)
    #define icL_pro_service_cast_EXPORT icL_Export
#else
    #define icL_pro_service_cast_EXPORT icL_Import
#endif

#if defined(icL_pro_service_keyword_LIBRARY)
    #define icL_pro_service_keyword_EXPORT icL_Export
#else
    #define icL_pro_service_keyword_EXPORT icL_Import
#endif

#if defined(icL_pro_service_main_LIBRARY)
    #define icL_pro_service_main_EXPORT icL_Export
#else
    #define icL_pro_service_main_EXPORT icL_Import
#endif

#if defined(icL_pro_service_value_base_LIBRARY)
    #define icL_pro_service_value_base_EXPORT icL_Export
#else
    #define icL_pro_service_value_base_EXPORT icL_Import
#endif

#if defined(icL_pro_service_value_browser_LIBRARY)
    #define icL_pro_service_value_browser_EXPORT icL_Export
#else
    #define icL_pro_service_value_browser_EXPORT icL_Import
#endif

#if defined(icL_pro_service_value_system_LIBRARY)
    #define icL_pro_service_value_system_EXPORT icL_Export
#else
    #define icL_pro_service_value_system_EXPORT icL_Import
#endif

#if defined(icL_pro_sv_LIBRARY)
    #define icL_pro_sv_EXPORT icL_Export
#else
    #define icL_pro_sv_EXPORT icL_Import
#endif

#if defined(icL_pro_shared_LIBRARY)
    #define icL_pro_shared_EXPORT icL_Export
#else
    #define icL_pro_shared_EXPORT icL_Import
#endif

#if defined(icL_pro_vm_LIBRARY)
    #define icL_pro_vm_EXPORT icL_Export
#else
    #define icL_pro_vm_EXPORT icL_Import
#endif


// clang-format on

#endif
