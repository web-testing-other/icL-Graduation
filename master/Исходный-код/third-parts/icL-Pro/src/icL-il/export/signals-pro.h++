#ifndef pro_il_Signals
#define pro_il_Signals

#include <icL-il/export/signals.h++>



namespace icL::il {

/**
 * The Signals namespace block the distribution of Signals items to memory
 */
namespace SignalsNM {

/**
 * @brief The Signals enum contains all predefined icL signals
 */
enum Signals {
    First = core::il::Signals::Last,
    NoSessions,               ///< There is not an opened session.
    NoSuchWindow,             ///< No such window.
    NoSuchElement,            ///< No such element.
    NoSuchFrame,              ///< Nu such frame.
    NoSuchCookie,             ///< No such cookie.
    NoSuchAlert,              ///< No such alert.
    NoSuchPlaceholder,        ///< No such placeholder.
    NoSuchDatabase,           ///< No such database.
    NoSuchServer,             ///< No such server.
    WrongUserPassword,        ///< Authentication failed.
    StaleElementReference,    ///< Stale element reference.
    FolderNotFound,           ///< Folder not found.
    FileNotFound,             ///< File not found.
    UnsupportedOperation,     ///< Web driver does not support this operation.
    EmptyElement,             ///< Invalid web element.
    MultiElement,             ///< Collection contains some web elements.
    InvalidSelector,          ///< Invalid selector
    InvalidElementState,      ///< Invalid element state.
    InvalidElement,           ///< Operation is not available for the element.
    InvalidSessionId,         ///< Session was closed.
    InvalidCookieDomain,      ///< Wrong domain of cookie.
    InsecureCertificate,      ///< Certificate is insecure.
    UnexpectedAlertOpen,      ///< Unexpected alert was opened.
    UnrealCast,               ///< The requested cast is impossible.
    ElementNotInteractable,   ///< Element is not intractable.
    ElementClickIntercepted,  ///< The click was intercepted.
    MoveTargetOutOfBounds,    ///< Coordinates for mouse are out of screen.
    UnableToSetCookie,        ///< Unable to set cookie.
    UnableToCaptureScreen,    ///< Unable to capture screen.
    JavaScriptError,          ///< Excution of JavaScript code failed.
    ScriptTimeout,            ///< Script execution timeout.
    SessionNotCreated,        ///< Session creation failed.
    QueryNotExecutedYet,      ///< The SQL query was not executed yet.
    UnknownCommand,           ///< [w3c] Unknown command.
    UnknownError,             ///< [w3c] Unknown error.
    UnknownMethod,            ///< [w3c] Unknown method.
    Last
};

}  // namespace SignalsNM

using SignalsNM::Signals;

}  // namespace icL::il

#endif  // SIGNALSPRO_H
