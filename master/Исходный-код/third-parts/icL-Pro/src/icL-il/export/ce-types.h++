#ifndef pro_ce_Types
#define pro_ce_Types

#include <icL-memory/structures/type.h++>



namespace icL::memory {

namespace TypesNM {
enum Type {
    First = core::memory::Type::Last,
    // values
    CookieValue,
    DocumentValue,
    ElementValue,
    ElementsValue,
    SessionValue,
    TabValue,
    WindowValue,
    JavaScriptLambdaValue,
    SqlLambdaValue,
    DatabaseValue,
    FileValue,
    HandlerValue,
    JsFileValue,
    ListenerValue,
    QueryValue,
    // browser
    Cookie,
    Cookies,
    Document,
    By,
    Key,
    Mouse,
    Move,
    icL,
    Alert,
    Session,
    Sessions,
    Tab,
    Tabs,
    Window,
    Windows,
    // system
    DB,
    DBManager,
    DSV,
    File,
    Files,
    Make,
    Query,
    Request,
    Last
};
}

using TypesNM::Type;

}  // namespace icL::memory

#endif  // pro_ce_Types
