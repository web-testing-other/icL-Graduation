#include "db-target.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/types.h++>



namespace icL::il {

DB::DB(const icVariant & value) {
    assert(value.is(static_cast<icType>(proType::DB)));
    *this = to<DB>(value);
}

bool DB::operator==(const DB & other) const {
    return target->databaseId == other.target->databaseId;
}

DB::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::DB));
}

Query::Query(const icVariant & value) {
    assert(value.is(static_cast<icType>(proType::Query)));
    *this = to<Query>(value);
}

bool Query::operator==(const Query & other) const {
    return target->queryId == other.target->queryId;
}

Query::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Query));
}

}  // namespace icL::il
