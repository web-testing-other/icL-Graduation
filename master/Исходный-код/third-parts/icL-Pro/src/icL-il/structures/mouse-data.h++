#ifndef il_MouseData
#define il_MouseData

#include "icL-il/main/enums.h++"

#include <icL-il/export/global-pro.h++>

#include <icL-memory/structures/type.h++>



class icString;

namespace icL {

namespace core::memory {
struct Object;
}  // namespace core::memory

namespace il {

/**
 * @brief The MouseData struct contians mouse event data
 */
struct icL_pro_il_EXPORT MouseData
{
    virtual ~MouseData() = default;

    double rx = 0.5;  ///< relative x coordinate
    double ry = 0.5;  ///< relative y coordinate

    int ax = -1;  ///< absolute x coordinate
    int ay = -1;  ///< absolute y coordinate

    int button = MouseButtons::Left;  ///< mouse button

    // padding
    int : 32;

    /**
     * @brief loadDictionary loads data from dictionary
     * @param dic is the dictionary to load data
     */
    virtual void loadDictionary(const core::memory::Object & dic);

protected:
    /**
     * @brief checkType checks the type of a dictionaty object
     * @param dic is the dictionary to load the field
     * @param name is the name of the field
     * @param type is the expected type
     * @return true if type matches, otherwise false
     */
    static bool checkType(
      const core::memory::Object & dic, const icString & name,
      core::memory::Type type);

    /**
     * @brief checkAndAssign checks the type and assigns on success
     * @param dic is the dictionary to load the field
     * @param name is the name of the field (must be an int)
     * @param field is the variable which will get the value from dictionary
     */
    static void checkAndAssign(
      const core::memory::Object & dic, const icString & name, int & field);

    /**
     * @brief checkAndAssign checks the type and assigns on success
     * @param dic is the dictionary to load the field
     * @param name is the name of the field (must be a double)
     * @param field is the variable which will get the value from dictionary
     */
    static void checkAndAssign(
      const core::memory::Object & dic, const icString & name, double & field);
};

/**
 * @brief The ClickData struct contains mouse click data
 */
struct icL_pro_il_EXPORT ClickData : public MouseData
{
    int delay = -1;  ///< time interval between mouse down and up events (in ms)
    int count = 1;   ///< the count of clicks

    void loadDictionary(const core::memory::Object & dic) override;
};

/**
 * @brief The HoverData struct contains mouse move data
 */
struct icL_pro_il_EXPORT HoverData : public MouseData
{
    double p1x = -1.0;  ///< P1.x of cubic Bezier function
    double p1y = -1.0;  ///< P1.y of cubic Bezier function
    double p2x = -1.0;  ///< P2.x of cubic Bezier function
    double p2y = -1.0;  ///< P2.y of cubic Bezier function

    int moveTime     = -1;  ///< time of moving by 100px (in ms)
    int moveFunction = 1;   ///< is the function of move path

    void loadDictionary(const core::memory::Object & dic) override;
};

}  // namespace il
}  // namespace icL

#endif  // il_MouseData
