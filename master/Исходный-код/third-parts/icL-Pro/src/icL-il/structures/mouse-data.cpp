#include "mouse-data.h++"

#include <icL-memory/structures/set.h++>


namespace icL::il {

void MouseData::loadDictionary(const core::memory::Object & dic) {
    checkAndAssign(dic, "button", button);
    checkAndAssign(dic, "rx", rx);
    checkAndAssign(dic, "ry", ry);
    checkAndAssign(dic, "ax", ax);
    checkAndAssign(dic, "ay", ay);
}

bool MouseData::checkType(
  const core::memory::Object & dic, const icString & name,
  core::memory::Type type) {
    return dic.data->checkType(name, type);
}

void MouseData::checkAndAssign(
  const core::memory::Object & dic, const icString & name, int & field) {
    if (checkType(dic, name, core::memory::Type::IntValue)) {
        field = dic.data->getValue(name).toInt();
    }
}

void MouseData::checkAndAssign(
  const core::memory::Object & dic, const icString & name, double & field) {
    if (checkType(dic, name, core::memory::Type::DoubleValue)) {
        field = dic.data->getValue(name).toDouble();
    }
}

void ClickData::loadDictionary(const core::memory::Object & dic) {
    MouseData::loadDictionary(dic);

    checkAndAssign(dic, "delay", delay);
    checkAndAssign(dic, "count", count);
}

void HoverData::loadDictionary(const core::memory::Object & dic) {
    MouseData::loadDictionary(dic);

    checkAndAssign(dic, "moveTime", moveTime);
    checkAndAssign(dic, "moveFunction", moveFunction);
    checkAndAssign(dic, "p1x", p1x);
    checkAndAssign(dic, "p1y", p1y);
    checkAndAssign(dic, "p2x", p2x);
    checkAndAssign(dic, "p2y", p2y);
}

}  // namespace icL::il
