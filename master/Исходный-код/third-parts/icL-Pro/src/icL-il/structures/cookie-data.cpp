#include "cookie-data.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/types.h++>



namespace icL::il {

icL::il::Cookie::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Cookie));
}

Cookie::Cookie(const icVariant & value) {
    assert(value.is(static_cast<icType>(proType::Cookie)));
    data = to<Cookie>(value).data;
}

bool Cookie::operator==(const Cookie & other) const {
    return data->name == other.data->name;
}

}  // namespace icL::il
