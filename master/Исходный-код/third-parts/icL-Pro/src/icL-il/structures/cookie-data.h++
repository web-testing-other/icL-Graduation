#ifndef il_CookieData
#define il_CookieData

#include "target-data.h++"



namespace icL::il {

struct CookieData  ///< Contains all data of a `cookie` value
{
    TargetData target;  ///< Data about the tab of cookie

    icString domain;      ///< `[r/w] Cookie'domain : string`
    icString name;        ///< `[r/w] Cookie'name : string`
    icString path;        ///< `[r/w] Cookie'path : string`
    icString value;       ///< `[r/w] Cookie'value : string`
    int      expiry{};    ///< `[r/w] Cookie'expiry : int`
    bool     httpOnly{};  ///< `[r/w] Cookie'httpOnly : bool`
    bool     secure{};    ///< `[r/w] Cookie'secure : bool`
};

struct icL_pro_il_EXPORT Cookie  ///< Contains a reference to a cookie
{
    std::shared_ptr<CookieData> data;  ///< The pointer to a cookie

    Cookie() = default;

    /**
     * @brief CookieRef casts a variant a cookie reference
     * @param value is the value to cast
     */
    Cookie(const icVariant & value);

    /**
     * @brief operator icVariant casts the cookie reference to a variant
     */
    operator icVariant() const;

    /**
     * @brief operator == compares if two cookies has the same name
     * @param other is the cookie to copare with
     * @return true if they have the same name
     */
    bool operator==(const Cookie & other) const;
};

}  // namespace icL::il

#endif  // il_CookieData
