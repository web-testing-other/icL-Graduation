#include "target-data.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/types.h++>

namespace icL::il {

bool TargetData::operator==(const TargetData & other) const {
    return sessionId == other.sessionId && windowId == other.windowId;
}

bool TargetData::operator!=(const TargetData & other) const {
    return sessionId != other.sessionId || windowId != other.windowId;
}

Session::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Session));
}

icL::il::Tab::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Tab));
}

icL::il::Window::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Window));
}

Session::Session(const icVariant & value) {
    assert(value.is(proType::Session));
    *this = to<Session>(value);
}

bool Session::operator==(const Session & other) const {
    return data == other.data;
}

Tab::Tab(const icVariant & variant) {
    assert(variant.is(proType::Tab));
    *this = to<Tab>(variant);
}

bool Tab::operator==(const Tab & other) const {
    return data == other.data;
}

Window::Window(const icVariant & value) {
    assert(value.is(proType::Window));
    *this = to<Window>(value);
}

bool Window::operator==(const Window & other) const {
    return data == other.data;
}

Document::Document(const icVariant & variant) {
    assert(variant.is(proType::Document));
    *this = to<Document>(variant);
}

icL::il::Document::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Document));
}

bool Document::operator==(const Document & other) const {
    return data == other.data;
}

}  // namespace icL::il
