#include "element.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/types.h++>



namespace icL::il {

Element::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Element));
}

Element::Element(const icVariant & variant) {
    *this = to<Element>(variant);
}

bool Element::operator==(const Element & other) const {
    return variable == other.variable;
}

icL::il::Elements::Elements(const icVariant & variant) {
    *this = to<Elements>(variant);
}

int icL::il::Elements::size() const {
    return _size;
}

void Elements::setSize(int size) {
    _size = size;
}

icL::il::Elements::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Elements));
}

}  // namespace icL::il
