#ifndef ce_DBTarget
#define ce_DBTarget

#include <icL-types/replaces/ic-string.h++>

#include <icL-il/export/global-pro.h++>

#include <memory>

class icVariant;

namespace icL::il {

/**
 * @brief The DBTarget struct contains a target to database object
 */
struct icL_pro_il_EXPORT DBTarget
{
    icString databaseId;  ///< identifier of database
    icString queryId;     ///< indentifier of a query
};

/**
 * @brief The DB struct is used to save a reference to db in icVariant
 */
struct icL_pro_il_EXPORT DB
{
    std::shared_ptr<DBTarget> target;  ///< target data of db and current query

    DB() = default;
    DB(const icVariant & value);

    /**
     * @brief operator icVariant casts db value to icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == check if db are the same
     * @param other is the db to compare with
     * @return true if the dbs are the same, otherwise false
     */
    bool operator==(const DB & other) const;
};

/**
 * @brief The Query struct is used to save a reference to query in icVariant
 */
struct icL_pro_il_EXPORT Query
{
    std::shared_ptr<DBTarget> target;  ///< target data of db and query

    Query() = default;
    Query(const icVariant & value);

    /**
     * @brief operator icVariant casts query value to icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == checks if two query has the same target
     * @param other is the query to compare with
     * @return true if they has the same target, otherwise false
     */
    bool operator==(const Query & other) const;
};

}  // namespace icL::il

#endif  // ce_DBTarget
