#include "listen-handler.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::il {

Listener::Listener(const icVariant & /*value*/) {}

bool Listener::operator==(Listener & /*other*/) {
    return false;
}

icL::il::Listener::operator icVariant() const {
    return false;
}

Handler::Handler(const icVariant & /*value*/) {}

bool Handler::operator==(Handler & /*other*/) {
    return false;
}

icL::il::Handler::operator icVariant() const {
    return false;
}


}  // namespace icL::il
