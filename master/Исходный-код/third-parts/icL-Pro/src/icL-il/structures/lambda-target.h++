#ifndef il_LambdaTarget
#define il_LambdaTarget

#include "target-data.h++"

#include <icL-memory/structures/function.h++>

#include <memory>



namespace icL::il {

/**
 * @brief The LambdaTarget struct contains a pointer to a lambda function
 */
struct icL_pro_il_EXPORT LambdaTarget
{
    std::shared_ptr<core::memory::Function> target;  ///< Function target

    LambdaTarget() = default;
    LambdaTarget(const icVariant & value);

    /**
     * @brief operator icVariant casts the lambda value to icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == check if 2 lambdas has the same target
     * @param other is the lambda to compare with
     * @return true if they contain the same target, otherwise false
     */
    bool operator==(const LambdaTarget & other) const;
};

struct icL_pro_il_EXPORT CodeFragmentTarget
{
    std::shared_ptr<core::il::CodeFragment> target;  ///< code fragment target

    CodeFragmentTarget() = default;
};

struct icL_pro_il_EXPORT JsLambda : public CodeFragmentTarget
{
    std::shared_ptr<TargetData> data;

    JsLambda() = default;
    JsLambda(const icVariant & value);

    operator icVariant() const;

    bool operator==(const JsLambda & other) const;
};

}  // namespace icL::il

#endif  // il_LambdaTarget
