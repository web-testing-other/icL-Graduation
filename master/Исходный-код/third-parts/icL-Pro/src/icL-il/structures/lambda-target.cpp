#include "lambda-target.h++"

#include <icL-il/export/types.h++>



namespace icL::il {

LambdaTarget::LambdaTarget(const icVariant & value) {
    assert(value.is(proType::Lambda));
    *this = to<LambdaTarget>(value);
}

icL::il::LambdaTarget::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::Lambda));
}

bool LambdaTarget::operator==(const LambdaTarget & other) const {
    return target == other.target;
}

JsLambda::JsLambda(const icVariant & value) {
    *this = to<JsLambda>(value);
}

bool JsLambda::operator==(const JsLambda & other) const {
    return target == other.target && data == other.data;
}

icL::il::JsLambda::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::JsLambda));
}

}  // namespace icL::il
