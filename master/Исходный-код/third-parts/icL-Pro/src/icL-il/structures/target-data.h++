#ifndef il_TargetData
#define il_TargetData

#include <icL-types/replaces/ic-string.h++>

#include <icL-il/export/global-pro.h++>

#include <memory>



class icVariant;

namespace icL::il {

/**
 * @brief The TargetData struct respresents a pair of session id and window id
 */
struct icL_pro_il_EXPORT TargetData
{
    icString sessionId;  ///< idetifier of session
    icString windowId;   ///< identifier of window (or tab)

    /**
     * @brief operator == compares two targets
     * @param other is the target to compare with
     * @return true if the targets have the same data, otherwise false
     */
    bool operator==(const TargetData & other) const;

    /**
     * @brief operator != compares two targets
     * @param other is the target to compare with
     * @return false if the targets have the same data, otherwise true
     */
    bool operator!=(const TargetData & other) const;
};

/**
 * @brief The Session struct respresents a session value
 */
struct icL_pro_il_EXPORT Session
{
    std::shared_ptr<TargetData> data;  ///< data of session target

    Session() = default;
    Session(const icVariant & value);

    /**
     * @brief operator icVariant casts session pointer to a icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == checks if the items have the same target
     * @param other is the item to compare with
     * @return true if they have the same target, otherwise false
     */
    bool operator==(const Session & other) const;
};

/**
 * @brief The Tab struct represents a tab value
 */
struct icL_pro_il_EXPORT Tab
{
    std::shared_ptr<TargetData> data;  ///< data of session target

    Tab() = default;

    /**
     * @brief TabPtr casts a qvaraint to tab pointer
     * @param variant is the variant to cast
     */
    Tab(const icVariant & variant);

    /**
     * @brief operator icVariant casts tab pointer to a icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == checks if the items have the same target
     * @param other is the item to compare with
     * @return true if they have the same target, otherwise false
     */
    bool operator==(const Tab & other) const;
};

/**
 * @brief The WindowPtr struct represents a window value
 */
struct icL_pro_il_EXPORT Window
{
    std::shared_ptr<TargetData> data;  ///< data of session target

    Window() = default;
    Window(const icVariant & value);

    /**
     * @brief operator icVariant casts a window pointer to a icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == checks if the items have the same target
     * @param other is the item to compare with
     * @return true if they have the same target, otherwise false
     */
    bool operator==(const Window & other) const;
};

/**
 * @brief The Tab struct represents a tab value
 */
struct icL_pro_il_EXPORT Document
{
    std::shared_ptr<TargetData> data;  ///< data of session target

    Document() = default;

    /**
     * @brief Document casts a variant to tab pointer
     * @param variant is the variant to cast
     */
    Document(const icVariant & variant);

    /**
     * @brief operator icVariant casts tab pointer to a icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == checks if the items have the same target
     * @param other is the item to compare with
     * @return true if they have the same target, otherwise false
     */
    bool operator==(const Document & other) const;
};

}  // namespace icL::il

#endif  // il_TargetData
