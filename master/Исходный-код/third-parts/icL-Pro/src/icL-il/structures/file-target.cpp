#include "file-target.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/types.h++>

namespace icL::il {

File::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::File));
}

File::File(const icVariant & value) {
    *this = to<File>(value);
}

bool File::operator==(const File & other) const {
    return target == other.target;
}

JsFile::JsFile(const icVariant & value) {
    *this = to<JsFile>(value);
}

bool JsFile::operator==(const JsFile & other) const {
    return target == other.target;
}

icL::il::JsFile::operator icVariant() const {
    return icVariant::fromValueTemplate(
      *this, static_cast<icType>(proType::JsFile));
}

}  // namespace icL::il
