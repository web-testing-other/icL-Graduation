#include "move.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::ce {

Move::Move(core::il::InterLevel * il)
    : BrowserValue(il) {}

void Move::runBezier() {
    m_newContext = il->factory->fromValue(il, bezier());
}

void Move::runCubic() {
    m_newContext = il->factory->fromValue(il, cubic());
}

void Move::runLinear() {
    m_newContext = il->factory->fromValue(il, linear());
}

void Move::runQuadratic() {
    m_newContext = il->factory->fromValue(il, quadratic());
}

void Move::runTeleport() {
    m_newContext = il->factory->fromValue(il, teleport());
}

Type Move::type() const {
    return static_cast<Type>(memory::Type::Move);
}

icString Move::typeName() {
    return "Move";
}

void Move::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Move::*)()> properties{
      {"bezier", &Move::runBezier},
      {"cubic", &Move::runCubic},
      {"linear", &Move::runLinear},
      {"quadratic", &Move::runQuadratic},
      {"teleport", &Move::runTeleport}};

    runPropertyWithPrefixCheck<Move, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
