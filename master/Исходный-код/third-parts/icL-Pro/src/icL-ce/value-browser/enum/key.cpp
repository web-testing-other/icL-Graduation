#include "key.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::ce {

Key::Key(core::il::InterLevel * il)
    : BrowserValue(il) {}

void Key::runAlt() {
    m_newContext = il->factory->fromValue(il, alt());
}

void Key::runCtrl() {
    m_newContext = il->factory->fromValue(il, ctrl());
}

void Key::runShift() {
    m_newContext = il->factory->fromValue(il, shift());
}

Type Key::type() const {
    return static_cast<Type>(memory::Type::Key);
}

icString Key::typeName() {
    return "Key";
}

void Key::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Key::*)()> properties{
      {"alt", &Key::runAlt},
      {"ctrl", &Key::runCtrl},
      {"shift", &Key::runShift}};

    runPropertyWithPrefixCheck<Key, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
