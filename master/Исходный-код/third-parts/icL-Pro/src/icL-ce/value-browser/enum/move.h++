#ifndef ce_Move
#define ce_Move

#include <icL-service/value-browser/enum/move.h++>

#include <icL-ce/base/value/browser-value.h++>



namespace icL::ce {

/**
 * @brief The Move class represents `Move` token
 */
class icL_pro_ce_value_browser_EXPORT Move
    : public BrowserValue
    , public service::Move
{
public:
    Move(core::il::InterLevel * il);

    // properties level 2

    /// `Move'bezier`
    void runBezier();

    /// `Move'cubic`
    void runCubic();

    /// `Move'linear`
    void runLinear();

    /// `Move'quadratic`
    void runQuadratic();

    /// `Move'teleport`
    void runTeleport();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::ce

#endif  // ce_Move
