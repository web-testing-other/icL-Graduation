#ifndef ce_By
#define ce_By

#include <icL-service/value-browser/enum/by.h++>

#include <icL-ce/base/value/browser-value.h++>



namespace icL::ce {

/**
 * @brief The By class represents a `By` token
 */
class icL_pro_ce_value_browser_EXPORT By
    : public BrowserValue
    , public service::By
{
public:
    By(core::il::InterLevel * il);

    // properties level 2

    /// `By'cssSelector`
    void runCssSelector();

    /// `By'linkText`
    void runLinkText();

    /// `By'partialLinkText`
    void runPartialLinkText();

    /// `By'tagName`
    void runTagName();

    /// `By'xPath`
    void runXPath();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::ce

#endif  // ce_By
