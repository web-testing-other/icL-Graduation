#include "by.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::ce {

By::By(core::il::InterLevel * il)
    : BrowserValue(il) {}

void By::runCssSelector() {
    m_newContext = il->factory->fromValue(il, cssSelector());
}

void By::runLinkText() {
    m_newContext = il->factory->fromValue(il, linkText());
}

void By::runPartialLinkText() {
    m_newContext = il->factory->fromValue(il, partialLinkText());
}

void By::runTagName() {
    m_newContext = il->factory->fromValue(il, tagName());
}

void By::runXPath() {
    m_newContext = il->factory->fromValue(il, xPath());
}

Type By::type() const {
    return static_cast<Type>(memory::Type::By);
}

icString By::typeName() {
    return "By";
}

void By::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (By::*)()> properties{
      {"cssSelector", &By::runCssSelector},
      {"linkText", &By::runLinkText},
      {"partialLinkText", &By::runPartialLinkText},
      {"tagName", &By::runTagName},
      {"xPath", &By::runXPath}};

    runPropertyWithPrefixCheck<By, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
