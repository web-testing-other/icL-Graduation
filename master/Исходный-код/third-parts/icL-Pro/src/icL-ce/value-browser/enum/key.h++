#ifndef ce_Key
#define ce_Key

#include <icL-service/value-browser/enum/key.h++>

#include <icL-ce/base/value/browser-value.h++>



namespace icL::ce {

/**
 * @brief The Key class represents `Key` token
 */
class icL_pro_ce_value_browser_EXPORT Key
    : public BrowserValue
    , public service::Key
{
public:
    Key(core::il::InterLevel * il);

    // properties level 2

    /// `Key'alt`
    void runAlt();

    /// `Key'ctrl`
    void runCtrl();

    /// `Key'shift`
    void runShift();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::ce

#endif  // ce_Key
