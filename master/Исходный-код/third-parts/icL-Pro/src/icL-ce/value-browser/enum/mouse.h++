#ifndef ce_Mouse
#define ce_Mouse

#include <icL-service/value-browser/enum/mouse.h++>

#include <icL-ce/base/value/browser-value.h++>



namespace icL::ce {

/**
 * @brief The Mouse class represents `Mouse` token
 */
class icL_pro_ce_value_browser_EXPORT Mouse
    : public BrowserValue
    , public service::Mouse
{
public:
    Mouse(core::il::InterLevel * il);

    // properties level 2

    /// `Mouse'left`
    void runLeft();

    /// `Mouse'middle`
    void runMiddle();

    /// `Mouse'right`
    void runRight();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::ce

#endif  // ce_Mouse
