#include "mouse.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::ce {

Mouse::Mouse(core::il::InterLevel * il)
    : BrowserValue(il) {}

void Mouse::runLeft() {
    m_newContext = il->factory->fromValue(il, left());
}

void Mouse::runMiddle() {
    m_newContext = il->factory->fromValue(il, middle());
}

void Mouse::runRight() {
    m_newContext = il->factory->fromValue(il, right());
}

Type Mouse::type() const {
    return static_cast<Type>(memory::Type::Mouse);
}

icString Mouse::typeName() {
    return "Mouse";
}

void Mouse::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Mouse::*)()> properties{
      {"left", &Mouse::runLeft},
      {"middle", &Mouse::runMiddle},
      {"right", &Mouse::runRight}};

    runPropertyWithPrefixCheck<Mouse, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
