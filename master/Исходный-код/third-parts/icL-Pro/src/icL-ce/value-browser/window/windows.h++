#ifndef ce_Windows
#define ce_Windows

#include <icL-service/value-browser/window/windows.h++>

#include <icL-ce/base/value/browser-value.h++>



namespace icL {

namespace il {
struct TargetData;
}

namespace ce {

class icL_pro_ce_value_browser_EXPORT Windows
    : public BrowserValue
    , public service::Windows
{
public:
    Windows(core::il::InterLevel * il, il::TargetData target = {});

    // properties level 2

    /// `Windows'current`
    void runCurrent();

    /// `Windows'length`
    void runLength();

    /// `Windows'session`
    void runSession();

    // methods level 2

    /// `Windows.get`
    void runGet(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Windows
