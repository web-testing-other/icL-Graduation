#include "session.h++"

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce/value-base/browser/session-value.h++>



namespace icL::ce {

Session::Session(core::il::InterLevel * il)
    : BrowserCommand(il) {}

int Session::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Session::runNow() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(pro(il)->server->getCurrentTarget());
    m_newContext = il->factory->fromValue(il, session);

    return StepType::CommandEnd;
}

Type Session::type() const {
    return static_cast<Type>(memory::Type::Session);
}

icString Session::typeName() {
    return "Session";
}

}  // namespace icL::ce
