#ifndef ce_Tabs
#define ce_Tabs

#include <icL-service/value-browser/window/tabs.h++>

#include <icL-ce/base/value/browser-value.h++>



class icRegEx;


namespace icL {

namespace il {
struct TargetData;
}

namespace ce {

class icL_pro_ce_value_browser_EXPORT Tabs
    : public BrowserValue
    , public service::Tabs
{
public:
    Tabs(core::il::InterLevel * il, il::TargetData target = {});

    // properties level 2

    /// `Tabs'current`
    void runCurrent();

    /// `Tabs'first`
    void runFirst();

    /// `Tabs'last`
    void runLast();

    /// `Tabs'length`
    void runLength();

    /// `Tabs'next`
    void runNext();

    /// `Tabs'previous`
    void runPrevious();

    /// `Tabs'session`
    void runSession();

    // methods level 2

    /// `Tabs.close`
    void runClose(const core::memory::ArgList & args);

    /// `Tabs.closeByTitle`
    void runCloseByTitle(const core::memory::ArgList & args);

    /// `Tabs.closeOthers`
    void runCloseOthers(const core::memory::ArgList & args);

    /// `Tabs.closetoLeft`
    void runCloseToLeft(const core::memory::ArgList & args);

    /// `Tabs.closeToRight`
    void runCloseToRight(const core::memory::ArgList & args);

    /// `Tabs.find`
    void runFind(const core::memory::ArgList & args);

    /// `Tabs.findByTitle`
    void runFindByTitle(const core::memory::ArgList & args);

    /// `Tabs.get`
    void runGet(const core::memory::ArgList & args);

    /// `Tabs.new`
    void runNew(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Tabs
