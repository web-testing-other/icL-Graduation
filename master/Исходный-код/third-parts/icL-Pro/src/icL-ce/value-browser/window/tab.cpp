#include "tab.h++"

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce/value-base/browser/tab-value.h++>



namespace icL::ce {

Tab::Tab(core::il::InterLevel * il)
    : BrowserCommand(il) {}

int Tab::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Tab::runNow() {
    il::Tab tab;

    tab.data =
      std::make_shared<il::TargetData>(pro(il)->server->getCurrentTarget());
    m_newContext = il->factory->fromValue(il, tab);

    return StepType::CommandEnd;
}

Type Tab::type() const {
    return static_cast<Type>(memory::Type::Tab);
}

icString Tab::typeName() {
    return "Tab";
}

}  // namespace icL::ce
