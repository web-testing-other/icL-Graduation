#include "windows.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-index-check.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Windows::Windows(core::il::InterLevel * il, il::TargetData target)
    : BrowserValue(il)
    , service::Windows(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = pro(il)->server->getCurrentTarget();
    }
}

void Windows::runCurrent() {
    m_newContext = il->factory->fromValue(il, current());
}

void Windows::runLength() {
    m_newContext = il->factory->fromValue(il, length());
}

void Windows::runSession() {
    m_newContext = il->factory->fromValue(il, session());
}

void Windows::runGet(const core::memory::ArgList & args) {
    m_newContext = il->factory->fromValue(il, get(args[0]));
}

Type Windows::type() const {
    return static_cast<Type>(memory::Type::Windows);
}

icString Windows::typeName() {
    return "windows";
}

void Windows::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Windows::*)()> properties{
      {"current", &Windows::runCurrent},
      {"length", &Windows::runLength},
      {"session", &Windows::runSession}};

    pro(il)->server->pushTarget(target);
    runPropertyWithIndexCheck<Windows, BrowserValue>(
      properties, prefix, name, [this](int i) -> icVariant { return get(i); });
    pro(il)->server->popTarget();
}

void Windows::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Windows::*)(const core::memory::ArgList &)>
      methods{{"get", &Windows::runGet}};

    pro(il)->server->pushTarget(target);
    runMethodNow<Windows, BrowserValue>(methods, name, args);
    pro(il)->server->popTarget();
}

}  // namespace icL::ce
