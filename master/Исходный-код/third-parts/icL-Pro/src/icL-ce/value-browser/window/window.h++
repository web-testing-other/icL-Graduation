#ifndef ce_Window
#define ce_Window

#include <icL-service/main/values/inode-pro.h++>

#include <icL-ce/base/value/browser-command.h++>



namespace icL::ce {

class icL_pro_ce_value_browser_EXPORT Window
    : public BrowserCommand
    , virtual public service::INode
{
public:
    Window(core::il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Window
