#ifndef ce_Sessions
#define ce_Sessions

#include <icL-service/value-browser/window/sessions.h++>

#include <icL-ce/base/value/browser-value.h++>



namespace icL {

namespace il {
struct TargetData;
}

namespace ce {

class icL_pro_ce_value_browser_EXPORT Sessions
    : public BrowserValue
    , public service::Sessions
{
public:
    Sessions(core::il::InterLevel * il);

    // properties / methods level 2

    /// `Sessions'current`
    void runCurrent();

    /// `Sessions'length`
    void runLength();

    /// `Sessions.closeAll`
    void runCloseAll(const core::memory::ArgList & args);

    /// `Sessions.get`
    void runGet(const core::memory::ArgList & args);

    /// `Sessions.new`
    void runNew(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Sessions
