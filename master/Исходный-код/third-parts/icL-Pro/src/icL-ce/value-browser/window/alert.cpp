#include "alert.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/string-value.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/browser/session-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Alert::Alert(core::il::InterLevel * il, il::TargetData target)
    : BrowserCommand(il)
    , service::Alert(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = pro(il)->server->getCurrentTarget();
    }
}

void Alert::runSession() {
    m_newContext = il->factory->fromValue(il, session());
}

void Alert::runText() {
    m_newContext = il->factory->fromValue(il, text());
}

void Alert::runAccept(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        accept();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void Alert::runDismiss(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        dismiss();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void Alert::runSendKeys(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        sendKeys(args[0]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

Type Alert::type() const {
    return static_cast<Type>(memory::Type::Alert);
}

icString Alert::typeName() {
    return "Alert";
}

void Alert::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Alert::*)()> properties{
      {"session", &Alert::runSession}, {"text", &Alert::runText}};

    pro(il)->server->pushTarget(target);
    runPropertyWithPrefixCheck<Alert, BrowserValue>(properties, prefix, name);
    pro(il)->server->popTarget();
}

void Alert::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Alert::*)(const core::memory::ArgList &)>
      methods{{"accept", &Alert::runAccept},
              {"dismiss", &Alert::runDismiss},
              {"sendKeys", &Alert::runSendKeys}};

    pro(il)->server->pushTarget(target);
    runMethodNow<Alert, BrowserValue>(methods, name, args);
    pro(il)->server->popTarget();
}

}  // namespace icL::ce
