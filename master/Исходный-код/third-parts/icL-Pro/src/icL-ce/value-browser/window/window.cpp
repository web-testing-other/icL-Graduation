#include "window.h++"

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce/value-base/browser/window-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Window::Window(core::il::InterLevel * il)
    : BrowserCommand(il) {}

int Window::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Window::runNow() {
    il::Window window;

    window.data =
      std::make_shared<il::TargetData>(pro(il)->server->getCurrentTarget());
    m_newContext = il->factory->fromValue(il, window);

    return StepType::CommandEnd;
}

Type Window::type() const {
    return static_cast<Type>(memory::Type::Window);
}

icString Window::typeName() {
    return "Window";
}

}  // namespace icL::ce
