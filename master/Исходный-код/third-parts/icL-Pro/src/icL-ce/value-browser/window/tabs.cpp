#include "tabs.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-index-check.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/browser/session-value.h++>
#include <icL-ce/value-base/browser/tab-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Tabs::Tabs(core::il::InterLevel * il, il::TargetData target)
    : BrowserValue(il)
    , service::Tabs(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = pro(il)->server->getCurrentTarget();
    }
}

void Tabs::runCurrent() {
    m_newContext = il->factory->fromValue(il, current());
}

void Tabs::runFirst() {
    m_newContext = il->factory->fromValue(il, first());
}

void Tabs::runLast() {
    m_newContext = il->factory->fromValue(il, last());
}

void Tabs::runLength() {
    m_newContext = il->factory->fromValue(il, length());
}

void Tabs::runNext() {
    m_newContext = il->factory->fromValue(il, service::Tabs::next());
}

void Tabs::runPrevious() {
    m_newContext = il->factory->fromValue(il, previous());
}

void Tabs::runSession() {
    m_newContext = il->factory->fromValue(il, session());
}

void Tabs::runClose(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, close(icString(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = il->factory->fromValue(il, close(icRegEx(args[0])));
    }
}

void Tabs::runCloseByTitle(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext =
          il->factory->fromValue(il, closeByTitle(icString(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext =
          il->factory->fromValue(il, closeByTitle(icRegEx(args[0])));
    }
}

void Tabs::runCloseOthers(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, closeOthers());
    }
}

void Tabs::runCloseToLeft(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, closeToLeft());
    }
}

void Tabs::runCloseToRight(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, closeToRight());
    }
}

void Tabs::runFind(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, find(icString(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = il->factory->fromValue(il, find(icRegEx(args[0])));
    }
}

void Tabs::runFindByTitle(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext =
          il->factory->fromValue(il, findByTitle(icString(args[0])));
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext =
          il->factory->fromValue(il, findByTitle(icRegEx(args[0])));
    }
}

void Tabs::runGet(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, get(args[0]));
    }
}

void Tabs::runNew(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, new_());
    }
}

Type Tabs::type() const {
    return static_cast<Type>(memory::Type::Tabs);
}

icString Tabs::typeName() {
    return "Tabs";
}

void Tabs::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Tabs::*)()> properties{
      {"current", &Tabs::runCurrent}, {"first", &Tabs::runFirst},
      {"last", &Tabs::runLast},       {"length", &Tabs::runLength},
      {"next", &Tabs::runNext},       {"previous", &Tabs::runPrevious},
      {"session", &Tabs::runSession}};

    pro(il)->server->pushTarget(target);
    runPropertyWithIndexCheck<Tabs, BrowserValue>(
      properties, prefix, name, [this](int i) -> icVariant { return get(i); });
    pro(il)->server->popTarget();
}

void Tabs::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Tabs::*)(const core::memory::ArgList &)>
      methods{{"close", &Tabs::runClose},
              {"closeByTitle", &Tabs::runCloseByTitle},
              {"closeOthers", &Tabs::runCloseOthers},
              {"closeToLeft", &Tabs::runCloseToLeft},
              {"closeToRight", &Tabs::runCloseToRight},
              {"find", &Tabs::runFind},
              {"findByTitle", &Tabs::runFindByTitle},
              {"get", &Tabs::runGet},
              {"new", &Tabs::runNew}};

    pro(il)->server->pushTarget(target);
    runMethodNow<Tabs, BrowserValue>(methods, name, args);
    pro(il)->server->popTarget();
}

}  // namespace icL::ce
