#include "sessions.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-index-check.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/browser/session-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Sessions::Sessions(core::il::InterLevel * il)
    : BrowserValue(il) {}

void Sessions::runCurrent() {
    m_newContext = il->factory->fromValue(il, current());
}

void Sessions::runLength() {
    m_newContext = il->factory->fromValue(il, length());
}

void Sessions::runCloseAll(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        closeAll();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void Sessions::runGet(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, get(args[0]));
    }
}

void Sessions::runNew(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, new_());
    }
}

Type Sessions::type() const {
    return static_cast<Type>(memory::Type::Sessions);
}

icString Sessions::typeName() {
    return "Sessions";
}

void Sessions::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Sessions::*)()> properties{
      {"current", &Sessions::runCurrent}, {"length", &Sessions::runLength}};

    runPropertyWithIndexCheck<Sessions, BrowserValue>(
      properties, prefix, name, [this](int i) -> icVariant { return get(i); });
}

void Sessions::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Sessions::*)(const core::memory::ArgList &)>
      methods{{"closeAll", &Sessions::runCloseAll},
              {"get", &Sessions::runGet},
              {"new", &Sessions::runNew}};

    runMethodNow<Sessions, BrowserValue>(methods, name, args);
}

}  // namespace icL::ce
