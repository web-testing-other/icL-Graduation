#ifndef ce_Alert
#define ce_Alert

#include <icL-service/value-browser/window/alert.h++>

#include <icL-ce/base/value/browser-command.h++>



namespace icL {

namespace il {
struct TargetData;
}

namespace ce {

/**
 * @brief The Alert class represents an `Alert` token
 */
class icL_pro_ce_value_browser_EXPORT Alert
    : public BrowserCommand
    , public service::Alert
{
public:
    Alert(core::il::InterLevel * il, il::TargetData target = {});

    // properties/methods level 2

    /// `Alert'session`
    void runSession();

    /// `Alert'text`
    void runText();

    /// `Alert.accept`
    void runAccept(const core::memory::ArgList & args);

    /// `Alert.dismiss`
    void runDismiss(const core::memory::ArgList & args);

    /// `Alert.sendKeys`
    void runSendKeys(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Alert
