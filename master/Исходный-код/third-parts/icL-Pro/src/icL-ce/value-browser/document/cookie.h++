#ifndef ce_Cookie
#define ce_Cookie

#include <icL-ce/base/value/browser-command.h++>



namespace icL::ce {

/**
 * @brief The Cookie class represents a `Cookie` token
 */
class icL_pro_ce_value_browser_EXPORT Cookie : public BrowserCommand
{
public:
    Cookie(core::il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Cookie
