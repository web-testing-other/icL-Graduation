#include "document.h++"

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce/value-base/browser/document-value.h++>



namespace icL::ce {

Document::Document(core::il::InterLevel * il)
    : BrowserCommand(il) {}

int Document::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Document::runNow() {
    il::Document document;

    document.data =
      std::make_shared<il::TargetData>(pro(il)->server->getCurrentTarget());
    m_newContext = il->factory->fromValue(il, document);

    return StepType::CommandEnd;
}

Type Document::type() const {
    return static_cast<Type>(memory::Type::Document);
}

icString Document::typeName() {
    return "Document";
}

}  // namespace icL::ce
