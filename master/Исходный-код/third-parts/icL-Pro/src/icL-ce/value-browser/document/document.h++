#ifndef ce_Document
#define ce_Document

#include <icL-service/main/values/inode-pro.h++>

#include <icL-ce/base/value/browser-command.h++>



namespace icL::ce {

class icL_pro_ce_value_browser_EXPORT Document
    : public BrowserCommand
    , virtual public service::INode
{
public:
    Document(core::il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type         type() const override;
    icString     typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Document
