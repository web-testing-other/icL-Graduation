#include "cookies.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/cookie-data.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/browser/cookie-value.h++>
#include <icL-ce/value-base/browser/tab-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Cookies::Cookies(core::il::InterLevel * il, il::TargetData target)
    : BrowserValue(il)
    , service::Cookies(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = pro(il)->server->getCurrentTarget();
    }
}

void Cookies::runTab() {
    m_newContext = il->factory->fromValue(il, tab());
}

void Cookies::runDeleteAll(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        deleteAll();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void Cookies::runGet(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, get(args[0]));
    }
}

void Cookies::runNew(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, new_());
    }
}

Type Cookies::type() const {
    return static_cast<Type>(memory::Type::Cookies);
}

icString Cookies::typeName() {
    return "Cookies";
}

void Cookies::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Cookies::*)()> properties{
      {"tab", &Cookies::runTab}};

    pro(il)->server->pushTarget(target);
    runPropertyWithPrefixCheck<Cookies, BrowserValue>(properties, prefix, name);
    pro(il)->server->popTarget();
}

void Cookies::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Cookies::*)(const core::memory::ArgList &)>
      methods{{"deleteAll", &Cookies::runDeleteAll},
              {"get", &Cookies::runGet},
              {"new", &Cookies::runNew}};

    pro(il)->server->pushTarget(target);
    runMethodNow<Cookies, BrowserValue>(methods, name, args);
    pro(il)->server->popTarget();
}

}  // namespace icL::ce
