#include "cookie.h++"

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/cookie-data.h++>

#include <icL-ce/value-base/browser/cookie-value.h++>



namespace icL::ce {

Cookie::Cookie(core::il::InterLevel * il)
    : BrowserCommand(il) {}

int Cookie::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Cookie::runNow() {
    il::Cookie ref;
    ref.data     = std::make_shared<il::CookieData>();
    m_newContext = il->factory->fromValue(il, ref);

    return StepType::CommandEnd;
}

Type Cookie::type() const {
    return static_cast<Type>(memory::Type::Cookie);
}

icString Cookie::typeName() {
    return "Cookie";
}

}  // namespace icL::ce
