#ifndef ce_icL
#define ce_icL

#include <icL-service/main/values/inode-pro.h++>

#include <icL-ce/base/value/browser-value.h++>



namespace icL {

namespace memory {
class Memory;
}

namespace il {
class FrontEnd;
}

namespace ce {

/**
 * @brief The icL class represents an `icL` token
 */
class icL_pro_ce_value_browser_EXPORT icL
    : public BrowserValue
    , virtual public service::INode
{
public:
    icL(core::il::InterLevel * il);

private:
    /**
     * @brief runIntProperty runs a int property
     * @return a contextual entity for needed int propeperty
     */
    core::il::CE * runIntProperty(
      int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int));

    /**
     * @brief runBoolProperty runs a bool property
     * @return a contextual entity for needed bool property
     */
    core::il::CE * runBoolProperty(
      bool (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(bool));

public:
    // properties level 1

    /// `[r/w] icL'clickTime : int`
    core::il::CE * clickTime();

    /// `[r/w] icL'flashMode : bool`
    core::il::CE * flashMode();

    /// `[r/w] icL'humanMode : bool`
    core::il::CE * humanMode();

    /// `[r/w] icL'moveTime : int`
    core::il::CE * moveTime();

    /// `[r/w] icL'pressTime : int`
    core::il::CE * pressTime();

    /// `[r/w] icL'silentMode : bool`
    core::il::CE * silentMode();

    // properties level 2

    /// `icL'clickTime`
    void runClickTime();

    /// `icL'flashMode`
    void runFlashMode();

    /// `icL'humanMode`
    void runHumanMode();

    /// `icL'moveTime`
    void runMoveTime();

    /// `icL'pressTime`
    void runPressTime();

    /// `icL'silentMode`
    void runSilentMode();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_icL
