#include "icl.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/bool-value.h++>
#include <icL-ce/value-base/base/int-value.h++>



namespace icL::ce {

icL::icL(core::il::InterLevel * il)
    : BrowserValue(il) {}

class IntFixator : public core::il::IValueFixator
{
    il::InterLevel * il;
    int (il::FrontEnd::*_getter)();
    void (il::FrontEnd::*_setter)(int);

public:
    IntFixator(
      il::InterLevel * il, int (il::FrontEnd::*getter)(),
      void (il::FrontEnd::*setter)(int))
        : il(il)
        , _getter(getter)
        , _setter(setter) {}
    IntFixator(const IntFixator &) = default;

    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = (il->server->*_getter)();
    }
    void setter(
      icVariant & /*value*/, const icVariant & request) const override {
        (il->server->*_setter)(request);
    }
};

core::il::CE * icL::runIntProperty(
  int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int)) {
    auto * node    = il->factory->fromValue(il, int(0));
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(
      std::make_shared<IntFixator>(pro(il), getter, setter));
    return node;
}

class BoolFixator : public core::il::IValueFixator
{
    il::InterLevel * il;
    bool (il::FrontEnd::*_getter)();
    void (il::FrontEnd::*_setter)(bool);

public:
    BoolFixator(
      il::InterLevel * il, bool (il::FrontEnd::*getter)(),
      void (il::FrontEnd::*setter)(bool))
        : il(il)
        , _getter(getter)
        , _setter(setter) {}

    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = (il->server->*_getter)();
    }
    void setter(
      icVariant & /*value*/, const icVariant & request) const override {
        (il->server->*_setter)(request);
    }
};

core::il::CE * icL::runBoolProperty(
  bool (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(bool)) {
    auto * node    = il->factory->fromValue(il, bool(0));
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(
      std::make_shared<BoolFixator>(pro(il), getter, setter));
    return node;
}

core::il::CE * icL::clickTime() {
    return runIntProperty(
      &il::FrontEnd::getClickTime, &il::FrontEnd::setClickTime);
}

core::il::CE * icL::flashMode() {
    return runBoolProperty(
      &il::FrontEnd::getFlashMode, &il::FrontEnd::setFlashMode);
}

core::il::CE * icL::humanMode() {
    return runBoolProperty(
      &il::FrontEnd::getHumanMode, &il::FrontEnd::setHumanMode);
}

core::il::CE * icL::moveTime() {
    return runIntProperty(
      &il::FrontEnd::getMoveTime, &il::FrontEnd::setMoveTime);
}

core::il::CE * icL::pressTime() {
    return runIntProperty(
      &il::FrontEnd::getPressTime, &il::FrontEnd::setPressTime);
}

core::il::CE * icL::silentMode() {
    return runBoolProperty(
      &il::FrontEnd::getSilentMode, &il::FrontEnd::setSilentMode);
}

void icL::runClickTime() {
    m_newContext = clickTime();
}

void icL::runFlashMode() {
    m_newContext = flashMode();
}

void icL::runHumanMode() {
    m_newContext = humanMode();
}

void icL::runMoveTime() {
    m_newContext = moveTime();
}

void icL::runPressTime() {
    m_newContext = pressTime();
}

void icL::runSilentMode() {
    m_newContext = silentMode();
}

Type icL::type() const {
    return static_cast<Type>(memory::Type::icL);
}

icString icL::typeName() {
    return "icL";
}

void icL::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (icL::*)()> properties{
      {"clickTime", &icL::runClickTime}, {"flashMode", &icL::runFlashMode},
      {"humanMode", &icL::runHumanMode}, {"moveTime", &icL::runMoveTime},
      {"pressTime", &icL::runPressTime}, {"silentMode", &icL::runSilentMode}};

    runPropertyWithPrefixCheck<icL, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
