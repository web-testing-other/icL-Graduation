#include "make.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Make::Make(core::il::InterLevel * il)
    : SystemValue(il) {}


void Make::runImage(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::StringValue})) {
        image(args[0], args[1]);
    }
}

Type Make::type() const {
    return static_cast<Type>(memory::Type::Make);
}

icString Make::typeName() {
    return "Make";
}

void Make::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Make::*)(const core::memory::ArgList &)>
      methods{{"image", &Make::runImage}};

    runMethodNow<Make, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
