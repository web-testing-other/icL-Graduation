#ifndef ce_DSV
#define ce_DSV

#include <icL-service/value-system/dsv.h++>

#include <icL-ce/base/value/system-value.h++>

namespace icL::ce {

using core::il::StepType;
using core::memory::Type;

class icL_pro_ce_value_system_EXPORT DSV
    : public core::ce::SystemValue
    , public service::DSV
{
public:
    DSV(core::il::InterLevel * il);

    // methods level 2

    /// `DSV.append`
    void runAppend(const core::memory::ArgList & args);

    /// `DSV.load`
    void runLoad(const core::memory::ArgList & args);

    /// `DSV.loadCSV`
    void runLoadCSV(const core::memory::ArgList & args);

    /// `DSV.loadTSV`
    void runLoadTSV(const core::memory::ArgList & args);

    /// `DSV.sync`
    void runSync(const core::memory::ArgList & args);

    /// `DSV.write`
    void runWrite(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_DSV
