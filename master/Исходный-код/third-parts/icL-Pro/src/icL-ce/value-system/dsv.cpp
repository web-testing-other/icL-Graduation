#include "dsv.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/file-target.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/complex/set-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

using mType = memory::Type;

DSV::DSV(core::il::InterLevel * il)
    : SystemValue(il) {}

void DSV::runAppend(const core::memory::ArgList & args) {
    if (checkArgs(args, {mType::FileValue, Type::SetValue})) {
        append(args[0], core::memory::Set(args[1]));
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
    else if (checkArgs(args, {mType::FileValue, Type::ObjectValue})) {
        append(args[0], core::memory::Object(args[1]));
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void DSV::runLoad(const core::memory::ArgList & args) {
    if (checkArgs(
          args, {Type::StringValue, mType::FileValue, Type::SetValue})) {
        m_newContext =
          il->factory->fromValue(il, load(args[0], args[1], args[2]));
    }
}

void DSV::runLoadCSV(const core::memory::ArgList & args) {
    if (checkArgs(args, {mType::FileValue, Type::SetValue})) {
        m_newContext = il->factory->fromValue(il, loadCSV(args[0], args[1]));
    }
}

void DSV::runLoadTSV(const core::memory::ArgList & args) {
    if (checkArgs(args, {mType::FileValue, Type::SetValue})) {
        m_newContext = il->factory->fromValue(il, loadTSV(args[0], args[1]));
    }
}

void DSV::runSync(const core::memory::ArgList & args) {
    if (checkArgs(args, {mType::FileValue, Type::SetValue})) {
        sync(args[0], args[1]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void DSV::runWrite(const core::memory::ArgList & args) {
    if (checkArgs(args, {mType::FileValue, Type::SetValue})) {
        write(args[0], args[1]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

Type DSV::type() const {
    return static_cast<Type>(memory::Type::DSV);
}

icString DSV::typeName() {
    return "DSV";
}

void DSV::runMethod(const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (DSV::*)(const core::memory::ArgList &)>
      methods{{"append", &DSV::runAppend},   {"load", &DSV::runLoad},
              {"loadCSV", &DSV::runLoadCSV}, {"loadTSV", &DSV::runLoadTSV},
              {"sync", &DSV::runSync},       {"write", &DSV::runWrite}};

    runMethodNow<DSV, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
