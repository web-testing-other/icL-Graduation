#ifndef ce_Make
#define ce_Make

#include <icL-service/value-system/make.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::ce {

using core::memory::Type;

class icL_pro_ce_value_system_EXPORT Make
    : public core::ce::SystemValue
    , public service::Make
{
public:
    Make(core::il::InterLevel * il);

    // methods level 2

    /// `Make.image`
    void runImage(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_Make
