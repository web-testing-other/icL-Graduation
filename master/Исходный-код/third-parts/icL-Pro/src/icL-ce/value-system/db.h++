#ifndef ce_DB
#define ce_DB

#include <icL-il/export/global-pro.h++>

#include <icL-service/main/values/inode-pro.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::ce {

using core::il::StepType;
using core::memory::Type;

class icL_pro_ce_value_system_EXPORT DB
    : public core::ce::SystemValue
    , virtual public service::INode
{
public:
    DB(core::il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_DB
