#ifndef ce_File
#define ce_File

#include <icL-service/value-system/file.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::ce {

using core::ce::Prefix;
using core::il::StepType;
using core::memory::Type;

/**
 * @brief The File class represents a `File` token
 */
class icL_pro_ce_value_system_EXPORT File
    : public core::ce::SystemValue
    , public service::File
{
public:
    /**
     * @brief File is the default constructor
     * @param il is the Node() argument
     */
    File(core::il::InterLevel * il);

    // properties level 2

    /// `File'csv`
    void runCsv();

    /// `File'none`
    void runNone();

    /// `File'tsv`
    void runTsv();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::ce

#endif  // ce_File
