#ifndef ce_Request
#define ce_Request

#include <icL-service/value-system/request.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::ce {

using core::il::StepType;
using core::memory::Type;

class icL_pro_ce_value_system_EXPORT Request
    : public core::ce::SystemValue
    , service::Request
{
public:
    Request(core::il::InterLevel * il);

    // methods level 2
    /// `Request.confirm`
    void runConfirm(const core::memory::ArgList & args);

    /// `Request.ask`
    void runAsk(const core::memory::ArgList & args);

    /// `Request.int`
    void runInt(const core::memory::ArgList & args);

    /// `Request.double`
    void runDouble(const core::memory::ArgList & args);

    /// `Request.string`
    void runString(const core::memory::ArgList & args);

    /// `Request.list`
    void runList(const core::memory::ArgList & args);

    /// `Request.tab`
    void runTab(const core::memory::ArgList & args);

    // Value interface
public:
    Type         type() const;
    icString     typeName();
    void runMethod(const icString & name, const core::memory::ArgList & args);
};

}  // namespace icL::ce

#endif  // ce_Request
