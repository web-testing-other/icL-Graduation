#include "db.h++"

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/db-server.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>

#include <icL-ce/value-base/system/db-value.h++>



namespace icL::ce {

DB::DB(core::il::InterLevel * il)
    : SystemValue(il) {}

int DB::currentRunRank(bool rtl) {
    return rtl ? 8 : 0;
}

StepType DB::runNow() {
    il::DB db;

    db.target = std::make_shared<il::DBTarget>(pro(il)->db->getCurrentTarget());
    m_newContext = il->factory->fromValue(il, db);

    return StepType::CommandEnd;
}

Type DB::type() const {
    return static_cast<Type>(memory::Type::DB);
}

icString DB::typeName() {
    return "DB";
}

}  // namespace icL::ce
