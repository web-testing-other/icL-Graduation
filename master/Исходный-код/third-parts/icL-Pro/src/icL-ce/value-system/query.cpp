#include "query.h++"

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/db-server.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>

#include <icL-ce/value-base/system/query-value.h++>



namespace icL::ce {

Query::Query(core::il::InterLevel * il)
    : SystemValue(il) {}

int Query::currentRunRank(bool rtl) {
    return rtl ? 8 : 0;
}

StepType Query::runNow() {
    il::Query query;

    query.target =
      std::make_shared<il::DBTarget>(pro(il)->db->getCurrentTarget());
    m_newContext = il->factory->fromValue(il, query);

    return StepType::CommandEnd;
}

Type Query::type() const {
    return static_cast<Type>(memory::Type::Query);
}

icString Query::typeName() {
    return "Query";
}

}  // namespace icL::ce
