#include "dbmanager.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/value-base/system/db-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

DBManager::DBManager(core::il::InterLevel * il)
    : SystemValue(il) {}

void DBManager::runConnect(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, connect(args[0]));
    }
    else if (checkArgs(
               args,
               {Type::StringValue, Type::StringValue, Type::StringValue})) {
        m_newContext =
          il->factory->fromValue(il, connect(args[0], args[1], args[2]));
    }
}

void DBManager::runOpenSQLite(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, openSQLite(args[0]));
    }
}

Type DBManager::type() const {
    return static_cast<Type>(memory::Type::DBManager);
}

icString DBManager::typeName() {
    return "DBManager";
}

void DBManager::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (DBManager::*)(const core::memory::ArgList &)>
      methods{{"connect", &DBManager::runConnect},
              {"openSQLite", &DBManager::runOpenSQLite}};

    runMethodNow<DBManager, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
