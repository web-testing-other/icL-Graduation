#include "request.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/value-base/base/bool-value.h++>
#include <icL-ce/value-base/base/double-value.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/base/string-value.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/browser/tab-value.h++>
#include <icL-ce/value-base/complex/list-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Request::Request(core::il::InterLevel * il)
    : SystemValue(il) {}

void Request::runConfirm(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        confirm(args[0]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void Request::runAsk(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, ask(args[0]));
    }
}

void Request::runInt(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, int_(args[0]));
    }
}

void Request::runDouble(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, double_(args[0]));
    }
}

void Request::runString(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, string(args[0]));
    }
}

void Request::runList(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, list(args[0]));
    }
}

void Request::runTab(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, tab(args[0]));
    }
}

Type Request::type() const {
    return static_cast<Type>(memory::Type::Request);
}

icString Request::typeName() {
    return "Request";
}

void Request::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Request::*)(const core::memory::ArgList &)>
      methods{
        {"confirm", &Request::runConfirm}, {"ask", &Request::runAsk},
        {"int", &Request::runInt},         {"double", &Request::runDouble},
        {"string", &Request::runString},   {"list", &Request::runList},
        {"tab", &Request::runTab}};

    runMethodNow<Request, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
