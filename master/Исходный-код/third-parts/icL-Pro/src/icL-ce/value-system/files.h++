#ifndef ce_Files
#define ce_Files

#include <icL-service/value-system/files.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::ce {

using core::il::StepType;
using core::memory::Type;

class icL_pro_ce_value_system_EXPORT Files
    : public core::ce::SystemValue
    , public service::Files
{
public:
    Files(core::il::InterLevel * il);

    // methods level 2

    /// `Files.create`
    void runCreate(const core::memory::ArgList & args);

    /// `Files.createDir`
    void runCreateDir(const core::memory::ArgList & args);

    /// `Files.createPath`
    void runCreatePath(const core::memory::ArgList & args);

    /// `Files.open`
    void runOpen(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_Files
