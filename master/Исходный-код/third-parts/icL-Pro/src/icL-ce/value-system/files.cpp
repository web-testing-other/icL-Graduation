#include "files.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/file-target.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/system/file-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Files::Files(core::il::InterLevel * il)
    : SystemValue(il) {}

void Files::runCreate(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, create(args[0]));
    }
}

void Files::runCreateDir(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        createDir(args[0]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void Files::runCreatePath(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        createPath(args[0]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void Files::runOpen(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, open(args[0]));
    }
}

Type Files::type() const {
    return static_cast<Type>(memory::Type::Files);
}

icString Files::typeName() {
    return "Files";
}

void Files::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (Files::*)(const core::memory::ArgList &)>
      methods{{"create", &Files::runCreate},
              {"createDir", &Files::runCreateDir},
              {"createPath", &Files::runCreatePath}};

    runMethodNow<Files, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
