#ifndef ce_DBManager
#define ce_DBManager

#include <icL-service/value-system/dbmanager.h++>

#include <icL-ce/base/value/system-value.h++>



namespace icL::ce {

using core::il::StepType;
using core::memory::Type;

class icL_pro_ce_value_system_EXPORT DBManager
    : public core::ce::SystemValue
    , public service::DBManager
{
public:
    DBManager(core::il::InterLevel * il);

    // methods level 2

    /// `DBManager.connect`
    void runConnect(const core::memory::ArgList & args);

    /// `DBManager.openSQLite`
    void runOpenSQLite(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_DBManager
