#ifndef ce_DocumentValue
#define ce_DocumentValue

#include <icL-service/main/values/inode-pro.h++>
#include <icL-service/value-base/browser/document-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL {

namespace il {
struct TargetData;
}

namespace ce {

using core::ce::Prefix;
using core::memory::Type;

/**
 * @brief The Document class represents a `Document` token
 */
class icL_pro_ce_value_base_EXPORT DocumentValue
    : public core::ce::BaseValue
    , public service::DocumentValue
    , virtual public service::INode
{
public:
    /// @brief SessionValue calls BaseValue(il, container, varName, readonly)
    DocumentValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief SessionValue calls BaseValue(il, rvalue)
    DocumentValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief SessionValue calls BaseValue(value)
    DocumentValue(core::ce::BaseValue * value);

    // properties level 2

    /// `document'tab`
    void runTab();

    // methods level 2

    /// `document.click`
    void runClick(const core::memory::ArgList & args);

    /// `document.hover`
    void runHover(const core::memory::ArgList & args);

    /// `document.mouseDown`
    void runMouseDown(const core::memory::ArgList & args);

    /// `document.mouseUp`
    void runMouseUp(const core::memory::ArgList & args);

    /// `document.type`
    void runType(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_DocumentValue
