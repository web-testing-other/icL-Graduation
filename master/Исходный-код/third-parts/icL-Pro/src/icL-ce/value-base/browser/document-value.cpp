#include "document-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-base/browser/element-value.h++>
#include <icL-ce/value-base/browser/tab-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

DocumentValue::DocumentValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DocumentValue::DocumentValue(
  core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DocumentValue::DocumentValue(core::ce::BaseValue * value)
    : BaseValue(value) {}

void DocumentValue::runTab() {
    m_newContext = il->factory->fromValue(il, tab());
}

void DocumentValue::runClick(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::ClickData data;

        data.loadDictionary(args[0]);
        click(data);

        m_newContext = il->factory->clone(this);
    }
}

void DocumentValue::runHover(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::HoverData data;

        data.loadDictionary(args[0]);
        hover(data);

        m_newContext = il->factory->clone(this);
    }
}

void DocumentValue::runMouseDown(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData data;

        data.loadDictionary(args[0]);
        mouseDown(data);

        m_newContext = il->factory->clone(this);
    }
}

void DocumentValue::runMouseUp(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData data;

        data.loadDictionary(args[0]);
        mouseUp(data);

        m_newContext = il->factory->clone(this);
    }
}

void DocumentValue::runType(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        typeMethod(args[0]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

Type DocumentValue::type() const {
    return static_cast<Type>(memory::Type::DocumentValue);
}

icString DocumentValue::typeName() {
    return "Doc";
}

void DocumentValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (DocumentValue::*)()> properties{
      {"tab", &DocumentValue::runTab}};

    pro(il)->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<DocumentValue, BaseValue>(
      properties, prefix, name);
    pro(il)->server->popTarget();
}

void DocumentValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (DocumentValue::*)(const core::memory::ArgList &)>
      methods{{"click", &DocumentValue::runClick},
              {"hover", &DocumentValue::runHover},
              {"mouseDown", &DocumentValue::runMouseDown},
              {"mouseup", &DocumentValue::runMouseUp},
              {"type", &DocumentValue::runType}};

    pro(il)->server->pushTarget(*_value().data);
    runMethodNow<DocumentValue, BaseValue>(methods, name, args);
    pro(il)->server->popTarget();
}

}  // namespace icL::ce
