#include "tab-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/bool-value.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-browser/document/cookies.h++>
#include <icL-ce/value-browser/window/tabs.h++>
#include <icL-ce/value-browser/window/windows.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

using core::il::CE;

TabValue::TabValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

TabValue::TabValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

TabValue::TabValue(BaseValue * value)
    : BaseValue(value) {}

CE * TabValue::cookies() {
    return new Cookies{il, *_value().data};
}

CE * TabValue::doc() {
    il::Document document;
    document.data = std::make_shared<il::TargetData>(*_value().data);
    return il->factory->fromValue(il, document);
}

CE * TabValue::tabs() {
    return new Tabs{il, *_value().data};
}

class UrlFixator : public core::il::IValueFixator
{
    il::InterLevel * il;

public:
    UrlFixator(il::InterLevel * il)
        : il(il) {}

    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        il->server->pushTarget(*to<il::Tab>(value).data);
        value = il->server->url();
        il->server->popTarget();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        il->server->pushTarget(*to<il::Tab>(value).data);
        il->server->load(request);
        il->server->popTarget();
    }
};

CE * TabValue::url() {
    auto * node    = il->factory->clone(this, memory::Type::TabValue);
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(std::make_shared<UrlFixator>(pro(il)));
    return node;
}

CE * TabValue::window() {
    return new Windows{il, *_value().data};
}

void TabValue::runCookies() {
    m_newContext = cookies();
}

void TabValue::runDoc() {
    m_newContext = doc();
}

void TabValue::runScreenshot() {
    m_newContext = il->factory->fromValue(il, screenshot());
}

void TabValue::runSource() {
    m_newContext = il->factory->fromValue(il, source());
}

void TabValue::runTitle() {
    m_newContext = il->factory->fromValue(il, title());
}

void TabValue::runTabs() {
    m_newContext = tabs();
}

void TabValue::runUrl() {
    m_newContext = url();
}

void TabValue::runWindow() {
    m_newContext = window();
}

void TabValue::runBack(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        back();
        m_newContext = il->factory->clone(this);
    }
}

void TabValue::runClose(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void TabValue::runFocus(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        focus();
        m_newContext = il->factory->clone(this);
    }
}

void TabValue::runForward(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        forward();
        m_newContext = il->factory->clone(this);
    }
}

void TabValue::runGet(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, get(args[0]));
    }
}

void TabValue::runLoad(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        load(args[0]);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

Type TabValue::type() const {
    return static_cast<Type>(memory::Type::TabValue);
}

icString TabValue::typeName() {
    return "tab";
}

void TabValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (TabValue::*)()> properties{
      {"cookies", &TabValue::runCookies},
      {"doc", &TabValue::runDoc},
      {"screenshot", &TabValue::runScreenshot},
      {"source", &TabValue::runSource},
      {"title", &TabValue::runTitle},
      {"tabs", &TabValue::runTabs},
      {"url", &TabValue::runUrl},
      {"window", &TabValue::runWindow}};

    pro(il)->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<TabValue, BaseValue>(properties, prefix, name);
    pro(il)->server->popTarget();
}

void TabValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (TabValue::*)(const core::memory::ArgList &)>
      methods{
        {"back", &TabValue::runBack},   {"close", &TabValue::runClose},
        {"focus", &TabValue::runFocus}, {"forward", &TabValue::runForward},
        {"get", &TabValue::runGet},     {"load", &TabValue::runLoad}};

    pro(il)->server->pushTarget(*_value().data);
    runMethodNow<TabValue, BaseValue>(methods, name, args);
    pro(il)->server->popTarget();
}

}  // namespace icL::ce
