#ifndef ce_WindowValue
#define ce_WindowValue

#include <icL-service/value-base/browser/window-value.h++>

#include <icL-ce/base/value/base-value.h++>



class icRect;

namespace icL {

namespace il {
struct Window;
struct Element;
}  // namespace il

namespace ce {

using core::ce::Prefix;
using core::memory::Type;

class icL_pro_ce_value_base_EXPORT WindowValue
    : public core::ce::BaseValue
    , public service::WindowValue
{
public:
    /// @brief WindowValue calls BaseValue(il, container, varName, readonly)
    WindowValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief WindowValue calls BaseValue(il, rvalue)
    WindowValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief WindowValue calls BaseValue(value)
    WindowValue(core::ce::BaseValue * value);

private:
    /**
     * @brief runIntProperty runs a int property
     * @return a contextual entity for needed int propeperty
     */
    core::il::CE * runRectProperty(
      double (icRect::*getter)() const, void (icRect::*setter)(double));

public:
    // properties level 1

    /// `[r/w] window'height : int`
    core::il::CE * height();

    /// `[r/o] window'tab : tab`
    core::il::CE * tab();

    /// `[r/w] window'width : int`
    core::il::CE * width();

    /// `[r/o] window'windows : Windows`
    core::il::CE * windows();

    /// `[r/w] window'x : int`
    core::il::CE * x();

    /// `[r/w] window'y : int`
    core::il::CE * y();

    // properties level 2

    /// `window'height`
    void runHeight();

    /// `window'tab`
    void runTab();

    /// `window'width`
    void runWidth();

    /// `window'windows`
    void runWindows();

    /// `window'x`
    void runX();

    /// `window'y`
    void runY();

    // methods level 2

    /// `window.close`
    void runClose(const core::memory::ArgList & args);

    /// `window.focus`
    void runFocus(const core::memory::ArgList & args);

    /// `window.fullscreen`
    void runFullscreen(const core::memory::ArgList & args);

    /// `window.maximize`
    void runMaximize(const core::memory::ArgList & args);

    /// `window.minimize`
    void runMinimize(const core::memory::ArgList & args);

    /// `window.restore`
    void runRestore(const core::memory::ArgList & args);

    /// `window.switchToFrame`
    void runSwitchToFrame(const core::memory::ArgList & args);

    /// `window.switchToParent`
    void runSwitchToParent(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_WindowValue
