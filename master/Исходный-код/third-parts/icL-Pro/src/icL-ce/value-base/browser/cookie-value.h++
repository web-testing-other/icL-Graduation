#ifndef ce_CookieValue
#define ce_CookieValue

#include <icL-service/value-base/browser/cookie-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL {

namespace il {
struct CookieData;
struct Cookie;
}  // namespace il

namespace ce {

using core::ce::Prefix;
using core::memory::Type;
using CE = core::il::CE;

/**
 * @brief The CookieValue class represents a `cookie` value
 */
class icL_pro_ce_value_base_EXPORT CookieValue
    : public core::ce::BaseValue
    , public service::CookieValue
{
public:
    /// @brief CookieValue calls BaseValue(il, container, varName, readonly)
    CookieValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief CookieValue calls BaseValue(il, rvalue)
    CookieValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief CookieValue calls BaseValue(value)
    CookieValue(BaseValue * value);

private:
    /**
     * @brief runCookieProperty runs a cookie specified property
     * @param getter is the getter of property value
     * @param setter is the setter of property value
     * @return a editable contextual entity value
     */
    core::il::CE * runCookieProperty(
      const std::shared_ptr<core::il::IValueFixator> & fixator, int type);

public:
    // properties level 1

    /// `[r/o] cookie'cookies : Cookies`
    core::il::CE * cookies();

    /// `[r/w] cookie'domain : string`
    core::il::CE * domain();

    /// `[r/w] cookie'expiry : int`
    core::il::CE * expiry();

    /// `[r/w] cookie'httpOnly : bool`
    core::il::CE * httpOnly();

    /// `[r/w] cookie'name : string`
    core::il::CE * name();

    /// `[r/w] cookie'path : string`
    core::il::CE * path();

    /// `[r/w] cookie'secure : bool`
    core::il::CE * secure();

    /// `[r/w] cookie'value : string`
    core::il::CE * value();

    // properties level 2

    /// `cookie'cookies`
    void runCookies();

    /// `cookie'domain`
    void runDomain();

    /// `cookie'expiry`
    void runExpiry();

    /// `cookie'httpOnly`
    void runHttpOnly();

    /// `cookie'name`
    void runName();

    /// `cookie'path`
    void runPath();

    /// `cookie'secure`
    void runSecure();

    /// `cookie'value`
    void runValue();

    /// `cookie.add`
    void runAdd(const core::memory::ArgList & args);

    /// `cookie.load`
    void runLoad(const core::memory::ArgList & args);

    /// `cookie.resetTime`
    void runResetTime(const core::memory::ArgList & args);

    /// `cookie.save`
    void runSave(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_CookieValue
