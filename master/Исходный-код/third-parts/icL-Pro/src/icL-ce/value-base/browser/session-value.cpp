#include "session-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/string-value.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-browser/window/alert.h++>
#include <icL-ce/value-browser/window/tabs.h++>
#include <icL-ce/value-browser/window/windows.h++>



namespace icL::ce {

using core::il::CE;

SessionValue::SessionValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

SessionValue::SessionValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

SessionValue::SessionValue(BaseValue * value)
    : BaseValue(value) {}

class TimeoutFixator : public core::il::IValueFixator
{
    il::InterLevel * il;
    int (il::FrontEnd::*_getter)();
    void (il::FrontEnd::*_setter)(int);

public:
    TimeoutFixator(
      il::InterLevel * il, int (il::FrontEnd::*getter)(),
      void (il::FrontEnd::*setter)(int))
        : il(il)
        , _getter(getter)
        , _setter(setter) {}

    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = (il->server->*_getter)();
    }
    void setter(
      icVariant & /*value*/, const icVariant & request) const override {
        (il->server->*_setter)(request);
    }
};

core::il::CE * SessionValue::runIntProperty(
  int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int)) {
    auto * node    = il->factory->clone(this, Type::IntValue);
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(
      std::make_shared<TimeoutFixator>(pro(il), getter, setter));
    return node;
}

CE * SessionValue::alert() {
    return new Alert{il, *_value().data};
}

CE * SessionValue::implicitTimeout() {
    return runIntProperty(
      &il::FrontEnd::implicitTimeout, &il::FrontEnd::setImplicitTimeout);
}

CE * SessionValue::pageLoadTimeout() {
    return runIntProperty(
      &il::FrontEnd::pageLoadTimeout, &il::FrontEnd::setPageLoadTimeOut);
}

CE * SessionValue::scriptTimeout() {
    return runIntProperty(
      &il::FrontEnd::scriptTimeout, &il::FrontEnd::setScriptTimeout);
}

CE * SessionValue::tabs() {
    return new Tabs{il, *_value().data};
}

class UrlFixator : public core::il::IValueFixator
{
    il::InterLevel * il;

public:
    UrlFixator(il::InterLevel * il)
        : il(il) {}

    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = il->server->url();
    }
    void setter(
      icVariant & /*value*/, const icVariant & request) const override {
        il->server->load(request);
    }
};

CE * SessionValue::url() {
    auto * node    = il->factory->clone(this, Type::StringValue);
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(std::make_shared<UrlFixator>(pro(il)));
    return node;
}

CE * SessionValue::windows() {
    return new Windows{il, *_value().data};
}

void SessionValue::runAlert() {
    m_newContext = alert();
}

void SessionValue::runImplicitTimeout() {
    m_newContext = implicitTimeout();
}

void SessionValue::runPageLoadTimeout() {
    m_newContext = pageLoadTimeout();
}

void SessionValue::runScriptTimeout() {
    m_newContext = scriptTimeout();
}

void SessionValue::runSource() {
    m_newContext = il->factory->fromValue(il, source());
}

void SessionValue::runTabs() {
    m_newContext = tabs();
}

void SessionValue::runTitle() {
    m_newContext = il->factory->fromValue(il, title());
}

void SessionValue::runUrl() {
    m_newContext = url();
}

void SessionValue::runWindows() {
    m_newContext = windows();
}

void SessionValue::runBack(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        back();
        m_newContext = il->factory->clone(this);
    }
}

void SessionValue::runClose(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void SessionValue::runForward(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        forward();
        m_newContext = il->factory->clone(this);
    }
}

void SessionValue::runRefresh(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        refresh();
        m_newContext = il->factory->clone(this);
    }
}

void SessionValue::runScreenshot(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, screenshot());
    }
}

void SessionValue::runSwitchTo(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        switchTo();
        m_newContext = il->factory->clone(this);
    }
}

void SessionValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (SessionValue::*)()> properties{
      {"alert", &SessionValue::runAlert},
      {"implicitTimeout", &SessionValue::runImplicitTimeout},
      {"pageLoadTimeout", &SessionValue::runPageLoadTimeout},
      {"scriptTimeout", &SessionValue::runScriptTimeout},
      {"source", &SessionValue::runSource},
      {"tabs", &SessionValue::runTabs},
      {"title", &SessionValue::runTitle},
      {"url", &SessionValue::runUrl},
      {"windows", &SessionValue::runWindows}};

    pro(il)->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<SessionValue, BaseValue>(
      properties, prefix, name);
    pro(il)->server->popTarget();
}

void SessionValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (SessionValue::*)(const core::memory::ArgList &)>
      methods{{"back", &SessionValue::runBack},
              {"close", &SessionValue::runClose},
              {"forward", &SessionValue::runForward},
              {"refresh", &SessionValue::runRefresh},
              {"screenshot", &SessionValue::runScreenshot},
              {"switchTo", &SessionValue::runSwitchTo}};

    pro(il)->server->pushTarget(*_value().data);
    runMethodNow<SessionValue, BaseValue>(methods, name, args);
    pro(il)->server->popTarget();
}

il::Session icL::ce::SessionValue::_value() {
    return value();
}

Type SessionValue::type() const {
    return static_cast<Type>(memory::Type::SessionValue);
}

icString SessionValue::typeName() {
    return "session";
}

}  // namespace icL::ce
