#include "element-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property.h++>
#include <icL-ce/literal/functional/property-pro.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

ElementValue::ElementValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ElementValue::ElementValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ElementValue::ElementValue(core::ce::BaseValue * value)
    : BaseValue(value) {}

void ElementValue::runAttr(const icString & name) {
    m_newContext = il->factory->fromValue(il, attr(name));
}

void ElementValue::runCss(const icString & name) {
    m_newContext = il->factory->fromValue(il, css(name));
}

void ElementValue::runProp(const icString & name) {
    m_newContext = il->factory->fromValue(il, prop(name));
}

void ElementValue::runDocument() {
    m_newContext = il->factory->fromValue(il, document());
}

void ElementValue::runEnabled() {
    m_newContext = il->factory->fromValue(il, enabled());
}

void ElementValue::runRect() {
    icRect       r = rect();
    icVariantMap map{
      {"width", r.width()}, {"height", r.height()}, {"x", r.x()}, {"y", r.y()}};
    m_newContext = il->factory->fromValue(il, core::memory::Object(il, map));
}

void ElementValue::runSelected() {
    m_newContext = il->factory->fromValue(il, selected());
}

void ElementValue::runTag() {
    m_newContext = il->factory->fromValue(il, tag());
}

void ElementValue::runText() {
    m_newContext = il->factory->fromValue(il, text());
}

void ElementValue::runVisible() {
    m_newContext = il->factory->fromValue(il, visible());
}

void ElementValue::runChild(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, child(args[0]));
    }
}

void ElementValue::runChildren(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, children());
    }
}

void ElementValue::runClear(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runClick(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::ClickData cd;

        cd.loadDictionary(args[0]);
        click(cd);

        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {})) {
        click({});
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runClosest(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, closest(args[0]));
    }
}

void ElementValue::runCopy(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, copy());
    }
}

void ElementValue::runFastType(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        fastType(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runForceClick(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::ClickData cd;

        cd.loadDictionary(args[0]);
        forceClick(cd);

        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runForceType(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        forceType(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runHover(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::HoverData hd;

        hd.loadDictionary(args[0]);
        hover(hd);

        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runKeyDown(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        keyDown(args[0], args[1]);
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runKeyPress(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue, Type::StringValue})) {
        keyPress(args[0], args[1], args[2]);
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runKeyUp(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        keyUp(args[0], args[1]);
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runMouseDown(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData md;

        md.loadDictionary(args[0]);
        mouseDown(md);

        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runMouseUp(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData md;

        md.loadDictionary(args[0]);
        mouseUp(md);

        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runNext(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext =
          il->factory->fromValue(il, service::ElementValue::next());
    }
}

void ElementValue::runParent(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, parent());
    }
}

void ElementValue::runPaste(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        paste(args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runPrev(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext =
          il->factory->fromValue(il, service::ElementValue::prev());
    }
}

void ElementValue::runScreenshot(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, screenshot());
    }
}

void ElementValue::runSendKeys(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::StringValue})) {
        sendKeys(args[0], args[1]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {Type::StringValue})) {
        sendKeys(0, args[0]);
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runSuperClick(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        superClick();
        m_newContext = il->factory->clone(this);
    }
}

void ElementValue::runOwnProperty(const icString & name) {
    static icObject<icString, void (ElementValue::*)()> properties{
      {"doc", &ElementValue::runDocument},
      {"enabled", &ElementValue::runEnabled},
      {"rect", &ElementValue::runRect},
      {"selected", &ElementValue::runSelected},
      {"tag", &ElementValue::runTag},
      {"text", &ElementValue::runText},
      {"visible", &ElementValue::runVisible}};

    Prefix prefix = Prefix::None;
    runPropertyNow<ElementValue, BaseValue>(properties, prefix, name);
}

Type ElementValue::type() const {
    return static_cast<Type>(memory::Type::ElementValue);
}

icString ElementValue::typeName() {
    return "element";
}

void ElementValue::runProperty(Prefix prefix, const icString & name) {
    pro(il)->server->pushTarget(_value().target);

    switch (int(prefix)) {
    case proPrefix::Attr:
        runAttr(name);
        break;

    case proPrefix::Css:
        runCss(name);
        break;

    case proPrefix::Prop:
        runProp(name);
        break;

    case Prefix::None:
        runOwnProperty(name);
        break;
    }

    pro(il)->server->popTarget();
}

void ElementValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (ElementValue::*)(const core::memory::ArgList &)>
      methods{{"child", &ElementValue::runChild},
              {"children", &ElementValue::runChildren},
              {"clear", &ElementValue::runClear},
              {"click", &ElementValue::runClick},
              {"closest", &ElementValue::runClosest},
              {"copy", &ElementValue::runCopy},
              {"fastType", &ElementValue::runFastType},
              {"forceClick", &ElementValue::runClick},
              {"forceType", &ElementValue::runForceType},
              {"hover", &ElementValue::runHover},
              {"keyDown", &ElementValue::runKeyUp},
              {"keyPress", &ElementValue::runKeyPress},
              {"keyUp", &ElementValue::runKeyUp},
              {"mouseDown", &ElementValue::runMouseDown},
              {"mouseUp", &ElementValue::runMouseUp},
              {"next", &ElementValue::runNext},
              {"parent", &ElementValue::runParent},
              {"paste", &ElementValue::runPaste},
              {"prev", &ElementValue::runPrev},
              {"screenshot", &ElementValue::runScreenshot},
              {"sendKeys", &ElementValue::runSendKeys},
              {"superClick", &ElementValue::runSuperClick}};

    pro(il)->server->pushTarget(_value().target);
    runMethodNow<ElementValue, BaseValue>(methods, name, args);
    pro(il)->server->popTarget();
}


}  // namespace icL::ce
