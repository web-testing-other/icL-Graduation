#include "elements-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property.h++>
#include <icL-ce/literal/functional/property-pro.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

ElementsValue::ElementsValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ElementsValue::ElementsValue(
  core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ElementsValue::ElementsValue(BaseValue * value)
    : BaseValue(value) {}

void ElementsValue::runAttr(const icString & name) {
    m_newContext = il->factory->fromValue(il, attr(name));
}

void ElementsValue::runEmpty() {
    m_newContext = il->factory->fromValue(il, empty());
}

void ElementsValue::runLength() {
    m_newContext = il->factory->fromValue(il, length());
}

void ElementsValue::runProp(const icString & name) {
    m_newContext = il->factory->fromValue(il, prop(name));
}

void ElementsValue::runRects() {
    //    m_newContext = il->factory->fromValue(il, rects());
}

void ElementsValue::runTags() {
    m_newContext = il->factory->fromValue(il, tags());
}

void ElementsValue::runTexts() {
    m_newContext = il->factory->fromValue(il, texts());
}

void ElementsValue::runAdd(const core::memory::ArgList & args) {
    if (checkArgs(args, {memory::Type::ElementValue})) {
        m_newContext = il->factory->fromValue(il, add_m1(args[0]));
    }
    else if (checkArgs(args, {memory::Type::ElementsValue})) {
        m_newContext = il->factory->fromValue(il, add_mm(args[0]));
    }
}

void ElementsValue::runCopy(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, copy());
    }
}

void ElementsValue::runFilter(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, filter(args[0]));
    }
}

void ElementsValue::runGet(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, get(args[0]));
    }
}

void ElementsValue::runOwnProperty(const icString & name) {
    static icObject<icString, void (ElementsValue::*)()> properties{
      {"empty", &ElementsValue::runEmpty},
      {"length", &ElementsValue::runLength},
      {"rects", &ElementsValue::runRects},
      {"tags", &ElementsValue::runTags},
      {"texts", &ElementsValue::runTexts}};

    bool isNumber;
    int  number = name.toInt(isNumber);

    if (isNumber) {
        m_newContext = il->factory->fromValue(il, get(number));
    }
    else {
        Prefix prefix = Prefix::None;
        runPropertyNow<ElementsValue, BaseValue>(properties, prefix, name);
    }
}

Type ElementsValue::type() const {
    return static_cast<Type>(memory::Type::ElementsValue);
}

icString ElementsValue::typeName() {
    return "element";
}

void ElementsValue::runProperty(Prefix prefix, const icString & name) {
    switch (int(prefix)) {
    case proPrefix::Attr:
        runAttr(name);
        break;

    case proPrefix::Prop:
        runProp(name);
        break;

    case proPrefix::Css:
        break;

    case Prefix::None:
        runOwnProperty(name);
        break;
    }
}

void ElementsValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (ElementsValue::*)(const core::memory::ArgList &)>
      methods{{"add", &ElementsValue::runAdd},
              {"filter", &ElementsValue::runFilter},
              {"get", &ElementsValue::runGet}};

    runMethodNow<ElementsValue, BaseValue>(methods, name, args);
}


}  // namespace icL::ce
