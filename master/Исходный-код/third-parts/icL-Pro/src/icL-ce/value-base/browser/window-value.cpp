#include "window-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/void-value.h++>
#include <icL-ce/value-browser/window/windows.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

using core::il::CE;

WindowValue::WindowValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

WindowValue::WindowValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

WindowValue::WindowValue(BaseValue * value)
    : BaseValue(value) {}

class RectFixator : public core::il::IValueFixator
{
    il::InterLevel * il;
    double (icRect::*_getter)() const;
    void (icRect::*_setter)(double);

public:
    RectFixator(
      il::InterLevel * il, double (icRect::*getter)() const,
      void (icRect::*setter)(double))
        : il(il)
        , _getter(getter)
        , _setter(setter) {}

    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        il->server->pushTarget(*to<il::Window>(value).data);
        value = (int)(il->server->getWindowRect().*_getter)();
        il->server->popTarget();
    }
    void setter(icVariant & value, const icVariant & request) const override {
        icRect rect;
        il->server->pushTarget(*to<il::Window>(value).data);
        rect = il->server->getWindowRect();
        (rect.*_setter)(request.toInt());
        il->server->setWindowRect(rect);
        il->server->popTarget();
    }
};

CE * WindowValue::runRectProperty(
  double (icRect::*getter)() const, void (icRect::*setter)(double)) {
    auto * node    = il->factory->clone(this, Type::IntValue);
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(
      std::make_shared<RectFixator>(pro(il), getter, setter));
    return node;
}

CE * WindowValue::height() {
    return runRectProperty(&icRect::height, &icRect::setHeight);
}

CE * WindowValue::tab() {
    il::Tab tab;

    tab.data = _value().data;
    return il->factory->fromValue(il, tab);
}

CE * WindowValue::width() {
    return runRectProperty(&icRect::width, &icRect::setWidth);
}

CE * WindowValue::windows() {
    return new Windows{il, *_value().data};
}

CE * WindowValue::x() {
    return runRectProperty(&icRect::x, &icRect::setX);
}

CE * WindowValue::y() {
    return runRectProperty(&icRect::y, &icRect::setY);
}

void WindowValue::runHeight() {
    m_newContext = height();
}

void WindowValue::runTab() {
    m_newContext = tab();
}

void WindowValue::runWidth() {
    m_newContext = width();
}

void WindowValue::runWindows() {
    m_newContext = windows();
}

void WindowValue::runX() {
    m_newContext = x();
}

void WindowValue::runY() {
    m_newContext = y();
}

void WindowValue::runClose(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void WindowValue::runFocus(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        focus();
        m_newContext = il->factory->clone(this);
    }
}

void WindowValue::runFullscreen(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        fullscreen();
        m_newContext = il->factory->clone(this);
    }
}

void WindowValue::runMaximize(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        maximize();
        m_newContext = il->factory->clone(this);
    }
}

void WindowValue::runMinimize(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        minimize();
        m_newContext = il->factory->clone(this);
    }
}

void WindowValue::runRestore(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        restore();
        m_newContext = il->factory->clone(this);
    }
}

void WindowValue::runSwitchToFrame(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        switchToFrame(int(args[0]));
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(args, {memory::Type::ElementValue})) {
        switchToFrame(il::Element(args[0]));
        m_newContext = il->factory->clone(this);
    }
}

void WindowValue::runSwitchToParent(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        switchToParent();
        m_newContext = il->factory->clone(this);
    }
}

Type WindowValue::type() const {
    return static_cast<Type>(memory::Type::WindowValue);
}

icString WindowValue::typeName() {
    return "window";
}

void WindowValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (WindowValue::*)()> properties{
      {"height", &WindowValue::runHeight},
      {"tab", &WindowValue::runTab},
      {"width", &WindowValue::runWidth},
      {"windows", &WindowValue::runWindows},
      {"x", &WindowValue::runX},
      {"y", &WindowValue::runY}};

    pro(il)->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<WindowValue, BaseValue>(
      properties, prefix, name);
    pro(il)->server->popTarget();
}

void WindowValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (WindowValue::*)(const core::memory::ArgList &)>
      methods{{"close", &WindowValue::runClose},
              {"focus", &WindowValue::runFocus},
              {"fullscreen", &WindowValue::runFullscreen},
              {"maximize", &WindowValue::runMaximize},
              {"minimize", &WindowValue::runMinimize},
              {"restore", &WindowValue::runRestore},
              {"switchToFrame", &WindowValue::runSwitchToFrame},
              {"switchToParent", &WindowValue::runSwitchToParent}};

    pro(il)->server->pushTarget(*_value().data);
    runMethodNow<WindowValue, BaseValue>(methods, name, args);
    pro(il)->server->popTarget();
}

}  // namespace icL::ce
