#include "cookie-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/cookie-data.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-browser/document/cookies.h++>

#include <icL-memory/structures/argument.h++>

#include <utility>

namespace icL::ce {

using core::il::CE;

CookieValue::CookieValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

CookieValue::CookieValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

CookieValue::CookieValue(BaseValue * value)
    : BaseValue(value) {}

CE * CookieValue::runCookieProperty(
  const std::shared_ptr<core::il::IValueFixator> & fixator, int type) {
    auto * node    = il->factory->clone(this, type);
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(fixator);
    return node;
}

core::il::CE * CookieValue::cookies() {
    return new Cookies{il, _value().data->target};
}

class DomainFixator : public core::il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = to<il::Cookie>(value).data->domain;
    }
    void setter(icVariant & value, const icVariant & request) const override {
        to<il::Cookie>(value).data->domain = request;
    }
};

CE * CookieValue::domain() {
    return runCookieProperty(
      std::make_shared<DomainFixator>(), Type::StringValue);
}

class ExpiryFixator : public core::il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = to<il::Cookie>(value).data->expiry;
    }
    void setter(icVariant & value, const icVariant & request) const override {
        to<il::Cookie>(value).data->expiry = request;
    }
};

CE * CookieValue::expiry() {
    return runCookieProperty(std::make_shared<ExpiryFixator>(), Type::IntValue);
}

class HttpOnlyFixator : public core::il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = to<il::Cookie>(value).data->httpOnly;
    }
    void setter(icVariant & value, const icVariant & request) const override {
        to<il::Cookie>(value).data->httpOnly = request;
    }
};

CE * CookieValue::httpOnly() {
    return runCookieProperty(
      std::make_shared<HttpOnlyFixator>(), Type::BoolValue);
}

class NameFixator : public core::il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = to<il::Cookie>(value).data->name;
    }
    void setter(icVariant & value, const icVariant & request) const override {
        to<il::Cookie>(value).data->name = request;
    }
};

CE * CookieValue::name() {
    return runCookieProperty(
      std::make_shared<NameFixator>(), Type::StringValue);
}

class PathFixator : public core::il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = to<il::Cookie>(value).data->path;
    }
    void setter(icVariant & value, const icVariant & request) const override {
        to<il::Cookie>(value).data->path = request;
    }
};

CE * CookieValue::path() {
    return runCookieProperty(
      std::make_shared<PathFixator>(), Type::StringValue);
}

class SecureFixator : public core::il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = to<il::Cookie>(value).data->secure;
    }
    void setter(icVariant & value, const icVariant & request) const override {
        to<il::Cookie>(value).data->secure = request;
    }
};

CE * CookieValue::secure() {
    return runCookieProperty(
      std::make_shared<SecureFixator>(), Type::BoolValue);
}

class ValueFixator : public core::il::IValueFixator
{
    // IValueFixator interface
public:
    void getter(icVariant & value) const override {
        value = to<il::Cookie>(value).data->value;
    }
    void setter(icVariant & value, const icVariant & request) const override {
        to<il::Cookie>(value).data->value = request;
    }
};

CE * CookieValue::value() {
    return runCookieProperty(
      std::make_shared<ValueFixator>(), Type::StringValue);
}

void CookieValue::runCookies() {
    m_newContext = cookies();
}

void CookieValue::runDomain() {
    m_newContext = domain();
}

void CookieValue::runExpiry() {
    m_newContext = expiry();
}

void CookieValue::runHttpOnly() {
    m_newContext = httpOnly();
}

void CookieValue::runName() {
    m_newContext = name();
}

void CookieValue::runPath() {
    m_newContext = path();
}

void CookieValue::runSecure() {
    m_newContext = secure();
}

void CookieValue::runValue() {
    m_newContext = value();
}

void CookieValue::runAdd(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue, Type::IntValue})) {
        add(args[0], args[1], args[2]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::IntValue, Type::IntValue,
                      Type::IntValue})) {
        add(args[0], args[1], args[2], args[3]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::IntValue, Type::IntValue,
                      Type::IntValue, Type::IntValue})) {
        add(args[0], args[1], args[2], args[3], args[4]);
        m_newContext = il->factory->clone(this);
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::IntValue, Type::IntValue,
                      Type::IntValue, Type::IntValue, Type::IntValue})) {
        add(args[0], args[1], args[2], args[3], args[4], args[5]);
        m_newContext = il->factory->clone(this);
    }
}

void CookieValue::runLoad(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        load();
        m_newContext = il->factory->clone(this);
    }
}

void CookieValue::runResetTime(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        resetTime();
        m_newContext = il->factory->clone(this);
    }
}

void CookieValue::runSave(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        save();
        m_newContext = il->factory->clone(this);
    }
}

Type CookieValue::type() const {
    return static_cast<Type>(memory::Type::CookieValue);
}

icString CookieValue::typeName() {
    return "cookie";
}

void CookieValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (CookieValue::*)()> properties{
      {"cookies", &CookieValue::runCookies},
      {"domain", &CookieValue::runDomain},
      {"expiry", &CookieValue::runExpiry},
      {"httpOnly", &CookieValue::runHttpOnly},
      {"name", &CookieValue::runName},
      {"path", &CookieValue::runPath},
      {"secure", &CookieValue::runSecure},
      {"value", &CookieValue::runValue}};

    runPropertyWithPrefixCheck<CookieValue, BaseValue>(
      properties, prefix, name);
}

void CookieValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (CookieValue::*)(const core::memory::ArgList &)>
      methods{{"add", &CookieValue::runAdd},
              {"load", &CookieValue::runLoad},
              {"resetTime", &CookieValue::runResetTime},
              {"save", &CookieValue::runSave}};

    runMethodNow<CookieValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
