#ifndef ce_SessionValue
#define ce_SessionValue

#include <icL-service/value-base/browser/session-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL {

namespace il {
struct Session;
class FrontEnd;
}  // namespace il

namespace ce {

using core::ce::Prefix;
using core::memory::Type;

class icL_pro_ce_value_base_EXPORT SessionValue
    : public core::ce::BaseValue
    , public service::SessionValue
{
public:
    /// @brief SessionValue calls BaseValue(il, container, varName, readonly)
    SessionValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief SessionValue calls BaseValue(il, rvalue)
    SessionValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief SessionValue calls BaseValue(value)
    SessionValue(core::ce::BaseValue * value);

private:
    /**
     * @brief runIntProperty runs a int property
     * @return a contextual entity for needed int propeperty
     */
    core::il::CE * runIntProperty(
      int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int));

public:
    // properties level 1

    /// `session'alert : Alert`
    core::il::CE * alert();

    /// `[r/w] session'implicitTimeout : int`
    core::il::CE * implicitTimeout();

    /// `[r/w] session'pageLoadTimeout : int`
    core::il::CE * pageLoadTimeout();

    /// `[r/w] session'scriptTimeout : int`
    core::il::CE * scriptTimeout();

    /// `session'tabs : Tabs`
    core::il::CE * tabs();

    /// `[r/w] session'url : string`
    core::il::CE * url();

    /// `session'windows : Windows`
    core::il::CE * windows();

    // properties level 2

    /// `session'alert`
    void runAlert();

    /// `session'implicitTimeout`
    void runImplicitTimeout();

    /// `session'loadTimeout`
    void runPageLoadTimeout();

    /// `session'scriptTimeout`
    void runScriptTimeout();

    /// `session'source`
    void runSource();

    /// `session'tabs`
    void runTabs();

    /// `session'title`
    void runTitle();

    /// `session'url`
    void runUrl();

    /// `session'windows`
    void runWindows();

    // methods level 2

    /// `session.back`
    void runBack(const core::memory::ArgList & args);

    /// `session.close`
    void runClose(const core::memory::ArgList & args);

    /// `session.forward`
    void runForward(const core::memory::ArgList & args);

    /// `session.refresh`
    void runRefresh(const core::memory::ArgList & args);

    /// `session.screenshot`
    void runScreenshot(const core::memory::ArgList & args);

    /// `session.switchTo`
    void runSwitchTo(const core::memory::ArgList & args);

protected:
    il::Session _value();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_SessionValue
