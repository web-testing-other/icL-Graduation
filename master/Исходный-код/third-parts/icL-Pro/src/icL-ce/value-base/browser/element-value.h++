#ifndef ce_ElementValue
#define ce_ElementValue

#include <icL-service/value-base/browser/element-value.h++>

#include <icL-ce/base/value/base-value.h++>


namespace icL {

namespace il {
struct MouseData;
struct ClickData;
struct HoverData;
struct Element;
}  // namespace il

namespace ce {

using core::ce::Prefix;
using core::memory::Type;

/**
 * @brief The ElementValue class represents an `element` value
 */
class icL_pro_ce_value_base_EXPORT ElementValue
    : public core::ce::BaseValue
    , public service::ElementValue
{
public:
    /// @brief ElementValue calls BaseValue(il, container, varName, readonly)
    ElementValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief ElementValue calls BaseValue(il, rvalue)
    ElementValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief ElementValue calls BaseValue(value)
    ElementValue(BaseValue * value);

    // properties level 2

    /// `element'attr-*`
    void runAttr(const icString & name);

    /// `element'css-*`
    void runCss(const icString & name);

    /// `element'doc`
    void runDocument();

    /// `element'enabled`
    void runEnabled();

    /// `element'prop-*`
    void runProp(const icString & name);

    /// `element'rect`
    void runRect();

    /// `element'selected`
    void runSelected();

    /// `element'tag`
    void runTag();

    /// `element'text`
    void runText();

    /// `eleemnt'visible`
    void runVisible();

    // methods level 2

    /// `element.child`
    void runChild(const core::memory::ArgList & args);

    /// `element.children`
    void runChildren(const core::memory::ArgList & args);

    /// `element.clear`
    void runClear(const core::memory::ArgList & args);

    /// `element.click`
    void runClick(const core::memory::ArgList & args);

    /// `element.closest`
    void runClosest(const core::memory::ArgList & args);

    /// `element.copy`
    void runCopy(const core::memory::ArgList & args);

    /// `element.fastType`
    void runFastType(const core::memory::ArgList & args);

    /// `element.forceClick`
    void runForceClick(const core::memory::ArgList & args);

    /// `element.forceType`
    void runForceType(const core::memory::ArgList & args);

    /// `element.hover`
    void runHover(const core::memory::ArgList & args);

    /// `element.keyDown`
    void runKeyDown(const core::memory::ArgList & args);

    /// `element.keyPress`
    void runKeyPress(const core::memory::ArgList & args);

    /// `element.keyUp`
    void runKeyUp(const core::memory::ArgList & args);

    /// `element.mouseDown`
    void runMouseDown(const core::memory::ArgList & args);

    /// `element.mouseUp`
    void runMouseUp(const core::memory::ArgList & args);

    /// `element.next`
    void runNext(const core::memory::ArgList & args);

    /// `element.parent`
    void runParent(const core::memory::ArgList & args);

    /// `element.paste`
    void runPaste(const core::memory::ArgList & args);

    /// `element.prev`
    void runPrev(const core::memory::ArgList & args);

    /// `element.screenshot`
    void runScreenshot(const core::memory::ArgList & args);

    /// `element.sendKeys`
    void runSendKeys(const core::memory::ArgList & args);

    /// `element.sendKeys`
    void runSuperClick(const core::memory::ArgList & args);

private:
    /**
     * @brief runOwnProperty runs a property with `None` prefix
     * @param name is the name of property
     */
    void runOwnProperty(const icString & name);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_ElementValue
