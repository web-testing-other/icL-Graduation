#ifndef ce_LambdaJsValue
#define ce_LambdaJsValue

#include <icL-il/structures/lambda-target.h++>

#include <icL-service/main/values/inode-pro.h++>

#include <icL-ce/value-base/lambda/lambda-base-value.h++>



namespace icL::ce {

using core::ce::Prefix;
using core::memory::Type;

class icL_pro_ce_value_base_EXPORT LambdaJsValue
    : public core::ce::LambdaValueBase
    , public service::INode
{
public:
    /// @brief LambdaJsValue calls BaseValue(il, container, varName, readonly)
    LambdaJsValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief LambdaJsValue calls BaseValue(il, rvalue)
    LambdaJsValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief LambdaJsValue calls BaseValue(il, getter, setter)
    LambdaJsValue(
      core::il::InterLevel * il, const icString & getter,
      const icString & setter);

    /// @brief LambdaJsValue calls BaseValue(value)
    LambdaJsValue(BaseValue * value);

private:
    // level 2

    /// `lambda-js.run (any..) : any`
    void runRun(const core::memory::ArgList & args);

    /// `lambda-js.runAsync (any..) : void`
    void runRunAsync(const core::memory::ArgList & args);

    icList<icVariant> prepareArgs(const core::memory::ArgList & args);

    // Value interface
public:
    Type         type() const override;
    icString     typeName() override;

    void runMethod(
      const icString & name, const core::memory::ArgList & args) override;

private:
    const il::JsLambda & _value();

    // LambdaValueBase interface
public:
    core::il::CodeFragment getCode() override;
};

}  // namespace icL::ce

#endif  // ce_LambdaJsValue
