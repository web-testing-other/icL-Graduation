#include "lambda-js-value.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/lambda-target.h++>

#include <icL-ce/base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

LambdaJsValue::LambdaJsValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : LambdaValueBase(il, container, varName, readonly) {}

LambdaJsValue::LambdaJsValue(
  core::il::InterLevel * il, const icVariant & rvalue)
    : LambdaValueBase(il, rvalue) {}

LambdaJsValue::LambdaJsValue(BaseValue * value)
    : LambdaValueBase(value) {}

void LambdaJsValue::runRun(const core::memory::ArgList & args) {
    if (checkArgs(args)) {
        m_newContext = il->factory->fromValue(
          il,
          pro(il)->server->executeSync(getCode().getCode(), prepareArgs(args)));
    }
}

void LambdaJsValue::runRunAsync(const core::memory::ArgList & args) {
    if (checkArgs(args)) {
        pro(il)->server->executeAsync(getCode().getCode(), prepareArgs(args));
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

icList<icVariant> LambdaJsValue::prepareArgs(
  const core::memory::ArgList & args) {
    icVariantList ret;

    for (const auto & arg : args) {
        ret.append(arg.value);
    }

    return ret;
}

Type LambdaJsValue::type() const {
    return static_cast<Type>(memory::Type::JavaScriptLambdaValue);
}

icString LambdaJsValue::typeName() {
    return "lambda-js";
}

void LambdaJsValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (LambdaJsValue::*)(const core::memory::ArgList &)>
           methods{{"run", &LambdaJsValue::runRun},
              {"runAsync", &LambdaJsValue::runRunAsync}};
    auto & lambda = _value();

    pro(il)->server->pushTarget(*lambda.data);
    runMethodNow<LambdaJsValue, LambdaValueBase>(methods, name, args);
    pro(il)->server->popTarget();
}

const il::JsLambda & LambdaJsValue::_value() {
    return to<il::JsLambda>(value());
}

core::il::CodeFragment LambdaJsValue::getCode() {
    return *_value().target;
}

}  // namespace icL::ce
