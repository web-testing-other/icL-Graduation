#include "db-value.h++"

#include "query-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

DBValue::DBValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DBValue::DBValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DBValue::DBValue(BaseValue * value)
    : BaseValue(value) {}

void DBValue::runClose(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void DBValue::runQuery(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, query(args[0]));
    }
}

Type DBValue::type() const {
    return static_cast<Type>(memory::Type::DatabaseValue);
}

icString DBValue::typeName() {
    return "db";
}

void DBValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<icString, void (DBValue::*)(const core::memory::ArgList &)>
      methods{{"close", &DBValue::runClose}, {"query", &DBValue::runQuery}};

    runMethodNow<DBValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
