#include "query-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/db-server.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-own-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/bool-value.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/base/string-value.h++>
#include <icL-ce/value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

QueryValue::QueryValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

QueryValue::QueryValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

QueryValue::QueryValue(BaseValue * value)
    : BaseValue(value) {}

core::il::CE * QueryValue::roProperty(const icString & name) {
    return il->factory->fromValue(il, pro(il)->db->get(name));
}

class PropertyFixator : public core::il::IValueFixator
{
    core::il::InterLevel * core;
    il::InterLevel *       pro;
    icString               name;

public:
    PropertyFixator(
      core::il::InterLevel * core, il::InterLevel * pro, const icString & name)
        : core(core)
        , pro(pro)
        , name(name) {}

    // IValueFixator interface
public:
    void getter(icVariant & /*value*/) const override {
        core->vm->signal({core::il::Signals::System, "Try to read w/o value"});
    }
    void setter(icVariant & value, const icVariant & request) const override {
        pro->db->pushTarget(*il::Query(value).target);
        pro->db->set(name, request);
        pro->db->popTarget();
    }
};

core::il::CE * QueryValue::woProperty(const icString & name) {
    auto * node    = il->factory->clone(this, Type::VoidValue);
    auto * fixable = dynamic_cast<core::il::FixableValue *>(node);

    fixable->installFixator(
      std::make_shared<PropertyFixator>(il, pro(il), name));
    return node;
}

core::il::CE * QueryValue::property(const icString & name) {
    core::il::CE * ret;

    pro(il)->db->pushTarget(*_value().target);

    if (pro(il)->db->isExecuted()) {
        ret = roProperty(name);
    }
    else {
        ret = woProperty(name);
    }

    pro(il)->db->popTarget();

    return ret;
}

void QueryValue::runPropertyLevel2(const icString & name) {
    m_newContext = property(name);
}

void QueryValue::runExec(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, exec());
    }
}

void QueryValue::runFirst(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, first());
    }
}

void QueryValue::runGet(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, get(args[0]));
    }
}

void QueryValue::runGetError(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, getError());
    }
}

void QueryValue::runGetLength(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, getLength());
    }
}

void QueryValue::runGetRowsAffected(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, getRowsAffected());
    }
}

void QueryValue::runLast(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, last());
    }
}

void QueryValue::runNext(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, service::QueryValue::next());
    }
}

void QueryValue::runPrevious(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, last());
    }
}

void QueryValue::runSeek(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = il->factory->fromValue(il, seek(args[0]));
    }
    else if (checkArgs(args, {Type::IntValue, Type::BoolValue})) {
        m_newContext = il->factory->fromValue(il, seek(args[0], args[1]));
    }
}

void QueryValue::runSet(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::AnyValue})) {
        icSet(args[0], args[1].value);
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

Type QueryValue::type() const {
    return static_cast<Type>(memory::Type::QueryValue);
}

icString QueryValue::typeName() {
    return "query";
}

void QueryValue::runProperty(Prefix prefix, const icString & name) {
    runOwnPropertyWithPrefixCheck<QueryValue, BaseValue>(
      &QueryValue::runPropertyLevel2, prefix, name);
}

void QueryValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (QueryValue::*)(const core::memory::ArgList &)>
      methods{{"exec", &QueryValue::runExec},
              {"first", &QueryValue::runFirst},
              {"get", &QueryValue::runGet},
              {"getError", &QueryValue::runGetError},
              {"getLength", &QueryValue::runGetLength},
              {"getRowsAffected", &QueryValue::runGetRowsAffected},
              {"last", &QueryValue::runLast},
              {"next", &QueryValue::runNext},
              {"previous", &QueryValue::runPrevious},
              {"seek", &QueryValue::runSeek},
              {"icSet", &QueryValue::runSet}};

    runMethodNow<QueryValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
