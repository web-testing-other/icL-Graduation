#ifndef ce_JsFileValue
#define ce_JsFileValue

#include <icL-service/value-base/system/js-file-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL::ce {

using core::ce::Prefix;
using core::memory::Type;

class icL_pro_ce_value_base_EXPORT JsFileValue
    : public core::ce::BaseValue
    , public service::JsFileValue
{
public:
    /// @brief JsFileValue calls BaseValue(il, container, varName, readonly)
    JsFileValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief JsFileValue calls BaseValue(il, rvalue)
    JsFileValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief JsFileValue calls BaseValue(value)
    JsFileValue(core::ce::BaseValue * value);

    // methods level 2

    /// `js-file.load`
    void runLoad(const core::memory::ArgList & args);

    /// `js-file.run`
    void runRun(const core::memory::ArgList & args);

    /// `js-file.runAsync`
    void runRunAsync(const core::memory::ArgList & args);

    /// `js-file.setAsUserScript`
    void runSetAsUserScript(const core::memory::ArgList & args);

    /// `js-file.setAsPersistentUserScript`
    void runSetAsPersistentUserScript(const core::memory::ArgList & args);

    // Value interface
public:
    Type         type() const;
    icString     typeName();
    void runMethod(const icString & name, const core::memory::ArgList & args);

    // JsFileValue interface
protected:
    il::JsFile _value();
};

}  // namespace icL::ce

#endif  // ce_JsFileValue
