#include "listener-value.h++"

#include "handler-value.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/listen.h++>
#include <icL-il/structures/listen-handler.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

ListenerValue::ListenerValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ListenerValue::ListenerValue(
  core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ListenerValue::ListenerValue(BaseValue * value)
    : BaseValue(value) {}

void ListenerValue::runHandle(const core::memory::ParamList & params) {
    m_newContext = il->factory->fromValue(il, handle(params));
}

Type ListenerValue::type() const {
    return static_cast<Type>(memory::Type::ListenerValue);
}

icString ListenerValue::typeName() {
    return "listener";
}

void ListenerValue::runMeta(
  const icString & name, const core::memory::ParamList & params) {
    if (name == "handle") {
        runHandle(params);
    }
    else {
        BaseValue::runMeta(name, params);
    }
}

}  // namespace icL::ce
