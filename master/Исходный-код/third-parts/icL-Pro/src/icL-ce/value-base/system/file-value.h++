#ifndef ce_FileValue
#define ce_FileValue

#include <icL-service/value-base/system/file-value.h++>

#include <icL-ce/base/value/base-value.h++>


namespace icL {

namespace il {
struct File;
}

namespace ce {

using core::ce::Prefix;
using core::memory::Type;

class icL_pro_ce_value_base_EXPORT FileValue
    : public core::ce::BaseValue
    , public service::FileValue
{
public:
    /// @brief FileValue calls BaseValue(il, container, varName, readonly)
    FileValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief FileValue calls BaseValue(il, rvalue)
    FileValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief FileValue calls BaseValue(value)
    FileValue(core::ce::BaseValue * value);

    // properties level 2

    /// `file'format`
    void runFormat();

    // methods level 2

    /// `file.close`
    void runClose(const core::memory::ArgList & args);

    /// `file.delete`
    void runDelete(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_FileValue
