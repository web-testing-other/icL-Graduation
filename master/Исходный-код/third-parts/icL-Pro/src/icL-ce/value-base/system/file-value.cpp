#include "file-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/file-target.h++>

#include <icL-ce/base/main/value-run-method.h++>
#include <icL-ce/base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce/value-base/base/int-value.h++>
#include <icL-ce/value-base/base/void-value.h++>



namespace icL::ce {

FileValue::FileValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

FileValue::FileValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

FileValue::FileValue(BaseValue * value)
    : BaseValue(value) {}

void FileValue::runFormat() {
    m_newContext = il->factory->fromValue(il, format());
}

void FileValue::runClose(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void FileValue::runDelete(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        delete_();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

Type FileValue::type() const {
    return static_cast<Type>(memory::Type::FileValue);
}

icString FileValue::typeName() {
    return "file";
}

void FileValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (FileValue::*)()> properties{
      {"format", &FileValue::runFormat}};

    runPropertyWithPrefixCheck<FileValue, BaseValue>(properties, prefix, name);
}

void FileValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (FileValue::*)(const core::memory::ArgList &)>
      methods{{"close", &FileValue::runClose},
              {"delete", &FileValue::runDelete}};

    runMethodNow<FileValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
