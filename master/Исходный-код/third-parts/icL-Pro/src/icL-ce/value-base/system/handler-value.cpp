#include "handler-value.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/listen.h++>
#include <icL-il/structures/lambda-target.h++>
#include <icL-il/structures/listen-handler.h++>

#include <icL-ce/base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

HandlerValue::HandlerValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

HandlerValue::HandlerValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

HandlerValue::HandlerValue(BaseValue * value)
    : BaseValue(value) {}

void HandlerValue::runSetup(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::LambdaValue})) {
        m_newContext = il->factory->fromValue(il, setup(args[0]));
    }
}

void HandlerValue::runActivate(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, activate());
    }
}

void HandlerValue::runDeactivate(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, deactivate());
    }
}

void HandlerValue::runKill(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        kill();
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

Type HandlerValue::type() const {
    return static_cast<Type>(memory::Type::HandlerValue);
}

icString HandlerValue::typeName() {
    return "handler";
}

void HandlerValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (HandlerValue::*)(const core::memory::ArgList &)>
      methods{{"setup", &HandlerValue::runSetup},
              {"ativate", &HandlerValue::runActivate},
              {"deactivate", &HandlerValue::runDeactivate},
              {"kill", &HandlerValue::runKill}};

    runMethodNow<HandlerValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
