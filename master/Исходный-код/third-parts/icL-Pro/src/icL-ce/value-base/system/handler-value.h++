#ifndef ce_HandlerValue
#define ce_HandlerValue

#include <icL-service/value-base/system/handler-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL::ce {

using core::ce::Prefix;
using core::memory::Type;

class icL_pro_ce_value_base_EXPORT HandlerValue
    : public core::ce::BaseValue
    , public service::HandlerValue
{
public:
    /// @brief QueryValue calls BaseValue(il, container, varName, readonly)
    HandlerValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief QueryValue calls BaseValue(il, rvalue)
    HandlerValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief QueryValue calls BaseValue(il, getter, setter)
    HandlerValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief QueryValue calls BaseValue(value)
    HandlerValue(core::ce::BaseValue * value);

    // methods level 2

    /// `handler.setup (code : lambda-icl) : handler`
    void runSetup(const core::memory::ArgList & args);

    /// `handler.activate () : handler`
    void runActivate(const core::memory::ArgList & args);

    /// `handler.deactivate () : handler`
    void runDeactivate(const core::memory::ArgList & args);

    /// `handler.kill () : void`
    void runKill(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_HandlerValue
