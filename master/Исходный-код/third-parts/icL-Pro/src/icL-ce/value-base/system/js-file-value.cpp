#include "js-file-value.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/file-target.h++>

#include <icL-service/main/values/set.h++>

#include <icL-ce/base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

JsFileValue::JsFileValue(
  core::il::InterLevel * il, core::memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

JsFileValue::JsFileValue(core::il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

JsFileValue::JsFileValue(BaseValue * value)
    : BaseValue(value) {}

void JsFileValue::runLoad(const core::memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = il->factory->fromValue(il, load(args[0]));
    }
}

void JsFileValue::runRun(const core::memory::ArgList & args) {
    if (checkArgs(args)) {
        m_newContext =
          il->factory->fromValue(il, run(core::service::Set::argsToList(args)));
    }
}

void JsFileValue::runRunAsync(const core::memory::ArgList & args) {
    if (checkArgs(args)) {
        runAsync(core::service::Set::argsToList(args));
        m_newContext = il->factory->fromValue(il, icVariant::makeVoid());
    }
}

void JsFileValue::runSetAsUserScript(const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, setAsUserScript());
    }
}

void JsFileValue::runSetAsPersistentUserScript(
  const core::memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = il->factory->fromValue(il, setAsPersistentUserScript());
    }
}

Type JsFileValue::type() const {
    return static_cast<Type>(memory::Type::JsFileValue);
}

icString JsFileValue::typeName() {
    return "js-file";
}

void JsFileValue::runMethod(
  const icString & name, const core::memory::ArgList & args) {
    static icObject<
      icString, void (JsFileValue::*)(const core::memory::ArgList &)>
      methods{{"load", &JsFileValue::runLoad},
              {"run", &JsFileValue::runRun},
              {"runAsync", &JsFileValue::runRunAsync},
              {"setAsUserScript", &JsFileValue::runSetAsUserScript},
              {"setAsPersistentUserScript",
               &JsFileValue::runSetAsPersistentUserScript}};

    runMethodNow<JsFileValue, BaseValue>(methods, name, args);
}

il::JsFile JsFileValue::_value() {
    return value();
}

}  // namespace icL::ce
