#ifndef ce_QueryValue
#define ce_QueryValue

#include <icL-service/value-base/system/query-value.h++>

#include <icL-ce/base/value/base-value.h++>



namespace icL {

namespace il {
struct Query;
}

namespace ce {

using core::ce::Prefix;
using core::memory::Type;

class icL_pro_ce_value_base_EXPORT QueryValue
    : public core::ce::BaseValue
    , public service::QueryValue
{
public:
    /// @brief QueryValue calls BaseValue(il, container, varName, readonly)
    QueryValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief QueryValue calls BaseValue(il, rvalue)
    QueryValue(core::il::InterLevel * il, const icVariant & rvalue);

    /// @brief QueryValue calls BaseValue(value)
    QueryValue(BaseValue * value);

    // properties level 1

    /// `[r/o] query'(name : string) : any`
    core::il::CE * roProperty(const icString & name);

    /// `[w/o] query'(name : string) : any`
    core::il::CE * woProperty(const icString & name);

    /// `query'(name : string) : any`
    core::il::CE * property(const icString & name);

    // properties level 2

    /// `query'(string)`
    void runPropertyLevel2(const icString & name);

    // methods level 2

    /// `query.exec`
    void runExec(const core::memory::ArgList & args);

    /// `query.first`
    void runFirst(const core::memory::ArgList & args);

    /// `query.get`
    void runGet(const core::memory::ArgList & args);

    /// `query.getError`
    void runGetError(const core::memory::ArgList & args);

    /// `query.getLength`
    void runGetLength(const core::memory::ArgList & args);

    /// `query.getRowsAffected`
    void runGetRowsAffected(const core::memory::ArgList & args);

    /// `query.last`
    void runLast(const core::memory::ArgList & args);

    /// `query.next`
    void runNext(const core::memory::ArgList & args);

    /// `query.previous`
    void runPrevious(const core::memory::ArgList & args);

    /// `query.seek`
    void runSeek(const core::memory::ArgList & args);

    /// `query.set`
    void runSet(const core::memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const core::memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_QueryValue
