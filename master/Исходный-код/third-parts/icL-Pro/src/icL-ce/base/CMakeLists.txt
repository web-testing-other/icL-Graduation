cmake_minimum_required(VERSION 3.8.0)

project(-icL-pro-ce-base LANGUAGES CXX)

file(GLOB HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/*/*.h++")
file(GLOB SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*/*.cpp")

add_library(${PROJECT_NAME} SHARED
  ${HEADERS}
  ${SOURCES}
)

target_compile_definitions(${PROJECT_NAME}
    PRIVATE icL_pro_ce_base_LIBRARY)

target_include_directories(${PROJECT_NAME}
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/advanced-operator
    ${CMAKE_CURRENT_SOURCE_DIR}/alu-operator
    ${CMAKE_CURRENT_SOURCE_DIR}/keyword
    ${CMAKE_CURRENT_SOURCE_DIR}/literal
    ${CMAKE_CURRENT_SOURCE_DIR}/main
    ${CMAKE_CURRENT_SOURCE_DIR}/value
    ${CMAKE_CURRENT_SOURCE_DIR}/../..
)

target_link_libraries(${PROJECT_NAME}
  PUBLIC
    -icL-types
)
