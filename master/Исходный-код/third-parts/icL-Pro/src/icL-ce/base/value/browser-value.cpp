#include "browser-value.h++"

namespace icL::ce {

BrowserValue::BrowserValue(core::il::InterLevel * il)
    : Value(il) {}

icString BrowserValue::toString() {
    return typeName();
}

}  // namespace icL::ce
