#ifndef ce_BrowserValue
#define ce_BrowserValue

#include <icL-il/export/global-pro.h++>

#include <icL-ce/base/main/value.h++>



namespace icL::ce {

using core::ce::Prefix;
using core::il::StepType;
using core::memory::Type;

/**
 * @brief The BrowserValue class as system value which manipulates a browser
 */
class icL_pro_ce_base_EXPORT BrowserValue : public core::ce::Value
{
public:
    BrowserValue(core::il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
};

}  // namespace icL::ce

#endif  // ce_BrowserValue
