#include "browser-command.h++"

#include <icL-types/replaces/ic-set.h++>



namespace icL::ce {

using core::ce::Role;

BrowserCommand::BrowserCommand(core::il::InterLevel * il)
    : BrowserValue(il) {}

const icSet<int> &BrowserCommand::acceptedNexts() {
    static icSet<int> roles{BrowserValue::acceptedNexts(), {Role::NoRole}};
    return roles;
}

}  // namespace icL::ce
