#ifndef ce_BrowserCommand
#define ce_BrowserCommand

#include "browser-value.h++"

#include <icL-il/export/global-pro.h++>



namespace icL::ce {

class icL_pro_ce_base_EXPORT BrowserCommand : public BrowserValue
{
public:
    BrowserCommand(core::il::InterLevel * il);

    // CE interface
protected:
    const icSet<int> & acceptedNexts();
};

}  // namespace icL::ce

#endif  // ce_BrowserCommand
