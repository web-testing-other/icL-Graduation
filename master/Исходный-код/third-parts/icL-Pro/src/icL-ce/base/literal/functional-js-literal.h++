#ifndef ce_FunctionalJsLiteral
#define ce_FunctionalJsLiteral

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/export/global-pro.h++>

#include <icL-ce/base/main/literal.h++>



namespace icL::ce {

class icL_pro_ce_base_EXPORT FunctionalJsLiteral : public core::ce::Literal
{
public:
    FunctionalJsLiteral(core::il::InterLevel * il);

    // CE interface
protected:
    const icSet<int> & acceptedPrevs();
    const icSet<int> & acceptedNexts();

    // CE interface
public:
    void colorize();
};

}  // namespace icL::ce

#endif  // ce_FunctionalJsLiteral
