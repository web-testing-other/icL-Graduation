#ifndef ce_JsFile
#define ce_JsFile

#include <icL-ce/base/literal/functional-js-literal.h++>



class icString;

namespace icL::ce {

using core::StepType;

/**
 * @brief The JsFile class represents a `$file` token
 */
class icL_pro_ce_literal_EXPORT JsFile : public FunctionalJsLiteral
{
    icString filename;

public:
    JsFile(core::il::InterLevel * il, const icString & filename);

protected:
    /**
     * @brief getFileContent gets the content of file
     * @return the content of file
     */
    icString getFileContent();

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    int role() override;
};

}  // namespace icL::ce

#endif  // ce_JsFile
