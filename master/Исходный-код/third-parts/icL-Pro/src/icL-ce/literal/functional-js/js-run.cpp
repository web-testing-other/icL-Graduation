#include "js-run.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>

#include <utility>

namespace icL::ce {

using core::ce::Role;

JsRun::JsRun(
  core::il::InterLevel * il, const icString & code, icString /*sessionId*/,
  icString /*windowsId*/)
    : FunctionalJsLiteral(il)
    , code(code) {}

icString JsRun::toString() {
    return "$run";
}

int JsRun::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType JsRun::runNow() {
    if (async) {
        pro(il)->server->executeAsync(code, {});
    }
    else {
        pro(il)->server->executeSync(code, {});
    }

    return StepType::CommandEnd;
}

int JsRun::role() {
    return Role::JsRun;
}

const icSet<int> & JsRun::acceptedPrevs() {
    static icSet<int> roles = {Role::NoRole};
    return roles;
}

const icSet<int> & JsRun::acceptedNexts() {
    static icSet<int> roles = {Role::NoRole};
    return roles;
}

}  // namespace icL::ce
