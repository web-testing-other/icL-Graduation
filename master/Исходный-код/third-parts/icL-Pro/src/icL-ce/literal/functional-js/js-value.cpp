#include "js-value.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/factory.h++>
#include <icL-il/main/interlevel.h++>

#include <utility>



namespace icL::ce {

using core::ce::Role;

JsValue::JsValue(
  core::il::InterLevel * il, const icString & getter, const icString & setter)
    : FunctionalJsLiteral(il)
    , getter(getter)
    , setter(setter) {}

icString JsValue::toString() {
    return "js::value{..}";
}

int JsValue::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType JsValue::runNow() {
    //    m_newContext = il->factory->fromValue(il, getter, setter);
    return StepType::CommandEnd;
}

int JsValue::role() {
    return Role::JsValue;
}

}  // namespace icL::ce
