#ifndef ce_JsRun
#define ce_JsRun

#include <icL-types/replaces/ic-string.h++>

#include <icL-service/main/values/inode-pro.h++>

#include <icL-ce/base/literal/functional-js-literal.h++>



namespace icL::ce {

using core::StepType;

class icL_pro_ce_literal_EXPORT JsRun
    : public FunctionalJsLiteral
    , virtual public service::INode
{
public:
    JsRun(
      core::il::InterLevel * il, const icString & code, icString sessionId,
      icString windowsId);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    int      role() override;

protected:
    const icSet<int> & acceptedPrevs() override;
    const icSet<int> & acceptedNexts() override;

    // fields
private:
    /// \brief code is the javascript code to run
    icString code;
    /// \brief async is true if code need to be executed asynchronously
    bool async = false;

    // padding
    long : 56;
};

}  // namespace icL::ce

#endif  // ce_JsRun
