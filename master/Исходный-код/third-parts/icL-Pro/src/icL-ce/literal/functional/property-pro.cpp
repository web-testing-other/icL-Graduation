#include "property-pro.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/export/ce-types.h++>



namespace icL::ce {

Property::Property(
  core::il::InterLevel * il, proPrefix prefix, const icString & name)
    : core::ce::Property(il, static_cast<core::ce::Prefix>(prefix), name) {}

StepType Property::runNow() {
    using core::ce::Role;

    static const icObject<int, icObject<icString, proPrefix>> types{
      {memory::Type::ElementValue,
       {{"attr", proPrefix::Attr},
        {"css", proPrefix::Css},
        {"prop", proPrefix::Prop}}},
      {memory::Type::ElementsValue,
       {{"attrs", proPrefix::Attrs}, {"props", proPrefix::Props}}}};

    if (m_next != nullptr && m_next->role() == Role::Property) {
        auto type = dynamic_cast<core::il::TypeableValue *>(m_prev)->type();
        auto it   = types.find(type);

        if (it != types.end()) {
            auto propIt = it.value().find(name);

            if (propIt != it.value().end()) {
                m_newContext = new Property{il, propIt.value(), name};
            }
        }
    }

    if (m_newContext == nullptr) {
        core::ce::Property::runNow();
    }

    return StepType::CommandEnd;
}

}  // namespace icL::ce
