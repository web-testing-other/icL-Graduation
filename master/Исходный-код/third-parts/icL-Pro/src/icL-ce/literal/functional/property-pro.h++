#ifndef icL_ce_Property
#define icL_ce_Property

#include <icL-ce/literal/functional/property.h++>



namespace icL::ce {

namespace proPrefixNM {
enum proPrefix {
    None  = core::ce::Prefix::None,
    First = core::ce::Prefix::Last,
    Attr,   ///< 'attr'* prefix
    Attrs,  ///< 'attrs'* prefix
    Prop,   ///< 'prop'* prefix
    Props,  ///< 'props'* prefix
    Css,    ///< 'css'* prefix
    Last    ///< Make it extendable
};
}

using proPrefixNM::proPrefix;

using core::il::StepType;

class Property : public core::ce::Property
{
public:
    Property(
      core::il::InterLevel * il, proPrefix prefix, const icString & name);

    // CE interface
public:
    StepType runNow() override;
};

}  // namespace icL::ce

#endif  // icL_ce_Property
