#include "element.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/main/enums.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/return.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service/main/args/listify.h++>
#include <icL-service/main/printing/stringify.h++>

#include <icL-memory/state/datacontainer.h++>
#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/packed-value-item.h++>



namespace icL::ce {

using il::Selectors;

Element::Element(
  core::il::InterLevel * il, const icString & pattern,
  const icString & searchMode, const icStringList & mods,
  core::memory::DataContainer * container, const icString & varName,
  const core::il::CodeFragment & code)
    : ConstLiteral(il, pattern)
    , searchMode(searchMode)
    , mods(mods)
    , container(container)
    , varName(varName)
    , code(code) {
    _search.tagName = &tagName;
}

icVariant Element::getValueOf() {
    icVariant   ret;
    il::Element parent;

    il::Tab tabTarget;

    tabTarget.data =
      std::make_shared<il::TargetData>(pro(il)->server->getCurrentTarget());

    if (container != nullptr) {
        if (container->checkType(varName, memory::Type::ElementValue)) {
            parent          = container->getValue(varName);
            _search.element = &parent;

            tabTarget.data = std::make_shared<il::TargetData>(parent.target);
        }
        else if (container->checkType(varName, memory::Type::TabValue)) {
            tabTarget = container->getValue(varName);
        }
        else {
            il->vm->signal({core::il::Signals::System, "Wrong element target"});
            return ret;
        }
    }

    if (!il->vm->hasOkState()) {
        return ret;
    }

    if (all) {
        if (search.min == -1)
            search.min = 0;
    }
    if (proxy) {
        if (search.se.minP < 0.0)
            search.se.minP = 0.0;
        if (search.se.maxP < 0.0)
            search.se.maxP = 1.0;
    }


    pro(il)->server->pushTarget(*tabTarget.data);

    switch (searchFunc) {
    case ByString:
        if (all)
            ret = pro(il)->server->queryAll(search, pattern);
        else
            ret = postWork(pro(il)->server->query(search.se, pattern));
        break;

    case ByRegEx:
        if (all)
            ret = pro(il)->server->queryAll(search, rex);
        else
            ret = postWork(pro(il)->server->query(search.se, rex));
        break;

    default:
        break;
    }

    pro(il)->server->popTarget();

    return ret;
}

StepType Element::runNow() {
    if (ready)
        return ConstLiteral::runNow();

    static icObject<icString, void (Element::*)()> methods{
      {"css", &Element::css},     {"xpath", &Element::xpath},
      {"link", &Element::link_},  {"links", &Element::links},
      {"tag", &Element::tag},     {"tags", &Element::tags},
      {"input", &Element::input}, {"field", &Element::field}};

    if (!il->vm->hasOkState()) {
        return StepType::MiniStep;
    }

    (this->*methods.find(searchMode).value())();

    core::memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.contextType = core::memory::ContextType::Value;

    il->vms->interrupt(fcall, [this](const core::il::Return & ret) {
        if (ret.signal.code != core::il::Signals::NoError) {
            il->vm->signal(ret.signal);
        }
        else if (ret.consoleValue.isString()) {
            searchFunc = ByString;
            pattern    = ret.consoleValue;
            ready      = true;
        }
        else if (ret.consoleValue.isRegEx()) {
            if (!proxy) {
                searchFunc = ByRegEx;
                rex        = ret.consoleValue;
                ready      = true;
            }
            else {
                il->vm->syssig(
                  searchMode + ": proximity works with string only");
            }
        }
        else if (
          ret.consoleValue.is(icType::Packed) &&
          _search.selectorType == Selectors::TagName) {
            auto args = core::service::Listify::toArgList(il, ret.consoleValue);

            using core::memory::Type;

            if (args.length() == 2 && args.first().type == Type::StringValue) {
                tagName = args.first();

                if (args.last().type == Type::StringValue) {
                    searchFunc = ByString;
                    pattern    = args.last();
                    ready      = true;
                }
                else if (args.last().type == Type::RegexValue) {
                    if (!proxy) {
                        searchFunc = ByString;
                        rex        = args.last();
                        ready      = true;
                    }
                    else {
                        il->vm->syssig(
                          searchMode + ": proximity works with string only");
                    }
                }
            }
        }

        if (!ready) {
            il->vm->syssig(
              searchMode + ": the selector must be a string or regex");
        }
        return false;
    });

    return StepType::CommandIn;
}

icString Element::toString() {
    auto ret = searchMode;
    if (!mods.isEmpty())
        ret += '-' + mods.join("-");
    if (container != nullptr)
        ret += core::service::Stringify::fromVariant(
          il, container->getValue(varName));
    ret += "[" % pattern % ']';

    return ret;
}

void Element::css() {
    search.se.selectorType = Selectors::CssSelector;

    for (auto & mod : mods) {
        if (mod == "all") {
            if (all) {
                il->vm->syssig("css: `all` modifier setted twince");
            }
            else {
                all = true;
            }
        }
        else if (!isMinOrMax(mod) && !isWait(mod)) {
            il->vm->syssig("css: unknown modifier " % mod % '`');
            return;
        }
    }
}

void Element::xpath() {
    search.se.selectorType = Selectors::XPath;

    for (auto & mod : mods) {
        if (mod == "all") {
            if (all) {
                il->vm->syssig("xpath: `all` modifier setted twince");
            }
            else {
                all = true;
            }
        }
        else if (!isMinOrMax(mod) && !isWait(mod)) {
            il->vm->syssig("xpath: unknown modifier `" % mod % '`');
            return;
        }
    }
}

void Element::link_() {
    search.se.selectorType = Selectors::LinkText;

    for (auto & mod : mods) {
        if (mod == "fragment") {
            if (search.se.selectorType == Selectors::PartialLinkText) {
                il->vm->syssig("link: `fragment` modifier setted twince");
            }
            else {
                search.se.selectorType = Selectors::PartialLinkText;
            }
        }
        else if (!isProxy(mod) && !isWait(mod)) {
            il->vm->syssig("link: unknown modifier `" % mod % '`');
            return;
        }
    }
}

void Element::links() {
    search.se.selectorType = Selectors::LinkText;
    all                    = true;

    for (auto & mod : mods) {
        if (mod == "fragment") {
            if (search.se.selectorType == Selectors::PartialLinkText) {
                il->vm->syssig("links: `fragment` modifier setted twince");
            }
            else {
                search.se.selectorType = Selectors::PartialLinkText;
            }
        }
        else if (!isMinOrMax(mod) && !isProxy(mod) && !isWait(mod)) {
            il->vm->syssig("links: unknown modifier `" % mod % '`');
            return;
        }
    }
}

void Element::tag() {
    search.se.selectorType = Selectors::TagName;

    for (auto & mod : mods) {
        if (!isProxy(mod) && !isWait(mod)) {
            il->vm->syssig("tag: unknown modifier `" % mod % '`');
            return;
        }
    }
}

void Element::tags() {
    search.se.selectorType = Selectors::TagName;
    all                    = true;

    for (auto & mod : mods) {
        if (!isProxy(mod) && !isMinOrMax(mod) && !isWait(mod)) {
            if (tagName.isEmpty()) {
                tagName = mod;
            }
            else {
                il->vm->syssig("tags: unknown modifier `" % mod % '`');
                return;
            }
        }
    }
}

void Element::input() {
    search.se.selectorType = Selectors::Input;

    for (auto & mod : mods) {
        if (!isWait(mod)) {
            il->vm->syssig("input: unknown modifier `" % mod % '`');
        }
    }
}

void Element::field() {
    search.se.selectorType = Selectors::Field;

    for (auto & mod : mods) {
        if (!isProxy(mod) && !isWait(mod)) {
            if (tagName.isEmpty()) {
                tagName = mod;
            }
            else {
                il->vm->syssig("field: unknown modifier `" % mod % '`');
                return;
            }
        }
    }
}


bool Element::isMinOrMax(const icString & modifier) {
    icRegEx min("min(\\d+)");
    icRegEx max("max(\\d+)");

    icRegExMatch match;

    if ((match = min.match(modifier)).hasMatch()) {
        if (!all) {
            il->vm->syssig(
              searchMode + ": it must be a `all` modifier before min");
        }
        else if (search.min == -1) {
            search.min = match.captured(1).toInt();
            return true;
        }
        else {
            il->vm->syssig(
              searchMode + ": wrong modifiers set, `min` setted twince.");
        }
    }
    else if ((match = max.match(modifier)).hasMatch()) {
        if (!all) {
            il->vm->syssig(
              searchMode + ": it must be a `all` modifier before max");
        }
        else if (search.max == -1) {
            search.max = match.captured(1).toInt();
            return true;
        }
        else {
            il->vm->syssig(
              searchMode + ": wrong modifiers set, `max` setted twince.");
        }
    }

    return false;
}

bool Element::isProxy(const icString & modifier) {
    auto from = R"|(from(\d+))|"_rx;
    auto to   = R"|(to(\d+))|"_rx;

    icRegExMatch match;

    if ((match = from.match(modifier)).hasMatch()) {
        if (search.se.minP <= -0.5) {
            search.se.minP = match.captured(1).toDouble() / 100.0;
            proxy          = true;
            return true;
        }
        else {
            il->vm->syssig(
              searchMode + ": wrong modifiers set, `from` setted twince");
        }
    }
    else if ((match = to.match(modifier)).hasMatch()) {
        if (search.se.maxP == -0.5) {
            search.se.maxP = match.captured(1).toDouble() / 100.0;
            proxy          = true;
            return true;
        }

        il->vm->syssig(searchMode + ": wrong modifier set, `to` setted twince");
    }

    return false;
}

bool Element::isWait(const icString & modifier) {
    auto s  = R"|(wait(\d+)s)|"_rx;
    auto ms = R"|(wait(\d+)ms)|"_rx;

    icRegExMatch match;

    if (modifier == "try") {
        if (!search.se.tryModifier) {
            search.se.tryModifier = true;
            return true;
        }
        else {
            il->vm->syssig(
              searchMode + ": wrong modifier set, `try` setted twince");
        }
    }
    if ((match = s.match(modifier)).hasMatch()) {
        if (search.se.searchTime == -1) {
            search.se.searchTime = match.captured(1).toInt() * 1000;
            return true;
        }
        else {
            il->vm->syssig(searchMode + ": wrong modifiers set");
        }
    }
    else if ((match = ms.match(modifier)).hasMatch()) {
        if (search.se.searchTime == -1) {
            search.se.searchTime = match.captured(1).toInt();
            return true;
        }

        il->vm->syssig(searchMode + ": wrong modifiers set");
    }

    return false;
}

icVariant Element::postWork(const il::Element & element) {
    if (element.variable.isEmpty()) {
        return icVariant::makeVoid();
    }

    return element;
}

void Element::colorize() {
    return;
}

}  // namespace icL::ce
