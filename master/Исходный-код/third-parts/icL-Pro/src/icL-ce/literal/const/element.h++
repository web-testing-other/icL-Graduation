#ifndef ce_Element
#define ce_Element

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/frontend.h++>

#include <icL-service/main/values/inode-pro.h++>

#include <icL-ce/base/literal/const-literal.h++>



namespace icL {

namespace core::memory {
class DataContainer;
}

namespace ce {

using core::il::StepType;

/**
 * @brief The Element class represent a element token `mode-mods@var[data]`
 */
class icL_pro_ce_literal_EXPORT Element
    : public core::ce::ConstLiteral
    , virtual public service::INode
{
public:
    Element(
      core::il::InterLevel * il, const icString & pattern,
      const icString & searchMode, const icStringList & mods,
      core::memory::DataContainer * container, const icString & varName,
      const core::il::CodeFragment & code);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;

    // CE interface
public:
    StepType runNow() override;
    icString toString() override;

private:
    /// \brief css handles `css` mode
    void css();

    /// \brief xpath handles `xpath` mode
    void xpath();

    /// \brief link_ handles `link` mode
    void link_();

    /// \brief links handles `links` mode
    void links();

    /// \brief tag handles `tag` mode
    void tag();

    /// \brief tags handles `tags` mode
    void tags();

    /// \brief input handles `input` mode
    void input();

    /// \brief field handles `field` mode
    void field();

    /**
     * @brief isMinOrMax checks if is a minX or maxX modifier
     * @param modifier is the modifier to check
     * @return true if modifier was handler successfully
     */
    bool isMinOrMax(const icString & modifier);

    /**
     * @brief isProxy checks if is a proxy modifier
     * @param modifier is the modifier to check
     * @return true if modifier was handler successfully
     */
    bool isProxy(const icString & modifier);

    /**
     * @brief isWait checks if is a wait madifier
     * @param modifier is the modifier to check
     * @return true if modifier was handler successfully
     */
    bool isWait(const icString & modifier);

    /**
     * @brief postWork returns a void value if element is invalid
     * @param element is the element to checkout
     * @return a void value if element is invalid, otherwise returns `eleement`
     */
    icVariant postWork(const il::Element & element);

    // fields
private:
    /// \brief searchMode defines the search mode `css selector`, `xpath` ...
    icString searchMode;  // `mode`-mods@var[data]
    /// \brief tagName is the name of needed tag
    icStringList mods;  // mode`-mods`@var[data]
    /// \brief tagName is the name of searched tag
    icString tagName;
    /// \brief container is the container of target
    core::memory::DataContainer * container;  // mode-mods`@`var[data]
    /// \brief varName is the name of target
    icString varName;  // mode-mods@`var`[data]
    /// \brief code is the code in square brackets
    core::il::CodeFragment code;  // mode-mods@var[`data`]
    /// \brief rex is the regex of square brackets
    icRegEx rex;

    il::FrontEnd::SearchElement  _search;
    il::FrontEnd::SearchElements search{_search};

    enum {
        Initial,              ///< search mode is not defined yet
        ByString,             ///< search by a string a string
        ByRegEx,              ///< a regular expression is a alternative
    } searchFunc = ByString;  ///< which query* function will be used

    bool all   = false;  ///< catch all element not just the first
    bool proxy = false;  ///< mark the string needs to be proximited
    bool ready = false;  ///< mark the ready state

    // CE interface
public:
    void colorize() override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Element
