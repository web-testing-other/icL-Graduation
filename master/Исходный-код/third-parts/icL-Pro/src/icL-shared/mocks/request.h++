#ifndef shared_Request
#define shared_Request

#include <icL-il/main/request.h++>



namespace icL::shared {

class icL_pro_shared_EXPORT Request : public il::Request
{
public:
    Request();

    // Request interface
public:
    void confirm(const icString & text);
    bool ask(const icString & question);
    int  int_(const icString & text);

    double   double_(const icString & text);
    icString string(const icString & text);

    icStringList list(const icString & text);

    il::Tab tab(const icString & text);
    void    notify(int secunds, const icString & message);
};

}  // namespace icL::shared

#endif  // shared_Request
