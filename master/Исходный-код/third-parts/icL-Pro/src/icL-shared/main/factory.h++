#ifndef shared_Factory
#define shared_Factory

#include <icL-il/export/global-pro.h++>

#include <icL-factory/factory.h++>



namespace icL::shared {

class icL_pro_shared_EXPORT Factory : public core::factory::Factory
{
public:
    Factory();

    // Factory interface
public:
    core::il::CE * fromValue(
      core::il::InterLevel * il, const icVariant & value) override;
    core::il::CE * fromValue(
      core::il::InterLevel * il, core::memory::DataContainer * container,
      const icString & var) override;
    core::il::CE * clone(core::il::PackableValue * value, short type) override;

    icL::core::memory::TypeNM::Type variantToType(
      const icVariant & var) override;
    icTypeNM::icType typeToVarType(
      const icL::core::memory::TypeNM::Type type) override;
    icString typeToString(const short & type) override;
};

}  // namespace icL::shared

#endif  // shared_Factory
