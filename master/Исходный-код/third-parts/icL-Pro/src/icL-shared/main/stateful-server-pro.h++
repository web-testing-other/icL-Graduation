#ifndef shared_StatefulServer
#define shared_StatefulServer

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/export/global-pro.h++>

#include <icL-shared/main/stateful-server.h++>



namespace icL::shared {

class icL_pro_shared_EXPORT StatefulServer : public core::shared::StatefulServer
{
public:
    StatefulServer();
};

}  // namespace icL::shared

#endif  // shared_StatefulServer
