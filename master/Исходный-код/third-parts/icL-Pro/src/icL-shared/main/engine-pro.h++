#ifndef shared_Engine
#define shared_Engine

#include <icL-il/export/global-pro.h++>
#include <icL-il/main/interlevel-pro.h++>

#include <icL-shared/main/engine.h++>



namespace icL::shared {

/**
 * @brief The Engine class represent a icL engine ready for use
 */
class icL_pro_shared_EXPORT Engine : public core::shared::Engine
{
    /// \brief m_il represent the interlevel node
    il::InterLevel m_pro;

public:
    /**
     * @brief setFrontEndServer sets the front-end node
     * @param server is the synronization server
     */
    void setFrontEndServer(il::FrontEnd * server);

    /**
     * @brief setDbServer sets the database service
     * @param db is the database service
     */
    void setDbServer(il::DBServer * db);

    /**
     * @brief setFileServer sets the file service
     * @param file is the file service to set
     */
    void setFileServer(il::FileServer * file);

    /**
     * @brief setListenService sets the listener service
     * @param listen is the listener service to set
     */
    void setListenService(il::Listen * listen);

    /**
     * @brief setRequestService sets the request server
     * @param request is the request server to set
     */
    void setRequestService(il::Request * request);

    /**
     * @brief finalize initilize unitialized services
     */
    void finalize();

    /**
     * @brief il gets a pointer to main interlevel node
     * @return a pointer to main interserver node
     */
    il::InterLevel * pro();
};

}  // namespace icL::shared

#endif  // shared_Engine
