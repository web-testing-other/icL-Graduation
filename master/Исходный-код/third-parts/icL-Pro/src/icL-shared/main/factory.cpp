#include "factory.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/export/ce-types.h++>
#include <icL-il/export/types.h++>

#include <icL-ce/value-base/browser/cookie-value.h++>
#include <icL-ce/value-base/browser/document-value.h++>
#include <icL-ce/value-base/browser/element-value.h++>
#include <icL-ce/value-base/browser/elements-value.h++>
#include <icL-ce/value-base/browser/session-value.h++>
#include <icL-ce/value-base/browser/tab-value.h++>
#include <icL-ce/value-base/browser/window-value.h++>
#include <icL-ce/value-base/lambda/lambda-js-value.h++>
#include <icL-ce/value-base/system/db-value.h++>
#include <icL-ce/value-base/system/file-value.h++>
#include <icL-ce/value-base/system/handler-value.h++>
#include <icL-ce/value-base/system/js-file-value.h++>
#include <icL-ce/value-base/system/listener-value.h++>
#include <icL-ce/value-base/system/query-value.h++>

#include <icL-memory/state/datacontainer.h++>



namespace icL::shared {

Factory::Factory()
{
    
}

using core::il::CE;
using core::il::InterLevel;

CE * Factory::fromValue(InterLevel * il, const icVariant & value) {
    CE * ret = nullptr;

    switch (int(value.type())) {
        using namespace proTypeNM;

    case Cookie:
        ret = new ce::CookieValue{il, value};
        break;

    case DB:
        ret = new ce::DBValue{il, value};
        break;

    case Query:
        ret = new ce::QueryValue{il, value};
        break;

    case Element:
        ret = new ce::ElementValue{il, value};
        break;

    case Elements:
        ret = new ce::ElementsValue{il, value};
        break;

    case File:
        ret = new ce::FileValue{il, value};
        break;

    case JsFile:
    case ResourceFile:
        ret = new ce::JsFileValue{il, value};
        break;

    case Lambda:
        // ret = new ce::LambdaValue{il, value};
        break;

    case JsLambda:
        ret = new ce::LambdaJsValue{il, value};
        break;

    case Listener:
        ret = new ce::ListenerValue{il, value};
        break;

    case Handler:
        ret = new ce::HandlerValue{il, value};
        break;

    case Session:
        ret = new ce::SessionValue{il, value};
        break;

    case Tab:
        ret = new ce::TabValue{il, value};
        break;

    case Window:
        ret = new ce::WindowValue{il, value};
        break;

    case Document:
        ret = new ce::DocumentValue{il, value};
        break;

    default:
        ret = core::factory::Factory::fromValue(il, value);
    }

    return ret;
}

CE * Factory::fromValue(
  InterLevel * il, core::memory::DataContainer * container,
  const icString & var) {
    CE * ret = nullptr;

    switch (int(container->getType(var))) {
        using namespace memory::TypesNM;

    case CookieValue:
        ret = new ce::CookieValue{il, container, var};
        break;

    case DatabaseValue:
        ret = new ce::DBValue{il, container, var};
        break;

    case QueryValue:
        ret = new ce::QueryValue{il, container, var};
        break;

    case ElementValue:
        ret = new ce::ElementValue{il, container, var};
        break;

    case ElementsValue:
        ret = new ce::ElementsValue{il, container, var};
        break;

    case FileValue:
        ret = new ce::FileValue{il, container, var};
        break;

    case JsFileValue:
        ret = new ce::JsFileValue{il, container, var};
        break;

    case JavaScriptLambdaValue:
        ret = new ce::LambdaJsValue{il, container, var};
        break;

    case ListenerValue:
        ret = new ce::ListenerValue{il, container, var};
        break;

    case HandlerValue:
        ret = new ce::HandlerValue{il, container, var};
        break;

    case SessionValue:
        ret = new ce::SessionValue{il, container, var};
        break;

    case TabValue:
        ret = new ce::TabValue{il, container, var};
        break;

    case WindowValue:
        ret = new ce::WindowValue{il, container, var};
        break;

    case DocumentValue:
        ret = new ce::DocumentValue{il, container, var};
        break;

    default:
        ret = core::factory::Factory::fromValue(il, container, var);
    }

    return ret;
}

core::il::CE * Factory::clone(core::il::PackableValue * value, short type) {
    CE * ret       = nullptr;
    auto baseValue = dynamic_cast<core::ce::BaseValue *>(value);

    if (type == -1) {
        type = value->type();
    }

    switch (type) {
        using namespace memory::TypesNM;

    case CookieValue:
        ret = new ce::CookieValue{baseValue};
        break;

    case DatabaseValue:
        ret = new ce::DBValue{baseValue};
        break;

    case QueryValue:
        ret = new ce::QueryValue{baseValue};
        break;

    case ElementValue:
        ret = new ce::ElementValue{baseValue};
        break;

    case ElementsValue:
        ret = new ce::ElementsValue{baseValue};
        break;

    case FileValue:
        ret = new ce::FileValue{baseValue};
        break;

    case JsFileValue:
        ret = new ce::JsFileValue{baseValue};
        break;

    case JavaScriptLambdaValue:
        ret = new ce::LambdaJsValue{baseValue};
        break;

    case ListenerValue:
        ret = new ce::ListenerValue{baseValue};
        break;

    case HandlerValue:
        ret = new ce::HandlerValue{baseValue};
        break;

    case SessionValue:
        ret = new ce::SessionValue{baseValue};
        break;

    case TabValue:
        ret = new ce::TabValue{baseValue};
        break;

    case WindowValue:
        ret = new ce::WindowValue{baseValue};
        break;

    case DocumentValue:
        ret = new ce::DocumentValue{baseValue};
        break;

    default:
        ret = core::factory::Factory::clone(value, type);
    }

    return ret;
}

core::memory::Type Factory::variantToType(const icVariant & var) {
    using namespace memory::TypesNM;

    static const icObject<proType, Type> map{
      {proType::Cookie, CookieValue},
      {proType::DB, DatabaseValue},
      {proType::Query, QueryValue},
      {proType::Element, ElementValue},
      {proType::Elements, ElementsValue},
      {proType::File, FileValue},
      {proType::JsLambda, JavaScriptLambdaValue},
      {proType::Listener, ListenerValue},
      {proType::Handler, HandlerValue},
      {proType::Session, SessionValue},
      {proType::Tab, TabValue},
      {proType::Window, WindowValue},
      {proType::Document, DocumentValue}};

    auto it = map.find(static_cast<proType>(var.type()));

    if (it == map.end()) {
        return core::factory::Factory::variantToType(var);
    }

    return static_cast<core::memory::Type>(*it);
}

icType Factory::typeToVarType(const core::memory::Type type) {
    using namespace memory::TypesNM;

    static const icObject<Type, proType> map{
      {CookieValue, proType::Cookie},
      {DatabaseValue, proType::DB},
      {QueryValue, proType::Query},
      {ElementValue, proType::Element},
      {ElementsValue, proType::Elements},
      {FileValue, proType::File},
      {JavaScriptLambdaValue, proType::JsLambda},
      {ListenerValue, proType::Listener},
      {HandlerValue, proType::Handler},
      {SessionValue, proType::Session},
      {TabValue, proType::Tab},
      {WindowValue, proType::Window},
      {DocumentValue, proType::Document}};

    auto it = map.find(static_cast<Type>(type));

    if (it == map.end()) {
        return core::factory::Factory::typeToVarType(type);
    }

    return static_cast<icType>(*it);
}

icString Factory::typeToString(const short & type) {
    using namespace memory::TypesNM;

    static const icObject<short, icString> map{
      {CookieValue, "cookie"},
      {DatabaseValue, "daabase"},
      {QueryValue, "query"},
      {ElementValue, "element"},
      {ElementsValue, "elements"},
      {FileValue, "file"},
      {JavaScriptLambdaValue, "lambda-js"},
      {ListenerValue, "listener"},
      {HandlerValue, "handler"},
      {SessionValue, "session"},
      {TabValue, "tab"},
      {WindowValue, "window"},
      {DocumentValue, "document"}};

    auto it = map.find(static_cast<Type>(type));

    if (it == map.end()) {
        return core::factory::Factory::typeToString(type);
    }

    return *it;
}



}  // namespace icL::shared
