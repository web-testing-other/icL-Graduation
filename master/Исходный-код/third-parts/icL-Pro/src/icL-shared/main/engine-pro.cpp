#include "engine-pro.h++"

#include "factory.h++"

#include <icL-cp/cp/factory-pro.h++>

#include <frontend.h++>
#include <request.h++>
#include <stateful-server-pro.h++>



namespace icL::shared {

void Engine::setFrontEndServer(il::FrontEnd * server) {
    m_pro.server = server;
}

void Engine::setDbServer(il::DBServer * db) {
    m_pro.db = db;
}

void Engine::setFileServer(il::FileServer * file) {
    m_pro.file = file;
}

void Engine::setListenService(il::Listen * listen) {
    m_pro.listen = listen;
}

void Engine::setRequestService(il::Request * request) {
    m_pro.request = request;
}

void Engine::finalize() {
    if (m_core.cpfactory == nullptr) {
        m_core.cpfactory = new cp::Factory{};
    }
    if (m_core.factory == nullptr) {
        m_core.factory = new Factory{};
    }
    if (m_pro.server == nullptr) {
        m_pro.server = new FrontEnd{};
    }
    if (m_pro.request == nullptr) {
        m_pro.request = new Request{};
    }

    m_core.ext = new std::any(&m_pro);

    core::shared::Engine::finalize();
}

il::InterLevel * Engine::pro() {
    return &m_pro;
}

}  // namespace icL::shared
