#include "stateful-server-pro.h++"

#include <icL-il/export/signals-pro.h++>


namespace icL::shared {

StatefulServer::StatefulServer() {
    using namespace il::SignalsNM;

    icObject<icString, int> map2{
      {"NoSessions", NoSessions},
      {"NoSuchWindow", NoSuchWindow},
      {"NoSuchElement", NoSuchElement},
      {"NoSuchFrame", NoSuchFrame},
      {"NoSuchCookie", NoSuchCookie},
      {"NoSuchAlert", NoSuchAlert},
      {"NoSuchPlaceholder", NoSuchPlaceholder},
      {"NoSuchDatabase", NoSuchDatabase},
      {"NoSuchServer", NoSuchServer},
      {"WrongUserPassword", WrongUserPassword},
      {"StaleElementReference", StaleElementReference},
      {"FolderNotFound", FolderNotFound},
      {"FileNotFound", FileNotFound},
      {"UnsupportedOperation", UnsupportedOperation},
      {"EmptyElement", EmptyElement},
      {"MultiElement", MultiElement},
      {"InvalidSelector", InvalidSelector},
      {"InvalidElementState", InvalidElementState},
      {"InvalidElement", InvalidElement},
      {"InvalidSessionId", InvalidSessionId},
      {"InvalidCookieDomain", InvalidCookieDomain},
      {"InsecureCertificate", InsecureCertificate},
      {"UnexpectedAlertOpen", UnexpectedAlertOpen},
      {"UnrealCast", UnrealCast},
      {"ElementNotInteractable", ElementNotInteractable},
      {"ElementClickIntercepted", ElementClickIntercepted},
      {"MoveTargetOutOfBounds", MoveTargetOutOfBounds},
      {"UnableToSetCookie", UnableToSetCookie},
      {"UnableToCaptureScreen", UnableToCaptureScreen},
      {"JavaScriptError", JavaScriptError},
      {"ScriptTimeout", ScriptTimeout},
      {"SessionNotCreated", SessionNotCreated},
      {"QueryNotExecutedYet", QueryNotExecutedYet},
      {"UnknownCommand", UnknownCommand},
      {"UnknownError", UnknownError},
      {"UnknownMethod", UnknownMethod}};

    for (auto it = map2.begin(); it != map2.end(); it++) {
        map.insert(it.key(), it.value());
    }

    lastSignalCode = Last;
}

}  // namespace icL::shared
