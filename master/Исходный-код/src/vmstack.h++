#ifndef icL_VMStack
#define icL_VMStack

#include <icL-vm/vmstack.h++>

#include <QObject>



class RunMe;

namespace icL {

class VMStack
    : public QObject
    , public core::vm::VMStack
{
    Q_OBJECT

    Q_PROPERTY(QString text READ text NOTIFY textChanged)
    Q_PROPERTY(QString color READ color NOTIFY colorChanged)

    QString m_text;
    QString   m_color = "#fff00f";
    RunMe *   thread;

public:
    explicit VMStack(core::il::InterLevel * il, RunMe * thread);

    QString text() const;
    QString color() const;

public slots:
    void makeAStep();
    void makeAStepOver();
    void makeAStepInto();
    void makeAStepOut();
    void makeAStepForever();

signals:
    void textChanged();
    void colorChanged();
    void select(int begin, int end);

    // VMStack interface
public:
    void init(const icString & file) override;
    void highlight(
      const core::il::Position & pos1,
      const core::il::Position & pos2) override;
    void setSColor(core::il::SelectionColor scolor) override;
};

} // namespace icL

#endif  // icL_VMStack
