#include "back-end.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/export/signals-pro.h++>
#include <icL-il/main/enums.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>
#include <icL-il/structures/signal.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-memory/structures/set.h++>
#include <icL-service/main/values/set.h++>

#include <QClipboard>
#include <QDateTime>
#include <QGuiApplication>
#include <QThread>

#include <iostream>
#include <src/server.h++>



namespace icL {

constexpr const char * sessionId =
  u8"icL-session-8963307c-dc2f-4dbd-83ef-d6762a12a4f5";

using namespace il::SignalsNM;

BackEnd::BackEnd(core::il::InterLevel * il, Server * server)
    : core::il::Node(il)
    , server(server) {
    clipboard = qApp->clipboard();
    newWindow();
}

void BackEnd::pushTarget(const il::TargetData & target) {
    if (target.sessionId == sessionId && !target.windowId.isEmpty()) {
        if (!server->checkTab(target.windowId)) {
            il->vm->signal({Signals::NoSuchWindow, "No such window"});
        }
        targets.push(&target);
    }
    else {
        il->vm->signal({Signals::NoSessions, "No sessions here"});
    }
}

void BackEnd::popTarget() {
    targets.pop();

    if (!targets.isEmpty()) {
        if (!server->checkTab(targets.top()->windowId)) {
            il->vm->signal({Signals::NoSuchWindow, "No such window"});
        }
    }
}

il::TargetData BackEnd::getCurrentTarget() {
    return {sessionId, server->getCurrentTab()};
}

void BackEnd::newSession() {
    il->vm->syssig("Session are not supported by icL-Magic driver");
}

void BackEnd::switchSession() {
    il->vm->syssig("Session are not supported by icL-Magic driver");
}

void BackEnd::closeSession() {
    il->vm->syssig("Session are not supported by icL-Magic driver");
}

int BackEnd::implicitTimeout() {
    return _implicitTimout;
}

int BackEnd::pageLoadTimeout() {
    return _pageLoadTimeout;
}

int BackEnd::scriptTimeout() {
    return _scriptTimeout;
}

void BackEnd::setImplicitTimeout(int ms) {
    _implicitTimout = ms;
}

void BackEnd::setPageLoadTimeOut(int ms) {
    _pageLoadTimeout = ms;
}

void BackEnd::setScriptTimeout(int ms) {
    _scriptTimeout = ms;
}

void BackEnd::focusSession(const il::TargetData & /*sessionId*/) {
    il->vm->syssig("Session are not supported by icL-Magic driver");
}

icList<il::TargetData> BackEnd::getSessions() {
    return {{sessionId, {}}};
}

void BackEnd::load(const icString & url) {
    il->vm->hasOkState() ? server->loadUrl(url) : void();
}

icString BackEnd::url() {
    return il->vm->hasOkState() ? server->getUrl() : "";
}

void BackEnd::back() {
    il->vm->hasOkState() ? server->back() : void();
}

void BackEnd::forward() {
    il->vm->hasOkState() ? server->forward() : void();
}

void BackEnd::refresh() {
    il->vm->hasOkState() ? server->refresh() : void();
}

icString BackEnd::title() {
    return il->vm->hasOkState() ? server->title() : "";
}

icString BackEnd::window() {
    return server->getCurrentTab();
}

il::TargetData BackEnd::newWindow() {
    return {sessionId, server->openTab()};
}

void BackEnd::closeWindow() {
    il->vm->hasOkState() ? server->closeTab() : void();
}

void BackEnd::focusWindow() {
    il->vm->hasOkState() ? server->switchToTab(targets.top()->windowId)
                         : void();
}

icList<il::TargetData> BackEnd::getWindows() {
    auto list = server->getTabs();

    icList<il::TargetData> ret;

    for (auto & tab : list) {
        ret.append({sessionId, tab});
    }

    return ret;
}

void BackEnd::switchToFrame(int id) {
    server->runJs("icL.switchToFrameById($<>)", {id});
}

void BackEnd::switchToFrame(const il::Element & el) {
    server->runJs("icL.switchtoFrame($<>)", {el});
}

void BackEnd::switchToParent() {
    server->runJs("icL.switchToParent()");
}

icRect BackEnd::getWindowRect() {
    return server->getWindowRect();
}

icRect BackEnd::setWindowRect(const icRect & rect) {
    return server->setWindowRect(rect);
}

void BackEnd::maximize() {
    server->maximize();
}

void BackEnd::minimize() {
    server->minimize();
}

void BackEnd::fullscreen() {
    server->fullscreen();
}

void BackEnd::restore() {
    server->restore();
}

il::Element BackEnd::query(
  il::FrontEnd::SearchElement & search, const icString & str) {
    auto elements = queryAll({search, 1, -1}, str);
    return empty(elements) ? il::Element{} : get(elements, 0);
}

il::Element BackEnd::query(
  il::FrontEnd::SearchElement & search, const icRegEx & rex) {
    auto elements = queryAll({search, 1, -1}, rex);
    return empty(elements) ? il::Element{} : get(elements, 0);
}

il::Elements BackEnd::queryAll(
  const il::FrontEnd::SearchElements & search, const icString & str) {

    return queryAllLowLevel(search, [&str, &search, this]() -> il::Elements {
        icVariant parent = icVariant::makeVoid();

        if (search.se.element != nullptr) {
            parent = *search.se.element;
        }

        switch (search.se.selectorType) {
            using namespace il::SelectorsNM;

        case CssSelector:
            return server->runJs(
              "($<1>||icL).nm_css($<>)", {str, parent}, il::Elements{});

        case XPath:
            return server->runJs(
              "($<1>||icL).nm_xpath($<>)", {str, parent}, il::Elements{});

        case LinkText:
            return server->runJs(
              "($<3>||icL).nm_links($<>, $<1>, $<2>)",
              {str, search.se.minP, search.se.maxP, parent}, il::Elements{});

        case PartialLinkText:
            return server->runJs(
              "($<1>||icL).nm_plinks($<>)", {str, parent}, il::Elements{});

        case TagName:
            if (str.isEmpty()) {
                return server->runJs(
                  "($<1>||icL).nm_css($<>)", {*search.se.tagName, parent},
                  il::Elements{});
            }
            else {
                return server->runJs(
                  "($<4>||icL).nm_tags($<>, $<1>, $<2>, $<3>)",
                  {*search.se.tagName, str, search.se.minP, search.se.maxP,
                   parent},
                  il::Elements{});
            }

        case Input:
            return server->runJs(
              "($<1>||icL).nm_input($<>)", {str, parent}, il::Elements{});

        case Field:
            return server->runJs(
              "($<3>||icL).nm_field($<>, $<1>, $<2>)",
              {str, search.se.minP, search.se.maxP, parent}, il::Elements{});
        }

        return {};
    });
}

il::Elements BackEnd::queryAll(
  const il::FrontEnd::SearchElements & search, const icRegEx & rex) {
    return queryAllLowLevel(search, [&rex, &search, this]() -> il::Elements {
        icVariant parent = icVariant::makeVoid();

        if (search.se.element != nullptr) {
            parent = *search.se.element;
        }

        switch (search.se.selectorType) {
            using namespace il::SelectorsNM;

        case LinkText:
            return server->runJs(
              "($<1>||icL).nm_links_rx($<>)", {rex, parent}, il::Elements{});

        case TagName:
            return server->runJs(
              "($<1>||icL).nm_tags_rx($<>)", {rex, parent}, il::Elements{});

        case Field:
            return server->runJs(
              "($<1>||icL).nm_field_rx($<>)", {rex, parent}, il::Elements{});

        default:
            il->vm->syssig("current method doesn't support regex input");
        }

        return {};
    });
}

il::Elements BackEnd::queryAllLowLevel(
  const il::FrontEnd::SearchElements &  search,
  const std::function<il::Elements()> & lambda) {
    il::Elements result;
    long long    startTime = QDateTime::currentMSecsSinceEpoch();

    do {
        result = lambda();
    } while (((result.size() < search.min &&
               int(QDateTime::currentMSecsSinceEpoch() - startTime) <
                 search.se.searchTime) ||
              result.isEmpty()) &&
             il->vm->hasOkState());

    if (result.size() < search.min) {
        std::cout << "try modifier value " << search.se.tryModifier
                  << std::endl;

        if (search.se.tryModifier) {
            return result;
        }

        il->vm->signal({Signals::NoSuchElement,
                        search.min == 1 ? "Web element was not found"
                                        : "Web elements were not found"});
        return {};
    }

    if (search.max > 0 && result.size() > search.max) {
        result = server->runJs(
          "$<>.first($<1>)", {result, search.max}, il::Elements{});
    }

    return result;
}

bool BackEnd::empty(const il::Elements & el) {
    return el.size() == 0;
}

int BackEnd::length(const il::Elements & el) {
    return el.size();
}

il::Element BackEnd::active() {
    return server->runJs("icL.doc.activeElement", {}, il::Element{});
}

bool BackEnd::clickable(const il::Element & el) {
    return server->runJs("$<>.clickable()", {el}, false);
}

bool BackEnd::visible(const il::Element & el) {
    return server->runJs("$<>.visible()", {el}, false);
}

bool BackEnd::selected(const il::Element & el) {
    return server->runJs("$<>.selected()", {el}, false);
}

icString BackEnd::attr(const il::Element & el, const icString & name) {
    return server->runJs("$<>.attr($<1>)", {el, name}, icString{});
}

icStringList BackEnd::attr(const il::Elements & el, const icString & name) {
    return server->runJs("$<>.attrs($<1>)", {el, name}, icStringList{});
}

icVariant BackEnd::prop(const il::Element & el, const icString & name) {
    return server->runJs("$<>.prop($<1>)", {el, name});
}

icVariant BackEnd::prop(const il::Elements & el, const icString & name) {
    return server->runJs("$<>.props($<1>)", {el, name});
}

icString BackEnd::css(const il::Element & el, const icString & name) {
    return server->runJs("$<>.css($<1>)", {el, name}, icString{});
}

icString BackEnd::text(const il::Element & el) {
    return server->runJs("$<>.text()", {el}, icString{});
}

icStringList BackEnd::text(const il::Elements & el) {
    return server->runJs("$<>.texts()", {el}, icStringList{});
}

icString BackEnd::name(const il::Element & el) {
    return server->runJs("$<>.name()", {el}, icString{});
}

icStringList BackEnd::name(const il::Elements & el) {
    return server->runJs("$<>.names()", {el}, icStringList{});
}

icRect BackEnd::rect(const il::Element & el) {
    core::memory::Object obj =
      server->runJs("$<>.rect()", {el}, core::memory::Object{il});

    return {obj.data->getValue("x"), obj.data->getValue("y"),
            obj.data->getValue("width"), obj.data->getValue("height")};
}

icRects BackEnd::rect(const il::Elements & el) {
    core::memory::Set set =
      server->runJs("$<>.rects()", {el}, core::memory::Set{});
    icRects ret;

    core::service::Set::forEach(
      il, set, [&ret](const core::memory::Object & obj) {
          ret.append({obj.data->getValue("x"), obj.data->getValue("y"),
                      obj.data->getValue("width"),
                      obj.data->getValue("height")});
      });

    return ret;
}

bool BackEnd::enabled(const il::Element & el) {
    return server->runJs("$<>.enabled()", {el}, false);
}

il::Elements BackEnd::add_11(
  const il::Element & el, const il::Element & toAdd) {
    return server->runJs("new icLnmXs($<>, $<1>)", {el, toAdd}, il::Elements{});
}

il::Elements BackEnd::add_m1(
  const il::Elements & el, const il::Element & toAdd) {
    if (el.isEmpty()) {
        il::Elements els{toAdd};
        els.setSize(1);
        return els;
    }
    return server->runJs("$<>.add($<1>)", {el, toAdd}, il::Elements{});
}

il::Elements BackEnd::add_mm(
  const il::Elements & el, const il::Elements & toAdd) {
    if (el.isEmpty()) {
        return toAdd;
    }
    return server->runJs("$<>.add($<1>)", {el, toAdd}, il::Elements{});
}

il::Element BackEnd::copy(const il::Element & el) {
    return server->runJs("$<>.copy()", {el}, il::Element{});
}

il::Elements BackEnd::copy(const il::Elements & el) {
    return server->runJs("$<>.copy()", {el}, il::Elements{});
}

void BackEnd::fastType(const il::Element & el, const icString & text) {
    focus(el);

    if (!il->vm->hasOkState()) {
        return;
    }

    simulateKeysPress(text, 0, 0);
}

void BackEnd::forceType(const il::Element & el, const icString & text) {
    server->runJs("$<>.put($<1>)", {el, text});
}

void BackEnd::keyDown(
  const il::Element * el, int modifiers, const icString & keys) {
    if (el != nullptr) {
        focus(*el);

        if (!il->vm->hasOkState()) {
            return;
        }
    }

    server->keyDown(keys[0], modifiers);
}

void BackEnd::keyPress(
  const il::Element * el, int modifiers, int delay, const icString & keys) {
    if (el != nullptr) {
        focus(*el);

        if (!il->vm->hasOkState()) {
            return;
        }
    }

    simulateKeysPress(keys, modifiers, delay);
}

void BackEnd::keyUp(
  const il::Element * el, int modifiers, const icString & keys) {
    if (el != nullptr) {
        focus(*el);

        if (!il->vm->hasOkState()) {
            return;
        }
    }

    server->keyUp(keys[0], modifiers);
}

void BackEnd::paste(const il::Element & el, const icString & text) {
    server->runJs("$<>.nm.focus()", {el});
    clipboard->setText(text);
    simulateKeyPress('V', 1, 0);
}

void BackEnd::sendKeys(
  const il::Element * el, int modifiers, const icString & keys) {
    if (flash && el != nullptr && modifiers == 0x0) {
        return paste(*el, keys);
    }

    if (el != nullptr) {
        focus(*el);

        if (!il->vm->hasOkState()) {
            return;
        }
    }

    simulateKeysPress(keys, modifiers, human ? pressTime : 0);
}

void BackEnd::clear(const il::Element & el) {
    server->runJs("$<>.clear()", {el});
}

void BackEnd::forceClick(const il::Element & el, const il::ClickData & data) {

    auto sc    = getScreenCoords(el, &data);
    int  delay = data.delay >= 0 ? data.delay : clickTime;

    for (int i = 0; i < data.count; i++) {
        simulateClick({sc.x, sc.y}, data.button, delay);
    }
}

void BackEnd::click(const il::Element * el, const il::ClickData & data) {

    if (flash && el != nullptr && data.button == il::MouseButtons::Left) {
        return superClick(*el);
    }

    int delay = data.delay >= 0 ? data.delay : human ? clickTime : 0;

    if (el != nullptr) {
        auto sc = getScreenCoords(*el, &data);

        if (sc.visible) {
            for (int i = 0; i < data.count; i++) {
                simulateClick({sc.x, sc.y}, data.button, delay);
            }
        }
        else {
            il->vm->signal({Signals::ElementClickIntercepted,
                            "Element is not visible on screen"});
        }
    }
    else {
        auto xy = server->coord(data.rx, data.ry, data.ax, data.ay);

        for (int i = 0; i < data.count; i++) {
            simulateClick(xy, data.button, delay);
        }
    }
}

void BackEnd::superClick(const il::Element & el) {
    server->runJs("$<>.superClick()", {el});
}

void BackEnd::hover(const il::Element * el, const il::HoverData & hover) {
    if (el != nullptr) {
        auto sc = getScreenCoords(*el, &hover);

        if (sc.visible) {
            server->hover({sc.x, sc.y});
        }
        else {
            il->vm->signal({Signals::ElementClickIntercepted,
                            "Element is not visible on screen"});
        }
    }
    else {
        server->hover(server->coord(hover.rx, hover.ry, hover.ax, hover.ay));
    }
}

void BackEnd::mouseDown(const il::Element * el, const il::MouseData & data) {
    if (el != nullptr) {
        auto sc = getScreenCoords(*el, &data);

        if (sc.visible) {
            server->mouseDown({sc.x, sc.y}, data.button);
        }
        else {
            il->vm->signal({Signals::ElementClickIntercepted,
                            "Element is not visible on screen"});
        }
    }
    else {
        server->mouseDown(
          server->coord(data.rx, data.ry, data.ax, data.ay), data.button);
    }
}

void BackEnd::mouseUp(const il::Element * el, const il::MouseData & data) {
    if (el != nullptr) {
        auto sc = getScreenCoords(*el, &data);

        if (sc.visible) {
            server->mouseUp({sc.x, sc.y}, data.button);
        }
        else {
            il->vm->signal({Signals::ElementClickIntercepted,
                            "Element is not visible on screen"});
        }
    }
    else {
        server->mouseUp(
          server->coord(data.rx, data.ry, data.ax, data.ay), data.button);
    }
}

icString BackEnd::source() {
    return server->source();
}

icVariant BackEnd::executeSync(
  const icString & code, const icVariantList & args) {
    auto value = server->runJs(code, args);
    server->runJs("true");
    return value;
}

void BackEnd::executeAsync(const icString & code, const icVariantList & args) {
    server->runJsAsync(code, args);
}

void BackEnd::setUserScript(const icString & code) {
    server->addUserScript(code, false);
}

void BackEnd::setAlwaysScript(const icString & code) {
    server->addUserScript(code, true);
}

icStringList BackEnd::getCookies() {
    il->vm->syssig("Cookies are not supported in icL-Magic");
    return {};
}

il::CookieData BackEnd::loadCookie(const icString & /*name*/) {
    il->vm->syssig("Cookies are not supported in icL-Magic");
    return {};
}

void BackEnd::saveCookie(const il::CookieData & /*data*/) {
    il->vm->syssig("Cookies are not supported in icL-Magic");
}

void BackEnd::deleteCookie(const icString & /*name*/) {
    il->vm->syssig("Cookies are not supported in icL-Magic");
}

void BackEnd::deleteAllCookies() {
    il->vm->syssig("Cookies are not supported in icL-Magic");
}

void BackEnd::alertDimiss() {
    il->vm->syssig("Alerts are not supported in icL-Magic");
}

void BackEnd::alertAccept() {
    il->vm->syssig("Alerts are not supported in icL-Magic");
}

icString BackEnd::alertText() {
    il->vm->syssig("Alerts are not supported in icL-Magic");
    return {};
}

void BackEnd::alertSendText(const icString & /*text*/) {
    il->vm->syssig("Alerts are not supported in icL-Magic");
}

icString BackEnd::screenshot() {
    il->vm->syssig("Screeshots are not supported in icL-Magic");
    return {};
}

icString BackEnd::screenshot(const il::Element & /*el*/) {
    il->vm->syssig("Screeshots are not supported in icL-Magic");
    return {};
}

il::Element BackEnd::get(const il::Elements & el, int n) {
    if (n < el.size() && n >= 0) {
        return server->runJs("$<>.get($<1>)", {el, n}, il::Element{});
    }

    il->vm->signal({core::il::Signals::OutOfBounds,
                    "Index " + icString::number(n) + " is out of bounds"});
    return {};
}

il::Elements BackEnd::filter(
  const il::Elements & el, const icString & selector) {
    return server->runJs("$<>.filter($<1>)", {el, selector}, il::Elements{});
}

il::Elements BackEnd::contains(
  const il::Elements & el, const icString & substring) {
    return server->runJs("$<>.contains($<1>)", {el, substring}, il::Elements{});
}

il::Element BackEnd::next(const il::Element & el) {
    return server->runJs("$<>.next()", {el}, il::Element{});
}

il::Element BackEnd::prev(const il::Element & el) {
    return server->runJs("$<>.prev()", {el}, il::Element{});
}

il::Element BackEnd::parent(const il::Element & el) {
    return server->runJs("$<>.parent()", {el}, il::Element{});
}

il::Element BackEnd::child(const il::Element & el, int n) {
    return server->runJs("$<>.child($<1>)", {el, n}, il::Element{});
}

il::Elements BackEnd::children(const il::Element & el) {
    return server->runJs("$<>.children()", {el}, il::Elements{});
}

il::Element BackEnd::closest(
  const il::Element & el, const icString & selector) {
    return server->runJs("$<>.closest($<1>)", {el, selector}, il::Element{});
}

int BackEnd::getClickTime() {
    return clickTime;
}

int BackEnd::getMoveTime() {
    return moveTime;
}

int BackEnd::getPressTime() {
    return pressTime;
}

bool BackEnd::getFlashMode() {
    return flash;
}

bool BackEnd::getHumanMode() {
    return human;
}

bool BackEnd::getSilentMode() {
    return silent;
}

void BackEnd::setClickTime(int clickTime) {
    this->clickTime = clickTime;
}

void BackEnd::setMoveTime(int moveTime) {
    this->moveTime = moveTime;
}

void BackEnd::setPressTime(int pressTime) {
    this->pressTime = pressTime;
}

void BackEnd::setFlashMode(bool flashMode) {
    flash = flashMode;
}

void BackEnd::setHumanMode(bool humanMode) {
    human = humanMode;
}

void BackEnd::setSilentMode(bool silentMode) {
    if (silentMode != silent) {
        server->setSilentMode(silentMode);
        silent = silentMode;
    }
}

void BackEnd::logError(const icString & error) {
    std::cerr << error.toString() << std::endl;
}

void BackEnd::logInfo(const icString & info) {
    std::cout << info.toString() << std::endl;
}

void BackEnd::setAjaxHandler(il::AjaxHandler * /*handler*/) {
    // TODO: Add support for this
}

void BackEnd::resetAjaxHandler() {
    // TODO: Add support for this
}

BackEnd::ScreenCoords BackEnd::getScreenCoords(
  const il::Element & el, const il::MouseData * data) {
    static core::memory::Object defaultObj(
      il, {{"x", -2.0}, {"y", -2.0}, {"visible", false}});

    core::memory::Object obj = server->runJs(
      "$<>.flash($<1>, $<2>, $<3>, $<4>)",
      {el, data->rx, data->ry, data->ax, data->ay}, defaultObj);

    return {obj.data->getValue("x"), obj.data->getValue("y"),
            obj.data->getValue("visible")};
}

void BackEnd::simulateClick(
  const icL::ScreenCoords & xy, int button, int pause) {
    server->setEventing(true);
    server->mouseDown(xy, button);
    QThread::msleep(pause);
    server->mouseUp(xy, button);
    QThread::msleep(pause / 10);
    server->setEventing(false);
}

void BackEnd::focus(const il::Element & el, bool input) {
    if (server->runJs("icL.doc.activeElement != $<>.nm", {el}, false)) {
        if (input) {
            server->runJs("$<>.prepareForType()", {el});
        }
    }
}

void BackEnd::simulateKeyPress(const icChar & ch, int modifiers, int delay) {
    server->keyDown(ch, modifiers);
    QThread::msleep(delay);
    server->keyUp(ch, modifiers);
    QThread::msleep(delay / 10);
}

void BackEnd::simulateKeysPress(
  const icString & str, int modifiers, int delay) {
    server->setEventing(true);
    for (int i = 0; i < str.length(); i++) {
        simulateKeyPress(str[i], modifiers, delay);
    }
    server->setEventing(false);
}

} // namespace icL
