#ifndef magic_BackEnd
#define magic_BackEnd

#include <icL-il/main/frontend.h++>
#include <icL-il/main/node.h++>

#include <QStack>

#include <tuple>



class icChar;
class QClipboard;

namespace icL {

namespace core::memory {
class Object;
}

class Server;
class ScreenCoords;

class BackEnd
    : public il::FrontEnd
    , public core::il::Node
{
public:
    BackEnd(core::il::InterLevel * il, Server * server);

private:
    QStack<const il::TargetData *> targets;

    Server * server = nullptr;

    int _implicitTimout  = 0;
    int _scriptTimeout   = 30000;
    int _pageLoadTimeout = 300000;

    int clickTime = 300;
    int moveTime  = 300;
    int pressTime = 54;

    bool human  = false;
    bool flash  = false;
    bool silent = false;

    QClipboard * clipboard = nullptr;

    // FrontEnd interface
public:
    void pushTarget(const il::TargetData & target) override;
    void popTarget() override;

    il::TargetData getCurrentTarget() override;

    void newSession() override;
    void switchSession() override;
    void closeSession() override;

    int implicitTimeout() override;
    int pageLoadTimeout() override;
    int scriptTimeout() override;

    void setImplicitTimeout(int ms) override;
    void setPageLoadTimeOut(int ms) override;
    void setScriptTimeout(int ms) override;
    void focusSession(const il::TargetData & sessionId) override;

    icList<il::TargetData> getSessions() override;

    void load(const icString & url) override;

    icString url() override;

    void back() override;
    void forward() override;
    void refresh() override;

    icString title() override;
    icString window() override;

    il::TargetData newWindow() override;

    void closeWindow() override;
    void focusWindow() override;

    icList<il::TargetData> getWindows() override;

    void switchToFrame(int id) override;
    void switchToFrame(const il::Element & el) override;
    void switchToParent() override;

    icRect getWindowRect() override;
    icRect setWindowRect(const icRect & rect) override;

    void maximize() override;
    void minimize() override;
    void fullscreen() override;
    void restore() override;

    il::Element query(SearchElement & search, const icString & str) override;
    il::Element query(SearchElement & search, const icRegEx & rex) override;

    il::Elements queryAll(
      const SearchElements & search, const icString & str) override;
    il::Elements queryAll(
      const SearchElements & search, const icRegEx & rex) override;

    il::Elements queryAllLowLevel(
      const SearchElements &                search,
      const std::function<il::Elements()> & lambda);

    bool empty(const il::Elements & el) override;
    int  length(const il::Elements & el) override;

    il::Element active() override;

    bool clickable(const il::Element & el) override;
    bool visible(const il::Element & el) override;
    bool selected(const il::Element & el) override;

    icString     attr(const il::Element & el, const icString & name) override;
    icStringList attr(const il::Elements & el, const icString & name) override;
    icVariant    prop(const il::Element & el, const icString & name) override;
    icVariant    prop(const il::Elements & el, const icString & name) override;
    icString     css(const il::Element & el, const icString & name) override;

    icString     text(const il::Element & el) override;
    icStringList text(const il::Elements & el) override;
    icString     name(const il::Element & el) override;
    icStringList name(const il::Elements & el) override;

    icRect  rect(const il::Element & el) override;
    icRects rect(const il::Elements & el) override;

    bool enabled(const il::Element & el) override;

    il::Elements add_11(
      const il::Element & el, const il::Element & toAdd) override;
    il::Elements add_m1(
      const il::Elements & el, const il::Element & toAdd) override;
    il::Elements add_mm(
      const il::Elements & el, const il::Elements & toAdd) override;

    il::Element  copy(const il::Element & el) override;
    il::Elements copy(const il::Elements & el) override;

    void fastType(const il::Element & el, const icString & text) override;
    void forceType(const il::Element & el, const icString & text) override;

    void keyDown(
      const il::Element * el, int modifiers, const icString & keys) override;
    void keyPress(
      const il::Element * el, int modifiers, int delay,
      const icString & keys) override;
    void keyUp(
      const il::Element * el, int modifiers, const icString & keys) override;

    void paste(const il::Element & el, const icString & text) override;
    void sendKeys(
      const il::Element * el, int modifiers, const icString & keys) override;

    void clear(const il::Element & el) override;

    void forceClick(
      const il::Element & el, const il::ClickData & data) override;
    void click(const il::Element * el, const il::ClickData & data) override;
    void superClick(const il::Element & el) override;

    void hover(const il::Element * el, const il::HoverData & hover) override;
    void mouseDown(const il::Element * el, const il::MouseData & data) override;
    void mouseUp(const il::Element * el, const il::MouseData & data) override;

    icString  source() override;
    icVariant executeSync(
      const icString & code, const icVariantList & args) override;

    void executeAsync(
      const icString & code, const icVariantList & args) override;
    void setUserScript(const icString & code) override;
    void setAlwaysScript(const icString & code) override;

    icStringList   getCookies() override;
    il::CookieData loadCookie(const icString & name) override;

    void saveCookie(const il::CookieData & data) override;
    void deleteCookie(const icString & name) override;
    void deleteAllCookies() override;

    void alertDimiss() override;
    void alertAccept() override;

    icString alertText() override;
    void     alertSendText(const icString & text) override;

    icString screenshot() override;
    icString screenshot(const il::Element & el) override;

    il::Element  get(const il::Elements & el, int n) override;
    il::Elements filter(
      const il::Elements & el, const icString & selector) override;
    il::Elements contains(
      const il::Elements & el, const icString & substring) override;
    il::Element  next(const il::Element & el) override;
    il::Element  prev(const il::Element & el) override;
    il::Element  parent(const il::Element & el) override;
    il::Element  child(const il::Element & el, int n) override;
    il::Elements children(const il::Element & el) override;
    il::Element  closest(
       const il::Element & el, const icString & selector) override;

    int  getClickTime() override;
    int  getMoveTime() override;
    int  getPressTime() override;
    bool getFlashMode() override;
    bool getHumanMode() override;
    bool getSilentMode() override;

    void setClickTime(int clickTime) override;
    void setMoveTime(int moveTime) override;
    void setPressTime(int pressTime) override;
    void setFlashMode(bool flashMode) override;
    void setHumanMode(bool humanMode) override;
    void setSilentMode(bool silentMode) override;

    void logError(const icString & error) override;
    void logInfo(const icString & info) override;

    void setAjaxHandler(il::AjaxHandler * handler) override;
    void resetAjaxHandler() override;

private:
    struct ScreenCoords
    {
        double x;
        double y;
        bool   visible;
    };

    ScreenCoords getScreenCoords(
      const il::Element & el, const il::MouseData * data);
    void simulateClick(const icL::ScreenCoords & xy, int button, int pause);

    void focus(const il::Element & el, bool input = true);
    void simulateKeyPress(const icChar & ch, int modifiers, int delay);
    void simulateKeysPress(const icString & str, int modifiers, int delay);
};

}  // namespace icL

#endif  // magic_BackEnd
