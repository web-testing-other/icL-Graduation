#ifndef magic_Server
#define magic_Server

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/node.h++>

#include <icL-service/main/values/inode-pro.h++>

#include <QObject>
#include <QVariant>



template <typename, typename>
class icPair;
class icRect;

class QQuickItem;

class Proxy;

namespace icL {

using VariantList = QList<icVariant>;

struct ScreenCoords
{
    double x;
    double y;
};

class Server
    : public QObject
    , virtual public service::INode
    , public core::il::Node
{
    Q_OBJECT

    QList<QQuickItem *> tabs;

    QQuickItem * current = nullptr;
    QQuickItem * working = nullptr;
    QQuickItem * handler = nullptr;

    Proxy * proxy = nullptr;

    icVariant jsResult;
    bool      responseGetted = false;

public:
    Server(
      core::il::InterLevel * il, Proxy * proxy, QObject * parent = nullptr);

    QString     openTab();
    QStringList getTabs();
    QString     getCurrentTab();

    bool checkTab(const QString & uuid);
    void switchToTab(const QString & uuid);
    void closeTab();
    void setSilentMode(bool silent);

    void    loadUrl(const QString & url);
    QString getUrl();
    QString title();
    QString source();

    void back();
    void refresh();
    void forward();

    icVariant runJs(
      const QString & code, const VariantList & args = {},
      const icVariant & _default = {}, bool async = false);
    void runJsAsync(const QString & code, const VariantList & args = {});
    void addUserScript(const QString & code, bool persistent);

    icRect getWindowRect();
    icRect setWindowRect(const icRect & rect);

    void maximize();
    void minimize();
    void fullscreen();
    void restore();

    void keyDown(QChar ch, int modifiers);
    void keyUp(QChar ch, int modifiers);

    ScreenCoords coord(double rx, double ry, int ax, int ay);

    void hover(const ScreenCoords & xy);
    void mouseDown(const ScreenCoords & xy, int button);
    void mouseUp(const ScreenCoords & xy, int button);

    void acceptResult(const QString & result);

    QQuickItem * getWorking();

    void setEventing(bool value);
    void setRunning(bool value);

private:
    void     invoke(const char * funcName);
    QVariant property(QObject * obj, const char * name);
    void setProperty(QObject * obj, const char * name, const QVariant & value);

signals:
    void requiereInvoke(const char * funcName);
    void requiereProperty(QObject * obj, const char * name);
    void requiereSetProperty(
      QObject * obj, const char * name, const QVariant & value);
    void requiereEvent(QEvent * event);

private slots:
    void resolveInvoke(const char * funcName);
    void resolveProperty(QObject * obj, const char * name);
    void resolveSetProperty(
      QObject * obj, const char * name, const QVariant & value);
    void resolveEvent(QEvent * event);

private:
    QString getNewTabId();

    void waitForPageLoad();
    void waitForPageLoadFromVmThread();

    jsObject  argToJson(const icVariant & var);
    icVariant jsonToArg(const jsObject & obj);

    static Qt::KeyboardModifiers modifiersToQt(int modifiers);
    static Qt::MouseButton       buttonToQt(int button);

    void releaseEvent(QEvent * event);
    bool inScreen(const ScreenCoords & xy);

private:
    QVariant      result;
    volatile bool runned = false;

    // INode interface
public:
    core::il::InterLevel * core() override;
};

}  // namespace icL

#endif  // magic_Server
