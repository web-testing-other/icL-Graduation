#include "run-me.h++"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtWebEngine>



int main(int argc, char * argv[]) {
    QCoreApplication::setOrganizationName("Lixcode Solutions");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QGuiApplication       app(argc, argv);
    QQmlApplicationEngine engine;
    QtWebEngine::initialize();


    RunMe runMe;

    engine.rootContext()->setContextProperty("bus", runMe.getProxy());
    engine.rootContext()->setContextProperty("vms", runMe.getVMStack());

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(
      &engine, &QQmlApplicationEngine::objectCreated, &app,
      [url, &runMe](QObject * obj, const QUrl & objUrl) {
          if (!obj && url == objUrl) {
              QCoreApplication::exit(-1);
          }
          else {
              runMe.init();
              runMe.stepType = RunMe::ToEnd;
              runMe.start();
          }
      },
      Qt::QueuedConnection);

    engine.load(url);

    return app.exec();
}
