#include "vmstack.h++"

#include "run-me.h++"

#include <icL-il/structures/position.h++>

#include <QFile>
#include <QTextStream>
#include <QThread>



namespace icL {

VMStack::VMStack(core::il::InterLevel * il, RunMe * thread)
    : core::vm::VMStack(il)
    , thread(thread) {}

QString VMStack::text() const {
    return m_text;
}

QString icL::VMStack::color() const {
    return m_color;
}

void VMStack::makeAStep() {
    thread->stepType = RunMe::Mini;
    thread->start();
}

void VMStack::makeAStepOver() {
    thread->stepType = RunMe::Over;
    thread->start();
}

void VMStack::makeAStepInto() {
    thread->stepType = RunMe::In;
    thread->start();
}

void VMStack::makeAStepOut() {
    thread->stepType = RunMe::Out;
    thread->start();
}

void VMStack::makeAStepForever() {
    thread->stepType = RunMe::ToEnd;
    thread->start();
}

void VMStack::init(const icString & file) {
    QFile       f(file);
    QTextStream s(&f);

    f.open(f.ReadOnly);

    m_text = s.readAll();
    emit textChanged();

    core::vm::VMStack::init(file);
}

void VMStack::highlight(
  const core::il::Position & pos1, const core::il::Position & pos2) {
    emit select(pos1.absolute, pos2.absolute);
}

void VMStack::setSColor(core::il::SelectionColor scolor) {
    switch (scolor) {
        using namespace core::il::SelectionColorNM;

    case Error:
        m_color = "#f8e1e1";
        break;

    case Parsing:
        m_color = "#e1f8e4";
        break;

    case NewStack:
        m_color = "#efe1f8";
        break;

    case Executing:
        m_color = "#e1e9f8";
        break;

    case Destroying:
        m_color = "#f4f8e1";
        break;

    default:
        break;
    }

    emit colorChanged();
}

} // namespace icL
