#include "proxy.h++"

#include "server.h++"

#include <icL-types/replaces/ic-rect.h++>

#include <QQuickItem>
#include <QQuickWindow>
#include <QThread>



Proxy::Proxy() {
    connect(
      this, &Proxy::requiereInvoke, this, &Proxy::resolveInvoke,
      Qt::BlockingQueuedConnection);
}

QObject * Proxy::openTab(const QString & uuid) {
    invoke("open", uuid);
    return m_tab;
}

bool Proxy::silent() const {
    return m_silent;
}

void Proxy::switchToTab(const QString & uuid) {
    invoke("switchTo", uuid);
}

void Proxy::closeTab(const QString & uuid) {
    invoke("close", uuid);
}

void Proxy::runJs(const QString & code, bool sync) {
    invoke("runJs", code, sync);
}

void Proxy::addUserScript(const QString & code, bool persistent) {
    invoke("addUserScript", code, persistent);
}

icRect Proxy::getWindowRect() {
    return {double(m_win->x()), double(m_win->y()), double(m_win->width()),
            double(m_win->height())};
}

void Proxy::setWindowRect(const icRect & rect) {
    m_win->setX(rect.x());
    m_win->setY(rect.y());
    m_win->setWidth(rect.width());
    m_win->setHeight(rect.height());
}

void Proxy::maximize() {
    m_win->showMaximized();
}

void Proxy::minimize() {
    m_win->showMinimized();
}

void Proxy::fullscreen() {
    m_win->showFullScreen();
}

void Proxy::restore() {
    m_win->showNormal();
}

bool Proxy::eventing() const {
    return m_eventing;
}

bool Proxy::running() const {
    return m_runningJs;
}

QQuickItem * Proxy::proxy() const {
    return m_proxy;
}

QQuickWindow * Proxy::win() const {
    return m_win;
}

QObject * Proxy::tab() const {
    return m_tab;
}

void Proxy::invoke(
  const char * member, const QVariant & a1, const QVariant & a2,
  const QVariant & a3) {
    runned = false;
    if (thread() == QThread::currentThread()) {
        resolveInvoke(member, a1, a2, a3);
    }
    else {
        emit requiereInvoke(member, a1, a2, a3);
    }

    while (!runned)
        QThread::msleep(1);
}

void Proxy::setSilent(bool silent) {
    if (m_silent == silent)
        return;

    m_silent = silent;
    emit silentChanged(m_silent);
}

void Proxy::setProxy(QQuickItem * proxy) {
    if (m_proxy == proxy)
        return;

    m_proxy = proxy;
    emit proxyChanged(m_proxy);
}

void Proxy::setWin(QQuickWindow * win) {
    if (m_win == win)
        return;

    m_win = win;
    emit winChanged(m_win);
}

void Proxy::setServer(icL::Server * server) {
    this->server = server;
}

void Proxy::setTab(QObject * tab) {
    if (m_tab == tab)
        return;

    m_tab = tab;
    emit tabChanged(m_tab);
}

void Proxy::setEventing(bool eventing) {
    if (m_eventing == eventing)
        return;

    m_eventing = eventing;
    emit eventingChanged(m_eventing);
}

void Proxy::setRunning(bool runningJs) {
    if (m_runningJs == runningJs)
        return;

    m_runningJs = runningJs;
    emit runningChanged(m_runningJs);
}

void Proxy::handleJsResult(QVariant result) {
    server->acceptResult(result.toString());
}

void Proxy::resolveInvoke(
  const char * member, const QVariant & a1, const QVariant & a2,
  const QVariant & a3) {
    QVariant ret;
    QMetaObject::invokeMethod(
      m_proxy, member, Q_RETURN_ARG(QVariant, ret), Q_ARG(QVariant, a1),
      Q_ARG(QVariant, a2), Q_ARG(QVariant, a3));

    result = ret;
    runned = true;
}

QQuickItem * Proxy::workingTab() {
    return server->getWorking();
}
