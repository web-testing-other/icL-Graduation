#include "run-me.h++"

#include <QCoreApplication>



RunMe::RunMe()
    : server(engine.core(), &proxy)
    , vms(engine.core(), this) {
    proxy.setServer(&server);
}

Proxy * RunMe::getProxy() {
    return &proxy;
}

icL::VMStack * RunMe::getVMStack() {
    return &vms;
}

void RunMe::init() {
    engine.setFrontEndServer(new BackEnd{engine.core(), &server});
    engine.setVMStack(&vms);

    engine.finalize();

    engine.core()->vms->init(QDir::homePath() + "/~/test/main.icL");
}

void RunMe::run() {
    switch (stepType) {
    case Mini:
        engine.core()->vms->debug();
        break;

    case Over:
        engine.core()->vms->stepOver();
        break;

    case In:
        engine.core()->vms->stepInto();
        break;

    case Out:
        engine.core()->vms->stepOut();
        break;

    case ToEnd:
        engine.core()->vms->run();
        QCoreApplication::quit();
        break;
    }
}
