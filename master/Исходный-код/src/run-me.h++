#ifndef icL_RunMe
#define icL_RunMe

#include "proxy.h++"
#include "server.h++"
#include "services/back-end.h++"
#include "vmstack.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-shared/main/engine-pro.h++>

#include <QDir>
#include <QThread>



using namespace icL;
using namespace icL::shared;

class RunMe : public QThread
{
    Engine       engine{};
    Proxy        proxy;
    Server       server;
    icL::VMStack vms;

public:
    RunMe();

    Proxy * getProxy();

    icL::VMStack * getVMStack();

    void init();

    void run() override;

    enum { Mini, Over, In, Out, ToEnd } stepType;
};

#endif  // icL_RunMe
