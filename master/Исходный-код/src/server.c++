#include "server.h++"

#include "proxy.h++"

#include <icL-types/json/js-array.h++>
#include <icL-types/json/js-document.h++>
#include <icL-types/json/js-object.h++>
#include <icL-types/replaces/ic-rect.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/export/signals-pro.h++>
#include <icL-il/export/types.h++>
#include <icL-il/main/cp.h++>
#include <icL-il/main/enums.h++>
#include <icL-il/main/factory.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel-pro.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/structures/set.h++>
#include <icL-service/main/values/set.h++>

#include <QDateTime>
#include <QFlags>
#include <QQuickItem>
#include <QThread>
#include <QUuid>

#include <iostream>



namespace icL {

using namespace il::SignalsNM;

Server::Server(core::il::InterLevel * il, Proxy * proxy, QObject * parent)
    : QObject(parent)
    , core::il::Node(il)
    , proxy(proxy) {
    connect(this, &Server::requiereEvent, this, &Server::resolveEvent);
    connect(this, &Server::requiereInvoke, this, &Server::resolveInvoke);
    connect(this, &Server::requiereProperty, this, &Server::resolveProperty);
    connect(
      this, &Server::requiereSetProperty, this, &Server::resolveSetProperty);
}

QString Server::openTab() {
    QString uuid = getNewTabId();
    tabs.append(dynamic_cast<QQuickItem *>(proxy->openTab(uuid)));
    working = current = tabs.last();
    return uuid;
}

QStringList Server::getTabs() {
    QStringList ret;

    for (auto tab : tabs) {
        ret.append(property(tab, "uuid").toString());
    }

    return ret;
}

QString Server::getCurrentTab() {
    if (current == nullptr) {
        il->vm->signal({NoSessions, "Current session is empty"});
        return {};
    }

    return property(current, "uuid").toString();
}

bool Server::checkTab(const QString & uuid) {
    if (property(working, "uuid").toString() == uuid)
        return true;

    for (auto tab : tabs) {
        if (property(tab, "uuid").toString() == uuid) {
            working = tab;
            handler = tab->childAt(0, 0);
            return true;
        }
    }

    il->vm->signal({Signals::NoSuchWindow, "No such window: " + uuid});
    return false;
}

void Server::switchToTab(const QString & uuid) {
    if (checkTab(uuid)) {
        current = working;
        proxy->switchToTab(uuid);
    }
}

void Server::closeTab() {
    if (working == current) {
        int index = tabs.indexOf(current);

        if (index + 1 < tabs.length()) {
            current = tabs[index + 1];
        }
        else if (index - 1 >= 0) {
            current = tabs[index - 1];
        }
        else {
            current = nullptr;
        }
    }

    if (il->vm->hasOkState() && working != nullptr) {
        proxy->closeTab(property(working, "uuid").toString());
    }

    tabs.removeOne(working);
    working = current;

    if (current != nullptr) {
        proxy->switchToTab(property(current, "uuid").toString());
        handler = working->childAt(0, 0);
    }
}

void Server::setSilentMode(bool silent) {
    proxy->setSilent(silent);
}

void Server::loadUrl(const QString & url) {
    if (il->vm->hasOkState()) {
        setProperty(working, "domReady", false);
        setProperty(working, "url", QUrl::fromUserInput(url));
        runJs("true");
        waitForPageLoad();
    }
}

QString Server::getUrl() {
    if (il->vm->hasOkState()) {
        return property(working, "url").toString();
    }
    return {};
}

QString Server::title() {
    if (il->vm->hasOkState()) {
        return property(working, "title").toString();
    }
    return {};
}

QString Server::source() {
    il->vm->syssig("Page source extract is not supported in icL-Magic");
    return {};
}

void Server::back() {
    if (il->vm->hasOkState()) {
        invoke("goBack");
    }
}

void Server::refresh() {
    if (il->vm->hasOkState()) {
        invoke("reload");
    }
}

void Server::forward() {
    if (il->vm->hasOkState()) {
        invoke("goForward");
    }
}

icVariant Server::runJs(
  const QString & code, const VariantList & args, const icVariant & _default,
  bool async) {

    setRunning(true);

    jsResult       = {};
    responseGetted = false;

    QStringList _args;

    QString exp = code;
    QString codeTemplate =
      R"(
        JSON.stringify(icL.z_object((function () {
            if (!icL.error) {
                try {
                    %1
                }
                catch (err) {
                    return icL.z_error("JavaScriptError", err)
                }
            }
        }).call(icL, %2)))
      )";

    exp.replace("$[]", "icL.buffer[0]");
    exp.replace("$<>", "arguments[0]");
    exp.replace("$<1>", "arguments[1]");
    exp.replace("$<2>", "arguments[2]");
    exp.replace("$<3>", "arguments[3]");
    exp.replace("$<4>", "arguments[4]");
    exp.replace("$<5>", "arguments[5]");

    if (!exp.contains(';')) {
        exp = "return " + exp;
    }

    for (auto & arg : args) {
        jsDocument jdoc;
        jsObject   obj = argToJson(arg);

        jdoc.root(obj);
        _args.append("icL.z_data(" + jdoc.toJson() + ')');
    }

    waitForPageLoadFromVmThread();

    proxy->runJs(codeTemplate.arg(exp, _args.join(',')), !async);

    if (async) {
        return {};
    }

    int64_t startTime = QDateTime::currentMSecsSinceEpoch();
    int     ms        = pro()->server->scriptTimeout();

    while (!responseGetted &&
           int(QDateTime::currentMSecsSinceEpoch() - startTime) < ms)
        ;

    auto result = jsResult;

    if (!responseGetted) {
        il->vm->signal({core::il::Signals::Timeout,
                        "Time is out of " + QString::number(ms) + " ms"});
    }
    else {
        waitForPageLoadFromVmThread();
    }

    setRunning(false);

    return result.isValid() ? result : _default;
}

void Server::runJsAsync(const QString & code, const VariantList & args) {
    runJs(code, args, {}, true);
}

void Server::addUserScript(const QString & /*code*/, bool persistent) {
    if (!persistent && working == nullptr) {
        il->vm->signal({Signals::NoSessions, "No active session"});
    }
    else {
        il->vm->syssig("User scripts are not supported by icL-Magic");
    }
}

icRect Server::getWindowRect() {
    icRect _rect{};

    if (il->vm->hasOkState()) {
        _rect = proxy->getWindowRect();
    }

    return _rect;
}

icRect Server::setWindowRect(const icRect & rect) {
    icRect _rect;

    if (il->vm->hasOkState()) {
        proxy->setWindowRect(rect);
        _rect = proxy->getWindowRect();
    }

    return _rect;
}

void Server::maximize() {
    if (il->vm->hasOkState()) {
        proxy->maximize();
    }
}

void Server::minimize() {
    if (il->vm->hasOkState()) {
        proxy->minimize();
    }
}

void Server::fullscreen() {
    if (il->vm->hasOkState()) {
        proxy->fullscreen();
    }
}

void Server::restore() {
    if (il->vm->hasOkState()) {
        proxy->restore();
    }
}

void Server::keyDown(QChar ch, int modifiers) {
    QKeyEvent * press = new QKeyEvent(
      QEvent::KeyPress, ch == '\n' ? Qt::Key_Return : Qt::Key_V,
      modifiersToQt(modifiers), QString(ch));
    releaseEvent(press);
}

void Server::keyUp(QChar ch, int modifiers) {
    QKeyEvent * release = new QKeyEvent(
      QEvent::KeyRelease, ch == '\n' ? Qt::Key_Return : Qt::Key_V,
      modifiersToQt(modifiers), QString(ch));
    releaseEvent(release);

    runJs("true");

    if (ch == '\n') {
        waitForPageLoadFromVmThread();
    }
}

ScreenCoords Server::coord(double rx, double ry, int ax, int ay) {
    if (il->vm->hasOkState()) {
        if (ax >= 0 && ay >= 0) {
            return {double(ax), double(ay)};
        }
        return {working->width() * rx, working->height() * ry};
    }

    return {0, 0};
}

void Server::hover(const ScreenCoords & xy) {
    if (inScreen(xy)) {
        QMouseEvent * move = new QMouseEvent(
          QEvent::MouseMove, {xy.x, xy.y}, Qt::NoButton, Qt::NoButton,
          Qt::NoModifier);
        releaseEvent(move);
    }
}

void Server::mouseDown(const ScreenCoords & xy, int button) {
    if (inScreen(xy)) {
        QMouseEvent * press = new QMouseEvent(
          QEvent::MouseButtonPress, {xy.x, xy.y}, buttonToQt(button),
          Qt::MouseButton::NoButton, Qt::NoModifier);
        releaseEvent(press);
    }
}

void Server::mouseUp(const ScreenCoords & xy, int button) {
    if (inScreen(xy)) {
        QMouseEvent * release = new QMouseEvent(
          QEvent::MouseButtonRelease, {xy.x, xy.y}, buttonToQt(button),
          Qt::MouseButton::NoButton, Qt::NoModifier);
        releaseEvent(release);
        QThread::msleep(10);
        waitForPageLoadFromVmThread();
    }
}

void Server::acceptResult(const QString & result) {
    jsError    jerror;
    jsDocument jdoc = jsDocument::fromJson(result, jerror);

    qDebug() << result;

    if (!responseGetted && !result.isEmpty()) {
        jsResult = jsonToArg(jdoc.object());
    }
    else {
        jsResult = icVariant::makeVoid();
    }
    QCoreApplication::processEvents();
    responseGetted = true;
}

QQuickItem * Server::getWorking() {
    return working;
}

void Server::setEventing(bool value) {
    proxy->setEventing(value);
    QCoreApplication::sendPostedEvents();
    QCoreApplication::processEvents();
    QThread::msleep(1);
}

void Server::setRunning(bool value) {
    proxy->setRunning(value);
    QCoreApplication::sendPostedEvents();
    QCoreApplication::processEvents();
    QThread::msleep(1);
}

void Server::invoke(const char * funcName) {
    runned = false;
    emit requiereInvoke(funcName);

    while (!runned)
        ;
    waitForPageLoad();
}

QVariant Server::property(QObject * obj, const char * name) {
    runned = false;
    emit requiereProperty(obj, name);

    while (!runned)
        ;

    return result;
}

void Server::setProperty(
  QObject * obj, const char * name, const QVariant & value) {
    runned = false;
    emit requiereSetProperty(obj, name, value);

    while (!runned)
        ;
}

void Server::resolveInvoke(const char * funcName) {
    QMetaObject::invokeMethod(working, funcName);
    QCoreApplication::processEvents();
    runned = true;
}

void Server::resolveProperty(QObject * obj, const char * name) {
    result = obj->property(name);
    runned = true;
}

void Server::resolveSetProperty(
  QObject * obj, const char * name, const QVariant & value) {
    obj->setProperty(name, value);
    QCoreApplication::processEvents();
    runned = true;
}

void Server::resolveEvent(QEvent * event) {
    QCoreApplication::sendEvent(working->childAt(100, 100), event);
    QCoreApplication::sendPostedEvents();
    QCoreApplication::processEvents();
    runned = true;
}

QString Server::getNewTabId() {
    return "icL-tab-" + QUuid::createUuid().toString().midRef(1, 36);
}

void Server::waitForPageLoad() {
    int64_t startTime = QDateTime::currentMSecsSinceEpoch();
    int     ms        = pro()->server->pageLoadTimeout();

    runJs("true");

    while (!working->property("domReady").toBool() &&
           int(QDateTime::currentMSecsSinceEpoch() - startTime) < ms)
        QThread::msleep(1);

    if (!working->property("domReady").toBool()) {
        il->vm->signal({core::il::Signals::Timeout,
                        "Time is out of " + QString::number(ms) + " ms"});
    }
    else {
        runJs("true");
    }
}

void Server::waitForPageLoadFromVmThread() {
    int64_t startTime = QDateTime::currentMSecsSinceEpoch();
    int     ms        = pro()->server->pageLoadTimeout();

    while (!property(working, "domReady").toBool() &&
           int(QDateTime::currentMSecsSinceEpoch() - startTime) < ms)
        ;
}

jsObject Server::argToJson(const icVariant & var) {
    jsObject  result;
    icString  type;
    icVariant value = var;

    switch (int(var.type())) {
    case icType::Bool:
        type = "bool";
        break;

    case icType::Int:
        type = "int";
        break;

    case icType::Double:
        type = "double";
        break;

    case icType::String:
        type = "string";
        break;

    case icType::StringList: {
        jsArray arr;

        for (auto & str : var.toStringList()) {
            arr.append(icString{str});
        }

        type  = "list";
        value = arr;
    }

    case proType::Element: {
        jsObject    elobj;
        il::Element el = var;

        elobj.insert("variable", el.variable);
        elobj.insert("selector", el.selector);

        type  = "element";
        value = elobj;
    } break;

    case proType::Elements:
        value = argToJson(to<il::Elements>(var)[0]).value("value", jsObject{});
        type  = "elements";
        break;

    case icType::Object: {
        auto &   map = to<core::memory::Object>(var).data->getMap();
        jsObject obj;


        for (auto it = map.begin(); it != map.end(); it++) {
            obj.insert(it.key(), argToJson(it.value()));
        }

        type  = "object";
        value = obj;
    } break;

    case icType::Set: {
        jsArray arr;

        core::service::Set::forEach(
          il, to<core::memory::Set>(var),
          [&arr, this](const core::memory::Object & obj) {
              arr.append(argToJson(obj));
          });

        type  = "set";
        value = arr;
    } break;

    case icType::RegEx: {
        const icRegEx & rex = value.toRegEx();
        jsObject        obj;

        auto options = rex.patternOptions();

        obj.insert("pattern", rex.pattern());
        obj.insert("i", (options & icRegEx::CaseInsensitiveOption) != 0);
        obj.insert("m", (options & icRegEx::MultilineOption) != 0);
        obj.insert("s", (options & icRegEx::DotMatchesEverythingOption) != 0);
        obj.insert("u", (options & icRegEx::UseUnicodePropertiesOption) != 0);

        type  = "regex";
        value = obj;
    }

    default:
        break;
    }

    result.insert("type", type);
    result.insert("value", value);

    return result;
}

icVariant Server::jsonToArg(const jsObject & obj) {
    icVariant result = obj.value("value", {});
    icString  type   = obj.value("type", icString{});

    if (type == "list") {
        icStringList list;

        for (auto & var : result.toArray()) {
            list.append(var.toString());
        }

        result = list;
    }
    else if (type == "element" || type == "elements") {
        il::Element      el;
        const jsObject & obj = result.toJsObject();

        el.target          = pro()->server->getCurrentTarget();
        el.target.windowId = working->property("uuid").toString();

        el.variable = obj.value("variable", icString{});
        el.selector = obj.value("selector", icString{});

        if (type == "element") {
            result = el;
        }
        else {
            il::Elements els;
            els.append(el);
            els.setSize(int(double(obj.value("size", 0.0))));
            result = els;
        }
    }
    else if (type == "object") {
        const jsObject & obj = result.toJsObject();
        core::memory::Object robj{il};

        robj.data = std::make_shared<core::memory::DataContainer>(il);

        for (auto it = obj.begin(); it != obj.end(); it++) {
            robj.data->setValue(it.key(), jsonToArg(it.value().toJsObject()));
        }

        result = robj;
    }
    else if (type == "set-8ba66190-a4bf-44d8-8722-7e17927498ed") {
        const jsArray & arr = result.toArray();
        core::memory::Set set;

        set.header  = std::make_shared<core::memory::Columns>();
        set.setData = std::make_shared<core::memory::SetData>();

        bool headerSetted = false;

        for (auto & item : arr) {
            const jsObject & obj = item.toJsObject();
            icVariantList    data;

            for (auto it = obj.begin(); it != obj.end(); it++) {
                data.append(jsonToArg(it.value().toJsObject()));

                if (!headerSetted) {
                    set.header->append(
                      {il->factory->variantToType(data.last()), it.key()});
                }
            }

            headerSetted = true;
        }
    }
    else if (type == "err-8ba66190-a4bf-44d8-8722-7e17927498ed") {
        const jsObject & obj = result.toJsObject();
        il->vm->signal({/*il->cpu->getSignal(obj["signal"])*/
                        Signals::JavaScriptError, obj["error"]});
        result = icVariant::makeVoid();
    }

    return result;
}

Qt::KeyboardModifiers Server::modifiersToQt(int modifiers) {
    Qt::KeyboardModifiers result;
    using namespace il::KeysNM;

    if (modifiers & Ctrl) {
        result |= Qt::ControlModifier;
    }
    if (modifiers & Shift) {
        result |= Qt::ShiftModifier;
    }
    if (modifiers & Alt) {
        result |= Qt::AltModifier;
    }

    return result;
}

Qt::MouseButton Server::buttonToQt(int button) {
    Qt::MouseButton result;
    using namespace il::MouseButtonsNM;

    switch (button) {
    case Left:
        result = Qt::LeftButton;
        break;

    case Middle:
        result = Qt::MiddleButton;
        break;

    case Right:
        result = Qt::RightButton;
    }

    return result;
}

void Server::releaseEvent(QEvent * event) {
    runned = false;
    emit requiereEvent(event);

    while (!runned)
        ;
    waitForPageLoad();
}

bool Server::inScreen(const ScreenCoords & xy) {
    if (il->vm->hasOkState()) {
        return xy.x >= 0 && xy.y >= 0 && xy.x <= working->width() &&
               xy.y <= working->height();
    }

    return false;
}

core::il::InterLevel * Server::core() {
    return il;
}

}  // namespace icL
