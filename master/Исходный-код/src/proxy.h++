#ifndef magic_Proxy
#define magic_Proxy

#include <QObject>
#include <QVariant>



class icRect;
class QQuickItem;
class QQuickWindow;

namespace icL {
class Server;
}

class Proxy : public QObject
{
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(bool        silent READ silent                  NOTIFY silentChanged)
    Q_PROPERTY(bool      eventing READ eventing                NOTIFY eventingChanged)
    Q_PROPERTY(bool     runningJs READ running                 NOTIFY runningChanged)
    Q_PROPERTY(QQuickItem * proxy READ proxy    WRITE setProxy NOTIFY proxyChanged)
    Q_PROPERTY(QQuickWindow * win READ win      WRITE setWin   NOTIFY winChanged)
    Q_PROPERTY(QObject *      tab READ tab      WRITE setTab   NOTIFY tabChanged)
    // clang-format on

public:
    Proxy();

    QObject * openTab(const QString & uuid);

    bool silent() const;
    void switchToTab(const QString & uuid);
    void closeTab(const QString & uuid);

    void runJs(const QString & code, bool sync);
    void addUserScript(const QString & code, bool persistent);

    icRect getWindowRect();
    void   setWindowRect(const icRect & rect);

    void maximize();
    void minimize();
    void fullscreen();
    void restore();

    bool eventing() const;
    bool running() const;

    QQuickItem *   proxy() const;
    QQuickWindow * win() const;
    QObject *      tab() const;

    void invoke(
      const char * member, const QVariant & a1 = {}, const QVariant & a2 = {},
      const QVariant & a3 = {});

public slots:
    void setSilent(bool silent);
    void setProxy(QQuickItem * proxy);
    void setWin(QQuickWindow * win);
    void setServer(icL::Server * server);
    void setTab(QObject * tab);
    void setEventing(bool eventing);
    void setRunning(bool runningJs);


    void handleJsResult(QVariant result);

    void resolveInvoke(
      const char * member, const QVariant & a1, const QVariant & a2,
      const QVariant & a3);

    QQuickItem * workingTab();

signals:
    void silentChanged(bool silent);
    void proxyChanged(QQuickItem * proxy);
    void winChanged(QQuickWindow * win);
    void tabChanged(QObject * tab);
    void eventingChanged(bool eventing);
    void runningChanged(bool runningJs);
    void requiereInvoke(
      const char * member, const QVariant & a1, const QVariant & a2,
      const QVariant & a3);

private:
    bool m_silent = false;

    QQuickItem *   m_proxy = nullptr;
    QQuickWindow * m_win   = nullptr;
    icL::Server *  server  = nullptr;
    QObject *      m_tab   = nullptr;
    QVariant       result  = {};
    volatile bool  runned  = false;
    bool           m_eventing;
    bool           m_runningJs;
};

#endif  // magic_Proxy
